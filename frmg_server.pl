#!/usr/bin/env perl

## a small server for FRMG
## WARNING: this code is not safe, because of possible code injection 
## either at Perl level (in DPath) or a Shell level (via call to parser, lexer, ...)

## some protection has been added (April 16th, 2013)

use strict;
use warnings;
use Mojolicious::Lite;
use Mojo::DOM;
use Mojo::ByteStream 'b';
use XML::Twig;
use DepXML;
use DepXML::Query::Compile;
use Mojo::Log;
use XML::Parser;
use XML::Simple;
##use XML::Hash::LX;

=head1 NAME

frmg_server.pl -- Experimental server for FRMG based on Mojolicious

=head1 SYNOPSIS

   morbo ./frmg_server.pl [options]
   hypnotoad ./frmg_server.pl [options]
   plackup ./frmg_server.pl -s FCGI --port 3000
   plackup ./frmg_server.pl -s FCGI --port 3000 -D

=head1 DESCRIPTION

B<frmg_server.pl> is an experimental Perl server to ease the
use of the FRMG parser. It runs on all WebServers provided by Mojolicious.

=head1 OPTIONS

=head1 SEE ALSO

=over 4

=item Alpage project team L<http://alpage.inria.fr>

=item Mojolicous::Lite

=back

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013, 2014, 2015, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

######################################################################
## frmg_shell initialization

use AppConfig qw/:argcount :expand/;

my $frmg_config = 
  app->{frmg}{config} = 
  AppConfig->new(
		 "verbose!" => {DEFAULT => 0},
		 "dis!" => {DEFAULT => 1},
		 "dep!" => { DEFAULT => 1,
			     ACTION => sub { 
			       if ($_[2]) {
				 $_[0]->passage(0);
				 $_[0]->depconll(0);
				 $_[0]->conll(0);
				 $_[0]->html(0);
			       }
			     }
			   },
		 "html!" => { DEFAULT => 1,
				ACTION => sub { if ($_[2]) {$_[0]->xml(0); $_[0]->format('html')} }
			    },
		 "raw!" => { DEFAULT => 0,
			     ACTION => sub { if ($_[2]) {$_[0]->format('txt')} }
			   },
		 "robust!" => { DEFAULT => 0},
		 "xml!" => { DEFAULT => 0,
			     ACTION => sub { if ($_[2]) { $_[0]->html(0); $_[0]->format('xml')} }
			   },
		 "passage!" => { DEFAULT => 0,
				 ACTION => sub { 
				   if ($_[2]) { 
				     $_[0]->dis(1);
				     $_[0]->dep(0);
				     $_[0]->tikzdep(0);
				     $_[0]->depconll(0);
				     $_[0]->conll(0);
				   }
				 }
			       },
		 "passage_strict!" => { DEFAULT => 0,
					ACTION => sub {$_[2] and $_[0]->passage(1)}
				      },
		 'conll!' => { DEFAULT => 0,
			       ACTION => sub { 
				 if ($_[2]) {
				   $_[0]->dis(1);
				   $_[0]->dep(0);
				   $_[0]->tikzdep(0);
				   $_[0]->depconll(0);
				   $_[0]->passage(0);
				   $_[0]->html(0);
				   $_[0]->format('txt');
				 }
			       }
			     },
		 'udep!' => { DEFAULT => 0,
			      ACTION => sub { if ($_[2]) {
				$_[0]->dep(0);
				$_[0]->passage(0);
				$_[0]->html(0);
				$_[0]->depconll(0);
				$_[0]->passage(0);
				$_[0]->html(0);
				$_[0]->format('txt');
			      }
					    }
			    },
		 'depconll' => { DEFAULT => 0,
				   ACTION => sub { 
				     if ($_[2]) {
				       $_[0]->dis(1);
				       $_[0]->dep(0);
				       $_[0]->tikzdep(0);
				       $_[0]->passage(0);
				       $_[0]->html(0);
				       $_[0]->format('txt');
				     }
				   }
			       },
		 'afp!' => {DEFAULT => 0},
		 'tikzdep!' => { DEFAULT => 0,
				 ACTION => sub { 
				   if ($_[2]) {
				       $_[0]->dep(1);
				       $_[0]->depconll(0);
				       $_[0]->passage(0);
				       $_[0]->html(0);
				       $_[0]->format('txt');
				   }
				 }
			       },
		 'lopts=s' => {DEFAULT => ''},
		 'settings=s' => { ACTION => sub { change_setting_set(@_) } },
		 'format=s' => { DEFAULT => 'svg' },
		 'dis_options=s' => { DEFAULT => '' },
		 'cost!' => { DEFAULT => 0},
		 'lexer!' => { DEFAULT => 0,
				 ACTION => sub {$_[2] and $_[0]->raw(1)}
			     },
		 'token!' => { DEFAULT => 0,
			       ACTION => sub { if($_[2]) { $_[0]->raw(1); }}
			     },
		 'lctag!' => { DEFAULT => 1 },
		 'correct!' => { DEFAULT => 1 },
		 'time!' => { DEFAULT => 0,
			      ACTION => sub {$_[2] and $_[0]->raw(1)}
			    },
		 "transform!" => { DEFAULT => 0 },
		 "rename!" => { DEFAULT => 0 },
		 "exotic!" => { DEFAULT => 0 },
		 ## handling small corpora (a priori in batch mode)
		 "utf8!" => {DEFAULT => 0},
		 "timeout=d" => {DEFAULT => 60},
		 'follow!' => { DEFAULT => 0,
				ACTION => sub {$_[2] and $_[0]->format('png')}
			      },

		);

######################################################################
## Server part

app->secrets('il observe une maman avec ses jumelles');

my $server_config = plugin 'Config' => { file => 'frmg_server.conf' };

my $ws = Mojo::Transaction::WebSocket->new;

# Customize log file location and minimum log level
my $log = Mojo::Log->new(path => "$server_config->{cfgdir}/server.log", level => 'info');

#plugin 'Log::Access';
##plugin 'Log::Timestamp' => {pattern => '%y%m%d %X'};

helper ip => sub {
  my $c = shift;
  return $c->req->headers->header('X-Real-IP')
    || $c->tx->remote_address;
};

my $ip;

$log->format(sub {
	       my ($time, $level, @lines) = @_;
	       return '[' . localtime(time) . "] [$level] [$ip] " . join("\n", @lines) . "\n";
	     });

helper frmg => sub { 
  my $self = shift;
  my $param = shift;
  return app->{frmg}{config}->get($param);
};

helper img => sub {
  my $self = shift;
  my $data = shift;
  return b($data)->b64_encode('');
};

helper graphical => sub {
  my $self = shift;
  my $format = app->{frmg}{config}->format;
  return grep {$format eq $_} qw{png gif}
};

helper svg => sub {
  my $self = shift;
  my $data = shift;
  ## there is a problem of encoding
  my $svg = Mojo::DOM->new($data)->at('svg');
  $svg->{width} = "100%";
#  delete $svg->{viewBox};
#  delete $svg->{height};
  $svg->{height} = "300";
#  $svg->{zoomAndPan} ='magnify';
#  $svg->at('g')->{transform} = "scale(1,1) rotate(0) translate(0,500)";
  $svg->{preserveAspectRatio} = "xMinYMin";
  $svg->{id} = 'frmg_svg';
  return $self->render('svg', svg => $svg, partial => 1);
};

helper dpath => sub {
  my $self = shift;
  my $data = shift;
  my $dpath = shift;
  my $depxml = DepXML->new();
  $depxml->parse($data);
  return $dpath->{code}->($depxml);
};

helper url_escape => sub {
  my $selft = shift;
  my $s = shift;
  return b($s)->xml_escape;
};

require File::Temp;

helper tikzdep => sub {
  my $self = shift;
  my $data = shift;
  $data =~ /\\depedge/mo or return; # pb sometimes with tikzdep !!
  my $tmp = File::Temp->new( TEMPLATE => 'frmgXXXXX',
			    DIR => '/tmp',
			    SUFFIX => '.tex'
			  );
  my $filename = $tmp->filename;
  $filename =~ s/\.tex$//o;
  print $tmp <<EOF;
\\documentclass[crop,convert={outfile=\\jobname.png}]{standalone}
\\usepackage{times}
\\usepackage[T1]{fontenc}
\\usepackage[latin1]{inputenc}
\\usepackage[x11names]{xcolor}
\\usepackage{tikz}
\\usepackage{tikz-dependency}
\\pgfkeys{%
     /tikz/adj/.style={IndianRed3,dashed,thick},%
     /tikz/subst/.style={LightSkyBlue1,thick},%
     /tikz/lexical/.style={Purple1,thick},%
     /tikz/skip/.style={green,thick},%
     /tikz/filled/.style={rectangle},%
     /tikz/word/.style={rectangle,fill=orange!10,draw},
     /tikz/empty/.style={rectangle,fill=blue!10,draw},
     /tikz/dnode/.style={fill=Goldenrod1},
     /tikz/lstyle/.style={font=\tiny,black,yshift=1pt},
   }
\\begin{document}
$data
\\end{document}
EOF
  system("cd /tmp; pdflatex --shell-escape $filename");
#  system("cd /tmp; htlatex $filename");
#  $data = `cat ${filename}-1.svg`;
#  my $svg =  Mojo::DOM->new($data)->at('svg');
#  delete $svg->{width};
#  delete $svg->{height};
##  delete $svg->{viewBox};
#  return $svg;
  $data = `cat ${filename}.png`;
  return b($data)->b64_encode;
};

## plugin 'TagHelpers';

hook after_render => sub {
  my ($self, $output, $format) = @_;
  
  # Check if "gzip => 1" has been set in the stash
  return unless $self->stash->{gzip};
  
  # Check if user agent accepts GZip compression
  return unless ($self->req->headers->accept_encoding // '') =~ /gzip/i;
  
  # Compress content with GZip
  $self->res->headers->content_encoding('gzip');
  gzip $output, my $compressed;
  $$output = $compressed;
};

get '/process' => sub {
  my $self = shift;
  $ip = $self->ip;
    $self->stash(dpath => '');
    my $format = $self->param('format');
    my $xoptions = b($self->param('options'))->squish;
    my @options = map {s/\s+//og; process_server_option($_)} split(/(?::|\s+)/,$xoptions);
    $format and push(@options,$format);
    if (0) {
      if (defined $self->param('schema')) {
	unshift(@options,$self->param('schema'));
      }
      if (defined $self->param('view')) {
	unshift(@options,$self->param('view'));
      }
      $self->param('robust') and unshift(@options,'robust');
      !$self->param('disamb') and unshift(@options,'nodis');
      $self->param('transform') and unshift(@options,'transform');
      $self->param('rename') and unshift(@options,'rename');
      $self->param('exotic') and unshift(@options,'exotic');
    }

    if (my $query = b($self->param('dpath'))->squish->to_string) {
      if (my ($select,$from,$dpath) = $query =~ /^\s*select\s+(.*?)\s* from\s+(nodes|edges)\s+where\s+([^{}]*)\s*$/) {
	push(@options,qw{dep xml dis});
	$select = expand_elem_select($select);
	$select =~ s/[#!|&{};\n\$\@]//og;
	my @select = map {s/^\s*//o; $_} split(/,/,$select);
	my $xselect = join(",",map {s/\b\./ /og; "\$_->apply(dpath $_ )"} @select);
	## warning: this following eval is a security hole that should be protected
	my $code = eval( <<EOF );
sub { 
     map {[$xselect]} (\$_[0]->find_$from( dpath .( $dpath ) ));
}
EOF
	if ($@) {
	  #	  return $self->render_exception("not a correct query expression: $query");
	  return $self->render("not a correct query expression: $query $@");
	}
	$self->stash(dpath => { select => [split(/,/,$select)], 
				code => $code
			      });
      } else {
	#	return $self->render_exception("not a correct query expression: $query");
	return $self->render("not a correct query expression: $query");
      }
    }

    my $options = join(':',@options);
    my $sentence = b($self->param('sentence'))->squish;
    #  my $data = `echo ":$options $sentence" | ./frmg_shell --local --quiet`;
    my $data = sentence_process(\@options,$sentence);
    $ws->send($sentence);
    $self->stash(options => $xoptions);
    $self->stash(sentence => $sentence);
    $self->stash(frmg_data => $data);
    $self->res->headers->header('Access-Control-Allow-Origin' => '*'); 
##    print $data;
    $self->respond_to(
		      'xml' =>  {text => $data},
		      'txt' => {text => $data},
		      'json' => {json => data2json($data,\@options)},
#		      'json' => {json => data2json($data,\@$options)},
#		      'any' => 'process'
	'any' => sub { $self->render('process') }
		     );
    ##  $self->render( text => $data );
  };

sub process_server_option {
  my $option = $_[0];
  $option =~ s/^(\w+)~/$1=/o;
  ($option =~ /^(?:edge|eid|xedge|node|nid)=/) and $option =~ s/\$/\&/og;
  return $option;
}


######################################################################
## frmg_shell functions

use IPC::Open2;

sub expand_elem_select {
  my $s = shift;
  while ($s =~ s/([^(),]+)\.\(([^()]+)\)/join(',',map {"$1\.$_"} split(m{,},$2))/oge) {};
  return $s;
}

sub sentence_process {
  my ($options,$sentence) = @_;
  restore_settings();
  if (my $default_settings = $server_config->{settings}) {
    push(@$options,split(/:/,$default_settings));
  }
  change_settings(@$options);
  my $cmd = build_cmd();
  $log->info("$sentence");
  ##  $sentence =~ s/\"/\\"/og;
  ## warning: the following line is a security hole that should be protected
  ## both sentence and cmd should be carefully analyzed to avoid shell code injection
  my ($fhread,$fhwrite);
  my $pid = open2($fhread, $fhwrite, "$cmd");
  print $fhwrite $sentence;
  close($fhwrite);
  my $data = '';
  while(<$fhread>) { $data .= $_; }
  close($fhread);
  waitpid $pid, 0;
#  return `echo "$sentence" | $cmd`;
  return $data;
}

sub build_cmd {
  my $config = app->{frmg}{config};
  my $format = $config->format;
  my $display = '';
  my $inline_cleaner = inline_cleaner();
  if ($config->tikzdep) {
    $display = join(" ",split(/\n/,<<EOF));
perl -p $server_config->{transcode}
EOF
  } elsif ($format eq 'svg') {
    $display = "dot -T$format -Gcharset=latin1 | yarecode -u -l=fr";
  } else {
    $display = "dot -T$format -Gcharset=latin1";
  }
  my $lexopts = $config->lopts;
  my $cmd = ($config->token) ?  "cat " : "$server_config->{lexer} $lexopts";
  unless ($config->lexer || $config->token) {
    my $parser = $server_config->{parser};
    $cmd .= " | $parser -loop -no_occur_check -utime";
    ($config->timeout) and $cmd .= ' -timeout '.$config->timeout;
    ($config->robust) and $cmd .= " -robust";
    ($config->lctag) or $cmd .= " -no_lctag";
    ($config->correct) or $cmd .= " -nocorrect";
    ($config->follow) and $cmd .= " -v dyam";
    $cmd .= " -disamb -multi";
  }
  my $sxpipe = $server_config->{sxpipe};
  if (my $npnormalizer = $server_config->{np_normalizer}) {
    $sxpipe .= " | $npnormalizer ";
  }
#  my $utf8 = $config->utf8 ? " -u" : '';
#  $cmd = $sxpipe . $utf8 . ' | yarecode  -u -l=fr | ilimp -nt -nv | dag2udag |' . $cmd;
  $cmd = $sxpipe . ' | yarecode  -u -l=fr | ilimp -nt -nv | dag2udag |' . $cmd;
  if ($config->follow) {
    my $follow_cmd = $server_config->{follow_exec_cmd};
    my $ploticus = $server_config->{ploticus};
    $cmd .= "| perl $follow_cmd | $ploticus -stdin -png -o stdout";
  } elsif ($config->time) {
    $cmd .= '| grep "<parsing_time>" | perl -pe "s/<parsing_time>/Time/"';
  } elsif ($config->raw) {
  } elsif (!$config->dep && $config->dis) {
    ## EASy (XML or HTML) Dis
    if ($cmd =~ /forest_convert/ || $cmd =~ /-disamb/) {
      my $dis_opt = $config->dis_options || "";
      $cmd .= " $dis_opt";
      $config->passage and $cmd .= " -passage";
      $config->passage_strict and $cmd .= " -passage_strict";
      $config->conll and $cmd .= " -conll -conll_verbose";
      $config->udep and $cmd .= " -udep -conll_verbose";
      $config->verbose and $cmd .= " -verbose";
      $config->transform and $cmd .= " -transform";
      $config->rename and $cmd .= " -rename";
      $config->exotic and $cmd .= " -exotic";
      $config->depconll and $cmd .= " -depconll";
      $config->cost and $cmd .= " -cost";
      my $restr = $server_config->{restr};
      if ($restr && -f $restr) {
	$cmd .= " -restrictions $restr";
      }
    }
    $cmd .= " | $inline_cleaner";
    my $xsl = $server_config->{easy2html};
    ($config->html) and $cmd .= "|xsltproc $xsl -";
  } elsif ($config->dis) {
    ## (DEP XML or DOT DEP) Dis. 
    my $discmd = '';
    my $dis_opt = $config->dis_options || "";
    $discmd .= " -xmldep $dis_opt";
    $config->transform and $discmd .= " -transform";
    $config->rename and $discmd .= " -rename";
    $config->exotic and $discmd .= " -exotic";
    $config->cost and $cmd .= " -cost";
    my $restr = $server_config->{restr};
    if ($restr && -f $restr) {
      $cmd .= " -restrictions $restr";
    }
    $discmd .= "| $inline_cleaner ";
    if ($config->tikzdep) {
      $cmd .= "$discmd | forest_convert.pl -f xmldep -t tikzdep -v | $display";
    } elsif (!$config->xml) {
      ## DOT DEP dis
      $cmd .= "$discmd | forest_convert.pl -f xmldep -t dep -v | $display";
    } else {
      ## DEP XML dis
      $cmd .= "$discmd | perl -pe 's/ISO-8859-1/UTF-8/o' ";
      ##      $config->utf8 and $cmd .= " | xmllint --encode UTF-8 -";
    }
  } elsif ($config->xml) {
    ## XML DEP No dis.
    $cmd .= " -nodis | $inline_cleaner";
  } else {
    ## DOT DEP No dis.
    $cmd .= " -nodis | $inline_cleaner | forest_convert.pl -f xmldep -t dep -v | $display";
  }
  #  print "cmd is $cmd\n";
  $config->xml and $format = 'xml';
  $config->html and $format = 'html';
  return $cmd;
}

sub change_settings {
  my $config = app->{frmg}{config};
  my $cfgdir = $server_config->{cfgdir};
  my $special = {};
  my @opts = grep { /^-\S+/ }
    map { 
	  s/^(svg|png|gif|pdf)$/format=$1/;	# a shortcut for some output formats
	  s/^\|/less/;		# a shortcut for less
	  s/^[\+\@](\S+)/settings=$1/; # a shortcut for setting=
	  if (s/^eid=(\S+?)\&(\S+)$//) {
	    push(@{$special->{dis}},"-eid $1 $2");
	  }
	  if (s/^nid=(\S+?)\&(\S+)$//) {
	    push(@{$special->{dis}},"-nid $1 $2");
	  }
	  if (s/^edge=(.+)$//) {
	    my ($slemma,$scat,$label,$tlemma,$tcat,$w) = map { $_ || '_' } split(m{[\&/]}o,$1);
	    push(@{$special->{dis}},"-edge $slemma $scat $label $tlemma $tcat $w");
	  }
	  if (s/^xedge=(.+)$//) {
	    my ($source,$target,$w) = map { $_ || '_' } split(/\&/,$1);
	    push(@{$special->{dis}},"-xedge $source $target $w");
	  }
	  if (s/^node=(.+)$//) {
	    my ($lemma,$cat,$left,$right,$w) = map { $_ || '_' } split(m{[\&/]}o,$1);
	    if ($cat =~ /[|]/) {
	      $cat = join(",",split(/[|]/,$cat));
	      $cat = "cat[$cat]";
	    }
	    push(@{$special->{dis}},"-node $lemma $cat $left $right $w");
	  }
	  if (s/^chunk=(.+)$//) {
	    my ($chunk,$left,$right,$w) = map { $_ || '_' } split(m{[\&/]}o,$1);
	    push(@{$special->{dis}},"-chunk $chunk $left $right $w");
	  }
	  $_ = "settings=$_" if (/^\w+$/ && -f "$cfgdir/$_.conf");
	  /=/ ? "--$_" : "-$_"
	} @_;
  ##  info "change: @opts";
  my %cfgbackup = $config->varlist(".+");
  $config->args(\@opts);
  if (exists $special->{dis}) {
    $config->set( dis_options => join(' ',
				      $config->dis_options,@{$special->{dis}}
				     )
		);
    ##    print "dis_options=".$config->dis_options."\n";
  }
  my $dis_opts = $config->dis_options;
  if ( $dis_opts =~ /[#!|&{};\n\$\@]/) {
    ## we remove potentially unsafe disamb options
    print "unsafe dis_options '$dis_opts'\n";
    $config->dis_options('');
  }
  app->{frmg}{cfgbackup} = \%cfgbackup;
}

sub restore_settings {
  exists app->{frmg}{cfgbackup} or return;
  my $cfgbackup = app->{frmg}{cfgbackup};
  my $config = app->{frmg}{config};
  delete app->{frmg}{cfgbackup};
  while (my ($k,$v) = each %$cfgbackup) {
    $config->set($k => $v);
  }
}

sub change_setting_set {
  my ($state,$var,$set) = @_;
  my $config = app->{frmg}{config};
  my $cfgdir = $server_config->{cfgdir};
  $set or return;
  my $cfg = "$cfgdir/$set.conf";
  if (-f $cfg) {
    $config->file($cfg);
  } else {
    warning("no config file for setting set '$cfg'");
  }
}

sub inline_cleaner {
  my $inline_cleaner = <<EOF;
perl -e 'while(<>) { /----\\s+([A-Z]+)\\s+----/ and last }; while (<>) { /^<[a-z]+_time>/ and last; print; }'
EOF
  chomp $inline_cleaner;
  return $inline_cleaner;
}

sub  data2json {
  my ($data,$options) = @_;
  if (grep {$_ eq 'conll' || $_ eq 'udep'} @$options) {
    my $json = [];
    foreach my $dep (split(/\n/,$data)) {
      $dep =~ /^\d+/ or next;
      chomp $dep;
      my ($id,$form,$lemma,$pos,$cpos,$mstag,$head,$rel) = split(/\t/,$dep);
      push(@$json,
	   { id => $id,
	     form => $form,
	     lemma => $lemma,
	     pos => $pos,
	     cpos => $cpos,
	     mstag => $mstag,
	     head => $head,
	     deprel => $rel
	   }
	  );
    }
    return $json;
  } elsif  (grep {$_ =~ /xml/} @$options) {
    print "DATA $data\n";
    $data =~ s/UTF-8/ISO-8859-1/;
    ## return xml2hash $data, trim => 1, attr => '';
    return XMLin($data,
		 NormalizeSpace => 2,
		 #		 SuppressEmpty => 1,
		 #		 ValueAttr => { f => 'val' },
		 KeyAttr => {f => 'name',
			     T => 'id',
			     W => 'id',
			     G => 'id',
			     R => 'id',
			     cluster => 'id',
			     node => 'id',
			     edge => 'id',
			     op => 'id',
			     hypertag => 'id'
			    });
  } else {
    return {};
  }
}

######################################################################
## we start the Mojolicious application

app->start;

__DATA__

@@ process.html.ep
% layout 'mylayout';

<p>
  Welcome to <b>FRMG Small Server</b>
</p>

%= form_for 'process' => begin
<label for="sentence">Enter your French sentence</label> </br>
  %= text_area 'sentence', cols => 70
<br/>
<label for="options" title="space or ':' separated list of options (eg: nodis svg)"> Options </label>   
  %= text_field 'options'
<br/>
<label for="schema" title="output schema"> Schema </label>     
  %= select_field schema => [['depxml' => 'dep'], 'conll', 'udep','depconll', 'passage']
<label for="view" title="output format"> Format </label>     
  %= select_field view => [qw{txt xml html png gif svg tikzdep json}]
<label for="robust" title="search for partial parses when no full parses"> Robust </label>     
  %= check_box robust => 0
<label for="disamb" title="apply disambiguation heuristics"> Disamb </label>     
  %= check_box disamb => 1
(experimental:) 
<label for="transform" title="removing empty nodes and moving forward deep syntax (experimental)"> Deeper Transform </label>     
  %= check_box transform => 0
<label for="exotic" title="dealing with sharing and ellipsis in coords (experimental)"> Exotic coords </label>     
  %= check_box exotic => 0
<br/>
<label for="dpath" title="apply on DepXML schema">DPath expression (optional) </label>
  %= text_area 'dpath', cols => 70
<br/>
  %= submit_button
% end

% if ( $dpath ) {
%= javascript "js/d3.v3.min.js"
%= javascript begin
d3.xml("<%= url_with('process.xml')->query([options => url_escape($options) . " svg", dpath => '', sentence => url_escape($sentence)]) %>",
       function (error,xml) { 
	 if (error) { return console.warn(error); }
	 var svg = d3.select(xml).selectAll('svg')
	   .attr("width","100%")
	     .attr('height',"300")
	       .attr('id','frmg_svg')
		 .attr('preserveAspectRatio','xMinYMin')
		   ;
	 d3.select('#frmg_output').node().appendChild(xml.documentElement);
	 var g = svg.select("g");
	 var transform = g.attr("transform");
	 // zoom and pan
	   var zoom = d3.behavior.zoom()
	     .on("zoom",function() {
	       var mouse = d3.mouse(this);
	       g.attr("transform","scale("+d3.event.scale+")" + "translate(" + d3.event.translate[0] + "," + Math.max(-50,d3.event.translate[1]) + ") " + transform);       
	     });
	 svg.call(zoom);
       }
);
% end
<h2>DPath output</h2>
<table border="1">
<tr>
% foreach my $v (@{$dpath->{select}}) {
<th>
%= $v
</th>
% }
</tr>
% foreach my $row (dpath($frmg_data,$dpath)) {
<tr>
%   foreach my $v (@$row) {
<td>
%= $v;
</td>
%   }
</tr>
% }
</table>
<div id="frmg_output"></div>
% } elsif ( graphical ) {
<img src="data:image/<%= frmg('format') %>;base64,<%= img $frmg_data %>" width="100%"/>
% } elsif (frmg('format') eq 'svg' ) {
%== svg($frmg_data)
% } elsif (frmg('format') eq 'html' ) {
<div  id="frmg_output">
%== $frmg_data
</div>
% } elsif (frmg('tikzdep')) {
<div id="frmg_output">
<h2> Latex view </h2>
  <div>
<img src="data:image/png;base64,<%= tikzdep $frmg_data %>" width="100%"/>
  </div>
<h2> Latex source </h2>
  <pre>
%= $frmg_data
  </pre>
</div>
% } else {
<a href="<%= url_with 'process.xml' %>">XML version</a>
<div id="frmg_output">
  <pre>
%= $frmg_data
  </pre>
</div>
% }

@@ exception.production.html.ep
% layout 'mylayout';

<p>
%= $exception->message
</p>

@@ layouts/mylayout.html.ep
<!DOCTYPE html>
<html>
    <head>
       <title><%= config 'title' %></title>
%= stylesheet begin
    BODY {
       background-color: linen;
       font-family: verdana, sans-serif;
       font-size: 13px;
    }
    img { border-width: 0px; }
% end
    </head>
    <body><%= content %>
    <hr/>
    <br/>
  <a href="mailto:<%= config 'email'%>"><%= config 'author' %></a>
  <a href="<%= config 'url' %>">Home Page</a>
</body>
</html>

@@ svg.html.ep
%= javascript "js/d3.v3.min.js"
<div  id="frmg_output">
%== $svg
</div>
%= javascript begin
var div = d3.select("#frmg_output");
var svg = d3.select("#frmg_svg").attr("width","100%");
var g = svg.select("g");
var transform = g.attr("transform");
// zoom and pan
var zoom = d3.behavior.zoom()
    .on("zoom",function() {
      var mouse = d3.mouse(this);
        g.attr("transform","scale("+d3.event.scale+")" + "translate(" + d3.event.translate[0] + "," + Math.max(-50,d3.event.translate[1]) + ") " + transform);       
});
svg.call(zoom);
% end


