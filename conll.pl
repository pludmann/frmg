/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015, 2016, 2018 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  conll.pl -- CONLL-like schemas
 *
 * ----------------------------------------------------------------
 * Description
 * Conversion and Emission for the CONLL-schema used for the FrenchTreeBank (FTB, -conll), 
 * Sequoia (-sequoia), SPMRL (-spmrl), and French Question Bank (FQB, -fqb)
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.
:-require 'best.pl'.


:-finite_set(conll_fullcat,['NC',
			    'DET',
			    'P',
			    'PONCT',
			    'ADJ',
			    'V',
			    'ADV',
			    'NPP',
			    'VPP',
			    'P+D',
			    'CC',
			    'VINF',
			    'CLS',
			    'PROREL',
			    'CS',
			    'CLR',
			    'PRO',
			    'VPR',
			    'CLO',
			    'ET',
			    'VS',
			    'PREF',
			    'ADVWH',
			    'PROWH',
			    'P+PRO',
			    'VIMP',
			    'I',
			    'ADJWH',
			    'DETWH'
			   ]).

:-subset(conll_det,conll_fullcat['DET','DETWH']).
:-subset(conll_adv,conll_fullcat['ADV','ADVWH']).
:-subset(conll_cl,conll_fullcat['CLR','CLO','CLS']).
:-subset(conll_notponct,conll_fullcat[~ ['PONCT']]).
	
:-finite_set(conll_cat,['N',
			'V',
			'C',
			'P',
			'D',
			'PONCT',
			'A',
			'CL',
			'I',
			'ET',
			'ADV',
			'PRO'
		       ]
	    ).

:-light_tabular conll_cost_process/1.
:-mode(conll_cost_process/1,+(-)).

conll_cost_process(EId) :-
	'$answers'(edge_cost(EId,_,_,_))
	.

:-std_prolog conll_edge_reset/0.

conll_edge_reset :-
    abolish(conll_edge/6),	
    abolish(conll_potential_root/2),
    abolish(conll_best_potential_root/1),
    abolish(conll_node_info/6),
    abolish(node2conll_source/2),
    abolish(node2conll_target/2),
    true
.

:-std_prolog conll_emit/0.

conll_emit :-
    conll_edge_reset,
    emit_multi('CONLL'),
    every(( node{ id => _NId, cluster => cluster{ lex => _TId, token => _Token }},
            \+ _TId = [],
            \+ _TId = [_|_],
	    \+ domain(_Token,[des,au,aux,du]),
%	    format('register token2node ~w  => ~w\n',[_TId,_NId]),
	    record( conll_token2node(_TId,_NId) )
	  )),
    conll_ids(0,1,[]),
    conll_collect_node_info(1),
    every(( conll_head )),
    sentence(SId),
    recorded(mode(Mode)),
    ( recorded(has_best_parse) ->
	  Best = yes
     ;
	  Best = no
	),
    (
	opt(spmrl) ->
	    spmrl_handle_terms
     ;
     true
    ),
    (Mode = robust ->
	 true
     ;
     %% check for multiple roots
     every(( node2conll(_,_Pos),
             \+ conll_edge(_Pos,_,_,_,_,_),
             \+ recorded(spmrl_edge(_Pos,_,_,_,_,_)),
	     record_without_doublon(conll_potential_root(_Pos))
	   )),
     ( recorded(conll_potential_root(_Pos1)),
       recorded(conll_potential_root(_Pos2)),
       _Pos1 \== _Pos2 ->
	   %% select best root
	   ( ( domain(_CCat,['V','C','N','A','ET','ADV'])
	      ; _CCat = conll_cat[~ 'PONCT']
	     ),
	     recorded(conll_potential_root(_Pos)),
	     conll_node_info(_Pos,_CLex,_CLemma,_CCat,_FullCat,_MSTag) ->
		 record(conll_best_potential_root(_Pos))
	    ;
	    true
	   )
      ;
      true
     )
    ),
    (recorded(has_best_parse(AllW)) ->
     recorded('N'(N)),
     WperWord is AllW / N
     ;
     AllW = 0,
     WperWord = 0
    ),
    disable_verbose((
			   format('## sentence=~w mode=~w best=~w w=~w nw=~w\n',[SId,Mode,Best,AllW,WperWord]),
			   conll_emit(1),
			   conll_emit_cost,
			   conll_emit_stats,
			   true
		       )),
    true
.

:-std_prolog conll_emit_cost/0.

conll_emit_cost :-
    %% emit cost info
    every(( recorded(opt(cost)),
%	    format('## emit cost info\n',[]),
	    conll_cost_process(EId),
%	    format('conll cost process eid=~w\n',[EId]),
	    ( recorded(keep_edge(EId)) ->
		  Kept = yes,
		  E::edge{ id => EId,
			   label => Label,
			   type => Type::edge_kind[],
			   source => Source::node{ id => SNId,
						   cluster => cluster{ id => SCId,
								       left => SLeft,
								       right => SRight,
								       lex => SLex
								     }
						 },
			   target => Target::node{ id => TNId,
						   cluster => cluster{ id => TCId,
								       left => TLeft,
								       right => TRight,
								       lex => TLex
								     }
						 }
			 },
		  true
	     ;
	     Kept = no,
	     recorded(erased(E)),
	     true
	    ),
	    (node!empty(Source) -> SEmpty = 1 ; SEmpty = 0),
	    ( node!empty(Target) ->
		  %% Test: only keep cost info for edges with non empty targets
		  %% Warning: Edge is not necessarily an adj edge
		  %% we have some cases where we should follow more than one edge to find a non empty edge
		  TEmpty = 1,
		  search_non_empty_target(E,Kept,ITPos)
	     ;
	     TEmpty = 0,
	     ITPos = 0
	    ),
		edge_rank(EId,Rank,Dir),
		edge_delta(SLeft,TLeft,Dir,Delta),
		xnode2conll_target(Target,TPos),
		xnode2conll_source(Source,SPos),
		( Dir = right ->
		  in_between_ponct(SRight,TLeft,Between)
		;
		  in_between_ponct(TRight,SLeft,Between)
		),
		
		edge_min_height(EId,Height),
		edge_min_depth(EId,Depth),

		node_features(SNId,
			      [ cat: SCat,
				xcat: SXCat,
				form: SForm,
				lemma: SLemma,
				cap: SCap,
				(tree) : STree,
				vmode: SVMode,
				root: SRoot,
				subcat: SSubcat,
				xinfo: SXInfo,
				pos: _,
				postag: SPosTag,
				deprel: SDepRel,
				depdelta: SDepDelta,
				form_features: [ form: SForm2,
						 cluster: SCluster,
						 suff: SSuff,
						 cats: SCats
						],
				position_features: [
						    pfeatures: [
								form: PSForm,
								cluster: PSCluster,
								suff: PSSuff,
								cats: PSCats
							       ],
						    ppfeatures: [
								 form: PPSForm,
								 cluster: PPSCluster,
								 suff: PPSSuff,
								 cats: PPSCats
								],
						    nfeatures: [
								form: NSForm,
								cluster: NSCluster,
								suff: NSSuff,
								cats: NSCats
							       ]
				],
				msf: SMSF,
				vec: SVec
			      ]
			     ),

		node_features(TNId,
			      [ cat: TCat,
				xcat: TXCat,
				form: TForm,
				lemma: TLemma,
				cap: TCap,
				(tree): TTree,
				vmode: TVMode,
				root: TRoot,
				subcat: TSubcat,
				xinfo: TXInfo,
				pos: _,
				postag: TPosTag,
				deprel: TDepRel,
				depdelta: TDepDelta,
				form_features: [ form: TForm2,
						 cluster: TCluster,
						 suff: TSuff,
						 cats: TCats
					       ],
				position_features: [ pfeatures: [
								 form: PTForm,
								 cluster: PTCluster,
								 suff: PTSuff,
								 cats: PTCats
								],
						     ppfeatures: [
								  form: PPTForm,
								  cluster: PPTCluster,
								  suff: PPTSuff,
								  cats: PPTCats
								 ],
						     nfeatures: [
								 form: NTForm,
								 cluster: NTCluster,
								 suff: NTSuff,
								 cats:  NTCats
								]
						   ],
				msf: TMSF,
				vec: TVec
			      ]
			     ),

		(edge_brothers(EId,Brothers) xor Brothers=ukw),
		
		mutable(MMax,-100000,true),
		every((
		       edge_best_parse_constraint(EId,Constraints),
		       '$answers'( best_parse(TNId,_,_,Constraints,dstruct{ w => _W } ) ),
		       mutable_max(MMax,_W)
		      )),		
		mutable_read(MMax,WMax),

		(EId = root(TNId) ->
		 name_builder('root~w',[TNId],NormEId)
		;
		 NormEId = EId
		),
		AltPrepFeatures = [],
		(TLex = [_|_] -> XTLex = TLex ; XTLex = [TLex]),
		(SLex = [_|_] -> XSLex = SLex ; XSLex = [SLex]),
		
		%% warning: multiple _Cst are possible
		%% => emit several line for each SSNId
		'$answers'(edge_cost(EId,W,Ws1,_Cst)),
		mutable(MWs,Ws1,true),
		every((
			     mutable(NameM,[],true),
			     recorded(regional_edge_cost(EId,_RN,_)),
			     mutable_read(NameM,_Names),
			     \+ domain(_RN,_Names),
			     mutable_list_extend(NameM,_RN),
			     mutable(_MRW,0,true),
			     every((
					  recorded(regional_edge_cost(EId,_RN,_RW1)),
					  mutable_add(_MRW,_RW1)
				      )),
			     mutable_read(_MRW,_RW),
%			     format('retrieved regional edge cost ~w for ~w\n',[_RNW,Ws1]),
			     _RNW ::= (_RN:_RW),
			     mutable_list_extend(MWs,_RNW)
			 )),
		mutable_read(MWs,Ws),

%%		format('conll edge cost eid=e~w w=~w ws=~w cst=~w\n',[EId,W,Ws,_Cst]),
		(_Cst = [] ->
		 SFeatures = [],
		 XKept = Kept
		;
		 SFeatures = [sspos:SSPos,
			      ssnid: SSNId,
			      seid: _Cst,
			      sscat:SSCat,
			      sscluster:SSCluster,
			      ssvmode:SSVMode,
			      ssmsf: SSMSF,
%			      ssvec: SSVec,
			      sdelta:SDelta,
			      srank:SRank,
			      sdir:SDir,
			      sslemma: SSLemma,
			      ssform: SSForm,
			      sssuff: SSSuff,
			      slabel: SLabel,
			      sstree: SSTree,
			      sssubcat: SSSubcat,
			      ssxinfo: SSXInfo,
			      ssroot: SSRoot,
			      pssform: PSSForm,
			      nssform: NSSForm,
			      ppssform: PPSSForm,

			      psscats: PSSCats,
			      nsscats: NSSCats,
			      ppsscats: PPSSCats,
			      sscats: SSCats,

			      psscluster: PSSCluster,
			      nsscluster: NSSCluster,
			      ppsscluster: PPSSCluster
			     ],
		 ( ParentEdge::edge{ id => _Cst,
				      source => SSource::node{ id => SSNId,
							       cluster => cluster{ id => SSCId,
										   left => SSLeft
										 }
							     },
				     label => SLabel
				   } -> XKept = Kept
		 ; recorded( erased( ParentEdge ) ) ->
		   XKept = no
		 ;
		   fail
		 ),
		 
		 xnode2conll_source(SSource,SSPos),
		 edge_abstract_rank(_Cst,SRank,SDir),
		 edge_delta(SSLeft,TLeft,SDir,SDelta),
		 node_features(SSNId,
			        [ cat: SSCat,
				  xcat: SSXCat,
				  form: SSForm,
				  lemma: SSLemma,
				  cap: SSCap,
				  (tree): SSTree,
				  vmode: SSVMode,
				  root: SSRoot,
				  subcat: SSSubcat,
				  xinfo: SSXInfo,
				  pos: _,
				  postag: SSPosTag,
				  deprel: SSDepRel,
				  depdelta: SSDepDelta,
				  form_features: [ form: SSForm2,
						   cluster: SSCluster,
						   suff: SSSuff,
						   cats: SSCats
						 ],
				  position_features: [ pfeatures: [ form: PSSForm,
								    cluster: PSSCluster,
								    suff: PSSSuff,
								    cats: PSSCats
								  ],
						       ppfeatures: [
								    form: PPSSForm,
								    cluster: PPSSCluster,
								    suff: PPSSSuff,
								    cats: PPSSCats
								   ],
						       nfeatures: [
								   form: NSSForm,
								   cluster: NSSCluster,
								   suff: NSSSuff,
								   cats: NSSCats
								  ]
						     ],
				  msf: SSMSF,
				  vec: SSVec
				]
			      ),
		 true
		),
		
		format('## cost\ttpos=~w\tspos=~w\teid=~w\ttnid=~w\tsnid=~w\tkept=~w\tlabel=~w\ttype=~w\tdir=~w\tdelta=~w\ttcat=~w\ttlemma=~w\ttform=~w\ttleft=~w\ttright=~w\tttoken=~L\tscat=~w\tslemma=~w\tsform=~w\tsleft=~w\tsright=~w\tstoken=~L\ttxcat=~w\tsxcat=~w\tttree=~w\tstree=~w\trank=~w\ttvmode=~w\tsvmode=~w\ttmsf=~W\tsmsf=~W\ttvec=~W\tsvec=~W\ttcap=~w\tscap=~w\tpsform=~w\tnsform=~w\tptform=~w\tntform=~w\tppsform=~w\tpptform=~w\tspostag=~w\ttpostag=~w\tsdeprel=~w\tsdepdelta=~w\ttdeprel=~w\ttdepdelta=~w\tpscats=~W\tnscats=~W\tptcats=~W\tntcats=~W\tppscats=~W\tpptcats=~W\tscats=~W\ttcats=~W\ttsuff=~w\tssuff=~w\ttsubcat=~w\tssubcat=~w\ttxinfo=~w\tsxinfo=~w\ttcluster=~w\tscluster=~w\tpscluster=~w\tnscluster=~w\tptcluster=~w\tntcluster=~w\tppscluster=~w\tpptcluster=~w\titpos=~w\ttempty=~w\tsempty=~w\tsroot=~w\theight=~w\tdepth=~w\tinbetween=~w\txbrothers=~w\t~U\t~U\ttw=~w\t~U\n',
		       [TPos,
			SPos,
			NormEId,
			TNId,
			SNId,
			XKept,
			Label,
			Type,
			Dir,
			Delta,
			TCat,
			TLemma,
			TForm,
			TLeft,TRight,['~w','_'],XTLex,
			SCat,
			SLemma,
			SForm,
			SLeft,SRight,['~w','_'],XSLex,
			TXCat,
			SXCat,
			TTree,
			STree,
			Rank,
			TVMode,
			SVMode,
			TMSF,
			SMSF,
			TVec,
			SVec,
			TCap,
			SCap,
			
			PSForm,
			NSForm,
			PTForm,
			NTForm,
			PPSForm,
			PPTForm,

			SPosTag,
			TPosTag,

			SDepRel,
			SDepDelta,
			TDepRel,
			TDepDelta,
			
			PSCats,
			NSCats,
			PTCats,
			NTCats,
			PPSCats,
			PPTCats,
			SCats,
			TCats,
			
			TSuff,
			SSuff,
			TSubcat,
			SSubcat,
			TXInfo,
			SXInfo,
			TCluster,
			SCluster,
			PSCluster,
			NSCluster,
			PTCluster,
			NTCluster,
			PPSCluster,
			PPTCluster,
			ITPos,
			TEmpty,
			SEmpty,
			SRoot,
			Height,
			Depth,
			Between,
			Brothers,
			['~w=~W','\t'],SFeatures,
			['~w=~W','\t'],AltPrepFeatures,
			WMax,
			['<~w>=~w','\t'],Ws
			])
	      )),


	%% emit all brother features
	every(( recorded(opt(cost)),
		'$answers'(brother_edge_cost(_EId1,_EId2,_,_BW)),
		emit_brother_cost(_EId1,_EId2,_,_BW)
	      )),

	%% emit regional features
	every(( recorded(opt(cost)),
		recorded(regional_cost(NId,
				       Ws,
				       [
					   children: Children,
					   nedges: R_NEdges,
					   nadj: R_NAdj,
					   hfeatures: [ cat: R_HCat,
							xcat: R_HXCat,
							form: R_HForm,
							lemma: R_HLemma,
							cap: _,
							(tree): R_HTree,
							vmode: R_HVMode,
							root: R_HRoot,
							subcat: R_HSubcat,
							xinfo: R_HXInfo,
							pos: R_HPos,
							postag: R_HPosTag,
							deprel: R_HDepRel,
							depdelta: R_HDepDelta,
							form_features:  [ form: R_HForm2,
									  cluster: R_HCluster,
									  suff: R_HSuff,
									  cats: R_HCats
									],
							position_features: R_HPosition_Features,
							msf: R_HMSFeats,
							vec: R_HVec
						      ],
					   length: R_SpanLength,
					   cpattern: R_ChildPattern,
					   cpos: R_ChildPOS,
					   cclusters: R_ChildClustersPattern,
					   cadj: R_CAdjPattern,
					   first: R_First,
					   last: R_Last,
					   firstpos: R_FirstPos,
					   lastpos: R_LastPos,
					   firstcluster: R_FirstCluster,
					   lastcluster: R_LastCluster,
					   nextlastcluster: R_NextLastCluster,
					   valency: R_Valency
				       ],
				       Deriv,
				       XChildren
				      )),
		(recorded(keep_deriv(Deriv)) ->
		     Kept = yes
		 ;
		 Kept = no
		),
		mutable(MTPos,[]),
		every((domain(TNId,Children),
		       (TN::node{ id => TNId } xor recorded(erased(TN))),
		       xnode2conll_target(TN,TPos),
		       mutable_list_extend(MTPos,TPos)
		      )),
		mutable_read(MTPos,AllTPos),
		mutable(MEIds,[]),
		every((domain(dinfo(_,_EId,_,_),XChildren),
		       mutable_list_extend(MEIds,_EId)
		      )),
		mutable_read(MEIds,AllEIds),
		(Head::node{ id => NId } xor recorded(erased(Head))),
		xnode2conll_source(Head,HPos),
%		format('xconll2source head=~w => HPos=~w\n',[Head,HPos]),
		format('## rcost\tderiv=~w\thpos=~w\ttpos=~L\teids=~L\tkept=~w\tnedges=~w\tnadj=~w\thcat=~w\thform=~w\thlemma=~w\thtree=~w\thform2=~w\thcluster=~w\tlength=~w\tcpattern=~w\tcpos=~w\tcclusters=~w\tcadj=~w\tfirst=~w\tlast=~w\tfirstpos=~w\tlastpos=~w\tfirstcluster=~w\tlastcluster=~w\tnextlastcluster=~w\tvalency=~W\t~U\n',
		       [Deriv,
			HPos,
			['~w',':'], AllTPos,
			['~w',':'],AllEIds,
			Kept,
			R_NEdges,
			R_NAdj,
			R_HCat,
			R_HForm,
			R_HLemma,
			R_HTree,
			R_HForm2,
			R_HCluster,
			R_SpanLength,
			R_ChildPattern,
			R_ChildPOS,
			R_ChildClustersPattern,
			R_CAdjPattern,
			R_First,
			R_Last,
			R_FirstPos,
			R_LastPos,
			R_FirstCluster,
			R_LastCluster,
			R_NextLastCluster,
			R_Valency,
			['<~w>=~w','\t'],Ws
		       ]
		      ),
		true
	      )),
	
	true
	.

:-std_prolog conll_emit_stats/0.

conll_emit_stats :-
    every((
		 recorded(opt(altstats)),
		 ( tab_item_term(I,verbose!anchor(_,_,_,Tree,_,_,_)),
		   recorded(I),
		   (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
		   format('## stats anchor ~w\n',[MD5])
		  ;
		  tab_item_term(I,verbose!struct(Tree,_)),
		  recorded(I),
		  (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
		  format('## stats struct ~w\n',[MD5])
		  ;
		  recorded(checked_cat(Left,Tree,Cat,Mode,Top)),
		  (recorded(tree2md5(Tree,MD5)) xor Tree=MD5),
		  format('## stats cat ~w ~w ~w ~w ~w\n',[MD5,Cat,Mode,Left,Top])
		 ),
		 true
	     )),
    true
.

:-std_prolog search_non_empty_target/3.

search_non_empty_target(Edge::edge{ id => EId, target => Target},Kept,ITPos) :-
	( % Kept= yes,
	  IEdge::edge{ id => IEId,
		       source => Target,
		       target => ITarget::node{ id => ITNId,
						cluster => cluster{ id => ITCId,
								    left => ITLeft,
								    right => ITRight }},
		       type => IType
		     },
	  IType = subst,
	  \+ node!empty( ITarget ),
	  exists(( edge2top(EId,_OId), edge2sop(IEId,_OId))),
	  node2conll_target(ITNId,ITPos) -> true
	% ; % Kept= yes,
	%   IEdge::edge{ source => Target,
	% 	       target => ITarget::node{ id => ITNId,
	% 						  cluster => cluster{ id => ITCId,
	% 								      left => ITLeft,
	% 								      right => ITRight }},
	% 	       type => IType
	% 	     },
	%   IType = lexical,
	% 	    \+ node!empty( ITarget ),
	%   exists(( edge2top(EId,_OId), edge2sop(IEId,_OId))),
	%   node2conll_target(ITNId,ITPos) -> true
	; Kept = no,
	  recorded( erased( IEdge ) ),
	  IType = subst,
	  \+ node!empty( ITarget ),
	  exists(( edge2top(EId,_OId), edge2sop(IEId,_OId))),
	  xnode2conll_target(ITarget,ITPos) ->
	  true
	% ; Kept = no,
	%   recorded( erased( IEdge ) ),
	%   IType = lexical,
	%   \+ node!empty( ITarget ),
	%   exists(( edge2top(EId,_OId), edge2sop(IEId,_OId))),
	%   xnode2conll_target(ITarget,ITPos) ->
	%   true
	; IEdge,
	  IType = subst,
	  node!empty(ITarget),
	  exists(( edge2top(EId,_OId), edge2sop(IEId,_OId))),
	  search_non_empty_target(IEdge,Kept,ITPos) ->
	  true
	; Kept=no,
	  recorded(erased(IEdge)),
	  IType = subst,
	  node!empty(ITarget),
	  exists(( edge2top(EId,_OId), edge2sop(IEId,_OId))),
	  search_non_empty_target(IEdge,Kept,ITPos) ->
	  true
	;
	  ITPos = 0	% should try with fail !?

	)
	.

format_hook(0'W,Stream,[V|R],R) :- %'0
    (V=[_|_] ->
	 format(Stream,'~L',[['~w','|'],V])
     ;
     format('~w',[V])
    )
	.

:-std_prolog emit_brother_cost/4.

emit_brother_cost(EId,BEId,BDir,W) :-
	( Edge::edge{ id => EId, target => node{ cluster => cluster{ left => TLeft } }} -> Kept1=yes
	; recorded(erased(Edge)), Kept = no, Kept1 = no
	),
	edge_features(EId,TLeft,Features),
	( BEId == EId ->
	  BFeatures = [type: none],
	  Kept2 = Kept1,
	  Kept ?= yes
	;
	  ( BEdge::edge{ id => BEId, target => node{ cluster => cluster{ left => BTLeft } } } -> Kept ?= yes, Kept2=yes
	  ; recorded(erased(BEdge)),
	    Kept = no,
	    Kept2 = yes
	  ),
	  edge_features(BEId,BTLeft,BFeatures)
	),

	(EId = root(TNId) ->
	 name_builder('root~w',[TNId],NormEId)
	;
	 NormEId = EId
	),
	(BEId = root(BTNId) ->
	 name_builder('root~w',[BTNId],NormBEId)
	;
	 NormBEId = BEId
	),

	
	format('## bcost\t~U\t~U\tkept=~w\tkept1=~w\tkept2=~w\teid=~w\tbeid=~w\t<brother>=~w\n',
	       [
		['~w=~w','\t'],Features,
		['b~w=~w','\t'],BFeatures,
		Kept,
		Kept1,
		Kept2,
		NormEId,
		NormBEId,
		W
	       ]),
	
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CONLL 

:-std_prolog conll_ids/3, conll_emit/1.
:-extensional conll2node/2, node2conll/2.

conll_ids(Left,Pos,Prev) :-
	( N::node{ id => NId,
		   lemma => Lemma,
		   form => Form,
		   cat => Cat,
		   cluster => cluster{ left => Left, right => Right, lex => Lex, token => Token}
		 },
	  Right > Left ->
%	  format('conll ids left=~w pos=~w lex=~w right=~w lemma=~w token=~w\n',[Left,Pos,Lex,Right,Lemma,Token]),
	  ( Lemma = '_SENT_BOUND' ->
	    %% skip sentence bounds
	    XPos is Pos
	  ; Prev = node{ id => PNId, lemma => PLemma, cat => PCat, cluster => cluster{ lex => Lex } },
				%%	    lex_tokenize(Lex,[_]),
	    \+ ( Lex = [_|_] ),
	      %% some strange case where sxpipe has merged two CONLL tokens into a single SxPipe tokens
	      %% corresponding to 2 FRMG words (a-t-on)
	    \+ (
		PCat = cat[v,aux], Cat = cat[cln] % a-t-il a-t-on
	       )
	    ->
%	    format('here1\n',[]),
	    XPos is Pos,
	    U is Pos-1,
	    record( conll_compound(U) ),
	    record( conll_forward(Prev,N) ),
	    record( conll_backward(N,Prev) ),
	    register_conll_redirect(NId,U)
	  ; Prev = node{ cluster => cluster{ lex => PrevLex }, lemma => '_NUMBER' },
	    %%	    lex_tokenize(PrevLex,PrevLLex::[_,_|_]),
	    PrevLex = [_,_|_],
	    domain(Lex,PrevLex) ->
%	    format('here2\n',[]),
	    %% specific case for some numbers such as 20 millions !
	    XPos is Pos,
	    U is Pos-1,
	    record( conll_compound(U) ),
	    record( conll_forward(Prev,N) ),
	    record( conll_backward(N,Prev) ),
	    register_conll_redirect(NId,U)
	  ;   % lex_tokenize(Lex,[PrevLex,_|_]),
	    Lex = [PrevLex,_|_],
	    Prev =  node{ lemma => PLemma, cat => PCat, cluster => cluster{ lex => PrevLex }},
	    node{ id => NId2, cluster => cluster{ lex => Lex } },
	    NId \== NId2 ->
%	    format('here3\n',[]),
	    %% cases like "m�me_s'il"  with m�me_s = 1 CONLL tok for 1 Sxipe word (m�me)
	    %% and m�me_s'il = 2 CONLL tok for 2 sxpipe word (si + il)
	    XPos is Pos,
	    U is Pos-1,
	    record( conll_compound(U) ),
	    record( conll_forward(Prev,N) ),
	    record( conll_backward(N,Prev) ),
	    register_conll_redirect(NId,U),
	    record( conll_mixed_case(NId) )
	  ; %% fail,
	    part_of_agglutinate(Form,_),
%	    format('try agglutinate lex=~w form=~w token=~w\n',[Lex,Form,Token]),
	    domain(Token,[des,au,aux,du]),
%	    Lex = [_],
	    Prev = node{ cluster => cluster{ right => Left, lex => PrevLex } },
	    domain(Lex,PrevLex) ->
%	    format('here4\n',[]),
	    XPos is Pos,
	    U is Pos-1,
	    record_without_doublon( conll_compound(U) ),
	    record(conll_forward(Prev,N)),
	    record(conll_backward(N,Prev)),
	    register_conll_redirect(NId,U)

	  ; %fail,
	    part_of_agglutinate(Form,_),
	    Prev = node{ form => PrevForm, cluster => cluster{ right => Left, lex => Lex } },
	    (Lex = [TId1|_] ; conll_last_cat(Lex,TId1)),
	    'T'(TId1,Token1),
	    domain(Token1,[des,au,aux,du]) ->
%	    format('here5\n',[]),
		XPos is Pos,
		U is Pos-1,
		record_without_doublon( conll_compound(U)),
		record(conll_forward(Prev,N)),
		record(conll_backward(N,Prev)),
		register_conll_redirect(NId,U)

				       	  ; Lex=[TId|_], %opt(smprl),
        
	    %	    Prev = node{ cluster => cluster{ right => Left, lex => Lex } },
	    \+ ( domain(_TId,Lex),
                 \+ recorded(conll_same_lex(_TId,_,_))
	       ),
	    recorded(conll_same_lex(TId,_N,_U))
	    ->
%%		format('same lex pos=~w form=~w U=~w _N=~w\n',[Pos,Form,_U,_N]),
		XPos is Pos,
		U is Pos-1,
		record_without_doublon( conll_compound(U)),
		record(conll_forward(Prev,N)),
		record(conll_backward(N,Prev)),
		register_conll_redirect(NId,U),
		/*
		record_without_doublon(conll_compound(_U)),
		record(conll_forward(_N,N)),
		record(conll_backward(N,_N)),
		register_conll_redirect(NId,_U)
*/
		true
				       
	  ; fail,
	    Lex = [_,_|_],
	    Prev = node{ form => PrevForm, cluster => cluster{ right => Left, lex => Lex }}
	    ->
%	    format('here6\n',[]),
		(recorded(conll_same_tokens_rank(Prev,PrevRank,PrevSTLex)) xor PrevRank=0, PrevSTLex=Lex),
		PrevSTLex = [TId1|_STLex],
		'T'(TId1,Token1),
%%		format('%% same token pos=~w form=~w lex=~w prevrank=~w prevstlex=~w token1=~w\n',[Pos,Form,Lex,PrevRank,PrevSTLex,Token1]),
		(domain(Token1,[des,au,aux,du]) ->
		     %% agglutinate
		     Rank = PrevRank,
		     STLex = PrevSTLex,
		     XPos is Pos,
		     U is Pos-1,
		     record_without_doublon( conll_compound(U)),
		     record(conll_forward(Prev,N)),
		     record(conll_backward(N,Prev)),
		     register_conll_redirect(NId,U)
		 ;
		 Rank is PrevRank+1,
		 STLex = _STLex,
		 record( conll2node(Pos,NId) ),
		 record( node2allconll(NId,Pos) ),
		 register_node2conll(NId,Pos),
		 XPos is Pos + 1
		),
%%		format('%% => rank=~w stlex=~w\n',[Rank,STLex]),
		record(conll_same_tokens_rank(N,Rank,STLex))
		      
	  ;   %% this case should be controlled by an option
	    %% do we wish to follow the token segmentation or the wordform segmentatiom
	    % lex_tokenize(Lex,LLex::[_,_|_]),
	    Lex = [_,_|_],
	    %	    format('conll multi lex nid=~w lex=~w\n',[NId,Lex]),
	    %	    \+ recorded(conll_same_lex(Lex,_,_))
	    true
	    /*
	    ( \+ ( node{ id => _NId, cluster => cluster{ lex => Lex }},
		   _NId \== NId
		 )
		     xor
		     node{ id => _NId, lemma => _NextLemma, form => _NextForm, cluster => cluster{ left => Right, lex => Lex }},
	      part_of_agglutinate(_NextForm,_),
	      conll_last_cat(Lex,_LastTId),
	      'T'(_LastTId,_LastToken),
	      domain(_LastToken,[des,au,aux,du])
	    )
*/
	     ->
	     %%	    lex2conll_forms(LLex,CLexL),
%	     format('here7\n',[]),
		 every(( domain(_TId,Lex),
                         \+ recorded(conll_same_lex(_TId,_,_)),
			 record(conll_same_lex(_TId,N,Pos))
		       )),
	    lex2conll_forms(Lex,CLexL),
%	    format('=> conll multi lex nid=~w lex=~w clex=~w\n',[NId,Lex,CLexL]),
	    record( conll_compounds(NId,CLexL) ),
	    %% one single node for multiple tokens
	    mutable(MPos,Pos,true),
	    mutable(MRank,1,true),
	    ( 
	      conll_expand_sxpipe_compound(CLexL,NId,Expansion) ->
	      record( conll_expansion(NId,Expansion) ),
	      %% conll_register_expansion(NId,LLex,Expansion,MPos,MRank)
%%	      format('register expansion ~w ~w ~w\n',[Pos,NId,Expansion]),
	      conll_register_expansion(NId,Lex,Expansion,MPos,MRank,CLexL)
	    ;
		every(( % domain(_Lex,LLex),
			     domain(_Lex,Lex),
			     \+ recorded(conll_token2node(_Lex,_)),
			mutable_read(MPos,_Pos),
			mutable_read(MRank,_Rank),
			%%		      format('here0 ~w ~w ~w\n',[_Pos,_Lex,_Rank]),
			_XPos is _Pos + 1,
			mutable(MPos,_XPos),
			_XRank is _Rank+1,
			mutable(MRank,_XRank),
			record( conll2node(_Pos,NId) ),
			record( node2allconll(NId,_Pos) ),
			record( conll_lex(_Pos,_Lex) ),
			record( conll_rank(_Pos,_Rank) ),
			%%		      format('## seg ~w lex=~w lemma=~w\n',[_Pos,_Lex,Lemma]),
			( ( (Lemma = entities[~ ['_NUMBER']] ; Lemma = date[]),			  
			    \+ node2conll(NId,_),
			    conll_form(_Lex,_Form),
			    \+ is_number(_Form),
			    \+ domain(_Form,[le,la,les,de,du,des,'Le','La','Les','De','Du','Des']),
			    \+ domain(_Form,['.',',','-','(',')','_',':','[',']','<','>'])
			  )
			->
			    register_node2conll(NId,_Pos)
			 ; %fail,
			   opt(spmrl),
			   _Rank == 1 ->
			       register_node2conll(NId,_Pos)
			;
			true
			)
		      ))
	      ),
	    mutable_read(MPos,XPos),
	    LastPos is XPos - 1,
	    ( node2conll(NId,_) xor register_node2conll(NId,LastPos) ),
	    record( conll_last_rank(LastPos) ),
	    record( conll_last_rank(NId,LastPos) )
	  ;
%	    format('here1 ~w ~w\n',[Pos,Lex]),
	    (	  Prev = node{ id => PNId, cluster => cluster{ lex => Lex }},
		  recorded( conll_mixed_case(PNId) ),
		  %%	  lex_tokenize( Lex,[_,Lex2])
		  Lex = [_,Lex2]
	    ->
		  record( conll_lex(Pos,Lex2) )
	     ;	Lex = [_|_] ->
		    domain(_Lex,Lex),
		    record( conll_lex(Pos,_Lex) )
	     ;
	     record( conll_lex(Pos,Lex) ),
	     true
	    ),
%	    format('here8\n',[]),
	    record( conll2node(Pos,NId) ),
	    record( node2allconll(NId,Pos) ),
	    register_node2conll(NId,Pos),
	    XPos is Pos + 1
	  ),
	  conll_ids(Right,XPos,N)
	;
	  true
	)
	.

:-std_prolog conll_expand_sxpipe_compound/3.
:-extensional conll_simple_expansion/3.
:-rec_prolog conll_complex_expansion/3.

:-finite_set(spmrl_cpd_trigger,[de,des,du,le,la,'d''','l''',(-),(/),(:),(.)]).

conll_expand_sxpipe_compound(CLex,NId,Expansion) :-
%   format('try expansion clex=~w nid=~w\n',[CLex,NId]),
    (conll_simple_expansion(CLex,NId,Expansion) -> true	
     ; conll_complex_expansion(CLex,NId,Expansion) -> true
     ; conll_alt_expansion(CLex,NId,Expansion) -> true
     ; conll_base_expansion(CLex,NId,Expansion) -> true
     ; fail
    ),
    (opt(spmrl) ->
	 N::node{ id => NId, form => Form, cat => Cat, cluster => cluster{ lex => [TId|_]} },
%	 format('Test spmrl rewrite ~w ~w\n',[CLex,Expansion]),
	 spmrl_mark_compounds(TId,CLex,no,N,[]),
	 true
     /*
     (
	 domain(_Lex,CLex),
	 domain(_Lex,[de,des,du,le,la,'d''','l''',(-),(/),(:),(.),(',')]) ->
	     format('Test spmrl trig. rewrite ~w\n',[CLex]),
	     spmrl_mark_compounds(TIds,CLex,no,N),
	     Expansion2 = Expansion,
	     format('Test spmrl trig. rewrite ~w => ~w\n',[CLex,Expansion2]),
	     true
      ; Form = entities['_PRODUCT','_COMPANY'] ->
	    spmrl_rewrite_expansion(Expansion,Expansion2)
      ;
         \+ Form = date[],
         \+ Form = number[],
         \+ Cat = np
	 ->
	     spmrl_rewrite_expansion(Expansion,Expansion2)
      ;
      Expansion2 = Expansion
     )
     */
     ;
     true
    ),
%    format('=> expansion clex=~w nid=~w x=~w\n',[CLex,NId,Expansion]),
    true
.

conll_complex_expansion(K::[N,million[]],NId,[(Type:1) @ POS,head @ HPOS ]) :-
	(is_number(N) xor domain(N,numerals[])),
	( edge{ source => node{ id => NId }, label => det, target => node{ cat => det } } ->
	  Type = mod,
	  POS = ('A', 'ADJ'),
	  HPOS = ('N','NC')
	 ; chain( node{ id => NId } << adj @ 'N' << node{} >> subst @ det >> node{}) ->
	       %% this case should not really arise
	       Type = mod,
	       POS = ('A', 'ADJ'),
	       HPOS = ('N','NC')
	 ; %fail,
	   edge{ label => det, target => node{ id => NId } } ->
	       %%  this case should not arise but there are pb with (old) SxPipe DAGs
	       Type = det,
	       POS = ('D', 'DET'),
	       HPOS = ('D','DET')
	;
	  Type = det,
	  POS = ('D', 'DET'),
	  HPOS = ('N','NC')
	)
.

%% 20 mille milliard
conll_complex_expansion(K::[N1,N2,million[]],NId,[(Type:2) @ POS,(mod:1) @ ('A','ADJ'), head @ HPOS ]) :-
	(is_number(N1) xor domain(N1,numerals[])),
	( edge{ source => node{ id => NId }, label => det, target => node{ cat => det } } ->
	  Type = mod,
	  POS = ('A', 'ADJ'),
	  HPOS = ('N','NC')
	 ; fail,
	   edge{ label => det, target => node{ id => NId } } ->
	       %%  this case should not arise but there are pb with (old) SxPipe DAGs
	       Type = mod,
	       POS = ('D', 'DET'),
	       HPOS = ('D','DET')
	;
	  Type = det,
	  POS = ('D', 'DET'),
	  HPOS = ('N','NC')
	)
.

%% Number with spmrl
conll_complex_expansion(K::[N1,_|_],NId,L) :-
    fail,
    opt(spmrl),
    node{ id => NId, form => number[]},
    domain(million[],K),
    (is_number(N1) xor domain(N1,numerals[])),
    ( edge{ source => node{ id => NId }, label => det, target => node{ cat => det } } ->
	  Type = mod,
	  POS = ('A', 'ADJ'),
	  HPOS = ('N','NC')
     ;
     Type = det,
     POS = ('D', 'DET'),
     HPOS = ('N','NC')
    ),
%    format('spmrl million rewrite ~w\n',[K]),
    spmrl_million_rewrite(K,NId,L,0,(Type,POS,HPOS)),
    %    format('=> ~w\n',[L]),
    true
.

:-std_prolog spmrl_million_rewrite/5.
spmrl_million_rewrite(K,NId,L,Rank,Info::(Type,POS,HPOS)) :-
    ( K=[million[]] ->
	  L=[head @ HPOS]
     ; K = [V1|K2],
       ( Rank == 0 ->
	     length(K,Delta),
	     L = [(Type:Delta) @ POS|L2]
	; is_number(V1) ->
	      L = [(dep_cpd : Rank) @ POS | L2]
	; V1 == ',' ->
	      L = [(dep_cpd : Rank) @ ('PONCT','PONCT') | L2]
	;
	length(K,Delta),
	L = [(mod:Delta) @ ('A','ADJ')|L2]
       ),
       XRank is Rank-1,
       spmrl_million_rewrite(K2,NId,L2,XRank,Info)
    )
.

conll_complex_expansion([N,month[]],_,[ (mod:1) @ ('A', 'ADJ'), % 5 Janvier
					head
				      ]
		       ) :- is_number(N).
conll_complex_expansion([month[],Year],_,[head,mod: -1]) :- is_number(Year). % Janvier 2013
conll_complex_expansion([day[],N,month[]],_, % Mercredi 26 Novembre
			[head @ ('N', 'NC'),
			 (mod: -1) @ ('A', 'ADJ'),
			 (mod: -2) @ ('N', 'NC')
			]
		       ) :- is_number(N).

conll_complex_expansion([day[],N,month[],Year],_, % Mercredi 26 Novembre 2014
			[head @ ('N', 'NC'),
			 (mod: -1) @ ('A', 'ADJ'),
			 (mod: -2) @ ('N', 'NC'),
			 (mod: -3) @ ('N', 'NC')
			]
		       ) :- is_number(N),
			    is_number(Year).

conll_complex_expansion([N,month[],Year],_, % 26 Novembre 2014
			[(mod: 1) @ ('A', 'ADJ'),
			 head @ ('N', 'NC'),
			 (mod: -1) @ ('N', 'NC')
			]
		       ) :- is_number(N),
			    is_number(Year).

conll_complex_expansion([N1,'-',N2,month[]],_,% 10 - 12 d�cembre
			[(mod : 3) @ ('A','ADJ'),
			 (ponct : 2) @ ('PONCT','PONCT'),
			 (mod : 1) @ ('A', 'ADJ'),
			 head @ ('N', 'NC')
			]
		       ) :- is_number(N1),
			    is_number(N2)
.

conll_complex_expansion([N1,'-',N2],NId,
			[head @ ('A', 'ADJ'),
			 head @ ('PONCT', 'PONCT'),
			 head @ ('A', 'ADJ')
			]
		       ) :-
    fail,
    is_number(N1),
    is_number(N2),
    chain(node{ id => NId, lemma => '_NUMBER'} << (adj @ 'N') << node{} >> (subst @ det) >> node{})
.
    

conll_complex_expansion(K::[Sign,N],NId,L) :-
	domain(Sign,[(+),('\\+'),(-)]),
	is_number(N),
	( chain( node{ id => NId, lemma => '_NUMBER' } << (subst @ det) << node{ lemma => pourcent } ) ->
	  L = [(mod: 2) @ ('P','P'),head]
	;
	  L = [head @ ('P','P'),obj: -1]
	),
	true
.

%conll_complex_expansion([N1,'-',N2],[head,ponct: -1,mod: -2]) :- is_number(N1), is_number(N2).
conll_complex_expansion([month[],Next],_,
			[head @ ('N', 'NC'),
			 (mod: -1) @ ('A', 'ADJ')]
		       ) :- domain(Next,[prochain,suivant,dernier]).

conll_complex_expansion([Prep1,Noun,Prep2],_,
			[ head @ ('P','P'),
			  (obj: -1) @ ('N','NC'),
			  (dep: -1) @ ('P','P')
			]
		       ) :-
	domain(Prep1,[au,�,en,aux]),
	domain(Prep2,[de,des,du,'d'''])
	.
conll_complex_expansion([Noun,Prep],_,[head_target,[(dep: -1) @ ('N','NC'),head_source @ ('P','P')]]) :-
%	\+ domain(Noun,[beaucoup]),
	domain(Prep,[de,des,du,'d'''])
	.

conll_complex_expansion([Prep,Noun],_,[head @ ('P','P'), (obj: -1) @ ('N', 'NC')]) :-
	domain(Prep,[de,des,du,'d''',en])
	.
conll_complex_expansion([Noun1,Prep,Noun2],_,[head_target @ ('N','NC'),
					      (dep : -1) @ ('P','P'),
					      [obj : -1,head_source]]) :-
	domain(Prep,[de,des,du,'d''',�,au,aux])
	.

conll_complex_expansion([Prep1,Noun,Csu],_,[head @ ('P', 'P'),
					    (obj: -1) @ ('N','NC'),
					    (dep: -1) @ ('C', 'CS')
					   ]) :-
	domain(Prep1,[au,�,en,aux,du,des,de]),
	domain(Csu,[que,'qu'''])
	.

conll_complex_expansion([de,entre],_,[head_target @ ('P','P'),
				      [obj: -1,head_source]]).

conll_complex_expansion([First,'-',Last],NId,[head,ponct: -1,mod: -2]) :-
	\+ domain(First,[demi]),
	node{ id => NId, cat => nc }
	.
conll_complex_expansion([First,'-',Last],NId,[(mod: 2) @ ('A','ADJ'),ponct: 1,head]) :-
	domain(First,[demi]),
	node{ id => NId, cat => nc }
	.

conll_complex_expansion([Coord,Adv],_,[head @ ('C', 'CC'), (mod: -1) @ ('ADV', 'ADV')]) :-
	domain(Coord,[mais,et,ou]),
	domain(Adv,[aussi,encore,bien])
	.

conll_complex_expansion([Prep,X],NId,[head @ ('P','P'), (obj: -1)]) :-
    node{ id => NId, cat => cat[adv], cluster => cluster{ left => P0 } },
    'C'(P0,lemma{ cat => prep, lemma => Prep },P1),
    P1 is P0+1
	.
    

conll_complex_expansion([Open,X,Close],NId,[(ponct:1) @ ('PONCT','PONCT'),head,(ponct: -1) @ ('PONCT','PONCT')]) :-
    domain(Open,['[','(']),
    domain(Close,[']',')'])
	.

conll_complex_expansion([Adv,Csu],_,[(obj: 1),head]) :-
    domain(Csu,[que,'qu''',si]).

conll_simple_expansion(['wall','street'],_,[(dep: 1) @ ('ET', 'ET'), head @ ('ET', 'ET')]).
conll_simple_expansion(['los','angeles'],_,[(dep: 1) @ ('ET', 'ET'), head @ ('ET','ET')]).
conll_simple_expansion(['vieux','continent'],_,[(dep: 1) @ ('A', 'ADJ'), head @ ('N', 'NC')]).
conll_simple_expansion([tf,1],_,[head @ ('N','NC'),(mod: -1) @ ('A', 'ADJ')]).
conll_simple_expansion([_,s],_,[head @ ('ET','ET'),(mod: -1) @ ('ET','ET')]).
conll_simple_expansion(['l''',_],_,[(det:1) @ ('D', 'DET'),head @ ('PRO', 'PRO')]).
conll_simple_expansion([general,motors],_,[(mod: 1) @ ('A','ADJ'),head @ ('N', 'NC')]).
conll_simple_expansion([british,gaz],_,[(mod: 1) @ ('A','ADJ'),head @ ('N', 'NC')]).
conll_simple_expansion([�,juste,titre],_,[head @ ('P','P'), (mod: 1) @ ('A','ADJ'), (obj: -2) @ ('N', 'NC')]).
conll_simple_expansion([tout,le,monde],_,
		       [(mod : 2) @ ('A', 'ADJ'),
			(det : 1) @ ('D', 'DET'),
			head @ ('N', 'NC')
		       ]).
conll_simple_expansion([de,la],_,[(det : 2) @ ('D','DET'), head @ ('D','DET')]).
conll_simple_expansion([de,'l'''],_,[(det : 2) @ ('D','DET'), head @ ('D','DET')]).
conll_complex_expansion([quelque,chose],_,[(det: 1) @ ('D','DET'), head @ ('N','NC')]).
		       
:-std_prolog conll_alt_expansion/3.
:-std_prolog conll_alt_expansion_aux/8.

conll_alt_expansion(L,NId,Expansion) :-
    %    fail,
    % format('try conll alt expansion ~w ~w\n',[L,NId]),
    N::node{ id => NId, cluster => cluster{ id => CId, left => Left, right => Right} },
    % format('try conll alt expansion ~w ~w left=~w right=~w\n',[L,NId,Left,Right]),
    ( edge{ source => N, target => node{ cluster => cluster{ left => TLeft }}}, TLeft >= Right ->
	  MaxRight is Right+5,
	  term_range(Right,MaxRight,_Right),
	  _Left = Left
     ; edge{ source => N, target => node{ cluster => cluster{ right => TRight }}}, TRight =< Left ->
	   MinLeft is Left-5,
	   term_range(MinLeft,Left,_Left),
	   Right = _Right
     ;
     _Right = Right,
     _Left = Left
    ),
    recorded(span2best_parse_node(Span::[_Left,_Right|_],_NId)),
    NId \== _NId,
    % format('try conll alt expansion ~w ~w left=~w right=~w nid=~w\n',[L,NId,Left,_Right,_NId]),
    '$answers'(best_parse(_NId,_,[_Left,_Right|_],_,DStruct::dstruct{ w => _W})),
    % format('try conll alt expansion ~w ~w ~w dstruct=~w\n',[Left,Right,L,DStruct]),
    % \+ ( '$answers'( best_parse(__NId,_,[Left,Right|_],_,dstruct{ w => __W }) ),
    %      NId \== __NId,
    %      __W > _W
    %    ),
    dstruct2lists(DStruct,CIds,EIds::[_,_|_],DIds),
    \+ domain(CId,CIds),
    % format('try conll alt expansion ~w ~w ~w ~w eids=~w\n',[L,Left,Right,L,EIds]),
    mutable(MEdges,[],true),
    every(( domain(EId,EIds),
	    (recorded(erased(E::edge{ id => EId, target => node{ cluster => cluster{ left => P0, right => P1 } }})) xor E),
%	    format('\tregister ~w\n',[P0:E]),
	    _Info ::= (P0:E),
	    mutable_list_extend(MEdges,_Info)
	  )),
    mutable_read(MEdges,Edges),
    % format('try conll alt expansion aux ~w ~w ~w ~w edges=~w\n',[L,Left,Right,L,Edges]),
    conll_alt_expansion_aux(L,Expansion,Left,Right,Left,Edges,_NId,[]),
    %    format('conll alt expansion ~w ~w ~w => ~w\n',[Left,Right,L,Expansion]),
    true
.

conll_alt_expansion_aux([Lex|Rest],Exp,Left,Right,P0,Edges,NId,LastTokenId) :-
    P1 is P0+1,
    % format('\t\ttry at ~w\n',[P0]),
    ( domain( (P0: E::edge{ id => EId,
			    label => Label,
			    type => Type,
			    target => node{ cat => TCat,
					    id => TNId,
					    cluster => cluster{ left => P0, right => P1, lex => TokenId }
					  },
			    source => node{ cat => SCat,
					    cluster => cluster{ left => SLeft, right => SRight, lex => STokenId }
					  }
			  }
	      ),
	      Edges
	    ) ->
	  Delta is STokenId - TokenId,
	  %% Delta=0 should not arise ! (but it does)
	  Delta \== 0
     ; domain( _: edge{ source => node{ cluster => cluster{ left => P0, right => P1, lex => TokenId }}}, Edges) ->
	   Exp1 = head
     ; fail
    ),
    % format('\t\tfound ~w ~w ~w\n',[P0,EId,Exp1]),
    ( Exp1 == head -> true
     ; SCat = prep, Type = subst ->
	   conll_easy_cat(TCat,XCat,XFCat),
	   Exp1 = ((obj : Delta) @ (XCat, XFCat))
     ; TCat = prep ->
	   Exp1 = ((dep : Delta) @ ('P', 'P'))
     ; TCat = det ->
	   Exp1 = ((det : Delta) @ ('D', 'DET'))
     ; TCat = coo ->
	   Exp1 = ((coord : Delta) @ ('C','CC'))
     ; SCat = coo, Label = label[coord2,coord3] ->
	   conll_easy_cat(TCat,XCat,XFCat),
	   Exp1 = (dep_coord : Delta) @ (XCat,XFCat)
     ;
     conll_easy_cat(TCat,XCat,XFCat),
     Exp1 = (mod : Delta) @ (XCat,XFCat)
    ),
    %    format('\t\tfound2 ~w ~w ~w rest=~w\n',[P0,EId,Exp1,Rest]),
    %	format('alt expansion left=~w lex=~w exp1=~w e=~w\n',[P0,Lex,Exp1,E]),
    ( P1=Right ->
	  Exp = [Exp1]
     ; TokenId = LastTokenId ->
	   conll_alt_expansion_aux([Lex|Rest],Exp,Left,Right,P1,Edges,NId,TokenId)
     ;
     Exp = [Exp1|Exp2],
     conll_alt_expansion_aux(Rest,Exp2,Left,Right,P1,Edges,NId,TokenId)
    )
.

:-std_prolog conll_base_expansion/3.

conll_base_expansion(CLex,NId,Expansion) :-
    %% find potential head
%    format('try conll base expansion ~w\n',[CLex]),
    N::node{ id => NId, lemma => Lemma},
    mutable(MHead,0),
    mutable(MLast,0),
    every(( domain(_Form,CLex),
	    mutable_inc(MLast,_),
	    mutable_read(MHead,0),
	    (  (Lemma = entities[~ ['_NUMBER']] ; Lemma = date[]),
                \+ is_number(_Form),
                \+ domain(_Form,[le,la,les,de,du,des,'Le','La','Les','De','Du','Des']),
                \+ domain(_Form,['.',',','-','(',')','_',':','[',']','<','>'])
	     ->
		 mutable_read(MLast,Current),
		 mutable(MHead,Current)
	    ))),
    mutable_read(MHead,_Head),
    (_Head = 0 -> mutable_read(MLast,Head) ; Head = _Head),
    %% build expansion
%    format('head conll base expansion ~w => ~w\n',[CLex,Head]),
    conll_base_expansion_aux(CLex,N,Expansion,Head,1),
%    format('conll base expansion ~w => ~w\n',[CLex,Expansion]),
    true
.

:-std_prolog conll_base_expansion_aux/5.

conll_base_expansion_aux(CLex,N::node{ cat => Cat, lemma => Lemma, id => NId, cluster => cluster{ lex => [TId1|_] }},Expansion,HeadRank,Rank) :-
    (CLex = [] ->
	 Expansion = []
     ; CLex = [Lex|CLexT],
       ( 
	   ( ( Lemma = '_NUMBER',
                \+ chain( N >> subst @ det >> node{} )
	      ; domain(Lex,[le,la,les,de,du,des,'Le','La','Les','De','Du','Des','l''','L'''])),
	     Rank = 1
	   ) ->
	       Head = HeadRank, % use mod dependencies inside a splitted node (rough approx !)
	       Label = det,
	       XCCat = 'D',
	       XFullCat = 'DET'
	;  domain(Lex,['.',',',';','!','?','(',')','[',']','!?','??','?!','...','"','''','-',':','_','/','(...)']) ->
	       Head = HeadRank,
	       Label = ponct,
	       XCCat = 'PONCT',
	       XFullCat = 'PONCT'
	; domain(Lex,[de,'de''']) ->
	      Head = HeadRank, 
	      Label = dep,
	      XCCat = 'P',
	      XFullCat = 'P'
	; Lemma = '_META_TEXTUAL_GN' ->
	      Head = HeadRank, 
	      Label = dep,
	      XCCat = 'N',
	      XFullCat = 'NC'
	; domain(Lex,[et,ou]) ->
	      Head = HeadRank, 
	      Label = coord,
	      XCCat = 'C',
	      XFullCat = 'CC'
	; Cat = np,
	  chain(N << lexical @ 'Np2' << XN::node{ id => XNId }),
	  recorded(node2conll(XNId,PreHead)) ->
	      Head is PreHead + 1 - TId1,
%	      format('special expansion case Np2 ~w mod ~w (~w)\n',[Rank,Head,XN]),
	      Label = mod,
	      XCCat = 'N',
	      XFullCat = 'NPP'
	;
	Head = HeadRank, % use mod dependencies inside a splitted node (rough approx !)
	Label = mod,
	( conll_cat(Cat,XCCat,Lemma,N,Lex) xor XCCat = Cat ),
	(	node2op(NId,OId),
		op{ id => OId }
	 ->	true
	 ;	
	 OId = '_'
	),
	(	conll_fullcat(Cat,XCCat,OId,N,XFullCat,Lex) xor XFullCat = XCCat)
       ),
       ( HeadRank = Rank ->
	    %% we are processing the head
	    E1 = head @ (XCCat,XFullCat)
	; Delta is Head - Rank,
	  E1 = (Label : Delta) @ (XCCat,XFullCat)
       ),
       Expansion = [E1|ExpansionT],
       NewRank is Rank+1,
       conll_base_expansion_aux(CLexT,N,ExpansionT,HeadRank,NewRank)
    )
.
	
:-std_prolog spmrl_rewrite_expansion/2.

spmrl_rewrite_expansion(Expansion::[First|Rest],[First2|Rest2]) :-
    (First = _First @ _Cat -> First2 = head @ _Cat
     ; First2 = head
    ),
    spmrl_rewrite_expansion_aux(Rest,Rest2,-1)
.

:-std_prolog spmrl_rewrite_expansion_aux/3.

spmrl_rewrite_expansion_aux(E1,E2,Delta) :-
    (E1 = [] -> E2 = []
     ; E1 = [F1|R1],
       E2 = [F2|R2],
       NewDelta is Delta - 1,
       (F1 = _F1 @ _C1 -> F2 = (dep_cpd : Delta) @ _C1
	; F2 = (dep_cpd : Delta)
       ),
       spmrl_rewrite_expansion_aux(R1,R2,NewDelta)
    )
.

:-std_prolog spmrl_mark_compounds/5.

spmrl_mark_compounds(TId,CLex,Status,N::node{ cat => Cat, lemma => Lemma, form => Form},PrevLex) :-
    ( CLex = [] -> 
	  ( Status \== no,
	    TId > Status + 1
	   ->
		EndTId is TId-1,
		NewStatus = no,
		(recorded(compound(Status,EndTId,_,_)) xor
			 Delta is EndTId - Status,
%		 format('register spmrl compound ~w ~w ~w\n',[Status,EndTId,Form]),
		 record(compound(Status,EndTId,dynamic,Delta))
		)
	   ;
	   true
	  )
     ; CLex=[Lex|CLexT],
       
       ((
	       Status \== no,
	       PrevLex = '-',
	       Lemma = entities['_PERSON','_PERSON_m','_PERSON_f'] ->
		   NewStatus = no,
		   EndTId = TId,
		   (recorded(compound(Status,EndTId,_,_)) xor
			    Delta is EndTId - Status,
		    %	       format('register spmrl compound ~w ~w ~w\n',[Status,EndTId,Form]),
		    record(compound(Status,EndTId,dynamic,Delta))
		   )
	    ;	       %% Lex = spmrl_cpd_trigger[]
	       Lemma = number[], is_number(Lex) 
	   xor Lemma = entities['_ORGANIZATION','_PRODUCT']
	   xor domain(Lex,[de,des,du,le,la,'d''','l''']),  Cat=np
	   xor domain(Lex,[(-),(/),(:),(.),(',')]), \+ Lemma = '_DATE_year'
	   xor CLexT = [Lex1|_],
	       domain(Lex1,['-','/',':','.',(',')]),
	       \+ Lemma = date[]
           xor domain(PrevLex,['-','/',':','.',(','),de,des,du,le,la,'d''','l''']), Status \== no
	   )
	->
	    %% start or continue a compound
	    (Status = no ->
%		 format('start cpd at ~w ~w\n',[TId,Lex]),
		 NewStatus = TId
	     ;
%	     format('continue cpd at ~w (from ~w) ~w\n',[TId,Status,Lex]),
	     NewStatus = Status
	    )
	; Status \== no,
	  TId > Status + 1
	  ->
	      %% ends a compound and register it
%	      format('stop cpd at ~w (from ~w) ~w\n',[TId,Status,Lex]),
	      EndTId is TId-1,
	      NewStatus = no,
	      (recorded(compound(Status,EndTId,_,_)) xor
		       Delta is EndTId - Status,
%	       format('register spmrl compound ~w ~w ~w\n',[Status,EndTId,Form]),
	       record(compound(Status,EndTId,dynamic,Delta))
	      )
	;
	%% outside a compound
	NewStatus = no
       ),
       NewTId is TId+1,
       spmrl_mark_compounds(NewTId,CLexT,NewStatus,N,Lex)
    )
.
	   

:-std_prolog spmrl_handle_terms/0.

spmrl_handle_terms :-
    every(( recorded(conll_lex(_Pos,_Lex)),
	    record_without_doublon(lex_conll(_Lex,_Pos))
	  )),
    every(( spmrl_handle_terms(1) )),
    true
.

:-std_prolog spmrl_handle_terms/1.

spmrl_handle_terms(Left) :-
    recorded('T'(Left,_)),
    (
	compound(Left,Right,Term,_),
        \+ ( compound(Left,_Right,_,_),
             _Right > Right
	   ),
	%		 format('spmrl handle term ~w ~w ~w\n',[Term,Left,Right]),
	mutable(ML,[],true),
	mutable(MMin,1000,true),
	every((
		     term_range(Left,Right,Range),
		     domain(_Lex,Range),
		     recorded(lex_conll(_Lex,_Pos)),
		     %			      format('spmrl potential comp ~w ~w ~w => ~w\n',[Term,Left,Right,_Pos]),
		     mutable_list_extend(ML,_Pos),
		     mutable_min(MMin,_Pos),
		     true
		 )),
	mutable_read(ML,PosL),
	mutable_read(MMin,MinPos),
       \+ (domain(_Pos,PosL),
	   recorded(spmrl_in_compound(_Pos))
	  ),
        %% we assume that the term is compatible with the current sequence of words
        %% only if there is at most one head for it (a single governor outside PosL)
        \+ ( domain(_Pos1,PosL),
	     conll_edge(_Pos1,_Head1,_,_,_,_),
             \+ domain(_Head1,PosL),
             domain(_Pos2,PosL),
	     conll_edge(_Pos2,_Head2,_,_,_,_),
            \+ domain(_Head2,PosL),
            \+ _Pos1 = _Pos2,
            \+ _Head1 = _Head2
	   ) ->
%		 format('spmrl term ~w L=~w min=~w\n',[Term,PosL,MinPos]),
	every((domain(Pos,PosL),
	       record(spmrl_in_compound(Pos)),
	       Pos \== MinPos,
	       record( spmrl_head_reroot(Pos,MinPos)),
	       record(spmrl_edge(Pos,MinPos,dep_cpd,'R_smprl',[],0)),
	       %			format('spmrl reroot0 <~w,~w,~w>\n',[Pos,MinPos,dep_cpd]),
	       recorded(K::conll_edge(Pos,Head,Label,Name,Reroot,EId),Addr),
               \+ domain(Head,PosL),
               \+ recorded(spmrl_edge(MinPos,_,_,_,_,_)),
	       record(spmrl_edge(MinPos,Head,Label,Name,Reroot,EId)),
	       %			format('spmrl reroot3 <~w,~w,~w> -> <~w,~w,~w>\n',[Pos,Head,Label,MinPos,Head,Label]),
	       true
	      )),
	XPos is Right + 1
     ;
     XPos is Left+1
    ),
    spmrl_handle_terms(XPos)
.

:-extensional conll_easy_cat/3.

conll_easy_cat(cat[nc,ncpred,title],'N','NC').
conll_easy_cat(np,'N','NPP').
conll_easy_cat(cat[v,aux],'V','V').
conll_easy_cat(adj,'A','ADJ').
conll_easy_cat(adv,'ADV','ADV').
conll_easy_cat(cat[det,number],'D','DET').
conll_easy_cat(cat[ponctw,poncts],'PONCT','PONCT').
conll_easy_cat(prep,'P','P').
conll_easy_cat(cat[csu,que],'C','CS').
conll_easy_cat(cat[coo],'C','CC').

/*
conll_simple_expansion(['l''',un],[det: 1, head]).
conll_simple_expansion(['les',autres],[det: 1, head]).
conll_simple_expansion(['mais',aussi],[head, mod: -1]).
conll_simple_expansion(['m�me',si],[obj:1,head]).
conll_simple_expansion(['comme',si],[head,obj: -1]).
conll_simple_expansion([�,terme],[head, obj: -1]).
conll_simple_expansion([pour,autant],[head, obj: -1]).
conll_simple_expansion([en,vigueur],[head, obj: -1]).
conll_simple_expansion([en,place],[head, obj: -1]).
conll_simple_expansion([depuis,longtemps],[head, obj: -1]).
conll_simple_expansion([de,plus],[head, obj: -1]).
conll_simple_expansion([de,moins],[head, obj: -1]).
conll_simple_expansion([de,loin],[head, obj: -1]).
conll_simple_expansion([au,total],[head, obj: -1]).
conll_simple_expansion(['afrique',du,'sud'],[head, dep: -1, obj: -1]).
*/

:-std_prolog conll_register_expansion/6.

conll_register_expansion(NId,LLex,Expansion,MPos,MRank,LCLex) :-
	( LLex = [_Lex|LLex2],
	  LCLex = [_CLex|LCLex2],
	  Expansion = [Exp|Expansion2] ->
	  mutable_read(MPos,_Pos),
	  mutable_read(MRank,_Rank),
	  _XPos is _Pos+1,
	  mutable(MPos,_XPos),
	  _XRank is _Rank + 1,
	  mutable(MRank,_XRank),
	  record( conll2node(_Pos,NId) ),
	  record( node2allconll(NId,_Pos) ),
	  record( conll_lex(_Pos,_Lex) ),
	  record( conll_rank(_Pos,_Rank) ),
	  conll_expansion_data(Exp,_CLex,NId,_Pos),
	  conll_register_expansion(NId,LLex2,Expansion2,MPos,MRank,LCLex2)
	;
	  true
	)
	.

:-std_prolog conll_expansion_data/4.

conll_expansion_data(Exp,Lex,NId,_Pos) :-
	( Exp = head ->
	      register_node2conll(NId,_Pos),
%	      record( node2conll_source(NId,_Pos)),
%	      record( node2conll_target(NId,_Pos)),
	      true
	; Exp = head_source ->
	  record( node2conll_source(NId,_Pos))
	; Exp = head_target ->
	  register_node2conll(NId,_Pos),
	  record( node2conll_target(NId,_Pos))
	; fail,
	  opt(spmrl),
	  Exp = (_Type : Delta) ->
	      Head is _Pos + Delta,
	      record( conll_expansion_edge(_Pos,dep_cpd,Head) )
	; Exp = (_Type : Delta) ->
	  Head is _Pos + Delta,
	  (opt(sequoia), _Type = obj, Delta < 0 -> Type = 'obj.p' ; Type = _Type),
	  record( conll_expansion_edge(_Pos,Type,Head) ),
	  true
	; Exp = [Exp1|Exp2] ->
	  conll_expansion_data(Exp1,Lex,NId,_Pos),
	  conll_expansion_data(Exp2,Lex,NId,_Pos)
	; Exp = [] ->
	  true
	; Exp = (Exp1 @ (_Cat,_FullCat)) ->
	  ( fail, _Cat = 'P', domain(Lex,[au,aux,des,du]) ->
		Cat = 'P+D', FullCat = 'P+D'
	   ; domain(Lex,[de]) ->
		 Cat = 'P', FullCat = 'P'
	  ;
	    Cat = _Cat, FullCat = _FullCat
	  ),
	  record( conll_cat(_Pos,Cat,FullCat) ),
	  conll_expansion_data(Exp1,Lex,NId,_Pos)
	;
	  fail
	)
	.
	
:-std_prolog xnode2conll_source/2.

xnode2conll_source(node{ id => NId, cluster => cluster{ id => CId, left => Left, right => Right, lex => _TId}}, Pos) :-
	( node2conll_source(NId,Pos) -> true
	; node{ id => _NId, cluster => cluster{ id => CId }},
	  node2conll_source(_NId,Pos) -> true
	; node{ id => _NId, cluster => cluster{ left => Left }},
	  node2conll_source(_NId,Pos) -> true
	; node{ id => _NId, cluster => cluster{ right => Right }},
	  node2conll_source(_NId,Pos) -> true
	; (_TId=[TId|_] xor _TId=TId),
	  recorded(conll_same_lex(_TId,node{ id => _NId },_Pos)),
	  node2conll_source(_NId,Pos) -> 
	      %	      format('pos ~w / ~w conll2target ~w\n',[Pos,_Pos,N]),
	      true
	; Pos = 0
	)
	.

:-std_prolog xnode2conll_target/2.

xnode2conll_target(N::node{ id => NId, cluster => cluster{ id => CId, left => Left, right => Right, lex => _TId}}, Pos) :-
	( node2conll_target(NId,Pos) -> true
	; node{ id => _NId, cluster => cluster{ id => CId }},
	  node2conll_target(_NId,Pos) -> true
	; node{ id => _NId, cluster => cluster{ left => Left }},
	  node2conll_target(_NId,Pos) -> true
	; node{ id => _NId, cluster => cluster{ right => Right }},
	  node2conll_target(_NId,Pos) -> true
	; (_TId=[TId|_] xor _TId=TId),
	  recorded(conll_same_lex(_TId,node{ id => _NId },_Pos)),
	  node2conll_target(_NId,Pos) -> 
%	      format('pos ~w / ~w conll2target ~w\n',[Pos,_Pos,N]),
	      true
	; Pos = 0,
%	  format('null conll2target ~w\n',[N]),
	  true
	)
	.

:-light_tabular conll_rep/2.
:-mode(conll_rep/2,+(+,-)).

conll_rep(NId,CId) :-
	( node2conll(NId,CId)
	xor recorded( conll_redirect(NId,CId) )
	)
	.

:-xcompiler
conll_rep_source(NId,CId) :-
	( node2conll_source(NId,CId) xor recorded(conll_redirect(NId,CId)) )
	.

:-xcompiler
node2conll_target(NId,Pos) :-
	recorded(node2conll_target(NId,Pos))
	xor node2conll(NId,Pos)
	xor recorded( conll_redirect(NId,Pos) )
	.

:-xcompiler
node2conll_source(NId,Pos) :-
	recorded(node2conll_source(NId,Pos))
	xor node2conll(NId,Pos)
	xor recorded( conll_redirect(NId,Pos) )
	.

:-xcompiler
register_node2conll(NId,Pos) :-
%	format('register node2conll nid=~w pos=~w\n',[NId,Pos]),
	record(node2conll(NId,Pos))
	.

:-xcompiler
register_conll_redirect(NId,Pos) :-
%	format('register conll_redirect nid=~w pos=~w\n',[NId,Pos]),
	record(conll_redirect(NId,Pos))
	.


:-std_prolog conll_collect_node_info/1.
:-extensional conll_node_info/6.

conll_collect_node_info(Pos) :-
	( conll2node(Pos,NId) ->
	    N::node{ id => NId,
		     cat => Cat,
		     lemma => Lemma,
		     form => Form,
		     cluster => cluster{ lex => _Lex, token => Token, left => Left, right => Right },
		     lemmaid => Lemmaid
		 },
	    (	node2op(NId,OId),
		op{ id => OId }
	    ->	true
	    ;	
		OId = '_'
	    ),
	    XPos is Pos + 1,
	    ( recorded(node2lexaddr(NId,LexAddr))  xor true),
	    ( recorded(node2top(NId,LexTop))
		      xor recorded('C'(Left,lemma{ cat => Cat, lemma => Lemma, top => LexTop},Right),LexAddr)
		      xor LexTop = none ),
%	    format('node lextop ~w => ~w\n',[N,LexTop]),
	  (	recorded( conll_lex(Pos,Lex) ), Compound = 1 xor Lex = _Lex, Compound = 0 ),
%	  format('try conll form and lemma pos=~w lex=~w token=~w\n',[Pos,Lex,Token]),
	  conll_form(Lex,CLex),
%	  format('=> clex=~w\n',[CLex]),
	  conll_lemma(Lemma,Token,CLemma),
%	  format('=> clemma=~w\n',[CLemma]),
	  ( domain(CLex,['.',',',';','!','?','(',')','[',']','!?','??','?!','...','"','''','-',':','_','/','(...)']) ->
	  CCat1 = 'PONCT',
	    FullCat1 = 'PONCT'
	  ; Compound = 1, recorded(conll_cat(Pos,CCat1,FullCat1)) ->
	    true
	  ; Compound = 1, Lemma = entities[],
	    domain(CLex,[de,'d'''])
	    ->
	    CCat1 = 'P',
	    FullCat1 = 'P'
	  ;
	    Compound = 1, Lemma = entities[],
	    domain(CLex,[le,la,'Le','La',les,'Les','l''','L'''])
	    ->
	    CCat1 = 'D',
	    FullCat1 = 'DET'
	  ; Lemma = '_META_TEXTUAL_GN' ->
	    CCat1 = 'N',
	    FullCat1 = 'NC'
	  ;
	    ( conll_cat(Cat,CCat1,Lemma,N,CLex) xor CCat1 = Cat ),
	    (	conll_fullcat(Cat,CCat1,OId,N,FullCat1,CLex) xor FullCat1 = CCat1)
	  ),
	  ( recorded(conll_forward(N,_)) ->
	    %% multitoken
	    conll_collect_cats(N,CCat1,FullCat1,CCat,FullCat)
	  ;	
	    CCat = CCat1,
	    FullCat = FullCat1
	  ),
	  mutable(MFeat,[],true),
	  ( node2op(NId,OId),
	    recorded(op{ id => OId, top => Top })
	  xor
	  recorded(node2top(NId,Top))
	  xor Top =[]
	  ),
	  every((
		 (Cat = cln -> FV = 's=suj'
		 ; Cat = cla -> FV = 's=obj'
		 ; Cat = clneg -> FV = 's=neg'
		 ; Cat = advneg -> FV = 's=neg'
		 ; Cat = que_restr -> FV = 's=excl'
		 ; Cat = clr -> FV = 's=refl'
		 ; Cat = prel -> FV = 's=rel'
		 ; FullCat = 'PRO', domain(Lemma,[rien,personne,aucun,nul]) -> FV= 's=neg'
		 ; FullCat = 'PRO', domain(Lemma,[chacun,tout,certain,un,autre,la_plupart,beaucoup,peu,'quelqu''un',quiconque,plusieurs,autrui,tel,untel]) -> FV= 's=ind'
		 ; FullCat = 'PRO', domain(Lemma,[celui,ce,cela]) -> FV= 's=dem'
		 ; FullCat = 'PRO', domain(CLemma,[le_sien]) -> FV = 's=poss'
		 ; FullCat = 'PRO', domain(CLemma,[lui_m�me]) -> FV = 's=refl'
		 ; Top = det{ dem => (+) } -> FV = 's=dem'
		 ; Top = det{ poss => (+) } -> FV = 's=poss' 
		 ; Top = det{ def => (+) } -> FV = 's=def'
		 ; FullCat = 'NC' -> FV = 's=c'
		 ; FullCat = 'NPP' -> FV = 's=p'
		 ; domain(FullCat,['ADJWH','ADVWH','PROWH']) -> FV = 's=int'
		 ; domain(Lemma,number[]) -> FV = 's=card'
		 ; FullCat = 'ADJ', domain(Lemma,[tout,autre,seul,mm�me,certain,quelque]) -> FV= 's=ind'
		 ; FullCat = 'ADJ' -> FV= 's=qual'
		 ; Cat = ponctw -> FV = 's=w'
		 ; Cat = poncts -> FV = 's=s'
		 ; FullCat = 'CC' -> FV= 's=c'
		 ; FullCat = 'CS' -> FV= 's=s'
		 ; FullCat = 'P+D' -> FV = 's=def'
		 ; Cat = 'PONCT' ->
		  ( 'C'(Left,lemma{ cat => poncts },Right) -> FV = 's=s' ; FV = 's=w' )
		 ; fail
		 ),
		 mutable_list_extend(MFeat,FV)
		)),
	  every((
%		 format('nid=~w fullcat=~w top=~w\n',[NId,FullCat,Top]),
		 Top \== [],
		 domain(F,[tense,person,number,mode,gender]),
%		 format('inlined_feature_arg call f=~w top=~w\n',[F,Top]),
		 full_feature_arg(Top,LexTop,F,_,V),
		 \+ (F=mode,FullCat='VPP'),
		 \+ (F=tense, FullCat \== 'VPP', (full_feature_arg(Top,LexTop,_Mode,_,conditional), _Mode = mode)),
%		 format('feature2conll call ~w ~w ~w\n',[F,V,FullCat]),
		 feature2conll(F,V,FV,FullCat),
		 mutable_list_extend(MFeat,FV)
		)),
	  mutable_read(MFeat,_MSTag),
	  conll_fvlist2fv(_MSTag,MSTag),
	  record( conll_node_info(Pos,CLex,CLemma,CCat,FullCat,MSTag) ),
	  conll_collect_node_info(XPos)
	;   
	    true
	)
.

:-extensional feature2conll/4.

:-light_tabular conll_fvlist2fv/2.
:-mode(conll_fvlist2fv/2,+(+,-)).

conll_fvlist2fv(L,MSTag) :-
	( L = [] -> MSTag = '_'
	; name_builder('~L',[['~w','|'],L],MSTag)
	).

feature2conll(gender,masc,'g=m',conll_fullcat[~ ['VPR','VINF','CC','CS']]).
feature2conll(gender,fem,'g=f',conll_fullcat[~ ['VPR','VINF','CC','CS']]).

feature2conll(number,pl,'n=p',conll_fullcat[~ ['VPR','VINF','CC','CS']]).
feature2conll(number,sg,'n=s',conll_fullcat[~ ['VPR','VINF','CC','CS']]).

feature2conll(person,1,'p=1',conll_fullcat[~ ['NC','NP','ADJ','VPR','VPP','VINF','CC','CS']]).
feature2conll(person,2,'p=2',conll_fullcat[~ ['NC','NP','ADJ','VPR','VPP','VINF','CC','CS']]).
feature2conll(person,3,'p=3',conll_fullcat[~ ['NC','NP','ADJ','VPR','VPP','VINF','CC','CS']]).

feature2conll(tense,future,'t=fut',conll_fullcat['V','VPR','VS','VIMP']).
feature2conll(tense,'future-perfect','t=fut',conll_fullcat['V','VPR','VS','VIMP']).
feature2conll(tense,present,'t=pst',conll_fullcat['V','VPR','VS','VIMP']).
feature2conll(tense,past,'t=past',conll_fullcat['V','VPR','VS','VIMP']).
feature2conll(tense,'past-historic','t=past',conll_fullcat['V','VPR','VS','VIMP']).
feature2conll(tense,'participle','t=past',conll_fullcat['V','VPR','VS','VIMP']).
feature2conll(tense,_,'t=past',conll_fullcat['VPP']).
feature2conll(tense,imperfect,'t=impft',conll_fullcat['V','VPP','VPR','VS','VIMP']).

feature2conll(mode,conditional,'t=cond',conll_fullcat['V','VPP','VPR','VS','VIMP']).

feature2conll(mode,indicative,'m=ind',conll_fullcat['V']).
feature2conll(mode,subjonctive,'m=subj',conll_fullcat['VS']).
feature2conll(mode,_,'m=inf',conll_fullcat['VINF']).
feature2conll(mode,_,'m=part',conll_fullcat['VPP','VPR']).
feature2conll(mode,_,'m=imp',conll_fullcat['VIMP']).

feature2conll(type,card,'s=card',_).
feature2conll(type,def,'s=def',_).
%feature2conll(type,dem,'s=card').
feature2conll(type,excl,'s=excl',_).
feature2conll(type,ind,'s=ind',_).
feature2conll(type,int,'s=int',_).
feature2conll(type,ord,'s=ord',_).
feature2conll(type,part,'s=part',_).
feature2conll(type,poss,'s=poss',_).
feature2conll(type,qual,'s=qual',_).
feature2conll(type,rel,'s=rel',_).

:-xcompiler
node2conllinfo(node{ id => NId},Lex,Lemma,Cat,FullCat,MSTag) :-
	conll_rep(NId,Pos),
	conll_node_info(Pos,Lex,Lemma,Cat,FullCat,MSTag)
	.

:-xcompiler
node2conllcat(N::node{},Cat) :-
	node2conllinfo(N,_,_,Cat,_,_)
	.

:-xcompiler
node2conllfullcat(N::node{},FullCat) :-
	node2conllinfo(N,_,_,_,FullCat,_)
	.

conll_emit(Pos) :-
%	format('conll emit1 pos=~w\n',[Pos]),
	( conll2node(Pos,NId) ->
	    conll_node_info(Pos,CLex,CLemma,CCat,FullCat,MSTag),
	    N::node{ id => NId, lemma => Lemma, cat=> Cat, lemma => Lemma, form => Form },
	    recorded( node2conll(NId,_Pos) ),
%	    format('\tpos=~w node=~w head=~w\n',[Pos,NId,_Pos]),
	    ( _Pos == Pos ->
%	      format('here1\n',[]),
	      ( conll_edge(Pos,Head,mirror,Name,_,EId) ->
		    %% for surviving mirror edges (because their head is a root)
		    %% we use a default mod label
		    Label = mod
	       ; (recorded(spmrl_edge(Pos,_Head,Label,Name,_,EId))
			  xor conll_edge(Pos,_Head,Label,Name,_,EId)) ->
%		     format('edge pos=~w head=~w label=~w\n',[Pos,_Head,Label]),
		     ( \+ opt(spmrl),
		       conll2node(_Head,_HNId),
		       recorded( conll_last_rank(_HNId,_LastHead) ),
		       _Head < _LastHead,
		       recorded(conll_lex(_LastHead,_LastLex)),
		       conll_form(_LastLex,_LastForm),
		       domain(_LastForm,[de,du,des,'d''','que','qu''']) ->
			   %% if the head correspond to a complex prep or complex csu, we take its last pos as the target head
			   Head = _LastHead
		      ; recorded(spmrl_head_reroot(_Head,Head)) ->
			    true
		      ;
		      Head = _Head
		     ),
%		     format('edge2 pos=~w head=~w label=~w\n',[Pos,Head,Label]),
		     true
	      ; 
		( recorded(mode(robust)) ->
		  true
		 ; ( recorded( conll_root(RootPos) )
		    ; recorded( conll_best_potential_root(RootPos) ), RootPos \== Pos
		   )
		   ->
		  ( edge{ target => N,
			  label => _Label,
			  type => __Type::edge_kind[~ virtual],
			  source => node{ cat => _SCat, lemma => _SLemma }
			},
		    _Type = __Type, _XLemma = Lemma, _XCat = Cat
		  xor
		  conll2node(RootPos,RootNId),
		    edge{ target => node{ id => RootNId, lemma => _XLemma, cat => _XCat },
			  label => _Label,
			  type => __Type::edge_kind[~ virtual],
			  source => node{ cat => _SCat, lemma => _SLemma }
			},
		    _Type = __Type
		  xor
		  _Label = none,
		    _SCat = none,
		    _SLemma = none,
		    _Type = none,
		    _XLemma = Lemma, _XCat = Cat
		  ),
		  Head = RootPos,
		  (FullCat = 'PONCT' -> Label = ponct ; Label = mod),
		  Name = 'R_modroot',
		  format('## *** multiple root ~w and ~w cat=~w lemma=~w uplabel=~w uptype=~w upcat=~w uplemma=~w\n',
			 [RootPos,Pos,_XCat,_XLemma,_Label,_Type,_SCat,_SLemma])
		;
		  record( conll_root(Pos) )
		),
		Head ?= 0,  Label ?= root, EId ?= -1,
		Name ?= 'R_root'  
	      )
	     ; %fail,
	       recorded(spmrl_edge(Pos,Head,Label,Name,_,EId)) ->
%		   format('here spmrl pos=~w head=~w label=~w\n',[Pos,Head,Label]),
		   true
	     ; 
%	     format('here\n',[]),
	     ( recorded( conll_expansion_edge(Pos,Label,_Head) ) ->
%		   format('here1 pos=~w label=~w head=~w\n',[Pos,Label,_Head]),
		true
	      ;
%	      format('here2 pos=~w _pos=~w\n',[Pos,_Pos]),
	      ( 
		( ( Lemma = '_NUMBER',
		    \+ chain( N >> subst @ det >> node{} )
		  ; domain(CLex,[le,la,les,de,du,des,'Le','La','Les','De','Du','Des','l''','L'''])),
		  recorded( conll_rank(Pos,1) )
		) ->
		_PreHead = _Pos, % use mod dependencies inside a splitted node (rough approx !)
		PreLabel = det,
		XCCat = 'D',
		XFullCat = 'DET'
	      ; CCat = 'P' ->
		_PreHead = _Pos, 
		PreLabel = dep
	      ; CCat = 'C' ->
		_PreHead = _Pos, 
		PreLabel = coord
	      ; Cat = np,
		chain( N << lexical @ 'Np2' << XN::node{ id => XNId } ),
		recorded(node2conll(XNId,_PreHead)) ->
		PreLabel = mod
	      ;
		_PreHead = _Pos, % use mod dependencies inside a splitted node (rough approx !)
		PreLabel = mod
	      ),

	      (opt(spmrl) ->
		   _Head = _Pos,
		   Label = dep_cpd
	       ;
	       _Head = _PreHead,
	       Label = PreLabel
	      )
	      
			
	     ),
%	     format('here3 pos=~w _head=~w label=~w\n',[Pos,_Head,Label]),
	      conll_edge_register(Pos,_Head,Label,'R_compounds',[],'_'),
	      ( conll_edge(Pos,Head,Label,Name,_,EId) xor
	      %% this case should not arise !
	      Head=0, EId = -1 )
	    ),
	    PHead = '_',
	    PLabel = '_',
	    XCCat ?= CCat,
	    XFullCat ?= FullCat,
	    (opt(spmrl), Label \== root -> 
		 conll_node_info(Head,HLex,HLemma,HCat,HFullCat,_),
		 (spmrl_rename(Label,XLabel,HLex,HCat) xor Label = XLabel)
	     ; opt(sequoia), Label \== root -> 
		   conll_node_info(Head,HLex,HLemma,HCat,HFullCat,_),
		   (sequoia_rename(Label,XLabel,HLex,HCat) xor Label = XLabel) 
	     ; 
	     XLabel=Label
	    ),
	  ( edge{ id => EId, label => FRMG_Label } xor FRMG_Label = ''),
	  %% emitting
	  format('~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w\t~w~w\t~w\n',
		 [Pos,CLex,CLemma,XCCat,XFullCat,MSTag,Head,XLabel,PHead,PLabel,Name,FRMG_Label,EId]),
	  XPos is Pos + 1,
	  conll_emit(XPos)
	;   
	true
	)
	.

:-extensional sequoia_rename/4.

sequoia_rename(aux_caus,'aux.caus',_,_).
sequoia_rename(aux_pass,'aux.pass',_,_).
sequoia_rename(aux_tps,'aux.tps',_,_).
sequoia_rename(dep_coord,'dep.coord',_,_).
sequoia_rename(p_obj,'p_obj.o',_,_).
sequoia_rename('p_obj.protect','p_obj',_,_).
sequoia_rename(mod_rel,'mod.rel',_,_).
sequoia_rename(_,'obj.cpl',que,_).
sequoia_rename(_,'obj.cpl','qu''',_).
sequoia_rename(obj,'obj.p',_,'P').

:-extensional spmrl_rename/4.

spmrl_rename(aux_caus,'aux.caus',_,_).
spmrl_rename(aux_pass,'aux.pass',_,_).
spmrl_rename(aux_tps,'aux.tps',_,_).
spmrl_rename(dep_coord,'dep.coord',_,_).
%spmrl_rename(p_obj,'p_obj.o',_,_).
%spmrl_rename('p_obj.protect','p_obj',_,_).
spmrl_rename(mod_rel,'mod.rel',_,_).
spmrl_rename(_,'obj.cpl',que,_).
spmrl_rename(_,'obj.cpl','qu''',_).
spmrl_rename(obj,'obj.p',_,'P').



:-light_tabular lex2conll_forms/2.
:-node(lex2conll_forms/2,+(+,-)).

lex2conll_forms(LTId,CLexL) :- lex2conll_forms_aux(LTId,CLexL).

:-std_prolog lex2conll_forms_aux/2.

lex2conll_forms_aux(LTId,CLexL) :-
    ( LTId = [TId|LTId2] ->
	  (TId = lex(CLex) ->
	       true
	   ;
	   'T'(TId,_CLex),
	   '$interface'(lowercase(_CLex:string),[return(CLex:string)])
	  ),
	  lex2conll_forms_aux(LTId2,CLexL2),
	  CLexL=[CLex|CLexL2]
     ;
     CLexL=[]
    )
.

:-light_tabular conll_form/2.
:-mode(conll_form/2,+(+,-)).

conll_form(TId,CLex) :-
	( TId = '' -> CLex = ''
	; TId = [_|_] ->
	  conll_form_aux(TId,CL),
	  name_builder('~L',[['~w','_'],CL],CLex)
	;
	  'T'(TId,CLex)
	)
	.

:-std_prolog conll_form_aux/2.

conll_form_aux(LTId,CL) :-
	( LTId = [TId|LTId2] -> 
	  conll_form_aux(LTId2,CL2),
	  'T'(TId,CSLex),
	  CL = [CSLex|CL2]
	;
	  CL = []
	).

:-std_prolog conll_lemma/3.

conll_lemma(Lemma,Form,CLemma) :-
    ( Lemma = cat[cln,ilimp,cla,clg,clr,clg,cll,cld] ->
	  rx!tokenize(Form,' ',L),
	  (L = [CLemma] xor name_builder('~L',[['~w','_'],L],CLemma))
	      % CLemma = Form
	; domain(Lemma,[ '_META_TEXTUAL_PONCT',
			 '_Uv',
			 '_NUMBER',
			 '_PERSON_m',
			 '_PERSON_f',
			 '_PERSON',
			 '_EPSILON',
			 '_DATE_artf',
			 '_DATE_arto',
			 '_LOCATION',
			 '_DATE_year',
			 '_COMPANY',
			 '_ORGANIZATION',
			 '_ETR',
			 '_PRODUCT',
			 '_ADRESSE',
			 '_ROMNUM',
			 '_NUM',
			 '_META_TEXTUAL_GN',
			 '_NP_WITH_INITIALS',
			 '_HEURE',
			 '_TEL'
		       ]) ->
	  rx!tokenize(Form,' ',L),
	  (L = [CLemma] xor name_builder('~L',[['~w','_'],L],CLemma))
	;
	  rx!tokenize(Lemma,' ',L),
	  (L = [CLemma] xor name_builder('~L',[['~w','_'],L],CLemma))
	)
	.


:-std_prolog conll_is_adj/2.

conll_is_adj(E::edge{ type => Type, source => N::node{} }, A) :-
	( chain(N >> subst @ label[quoted_N2,quoted_S,quoted_PP] >> A::node{}) -> true
	; Type = adj ->
	  A = N
	;   
	  node!empty(N),
	  _E::edge{ target => N },
	  conll_is_adj(_E,A)
	)
	.

:-std_prolog conll_ancestor/3.

conll_ancestor(N::node{ id => NId},A::node{ id => _NId },Head) :-
	%%	format('call conll ancestor ~w\n',[N]),
	( conll_rep(NId,Head) ->
	  A = N
	; edge{ target => N, source => _N, type => adj } ->
	  conll_ancestor(_N,A,Head)
	; edge{ source => N, target => A, type => subst },
	  conll_rep(_NId,Head) ->
	  A = _N
	; edge{ source => N, target => A, type => lexical },
	  conll_rep(_NId,Head) ->
	  A = _N
	;
	  fail
	)
	.

:-light_tabular conll_modal_climbing/2.
:-mode(conll_modal_climbing/2,+(+,-)).

conll_modal_climbing(V,Anchor) :-
	( chain( V::node{} >> (adj @ label['V',modal]) >> Modal::node{}) ->
	    conll_modal_climbing(Modal,Anchor)
	;   
	    V=Anchor
	)
	.

:-light_tabular conll_first_verb_climbing/2.
:-mode(conll_first_verb_climbing/2,+(+,-)).

conll_first_verb_climbing(V,Anchor) :-
	( chain( V::node{} >> (adj @ label['V','S',modal,'mod.xcomp']) >> Modal::node{}) ->
	    conll_first_verb_climbing(Modal,Anchor)
	;   
	    V=Anchor
	)
	.


:-light_tabular conll_aux2v/2.
:-mode(conll_aux2v/2,+(+,-)).

conll_aux2v(N::node{},V::node{ cat => v}) :-
	( edge{ target => N,
		source => _N::node{ cat => cat[v,aux] },
		label => label['Infl',aux],
		type => adj
	      } ->
	    conll_aux2v(_N,V)
	;   
	    N=V
	).
	      
:-light_tabular conll_intro_or_self/2.
:-mode(conll_intro_or_self/2,+(+,-)).

conll_intro_or_self(N::node{},Intro::node{}) :-
	( chain( N << subst << _N::node{ cat => cat[prep,csu] } ) ->
	    Intro = Prep
	;
	    Intro = N
	)
	.

:-light_tabular conll_main_verb/2.
:-mode(conll_main_verb/2,+(+,-)).

%% climb aux till main verb
conll_main_verb(N::node{},V::node{}) :-
	( chain( N << (adj @ label['Infl',aux]) << _N::node{ cat => cat[v,aux] } ) ->
	    conll_main_verb(_N,V)
	;   
	    V = N
	)
	.

:-features(conll!relation,[dep,head,type,name,reroot,eid]).

:-std_prolog conll_head/0.
       
conll_head :-
	%% find a relation
	( conll!relation{ type => Type, dep => _D, head => _H, name => Name, reroot => Reroot, eid => EId }
	; E::edge{ source => _H::node{}, target => _D::node{}, id => EId },
	  conll_simple_relation(E,Type,Name)
	),
	Reroot ?= [],
	conll_up_till_non_empty(_H,H::node{ id => HNId }),
	conll_down_till_non_empty(_D,D::node{ id => DNId}),
%	format('pre register name=~w _H=~w H=~w _D=~w D=~w\n',[Name,_H,H,_D,D]),
	conll_rep(DNId,DPos),
%	format('\tconll rep ~w => ~w\n',[DNId,DPos]),
	conll_rep_source(HNId,HPos),
%	format('\tconll rep ~w => ~w\n',[HNId,HPos]),
	DPos \== HPos,
	EId ?= -1,
	conll_edge_register(DPos,HPos,Type,Name,Reroot,EId)
	.

:-std_prolog conll_down_till_non_empty/2.

conll_down_till_non_empty(N::node{},M::node{}) :-
	( node!empty(N) ->
	    chain( N >> subst @ Label >> N2::node{} ),
	    Label \== start,
	    Label \== end,
	    conll_down_till_non_empty(N2,M)
	;   
	    M=N
	)
	.

:-light_tabular conll_up_till_non_empty/2.
:-mode(light_tabular/2,+(+,-)).

conll_up_till_non_empty(N::node{ id => NId },M::node{}) :-
	( node!empty(N) ->
	  ( chain( N << edge_kind[adj,subst] << N1::node{ id => NId1 } ),
	    NId \== NId1
	  ->
	    conll_up_till_non_empty(N1,M)
	  ; chain( N >> subst @ start >> node{} ) ->
	    chain( N >> subst @ Label >> A1::node{} ),
	    Label \== start,
	    Label \== end,
	    ( node!empty(A1) ->
		  chain( A1 >> subst >> A2::node{} ),
		  (node!empty(A2) -> chain(A2>>subst>>M) ; A2=M)
	    ;
	      A1 = M
	    )
	  ; chain(N >> adj @ 'S' >> _M::node{cat => aux, lemma => �tre}),
	    N = node{ cat => 'S' }
	    ->
		M=_M
	  ; chain( N >> subst >> M::node{} ),
	    \+ node!empty(M)
	  )
	;
	  M = N
	)
	.

:-xcompiler
conll_verbose(Msg,Args) :-
	( recorded( opt(conll_verbose) ) ->
	    format(Msg,Args)
	;
	    true
	)
.	

:-std_prolog conll_edge_register/6.
:-extensional conll_edge/6.

conll_edge_register(DPos,HPos,Type,Name,Reroot,EId) :-
	( recorded( Call::call_conll_edge_register(DPos,HPos,Type,Name,Reroot,EId) ) ->
	  format('## *** loop in edge register ~w ~w ~w ~w ~w ~w\n',[DPos,HPos,Type,Name,Reroot,EId]),
	  fail
	;
	  conll_verbose('## +++ edge register ~w ~w ~w ~w ~w ~w\n',[DPos,HPos,Type,Name,Reroot,EId]),
	  record( Call ),
	  true
	),
	( conll_edge(HPos,_HPos,_Type,_Name,_Reroot,_EId),
	  domain(redirect,_Reroot),
          \+ domain( dep_frozen, Reroot ) % NEW TO CHECK 2015/09/21
	 ->
	  %% TMP TAG+11 INFO
	  conll_verbose('## redirect 2 at pos=~w between (~w,~w,<~w>,~w) and *(~w,~w,<~w>,~w)\n',
			[DPos,HPos,Type,Name,Reroot,_HPos,_Type,_Name,_Reroot]),
	  conll_edge_register(DPos,_HPos,Type,Name,Reroot,EId)
	; domain(mirror,Reroot),
	  conll_edge(HPos,_HPos,_Type,_Name,_Reroot,_EId) ->
	  %% TMP TAG+11 INFO
	  conll_verbose('## mirror 1 at pos=~w between *(~w,~w,<~w>,~w) and (~w,~w,<~w>,~w)\n',
			[DPos,HPos,Type,Name,Reroot,_HPos,_Type,_Name,_Reroot]),
	  conll_edge_register(DPos,_HPos,_Type,_Name,_Reroot,_EId)
	; E::conll_edge(DPos,_HPos,_Type,_Name,_Reroot,_EId) ->
	  %% potential race between two governor
	  %% we try to solve the problem using some heuristic rules !!
	    conll_verbose('## *** potential head race at pos=~w between (~w,~w,<~w>,~w) and (~w,~w,<~w>,~w)\n',
			  [DPos,HPos,Type,Name,Reroot,_HPos,_Type,_Name,_Reroot]),
	  ( Type = _Type, Type = ponct ->
	      %% don't solve conflict for ponct-ponct races
	      true
	  ;   
	      recorded( conll_edge_shift(DPos,_HPos2,NewDPos,_HighPos) ),
	      conll_is_connected(_HPos2,_HPos),
	      \+ _HPos == _HighPos ->
	      %% explicit request for edge shift ! (eg: clefted construction)
	      conll_verbose('## +++ use shift info ~w ~w ~w\n',[DPos,_HPos2,NewDPos]),
	      erase(E),
	      conll_edge_register(NewDPos,_HPos,_Type,_Name,_Reroot,_EId),
	      erase(call_conll_edge_register(DPos,HPos,Type,Name,Reroot,EId)),
	      conll_edge_register(DPos,HPos,Type,Name,Reroot,EId)
	  ;
	      recorded( conll_edge_shift(DPos,HPos2,NewDPos,HighPos) ),
	      conll_is_connected(HPos2,HPos),
	      \+ HPos == HighPos ->
	      conll_verbose('## +++ use shift info ~w ~w ~w\n',[DPos,HPos2,NewDPos]),
	      conll_edge_register(NewDPos,HPos,Type,Name,Reroot,EId)
	  ;   ( domain(redirect,_Reroot), _HPos \== HPos
	      ;	  HPos < _HPos, _HPos < DPos
	      ;	  _HPos < DPos, DPos < HPos
	      ),
	      \+ domain( dep_frozen, Reroot ) ->
	      conll_edge_register(_HPos,HPos,Type,Name,Reroot,EId)
	  ;   ( domain(redirect,Reroot)
	      ;	  _HPos < HPos, HPos < DPos
	      ;	  HPos < DPos, DPos < _HPos
	      ),
	      \+ domain( dep_frozen, _Reroot) ->  
	      (	  \+ conll_is_connected(HPos,DPos)
	      xor
	      format('### *** acyclicity break for (~w,~w,<~w>)\n',[DPos,HPos,Type,Name]),
		  fail
	      ),
	      erase(E),
%	      record( conll_edge(DPos,HPos,Type,Name,Reroot,EId) ),
	      (_HPos = HPos ->
		   true
	       ;
	       conll_edge_register(HPos,_HPos,_Type,_Name,_Reroot,_EId)
	      ),
	      erase(call_conll_edge_register(DPos,HPos,Type,Name,Reroot,EId)),
	      conll_edge_register(DPos,HPos,Type,Name,Reroot,EId)
	  ; ( HPos == _HPos, _Type = Type ) -> true
	  ; (Type = mirror, HPos == _HPos) ->
		%% new edge is a mirror one: forget it
		true
	  ; (_Type = mirror, HPos == _HPos ) ->
		%% old edge is a mirror one: forget it
		erase(E),
		erase(call_conll_edge_register(DPos,HPos,Type,Name,Reroot,EId)),
		conll_edge_register(DPos,HPos,Type,Name,Reroot,EId)
	  ; (_HPos == HPos, Type = mod) ->
		%% more chance for 'mod' edges to be correct
		erase(E),
		erase(call_conll_edge_register(DPos,HPos,Type,Name,Reroot,EId)),
		conll_edge_register(DPos,HPos,Type,Name,Reroot,EId)
	  ;
	    %% true race condition
	    %% warn for tracing these cases and do nothing
	    format('## *** head race at pos=~w between (~w,~w,<~w>) and (~w,~w,<~w>)\n',
		   [DPos,HPos,Type,Name,_HPos,_Type,_Name]),
	    true
	  )
	; domain(up,Reroot),
	  conll_edge(HPos,HPos2,Type2,Name2,Reroot2,EId2) ->
	  %% try to move up when possible
	  %% TMP TAG+11 INFO
	  conll_verbose('## moveup 1 at pos=~w between *(~w,~w,<~w>,~w) and (~w,~w,<~w>,~w)\n',
			[DPos,HPos,Type,Name,Reroot,HPos2,Type2,Name2,Reroot2]),
	  conll_edge_register(DPos,HPos2,Type,Name,Reroot,EId)
	;
	  DPos \== HPos,
	    ( \+ conll_is_connected(HPos,DPos) -> true
	    %% cyclicity may arise from CONLL compound terms, that may play both head and dep roles for DEPXML
	    %% in that case, we favor the head role
	    ;	recorded(conll_compound(DPos)) ->
		%% keep old dep, don't add new one !
		fail
	    ;	E4::conll_edge(HPos,DPos,_,_,_,_), recorded(conll_compound(HPos)) ->
		%% erase the old one, add new one
		conll_verbose('## +++ erase ~w\n',[E4]),
		erase(E4)
	    ;	
		format('### *** acyclicity break for (~w,~w,<~w>)\n',[DPos,HPos,Type,Name]),
		fail
	    ),
	  record( conll_edge(DPos,HPos,Type,Name,Reroot,EId) ),
	  %% move up any waiting edge or if edge enforce redirection
	  every(( E1::conll_edge(_DPos,DPos,_Type,_Name,_Reroot,_EId),
		  \+ domain(head_frozen,_Reroot),
		  ( domain(up,_Reroot) ->
		    %% TMP TAG+11 INFO
		    conll_verbose('## moveup 2 at pos=~w between (~w,~w,<~w>,~w) and *(~w,~w,<~w>,~w)\n',
				  [DPos,HPos,Type,Name,Reroot,_DPos,_Type,_Name,_Reroot]),
		    true
		    
		   ; domain(redirect,Reroot),
		      \+ domain( dep_frozen, _Reroot ), % NEW TO CHECK 2015/09/21
		    %% TMP TAG+11 INFO
		    conll_verbose('## redirect 1 at pos=~w between *(~w,~w,<~w>,~w) and (~w,~w,<~w>,~w)\n',
				[DPos,HPos,Type,Name,Reroot,_DPos,_Type,_Name,_Reroot]),
		    true
		  ),
		  erase(E1),
		  every( erase( call_conll_edge_register(_DPos,HPos,_Type,_Name,_Reroot,_EId) ) ),
		  conll_edge_register(_DPos,HPos,_Type,_Name,_Reroot,_EId)
		)),
	  %% mirror for enum: the head of the first coord becomes the head of all coordinated
	  every(( E1::conll_edge(_DPos,DPos,_Type,_Name,_Reroot,_EId),
		  domain(mirror,_Reroot),
		  erase(E1),
		  %% TMP TAG+11 INFO
		  conll_verbose('## mirror 2 at pos=~w between (~w,~w,<~w>,~w) and *(~w,~w,<~w>,~w)\n',
				[DPos,HPos,Type,Name,Reroot,_DPos,_Type,_Name,_Reroot]),
		  every( erase( call_conll_edge_register(_DPos,HPos,_Type,_Name,_Reroot,_EId) ) ),
		  conll_edge_register(_DPos,HPos,Type,Name,Reroot,EId)
		))
	)
	.

:-std_prolog conll_is_connected/2.

conll_is_connected(Pos1,Pos2) :-
	( Pos1 == Pos2 ->
	  true
	; conll_edge(Pos1,Pos3,_,_,_,_),
	  conll_is_connected(Pos3,Pos2)
	)
	.

:-light_tabular conll_apply_reroot/4.
:-mode(conll_apply_reroot/4,+(+,+,+,-)).

:-rec_prolog
	conll!relation{},
	conll_simple_relation/3.

%% conll!relation{ dep => DepNode, head => HeadNode, type => Type }

%% obj      object (including sentential obj)
%% obj1     ? (error?)
%% mod      most modidiers
%% det      determiners
%% ponct    (almost) all punctuations [check coma as coord]
%% dep
%% suj     subject
%% root    entry point (no governor)
%% dep_coord  components of a coordination
%% coord    coordinations 
%% ats  attribut subject
%% ato  attribut object
%% mod_rel  relative sentenc
%% a_obj    preparg introduced by �
%% de_obj   preparg introduced by de
%% p_obj    other prepargs
%% aff
%% aux_tps     auxiliaries for past tense
%% aux_pass    auxiliaries for passive
%% aux_caus    auxiliaries for causative
%% arg
%% comp    PP modifiers on S
%% missinghead

conll!relation{ type => Type, dep => S, head => T , name => 'R_modal', eid => EId } :- % *** reroot
	chain( T::node{ cat => cat[~ coo], lemma => Lemma, id => NId } << (EId : adj @ label['V',modal]) << S::node{}),
	( Lemma = sembler -> Type = ats
	; Lemma = aller,	% aller +gerundive : aller s'amenuisant
	  node2live_ht(NId,HTId),
	  check_arg_feature(HTId,arg1,function,obl) ->
	  Type = mod
	;
	  Type = obj
	)
	.

conll!relation{ type => det, dep => T, head => Head, name => 'R_det', eid => EId } :-
    chain( S::node{ cat => SCat } >> (EId : _ @ label[det,det1,number]) >> T::node{} ),
    node2conllfullcat(T,conll_fullcat['DET','DETWH']),
    ( SCat = cat[adv] ->
	  chain( S << adj << Head::node{} )
     ;
     Head = S
    ),
    true
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_adj_range', eid => EId } :-
	%% eg: une feuille rouge � verte
	%% not clear what is the correct annotation for FTB
	%% we find:
	%%       - all det dep to noun (selected here)
	%%       - D1 det to governor of N, A: dep of D1, D2 obj of A or det of N
	%%       - use of arg deprel for A
	%% also seem different in cases such as "de X � Y N"
	chain( A1::node{ cat => adj }
	       >> ( (EIdP : lexical @ prep) >> P::node{ cat => prep, lemma => � }
		& (EId2 : lexical @ range) >> A2::node{ cat => adj }
		)
	     ),
	( %Type = mod, 
	    Type = arg,
	  Dep = P, Head = A1, EId = EIdP
	  ; (opt(sequoia) -> Type = 'obj.p' ; Type = obj), 
	    Dep = A2, Head = P, EId = EId2
	)
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_det_range', eid => EId } :-
	%% eg: 2 � 3 pommes
	%% not clear what is the correct annotation for FTB
	%% we find:
	%%       - all det dep to noun (selected here)
	%%       - D1 det to governor of N, A: dep of D1, D2 obj of A or det of N
	%%       - use of arg deprel for A
	%% also seem different in cases such as "de X � Y N"
	chain( N::node{}
	     >> (EId1 : subst @ det) >> D1::node{ cat => number }
	     >> ( (EIdA : lexical @ prep) >> A::node{ cat => prep, lemma => � }
		& (EId2 : lexical @ number2) >> D2::node{ cat => number }
		)
	     ),
	( chain( N << subst << P::node{ cat => prep } ) ->
	      ( Type = det, Dep = D1, Head = P, EId = EId1
	      ; Type = arg, Dep = A, Head = P, EId = EIdA
	      ; Type = det, Dep = D2, Head = N, EId = EId2
	      )
	  ;
	  ( Type = det, Dep = D1, Head = N, EId = EId1
	    ; Type = det, Dep = A, Head = N, EId = EIdA
	    ; Type = det, Dep = D2, Head = N, EId = EId2
	  )
	)
	.

%% predet de
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_predet_de', eid => EId } :-
	chain( N::node{} >> (EId : subst @ det) >> PDet::node{ cat => cat[predet,nc], lemma => Lemma,
							       cluster => cluster{ left => Left, right => Right}
							     } >> lexical @ de >> De::node{} ),
	( 'C'(Left,lemma{ cat => predet, top => predet{ predet_kind => cat[adv,advneg] }},Right) ->
	  ( Type = mod, Dep = PDet, Head = N
	  ; Type = det, Dep = De, Head = N
	  )
	;
	  ( Type = dep, Dep = De, Head = PDet
	   ;   Dep = N, Head = De,
	       (opt(sequoia) -> Type = 'obj.p' ; Type = obj)
	  )
	)
	.

%% adv quantity modifier (pr�s de la moiti� de X)
%% RenameFix
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_adv_qmod', eid => EId } :-
	chain( N::node{}  >> adj @ 'N2' >> Adv::node{ cat => adv, tree => Tree } >> (EId : lexical @ prep) >> Prep::node{} ),
	domain(quantity_adv_N2,Tree),
	( Type = dep, Dep = Prep, Head = Adv
	; Type = obj, Dep = N, Head = Prep
	)
	.

%% partitive
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_partitive', eid => EId } :-
	chain( Pro::node{ cat => cat[pro,nc,pri] } >> (Eid : lexical @ prep) >> De::node{ cat => prep, lemma => de } ),
	Type = obj,
	Head = De,
	Dep = Pro
	.

%% predet2 'beaucoup de nos '
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_predet2', eid => EId } :-
	chain( Predet::node{ cat => cat[predet,nc] }
	     << (EId : adj @ det) << Det::node{ cat => cat[det,number] }
	     << subst @ det << N::node{} ),
	chain( Predet >> lexical @ de >> De::node{ cat => prep } ),
	( chain( Predet >> lexical @ entre >> Entre::node{ cat => prep }) ->
	  ( Type = dep, Dep = De, Head = Predet
	  ;  Type = obj, Dep = Entre, Head = De
	  ; (opt(sequoia) -> Type = 'obj.p' ; Type = obj),
	    Dep = N, Head = Entre
	  )
	;
	  ( Type = dep, Dep = De, Head = Predet
	   ; (opt(sequoia) -> Type = 'obj.p' ; Type = obj),
	     Dep = N, Head = De
	  )
	)
	.

%% predet2 'beaucoup de nos '
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_predet3', eid => EId } :-
	chain( Predet::node{ cat => cat[predet,nc] }
	     << (EId : adj @ pro) << N::node{ cat => cat[pro] }
	     ),
	chain( Predet >> lexical @ de >> De::node{ cat => prep } ),
	( chain( Predet >> lexical @ entre >> Entre::node{ cat => prep }) ->
	  ( Type = dep, Dep = De, Head = Predet
	   ;  (opt(sequoia) -> Type = 'obj.p' ; Type = obj),
	      Dep = Entre, Head = De
	   ; Type = obj, Dep = N, Head = Entre
	  )
	;
	  ( Type = dep, Dep = De, Head = Predet
	   ; (opt(sequoia) -> Type = 'obj.p' ; Type = obj),
	     Dep = N, Head = De
	  )
	)
	.

conll!relation{ type => suj, dep => T, head => V, name => 'R_subject', eid => EId } :-
	edge{ source => _V::node{},
	      label => Label::label[subject,impsubj],
	      target => T::node{},
	      id => EId
	    },
	%% deep causative subj are not used by CONLL
	\+ chain( _V >> lexical @ causative_prep >> node{}),
	\+ ( chain( _V << subst @ xcomp << K::node{ id => KId, lemma => faire } ),
	       node2live_ht(KId,HTId),
	       check_arg_feature(HTId,arg1,kind,vcompcaus)
	   ),
	%% no deep subject (in concurrence with impsub)
	( Label = impsubj xor \+ chain( _V >> (_ @ impsubj) >> node{})),
	%% climb to non aux verb
	conll_aux2v(_V,V1::node{ cat => Cat1 }),
	%% adjectives may not be subj head
	( Cat1 = adj -> chain( V1 >> adj @ label['Infl',aux] >> V2::node{ cat => v}) ; V1=V2 ),
	%% use first modal verb if any
	conll_modal_climbing(V2,V)
	.

%% deep causative subj are not used by CONLL
%% => prep as mod of verb and deep subj obj of prep
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_causative' , eid => EId} :-
	( chain( Faire::node{ cat => v, lemma => faire, id => NId }
	       >> subst @ xcomp
	       >> V::node{ cat => v }
	       >> (EIdDeep : subst @ subject) >> DeepSubj::node{}
	       ),
	  node2live_ht(NId,HTId),
	  check_arg_feature(HTId,arg1,kind,vcompcaus),
	  true
	;
	  chain( Faire
	       << adj @ label['S','mod.xcomp']
	       << V::node{ cat => v }
	       >> (EIdDeep : subst @ subject) >> DeepSubj::node{}
	       )
	),
	(   chain( V >> (EIdPrep : lexical @ causative_prep) >>  P::node{ cat => prep }) ->
	    (	Type = mod, Dep = P, Head = V, EId = EIdPrep
	    ;	Type = obj, Dep = DeepSubj, Head = P, EId = EIdDeep
	    )
	;
	    Type = obj, Dep =DeepSubj, Head = V, EId = EIdDeep
	)
	.

%% adj with impsubj become ats and its deep subj become an obj of verb
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_adj_head', eid => EId } :-
	chain( Adj::node{ cat => cat[adj]} >> (EId : adj @ label['Infl',aux]) >> V::node{}),
	( Type = ats, Dep = Adj, Head = V
	;   ( chain( Adj >> lexical @ impsubj >> S1::node{} ) ->
		(   
		    chain( Adj >> subst @ subject >> S2::node{}),
		    Type = obj, Dep = S2, Head = V
		;   conll_modal_climbing(V,V1),
		    Type = suj, Dep = S1, Head = V1
		)
	    ;	chain( Adj >> _ @ subject >> S1::node{}),
		conll_modal_climbing(V,V1),
		Type = suj, Dep = S1, Head = V1
	    )
	)
	.

%% sentence with adj as head and a deep subj but no verb
conll!relation{ type => dep, dep => Subj, head => Adj, name => 'R_adj_head_short' , eid => EId} :-
	chain( Adj::node{ cat => adj } >> (EId : subst @ subject) >> Subj::node{} ),
	\+ chain( Adj >> adj @ label['Infl',aux] >> node{} )
	.

%% v with impsubj and deep subj
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_v_impsubj', eid => EId } :-
	chain( S1::node{}
	     << (EId : lexical @ impsubj) << V::node{ cat => cat[v]}
	     >> subst @ subject >> S2::node{ cat =>  S2Cat } ),
	( Type = obj, Dep = S2, Head = V
	; conll_modal_climbing(V,V1),
	  Type = suj, Dep = S1, Head = V1
	; % the deep subject may be a CS
	  S2Cat = csu,
	  chain( S2 >> subst @ 'SubS' >> S3 :: node{} ),
	  conll_modal_climbing(S3,V3),
	  (opt(sequoia) -> Type = 'obj.cpl' ; Type = obj), 
	  Dep = V3, Head = S2
	)
	.

%% object
conll!relation{ type => obj, dep => Dep, head => Head, name => 'R_object', eid => EId} :-
	edge{ label => object, target => T::node{ cat => TCat } , source => _Head::node{ id => NId }, id => EId },
%	node2live_ht(NId,HTId),
%	\+ ( check_arg_allfeatures(HTId,args[arg1,arg2],arg{ function => obj, extracted => cleft })
%	   ),
	( node!empty(T) ->
	  Head = _Head,
	  %% il veut de quoi manger
	  ( chain( T >> lexical @ prep >> De::node{ cat => prep, lemma => de } ) ->
	    Dep = De
	  ;
	    Dep = T
	  )
	; TCat = cat[prel], conll_first_verb_climbing(_Head,Head) ->
	  Dep = T
	;
	  Dep = T,
	  Head = _Head
	).

conll_simple_relation(edge{ label => label[clr],
			    source => V::node{ id => NId, lemma => Lemma }
			  },L,'R_CLR') :-
	node2live_ht(NId,HTId),
	( 
	  check_ht_feature(HTId,refl,(+)) ->
	    L = aff
	;   conll_se_moyen(Lemma) ->
	    L = aff
	;   chain( V >> (lexical @ impsubj) >> node{} ) ->
	    L = aff
	;  check_xarg_feature(HTId,args[arg1,arg2],Fun::function[],_,clr) ->
	  (	
		Fun = function[obj],
		conll_se_obj(Lemma) ->
		L=obj
	  ;     Fun = function[att],
		conll_se_obj(Lemma) ->
		L = obj
	  ;	Fun = function[obj�],
		conll_se_aobj(Lemma) ->
		L = a_obj
	  ;
		L=aff
	  )
	;
	  L = aff
	)
	.

conll_simple_relation(edge{ label => label[clg],
			    source => node{ id => NId }
			  },L,'R_CLG') :-
	node2live_ht(NId,HTId),
	(   check_xarg_feature(HTId,args[arg1,arg2],Fun::function[],_,clg) ->
	    (	
		Fun=function[obj,att] ->
		L = obj
	    ;	Fun=function[objde] ->
		L = de_obj
	    ;	Fun=function[loc,dloc,obl,obl2]->
		    ( opt(sequoia), \+ opt(spmrl)
		     ->
			 (Fun=function[loc] ->
			      L = 'p_obj.loc'
			  ; Fun = function[dloc] ->
				L = 'p_obj.dloc'
			  ; Fun = function[obl2],
			    check_ht_feature(HTId,diathesis,passive) ->
				L = 'p_obj.agt'
			  ;
			  L= 'p_obj.o'
			 )
		     ;
		     L=p_obj
		    )
	    ;	
		L = aff
	    )
	;
	    L = aff
	)
	.

conll_simple_relation(edge{ label => label[cll], source => node{ lemma => SLemma } },Type,'R_CLL') :-
	Type = aff
	.

%% RenameFix
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_coord', eid => EId } :-
	%% reused from Passage conversion
	E::edge{ type => adj,
		 id => EId,
		 target => LastCoord::node{ cat => coo },
		 label => StartLabel,
		 source => Start::node{}
	       },
	\+ chain( Start << subst @ det << node{} ),
	( StartLabel = xcomp ->
	  ( edge{ source => Start,
		  target => XStart,
		  label => label[csu,prep],
		  type => lexical
		}
	  xor 
	  edge{ source => Start,
		target => XStart,
		label => xcomp,
		type => edge_kind[~ [adj]]
	      }
	  )
	 ; StartLabel = supermod ->
	       edge{ source => Start,
		     target => XStart,
		     label => que,
		     type => lexical
		   }
	; node!empty(Start), chain( Start >> lexical @ en >> XStart::node{}) ->
	  true
	; node!empty(Start), chain( Start >> subst @ start >> node{} ) ->
	  conll_down_till_non_empty(Start,XStart)
	;
	  XStart = Start
	),
	( _N2 = Start
	; edge{ source => LastCoord, target => _N2, label => coord2 }
	),
	coord_next(LastCoord,_N2,Coord),
	coord_next(LastCoord,Coord,_N3),
	\+ node!empty(Coord),
	( node!empty(_N2) ->
	  ( edge{ source => _N2,
		  target => N2,
		  type => edge_kind[subst,lexical]
		},
	    N2=node{ cat => cat[v,prep,nc,np] }
	  xor _N2 = Start, N2=XStart
	  )
	; _N2 = Start ->
	  N2 = XStart
	;
	  N2 = _N2
	),
	( node!first_main_verb(N2,XN2)
	xor get_head(N2,XN2)
	),
	( node!empty(_N3) ->
	  edge{ source => _N3,
		target => N3,
		type => edge_kind[subst,lexical]
	      },
	  N3=node{ cat => cat[v,prep,nc,np] }
	;   
	  N3 = _N3
	),
	get_head(N3,XN3),
	( Type = coord, Dep = Coord, Head = XStart
	;
	  XN3 = node{ cluster => cluster{ left => XN3_Left } },
	  ( chain( Coord >> lexical @ en >> En::node{ cluster => cluster{ right => En_Right } } ),
	    En_Right =< XN3_Left,
	    \+ ( chain( Coord::node{} >> subst @  label[coord2,coord3]
		      >> N4::node{ cluster => cluster{ left => N4_Left, right => N4_Right}}
		      ),
		 En_Right =< N4_Left,
		 N4_Right =< XN3_Left
	       )
	  ) ->
	  ( Type = dep_coord, Dep = En, Head = Coord
	  ; Type = obj, Dep = XN3, Head = En
	  )
	;
	  Type = dep_coord, Head = Coord,
	  ( XN3 = node{ cat => v, cluster => cluster{ left => XN3_Left, right => XN3_Left }} ->
	    %% ellipsis on verb
	    edge{ source => XN3, target => N4 },
	    get_head(N4,XN4),
	    Dep = XN4
	  ;
	    Dep = XN3
	  )
	),
	true
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_coord_det', eid => EId } :-
	E::edge{ type => adj,
		 id => EId,
		 target => LastCoord::node{ cat => coo },
		 label => StartLabel,
		 source => Start::node{}
	       },
	chain(Start << subst @ det << Noun::node{}),
	( N2 = Start
	; edge{ source => LastCoord, target => N2, label => coord2 }
	),
	coord_next(LastCoord,N2,Coord),
	coord_next(LastCoord,Coord,N3),
	\+ node!empty(Coord),
	( Type = coord, Dep = Coord, Head = Noun
	; Type = det, Dep = N3, Head = Coord
	)
	.

%% coma before last coord is a ponct
conll_simple_relation( edge{ type => lexical,
			     source => node{ cat => coo, cluster => cluster{ left => L} },
			     target => node{ lemma => ',', cluster => cluster{ right => L} }
			   },
		       ponct, 'R_coma_before_coo'
		     ).

%% coord on xcomp arg need rerooting to prep and csu
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_xcomp_intro_in_coord', eid => EId } :-
	edge{ source => COO::node{ cat => coo },
	      type => lexical,
	      label => L::label[prep,csu],
	      target => Head::node{ cluster => cluster{ right => Right } }
	    },
	(opt(sequoia) ->
	     ( L = csu -> Type = 'obj.cpl'
	       ; L = prep -> Type = 'obj.p'
	       ; Type = obj
	     )
	 ; Type = obj
	),
	edge{  source => COO,
	       id => EId,
	       type => subst,
	       label => xcomp,
	       target => Dep::node{ cluster => cluster{ left => Left } }
	    },
	\+ ( edge{ source => COO,
		   target => node{ cluster => cluster{ left => _Left, right => _Right } }
		 },
	     Right =< _Left, _Right =< Left
	   )
	.
	
%% pb with head: need to understand the rules
conll_simple_relation( edge{ label => ni, type => lexical },coord, 'R_ni'). 

%% X ou non
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_ou_non', eid => EId } :-
	chain( N::node{} >> adj >> node{}
	     >> ( (EIdOu : lexical @ coo) >> Ou::node{ cat => coo }
		& (EIdNon : lexical @ advneg) >> Non::node{}
		)
	     ),
	( Type = coord, Dep = Ou, Head = N, EId = EIdOu
	; Type = dep_coord, Dep = Non, Head = Ou, EId = EIdNon
	)
	.

%% a starter coord become the head of a sentence
%% reroot
conll!relation{ type => dep_coord, head => COO, dep => V, name => 'R_starter_coord', eid => EId } :-
	E::edge{ label => starter,
		 type => lexical,
		 id => EId,
		 target => COO::node{ cat  => coo}
	       },
	conll_is_adj(E,V)
	.

conll!relation{ type => mod, head => N, dep => V, name => 'R_starter_N2' , eid => EId} :-
	E::edge{ label => starter,
		 type => subst,
		 id => EId,
		 target => N::node{ cat  => cat[nc]}
	       },
	conll_is_adj(E,V)
	.

conll!relation{ type => dep_coord, head => COO, dep => N, name => 'R_short_starter_coord', eid => EId } :-
	edge{ type => lexical,
	      label => coo,
	      id => _EId,
	      source => _N::node{},
	      target => COO::node{ cat => coo }
	    },
	chain( _N >> subst @ start >> node{ cat => start } ),
	( chain( _N >> (EId : lexical @ pri) >> N::node{} ) ->
	  true
	; node!empty(_N) ->
	  chain( _N >> (EId : subst @ Label) >> N::node{} ),
	  \+ Label = start
	;   
	  N = _N, EId = _EId
	)
	.

conll!relation{ type => obj, head => COO, dep => N, name => 'R_short_starter_pourquoi', eid => EId } :-
	edge{ type => lexical,
	      label => pri,
	      id => _EId,
	      source => _N::node{ },
	      target => COO::node{ cat => pri, lemma => 'pourquoi?' }
	    },
	chain( _N >> subst @ start >> node{ cat => start } ),
	( node!empty(_N) ->
	    chain( _N >> (EId : subst @ Label) >> N::node{} ),
	    \+ Label = start
	;   
	    N = _N, EId = _EId
	)
	.


conll!relation{ type => mirror, dep => Dep, head => Head, name => 'R_enum_mirror', reroot => [mirror], eid => EId } :-
	chain( Head::node{} >> adj >> node{} >> (EId : edge_kind[subst,lexical] @ coord) >> Dep::node{} )
	.

conll!relation{ type => L, dep => Dep, head => Head, name => 'R_att' , eid => EId } :-
	edge{ label => comp,
	      id => EId,
	      type => edge_kind[subst,lexical],
	      source => V::node{ cat => v, id => NId},
	      target => _Dep::node{ cat => TCat }
	    },
	node2live_ht(NId,HTId),
	( check_ht_feature(HTId,ctrsubj,suj) ->
	    L = ats
	; chain(V >> lexical @ clr >> node{}) ->
	  %% control on a reflexive object equiv to subject !
	  %% eg.: il se trouve joli
	  L = ats
	;
	  L = ato
	),
	( node!empty(_Dep) ->
	  chain( _Dep >> subst >> Dep::node{} ),
	  Head = V
	; TCat = cat[prel] ->
	  conll_first_verb_climbing(V,Head),
	  Dep = _Dep
	;
	  Dep = _Dep,
	  Head = V
	)
	.

conll_simple_relation( edge{ type => edge_kind[subst,lexical],
			     label => Label,
			     source => node{ cat => prep } },
		       Type, 'R_prep_obj' ) :-
	Label \== skip,
	(opt(sequoia) -> Type = 'obj.p' ; Type = obj )
	.

%% RenameFix
conll!relation{ type => Type, dep => T, head => V, name => 'R_adv_mod', eid => EId } :-
	E::edge{ type => edge_kind[adj],
		 label => Label,
		 id => EId,
		 source => _V::node{ cat => SCat, lemma => SLemma, cluster => cluster{ left => SLeft }},
		 target => T::node{ lemma => TLemma, cat => TCat::cat[adv,advneg,que_restr,adj,adjPref,advPref], 
				    cluster => cluster{ left => TLeft } 
				  }
	       },
	\+ domain(Label,['S','S2',vmod,det,supermod]),
	(   Label = adj,
	    TCat = cat[adv,advneg],
	    SCat = 'N2',
	    chain( _V >> subst @ 'SubS' >> V::node{})
	->  
	    true
	;   Label = 'V1', TCat=advneg, SCat = cat[v,aux], chain( _V << adj @ label['Infl',aux] << node{ cat => adj }) ->
	    V = _V
	;  % fail,
	   domain(TLemma,[number['_NUMBER'],tout,chacun,quelque]),
	   TCat = cat[adj],
	   TLeft < SLeft,
           (\+ chain( _V >> subst @ det >> node{ cat => det } ) xor SLemma = 'pourcent' )
	   ->
	   Type = det,
	   V=_V
	;
	    conll_main_verb(_V,V)
	),
	Type ?= mod
	.

conll_simple_relation( edge{ type => lexical, label => advneg, source => node{ cat => cat[~ coo]} },mod,'R_ante_advneg').

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_quantity_on_adv', eid => EId } :-
	%% eg: un an plus tard
	%% FTB6 not clear on coherent annotation !
	chain( Quant::node{ cat => nominal[] }
	     << (EId : adj @ quantity) << Adv::node{ cat => adv }
	     << adj << H::node{}
	     ),
	Type = dep,
	Dep = Quant,
	Head = H
	.

conll!relation{ type => mod, dep => Dep, head => Head, name => 'R_adj_on_det', eid => EId } :-
	chain( Dep::node{ cat => adj } << (EId : adj @ det) << node{ cat => det } << subst << Head::node{} )
	.

conll_simple_relation(edge{ label => 'Np2', target => node{ cat => cat[~ coo]}},mod, 'R_Np2').
conll_simple_relation(edge{ label => 'N2',  type => adj, target => node{ cat => cat[np] }},mod, 'R_np').
conll_simple_relation(edge{ label => 'N',  type => adj, target => node{ cat => cat[nc] }},mod, 'R_nc_seq').
conll_simple_relation(edge{ label => 'adjP',
			    type => adj,
			    target => node{ cat => cat[~ [prep,coo]] }},mod,'R_adjP').

conll_simple_relation(edge{ label => label[advneg,mod],
			    type => edge_kind[adj,lexical],
			    target => node{ cat => que_restr} },mod,'R_advneg_que').

conll_simple_relation(edge{ label => nc, type => adj, target => node{ cat => cat[~ coo]}},mod,'R_nc_mod').
conll_simple_relation(edge{ label => clneg, source => node{ cat => cat[v,aux] }},mod,'R_clneg').

conll_simple_relation(edge{ label => 'N2', type => adj, target => node{ cat => xpro }},dep,'R_xpro').

conll_simple_relation(edge{ label => label[mod,'mod.quantity'], type => adj },mod,'R_mod').

conll!relation{ type => mod, dep => CLNEG, head => V, name => 'R_clneg_on_adj', eid => EId } :-
	chain( CLNEG::node{ cat => clneg }
	     << (EId : lexical @ clneg)
	     << node{ cat => adj }
	     >> adj @ label['Infl',aux] >> V::node{ cat => v }
	     ).

/*
conll!relation{ type => ponct, dep => T, head => A, name => 'R_incise' } :-
	E::edge{ source => node{ cat => incise }, target => T::node{}},
	node2conllfullcat(T,'PONCT'),
	conll_is_adj(E,A).
*/

%% follow internal reroot for punctuation
conll!relation{ type => ponct, dep => T, head => A, name => 'R_ponct', reroot => Reroot, eid => EId } :-
	E::edge{ source => S::node{ cat => SCat, cluster => cluster{ left => Left }, tree => Tree },
		 id => EId,
		 target => T::node{ cluster => cluster{ right => Right, token => Token }}
	       },
	node2conllfullcat(T,'PONCT'),
\+ (SCat == coo, Right < Left ),
	( \+ node!empty(S) ->
	    A=S
	; domain(K,quoted[]),
	  domain(K,Tree) ->
	    (conll_down_till_non_empty(S,A) xor chain( S << adj << A::node{}) )
	;   SCat = incise, chain(S << adj << I::node{}) ->
		( domain(Token,[',']),
		  chain(I >> subst @ 'S_incise' >> _A::node{}) ->
		      conll_modal_climbing(_A,A)
		 ; fail,
                   \+ node!empty(I),
		   domain(Token,['(',')']) ->
		       A = I
		 ; chain(I << adj << _A::node{}) ->
		       conll_modal_climbing(_A,A)
		 ; A = I
		),
		true
	;   
	A  = S,
	Reroot = [up]
	),
	true
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_PP_mod', eid => EId, reroot => Reroot } :-
	E::edge{ label => L::label['N2','adjP', 'PP', pres,dep],
		 source => N::node{ id => NId, cat => NCat, cluster => cluster{ right => NRight }},
		 id => EId,
		 target => T::node{ id => TNId, cat => prep, lemma => TLemma, cluster => cluster{ right => TRight } } },
	conll_is_adj(E,_A),
	(conll_down_till_non_empty(_A,A) xor _A = A),
	node2conllfullcat(T,FullCat),
	\+ chain( N >> (lexical @ 'CleftQue') >> node{}),
	%% conll_fullcat['P','P+D']
	( FullCat = 'CC' ->
	  fail
	; A = node{ cat => coo } ->
	  Type = dep_coord,
	  Head = A,
	  Dep = T
	; chain(N << (subst @ time_mod) << _I::node{} << adj << _V::node{ cluster => cluster{ left => _VLeft}}),
	  chain(_I >> adj @ incise >> node{})
	  ->
	      (TRight =< _VLeft -> conll_modal_climbing(_V,Head) ; Head = _V),
	      Dep = T,
	      Type = mod
	; FullCat = 'ADV' ->
	  Type = mod,
	  ( A = node{ cat => v, cluster => cluster{ left => ALeft} }, TRight =< ALeft ->
	    conll_modal_climbing(A,Head)
	  ;
	    Head = A
	  ),
	  Dep = T
	; opt(spmrl),
	  NCat = adj ->
	      Dep = T,
	      Head = A,
	      node2live_ht(NId,HTId),
	      ( TLemma = '�',
		check_xarg_feature(HTId,args[arg1],obj�,_,_)
	       -> Type = a_obj
	       ; TLemma = 'de',
		 check_xarg_feature(HTId,args[arg1],objde,_,_)
		 -> Type = de_obj
	       ; Type = mod
	      )
	; L = label['PP',dep], 
	  NCat = prep,
	  TRight < NRight ->
	      %% range construction
	      Type = arg, Head = T, Dep = N, Reroot = []
	; L = label['PP'],
	  NCat = vmodprep ->
	      %% PP as vmod-like modifier on prep (avec, selon moi, aucune raison)
	      Type = arg, Head = A, Dep = T
	; L = label['PP',dep] ->
	      Dep = T,
	  ( A = node{ cat => v, cluster => cluster{ left => ALeft} }, TRight =< ALeft ->
	    %% when the PP comes before the verb, we have a mod, and it attach to the first tensed verb
%	    conll_first_verb_climbing(A,Head),
	    conll_modal_climbing(A,Head),
	    Type = mod
	  ;
	    TLemma = de,
	      A = node{ cat => v, lemma => Lemma },
	      domain(Lemma,[augmenter,r�duire,doter,targuer,contraindre,ralentir,
			    cro�tre,progresser,chuter,manquer,retarder,d�border,
			    convenir,�lever,amputer,soucier,accompagner,b�n�ficier,
			    disposer,offusquer,menacer,priver,disposer,perdre,
			    rivaliser,inqui�ter,marquer,accoucher,�vincer,reculer,passer,tirer,
			    traiter,�quiper,diminuer,baisser,d�passer,d�missionner,
			    importer,accro�tre,d�valuer,obliger,contenter,rallonger,
			    t�moigner,�carter,qualifier,occuper,accommoder,s�parer,sortir,
			    �tonner,conna�tre,inculper,rapprocher,d�faire,appr�cier,composer,servir,provenir,
			    majorer,grimper,sur�valuer,dater,souffrir,revaloriser,prot�ger,pr�c�der,obtenir,
			    inspirer,gargariser,enorgueillir,�loigner,exiger,dispenser,discuter,d�tourner,d�tacher,
			    attendre,assortir,d�pendre,venir,revenir,aller,agir,r�duire,ramener,charger,
			    relever,parler,profiter,cesser,accuser,dire,na�tre,changer,suffir,'faire_l''objet',
			   essayer,accepter, garder, saisir, efforcer, r�sulter, jouir, 'faire_�tat',
			    'tenir_compte', retirer, 'faire_preuve', composer
			   ]) ->
	      %% the info about these verbs should take place in Lefff
	      Type = de_obj,
	    Head = A
	  ; TLemma = �,
	      A = node{ cat => v, lemma => Lemma },
	      domain(Lemma,[destiner,r�pondre,estimer,proc�der,participer,�valuer,limiter,situer,affecter,adapter,
			    tarder,suffir,servir,reverser,parvenir,opposer,octroyer,�tablir,
			    mettre, devoir, venir, permettre,
			    porter, revenir, engager, donner, attendre, vendre, r�duire, ouvrir,
			    d�cider, arriver, conduire, rendre, offrir, apporter, servir,
			    imposer, consacrer, consister,
			    remettre, livrer, verser, associer, opposer, payer, contribuer, r�ussir,
			    contraindre, limiter,
			    renoncer, ajouter, appeler, correspondre, succ�der,
			    participer, obliger, viser, suffire, accorder,
			    proc�der, aboutir, laisser, amener,
			    attribuer, r�unir, distribuer, r�sister, r�pondre,
			    refuser, pousser, inciter, h�siter, aider, confier,
			    assister, appr�ter, soumettre, recommencer, �chapper,
			    ressembler, attacher, r�soudre, adresser,
			    tendre, recourir, chiffer, int�resser,
			    confronter, inviter, 
			    forcer, comparer, habiliter, r�signer
			    ]) ->
	      Type = a_obj,
	    Head = A
	  ;
	    A = node{ cat => v, lemma => Lemma },
	    domain(Lemma:TLemma,[traduire:par,
				 porter:sur,
				 inscrire:dans,
				 d�boucher:sur,
				 aligner:sur,
				 expliquer:par,
				 choisir:entre,
				 peser:sur,
				 solder:par,
				 caract�riser:par,
				 tomber:dans,
				 lutter:contre,
				 tourner:vers,
				 red�ployer:vers,
				 prononcer:sur,
				 tirer:sur,
				 recentrer:sur,
				 concentrer:sur,
				 replier:sur,
				 pencher:sur,
				 consister:en,
				 protester:contre,
				 engager:dans,
				 protester:contre,
				 �lever:contre,
				 investir:dans,
				 r�investir:dans,
				 retomber:dans,
				 transformer:en,
				 entrer:dans,
				 faire:par,
				 passer:par,
				 revenir:sur,
				 reposer:sur,
				 insister:sur
				]) ->
	    Type = p_obj,
	    Head = A
	  ; A = node{ cat => cat[nc,adj,np] } ->
	    Head = A,
	    Type = dep		% may arise in short sentence
	  ;
	  Head = A,
	  (opt(fqb),
	   node2op(TNId,TOId),
	   ( chain( T >> subst >> node{ cat => cat[np] } )
		  xor chain( T >> subst >> node{ cat => cat[nc] }
			     >> adj >> node{ cat => prep, lemma => de }
			     >> subst >> node{ cat => cat[np] }
			   )
	   ),
	   check_op_top_feature(TOId,pcas,PCas) ->
	       ( PCas = loc ->
		     Type = 'mod.loc'
		; PCas = dloc ->
		      Type = 'mod.dloc'
		; Type = mod
	       )
	   ;
	   Type = mod
	  )
	  )
	; Type = dep,
	  Head = A,
	  Dep = T
	).

%% comparative
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_comparative', eid => EId } :-
	edge{ type => adj,
	      label => label[supermod,'mod.quantity'],
	      source => Adv::node{ cat => cat[adv,adj] },
	      target => Mod1::node{},
	      id => EId
	    },
	chain( Adv << adj << X::node{ cat => XCat } ),
	( XCat = v -> XType = mod ; XType = dep ),
	( node!empty(Mod1) ->
	    chain( Mod1 >> edge_kind[subst,lexical] @ 'Modifier' >>  Mod2::node{}),
	    (	chain( Mod1 >> lexical @ que >> Que::node{}) ->
		Mod = Mod2
	    ;	Mod2 = node{ cat => adj, lemma => possible } ->
		QueAlt=false, Que = Mod2
	    ;	Que  = Mod2,
		chain( Que >> subst @ label['S','SubS'] >> Mod::node{} )
	    ),
	    (	Type = XType, Dep = Que, Head = X
	    ;	QueAlt=true, 
		(opt(sequoia) -> Type = 'obj.cpl' ; Type = obj), 
		Dep = Mod, Head = Que
	    )
	  ; chain( Mod1 >> lexical @ que >> Que::node{}) ->
		Mod = Mod1,
		(	Type = XType, Dep = Que, Head = X
			; (opt(sequoia) -> Type = 'obj.cpl' ; Type = obj), 
			  Dep = Mod, Head = Que
		)
	  ;
	  Mod = Mod1,
	  Type = XType,
	  Dep = Mod,
	  Head = X
	)
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_comparative_m�me', eid => EId } :-
	chain( N::node{}
	     >> adj >> Adj::node{ cat => adj }
	     >> ( (EIdQue : lexical @ que)  >> Que::node{}
		& (EIdComp : subst @ 'Comparative') >> Mod::node{}
		)
	     ),
	( XCat = v -> XType = mod ; XType = dep ),
	( Type = XType,
	    Dep = Que,
	    Head = N,
	  EId = EIdQue
	; 
	  (opt(sequoia) -> Type = 'obj.cpl' ; Type = obj), 
	  Dep = Mod,
	  Head = Que,
	  EId = EIdComp
	)
	.

%% *** CONLL guideline: we climb till main verb (as found in FTB6 but in contradiction with guidelines !)
conll!relation{ type => Type, dep => T, head => V, name => 'R_aux', eid => EId } :-
	E::edge{ label => label['Infl',aux],
		 type => adj,
		 id => EId,
		 source => _V::node{ cat => SCat, id => NId },
		 target => T::node{ lemma => TLemma }
	       },
	conll_main_verb(_V,V::node{ cat => v}),
	( TLemma = avoir ->
	  Type = aux_tps
	; SCat = aux ->
	  Type = aux_tps
	; node2live_ht(NId,HTId),
	  ( check_ht_feature(HTId,diathesis,passive) ->
	    Type = aux_pass
	  ;
	    Type = aux_tps
	  )
	).

%% relative with antecedent
conll!relation{ type => mod_rel, dep => T, head => S::node{ }, name => 'R_SRel', eid => EId } :-
	E::edge{ label => label['SRel','N2Rel'], source => _S::node{ cat => Cat::cat['N2',ce] }, id => EId, target => T},
	( Cat = 'N2', conll_is_adj(E,S)
	xor Cat=ce, S=_S
	),
%%	format('conll relation R_SRel dep=~w head=~w\n',[T,S]),
	true
.

conll_simple_relation(edge{ label => ncpred, type => lexical},obj,'R_ncpred').
conll_simple_relation(edge{ label => csu,
			    type =>lexical,
			    source => cat[~coo]
			  }, obj, 'R_csu_arg'). % to check

conll!relation{ type => L, dep => T, head => Head, name => 'R_sentential_arg', eid => EId } :-
	E::edge{ label => xcomp,
		 type => subst,
		 id => EId,
		 target => T::node{ },
		 source => S::node{ cat => v, id => NId, lemma => Lemma }
	       },
	node2live_ht(NId,HTId),
	check_xarg_feature(HTId,Arg::args[arg1,arg2],
			   Fun,
			   FKind::fkind[scomp,prepscomp,vcomp,prepvcomp,whcomp,prepwhcomp,vcompcaus],
			   _
			  ),
%%	check_arg_feature(HTId,Arg,extracted,Extracted),
%%	format('here fun=~w fkind=~w ~w\n',[Fun,FKind,E]),
	( Lemma = faire, FKind = vcomp ->
	  fail
	; Fun = obj ->
	  ( FKind = scomp ->
	    (edge{ source => S, type => lexical, label => csu, target => Head::node{} }
	     xor Head = S	% when 'que' is omitted
	    )
	  ; FKind = whcomp,
	    edge{ source => S, type => lexical, label => siwh, target => Head } ->
	    true
	  ; FKind = prepvcomp ->
	    edge{ source => S, type => lexical, label => prep, target => Head }
	  ; FKind = vcompcaus -> % causative
	      \+ (
		     chain( S >> _ @ object >> node{} )
		 ;
		     chain( T >> _ @ subject >> node{} ) 
		 ),
	      Head = S
	  ;   
	      Head = S
	  ),
	  ( opt(sequoia) ->
		Head = node{ cat => HCat },
		( HCat = cat[csu,que] -> L = 'obj.cpl'
		  ; HCat = prep -> L = 'obj.p'
		  ; L = obj
		)
	    ;
	    L = obj
	  ),
	  true
	; Fun = att ->
	    %%	  Head = S,
	    ( check_ht_feature(HTId,ctrsubj,obj) ->
		_L = ato
	    ;	
		_L = ats
	    ),
	    ( FKind = scomp ->
		edge{ source => S, type => lexical, label => csu, target => Head::node{} },
		(opt(sequoia) -> L = 'obj.cpl' ; L = obj)
	    ;	FKind = prepvcomp ->
		edge{ source => S, type => lexical, label => prep, target => Head },
		L = obj
	    ;	
		Head = S,
		L = _L
	    )
	; Fun = function[obj�,objde] ->
	    edge{ source => S, type => lexical, target => _S::node{ cat => _SCat::cat[prep,que] } },
	    Head = _S,
	    ( opt(sequoia) ->
		  ( _SCat = que -> L = 'obj.cpl' 
		    ; _SCat = prep -> L = 'obj.p'
		    ; L = obj
		  )
	      ; L = obj
	    )
	;   
	    Head = S,
	    L = obj
	)
	.

conll!relation{ type => dep, dep => T, head => S, name => 'R_que_arg', eid => EId } :-
	edge{ label => csu,
	      type => lexical,
	      id => EId,
	      target => T::node{ cat => que },
	      source => V::node{ cat => cat[~ v], id => NId }},
	node2live_ht(NId,HTId),
	check_xarg_feature(HTId,args[arg1,arg2],_,fkind[prepscomp],_),
	edge{ source => V, target => S, label => xcomp, type => subst }
	.

conll!relation{ type => obj, dep => T, head => S, name => 'R_ce_que', eid => EId} :-
	edge{ label => ce,
	      id => EId,
	      type => lexical,
	      target => T::node{ lemma => ce },
	      source => V::node{ cat => cat[v,adj], id => NId }},
	node2live_ht(NId,HTId),
	check_xarg_feature(HTId,args[arg1,arg2],_,fkind[prepscomp],_),
	edge{ source => V, target => S, label => prep, type => lexical }
	.

conll!relation{ type => Type, dep => T, head => V, name => 'R_que_scomp', eid => EId} :-
	edge{ label => csu,
	      id => EId,
	      type => lexical,
	      target => T::node{ lemma => que },
	      source => V::node{ cat => v, id => NId }},
	node2live_ht(NId,HTId),
	check_xarg_feature( HTId,Arg::args[arg1,arg2],
			    F::function[obj,objde,att],
			    fkind[scomp],
			    _
			  ),
%%	format('que scomp arg=~w f=~w\n',[Arg,F]),
	( F = obj -> Type = obj
	; F = att -> Type = ats
	; Type = de_obj
	)
	.

conll!relation{ type => Type, dep => T, head => V, name => 'R_colon_scomp', eid => EId} :-
	edge{ label => xcomp,
	      id => EId,
	      type => subst,
	      target => T::node{ cat => cat[~ [csu]]},
	      source => V::node{ cat => v, id => NId }},
	chain( V >> lexical >> node{ lemma => ':'} ),
	Type = obj
	.


conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_ce_est_que', eid => EId } :-
	chain( Ce::node{ cat => cln, lemma => ce }
	     << (EIdCe : lexical @ subject) << Est::node{ cat => aux, lemma => �tre }
	     >> (EIdQue : lexical @ csu) >> Que::node{}
	     ),
	chain( Est << (EIdV : adj @ _) << V::node{} ),
	\+ node!empty(V),
	( Type = suj, Dep = Ce, Head = Est, EId = EIdCe
	; Type = ats, Dep = Que, Head = Est, EId = EIdQue
	; Type = obj, Dep = V, Head = Que, EId = EIdV
	)
	.


conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_ce_est_S', eid => EId } :-
	chain( Ce::node{ cat => cln, lemma => ce }
	     << (EIdCe : lexical @ subject) << Est::node{ cat => aux, lemma => �tre }
	     ),
	chain( Est << (EIdV : adj @ _) << V::node{} ),
        \+ chain( Est >> (lexical @ csu) >> Que::node{}),
	\+ node!empty(V),
	( Type = suj, Dep = Ce, Head = Est, EId = EIdCe
	; Type = ats, Dep = V, Head = Est, EId = EIdQue
	)
	.

conll!relation{ type =>Type, dep => Dep, head => Head, name => 'R_dep_sentential', eid => EId } :-
	chain( N::node{ cat => NCat::cat[adj,nc,adv] } >>
	     ( (EIdSubS : subst @ xcomp) >> SubS::node{}
	     & (EIdIntro : lexical @ label[prep,csu]) >> Intro::node{ cat => IntroCat }
	     )),
	( (NCat = adj, IntroCat = prep, opt(spmrl) -> Type = mod ; Type = dep),
	  Dep = Intro, Head = N, EId = EIdIntro
	; (opt(sequoia) -> Type = 'obj.cpl' ; Type = obj), 
	  Dep = SubS, Head = Intro, EId = EIdSubS
	)
	.

%% causative as aux_caus
conll!relation{ type => aux_caus, dep => Faire, head => V, name => 'R_aux_caus', reroot => [redirect,dep_frozen], eid => EId } :-
	( chain( Faire::node{ lemma => faire, cat => v, id => NId } >> (EId : subst @ xcomp) >> V::node{} )
	;
	  chain( Faire << (EId : adj @ label['S','mod.xcomp']) << V ),
	  Adj = yes
	),
	(   chain( Faire >> _ @ object >> node{} )
	;   Adj == yes ->
	    chain( V >> _ @ subject >> node{} )
	;
	    node2live_ht(NId,HTId),
	    check_arg_feature(HTId,args[arg1,arg2],kind,fkind[vcompcaus,vcomp])
	)
	.

conll!relation{ type => obj, dep => T, head => V, name => 'R_siwh', eid => EId} :-
	edge{ label => siwh,
	      type => lexical,
	      id => EId,
	      target => T::node{ lemma => TLemma },
	      source => V::node{ cat => cat[v], id => NId }},
	domain(TLemma,[si,comme]),
	node2live_ht(NId,HTId),
	check_xarg_feature(HTId,args[arg1,arg2],obj,fkind[whcomp],_)
	.

conll!relation{ type => L, dep => T, head => V, name => 'R_v_preparg', eid => EId } :-
	edge{ label => label[prep,preparg],
	      type => lexical,
	      id => EId,
	      target => T::node{ lemma => TLemma, cat => cat[prep,prel,pri] },
	      source => V::node{ cat => cat[v], id => NId}
	    },
	node2live_ht(NId,HTId),
	check_xarg_feature( HTId,args[arg1,arg2],
			    Fun::function[obj�,objde,obl,obl2,obj,att],
			    fkind[prepscomp,prepvcomp,prepwhcomp],
			    _
			  ),
	( Fun = obj -> L=obj
	; TLemma = 'de' -> L='de_obj'
	; TLemma = '�' -> L = 'a_obj'
	; L = 'p_obj'
	)
	.

conll!relation{ type => dep, dep => Dep, head => Head, name => 'R_preparg_alt', eid => EId } :-
	chain( Head::node{ cat => cat[pres,adv] } >> (EId : subst @ preparg) >> Dep::node{ cat => prep } )
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_obj_by_xcomp_adj', eid => EId } :-
	edge{ target => V1::node{ cat => v, lemma => Lemma },
	      source => V2::node{ cat => v },
	      id => EId,
	      type => adj,
	      label => label['S', 'mod.xcomp']
	    },
	\+ ( Lemma = faire, chain( V2 >> lexical @ causative_prep >> node{} )),
	( chain( V1 >> lexical @ (L::label[csu,prep]) >> Intro::node{} ) ->
	  ( fail, Type = obj, Dep = Intro, Head = V1 % dealt with R_v_preparg
	    ; ( opt(sequoia) ->
		    ( L = csu -> Type = 'obj.cpl' 
		     ; L = prep -> Type = 'obj.p'
		     ; Type = obj)
		; Type = obj
	      ),
	      Dep = V2, Head = Intro
	  )
	; Type = obj, Dep = V2, Head = V1
	)
	.

conll!relation{ type => Type, dep => T, head => A, name => 'R_vmod', eid => EId} :-
	E::edge{ target => _T::node{ cat => _TCat::cat[~ coo], tree => Tree, cluster => cluster{ left => TLeft } },
		 source => _S::node{ cat => SCat },
		 id => _EId,
		 label => _Label::label[vmod,audience,'SubS',reference,person_mod,time_mod,'S','S2',np,
					'S_incise', position, ce_rel,coo,
					dcln,dcla,dcld,dcll,dclg
				       ] },
	( _Label = coo -> E=edge{ type => adj } ; true ),
	( node!empty(_T) ->
	  edge{ source => _T,
		target => T::node{ cat => TCat::cat[~ coo] },
		id => EId,
		type => edge_kind[~ adj],
		label => Label
	      },
	  Label = label[audience,'SubS',reference,person_mod,np,'S_incise',position, ce_rel, dcln, dcla, dcld, dcll, dclg]
	; T = _T,
	  EId = _EId,
	  Label = _Label,
	  node2conllfullcat(T,conll_notponct[~ ['CC']])
	),
	conll_is_adj(E,_A::node{ cat => _ACat, cluster => cluster{ left => _ALeft }}),
	%%	format('tree ~w\n',[Tree]),
	\+ domain(cleft_verb, Tree),
	\+ ( _Label = 'S', TCat = 'v' ), % xcomp with extraction done by adj
	\+ ( _Label = label[vmod,'S','S2'], chain( _T >> subst @ 'PP' >> node{} )),
	( _ACat = v,
	  ( TLeft < _ALeft xor TCat = cat[csu] xor Label = label['SubS',audience,time_mod,position, ce_rel])
	->
	  conll_modal_climbing(_A,A)
	;
	  A = _A
	),
	( fail, SCat == 'N2' ->
	  Type = dep		% those cases in FTB6 seem to be annotation errors !
	; Label = dcln ->
	  Type = 'dis.suj'
	;  Label = dcla ->
	   ( edge{ source => _V::node{}, target => _S, type => adj, label => 'S' },
	     edge{ source => _V, label => comp, target => node{ cat => cla }}
	   ->
	   Type = 'dis.ats'
	   ;
	   Type = 'dis.obj'
	   )
	;  Label = dcld ->
	   Type = 'dis.a_obj'
	; Label = dclg ->
	  ( edge{ source => _V::node{ id => _VId }, type => adj, label => 'S', target => _S },
	    node2live_ht(_VId,_HTId),
	    check_arg_feature(_HTId,_Arg,function,objde),
	    check_arg_feature(_HTId,_Arg,real,clg) ->
	    Type = 'dis.de_obj'
	  ;	    
	  Type = 'dis.mod'
	  )
	; Label = dcll ->
	  Type = 'dis.mod'
	;
	  Type = mod
	),
	true
	.

conll_simple_relation( edge{ label => label[predet_ante,predet_post,pas], type => lexical },mod,'R_det_mod').

%conll_simple_relation( edge{ label => supermod, type => adj },mod,'R_supermod').

%% comparative

%% head inversion for Monsieur
%% rerooting
conll!relation{ type => mod, dep => S, head => T, name => 'R_Monsieur', reroot => [redirect], eid => EId } :-
	E::edge{ label => 'Monsieur', type => lexical, target => T, source => S, id => EId }
	.

%% preparg -> <x>_obj x in {a,de,p [par,sur,pour,avec ...]}
conll!relation{ type => L, dep => Dep, head => Head, name => 'R_x_obj', eid => EId } :-
	E::edge{ label => label[preparg],
		 type => edge_kind[subst,lexical],
		 id => EId,
		 target => T::node{ cat => TCat, lemma => TLemma, cluster => cluster{ left => Left, lex => Lex} },
		 source => S::node{ id => NId, cat => SCat::cat[v,adj]}
	       },
	( TCat = cld ->
	  L = 'a_obj', Dep = T, Head = S
	; TCat = adv ->
          L = mod, Dep = T, Head = S
	; TCat = prep ->
	  Dep = T, Head = S,
	  node2live_ht(NId,HTId),
	  (
	   %% if S is an adjective, it should used as a verb
	   SCat=adj,
	   \+ chain( S >> edge_kind[lexical,subst] @ subject >> node{} ) ->
	       (opt(spmrl) ->
		    ( TLemma = 'de',
		      check_xarg_feature(HTId,args[arg1],objde,_,_) -> L = 'de_obj'
		     ; TLemma = '�',
		       check_xarg_feature(HTId,args[arg1],obj�,_,_) -> L = 'a_obj'
		     ; L = mod
		    )
		;
		L = mod
	       )
	  ; TLemma = 'de' %, check_xarg_feature(HTId,args[],function[objde],fkind[prepobj,prepscomp,prepvcomp],'PP') 
	    -> L='de_obj'
	  ; TLemma = '�' %, check_xarg_feature(HTId,args[],function[obj�],fkind[prepobj,prepscomp,prepvcomp],'PP')
	    -> L = 'a_obj'
	  ; opt(sequoia),
	    \+ opt(spmrl) ->
		(
		    %% we have a pb because we can't really distinguish arg1 and arg2
		    %% when there are two preposional args
		    check_xarg_feature(HTId,
				       Arg::args[arg0,arg1,arg2],
				       Fun::function[],
				       fkind[prepobj,prepscomp,prepvcomp],
				       'PP'
				      ),
		    (
			(Fun=function[loc] ->
			     L = 'p_obj.loc'
			 ; Fun = function[dloc] ->
			       L = 'p_obj.dloc'
			 ; Fun = function[obl2,obl],
			   Arg = args[arg1,arg2] ->
			       ( TLemma = '�' -> L = 'p_obj.loc' % approx
				 ; TLemma = 'de' -> L = 'p_obj.dloc' % approx
				 ; L = 'p_obj.o'
			       )
			 ; Fun = function[obl2],
			   Arg = arg0,
			   check_ht_feature(HTId,diathesis,passive) ->
			       ( 
				   chain( S >> adj @ label['Infl',aux] >> node{} ) -> L = 'p_obj.agt'
				   ; L = 'p_obj.protect'
			       )
			 ;
			 L= 'p_obj.o'
			)
		    )
		)
	  ; L = 'p_obj'
	  )
	; TCat = prel ->
	  ( TLemma = dont ->
	    L='de_obj', Dep = T,
	    conll_first_verb_climbing(S,Head)
	  ; TLemma = lequel,
	    node{ cat => prep, lemma => TTLemma, cluster => cluster{ right => Left, lex => Lex } } ->
	    ( TTLemma = '�' -> L = 'a_obj'
	    ; TTLemma = 'de' -> L = 'de_obj'
	    ; L = 'p_obj'
	    ),
	    Dep = T,
	    conll_first_verb_climbing(S,Head)
	  ; chain( S >> lexical @ prep
		 >> Prep::node{ cat => prep,
				lemma => TTLemma,
				cluster => cluster{ right => PRight } }
		 ),
	    PRight =< Left
	    ->
	    %% To be completed
	    ( Dep = Prep, Head = S,
	      ( TTLemma = 'de' -> L='de_obj'
	      ; TTLemma = '�' -> L = 'a_obj'
	      ; opt(sequoia), \+ opt(spmrl) ->
		(
		    %% we have a pb because we can't really distinguish arg1 and arg2
		    %% when there are two preposional args
		    node2live_ht(NId,HTId),
		    check_xarg_feature(HTId,
				       Arg::args[arg0,arg1,arg2],
				       Fun::function[],
				       fkind[prepobj,prepscomp,prepvcomp],
				       'PP'
				      ),
		    (
			(Fun=function[loc] ->
			     L = 'p_obj.loc'
			 ; Fun = function[dloc] ->
			       L = 'p_obj.dloc'
			 ; Fun = function[obl2],
			   Arg = arg0,
			   check_ht_feature(HTId,diathesis,passive) ->
			       L = 'p_obj.agt'
			 ;
			 L= 'p_obj.o'
			)
		    )
		)
	      ; L = 'p_obj'
	      )
	    ; L = obj, Dep = T, Head = Prep
	    )
	  ;
	    L = p_obj, Dep = T,
	    conll_first_verb_climbing(S,Head)
	  )
	;
	  fail
	)
	.

conll!relation{ type => mirror, dep => Dep, head => Head, name => 'R_etc_enum', reroot => [mirror], eid => EId } :-
	chain( Dep::node{ form => 'etc.' } << (EId : lexical @ void) << Head::node{} )
	.
%%conll_simple_relation(edge{ label => 'PP', type => adj},,comp).

conll!relation{ type => mod, dep => T, head => S, name => 'R_N2app', eid => EId} :-
	E::edge{ label => 'N2app', target => T, id => EId},
        conll_is_adj(E,S).

conll_simple_relation(edge{ label => skip, type => epsilon },ponct,'R_skip_ponct').

conll_simple_relation(edge{ label => skip, type => lexical, target => node{ cat => '_' } },ponct,'R_skip_ponct').

conll_simple_relation(edge{ label => skip, target => node{ lemma => '_META_TEXTUAL_GN'} },mod,'R_skip_metatextual').

conll!relation{ type => ponct, dep => T, head => A, name => 'R_ponct', eid => EId } :-
	edge{ source => node{ cat => unknown}, target => T::node{ id => NId}, id => EId},
	node2conllfullcat(T,'PONCT'),
	%% robust mode: need to find some plausible governor !
	conll_rep(NId,TPos),
	( TPos > 1 ->
	    Pos is TPos -1
	;   Pos is TPos + 1
	),
	conll2node(Pos,ANId),
	A::node{ id => ANId }
	.

%%conll!relation(edge{ label => void, type => lexical},'PONCT',_,ponct).

conll_simple_relation(edge{ label => 'Nc2', type => lexical},mod,'R_Nc2').

%% xcomp before verb are considered as principals => Rerooting
conll!relation{ type => mod, dep => V, head => N, name => 'R_xcomp_incise', eid => EId } :-
	E::edge{ source => V::node{ cat => v, cluster => cluster{ left => VLeft} },
		 id => EId,
		 target => _N::node{ cluster => cluster{ left => _NLeft }, tree => Tree},
		 type => adj,
		 label => label['S',mod]
	       },
	_NLeft < VLeft,
	domain(quoted_sentence_as_ante_mod,Tree),
	conll_down_till_non_empty(_N,N)
	.

%% same for quoted sentences in ante position
conll!relation{ type => mod, dep => V, head => Head, name => 'R_quoted_incise', eid => EId } :-
	E::edge{ source => V::node{ cat => v, cluster => cluster{ left => VLeft} },
		 target => _N::node{ cluster => cluster{ left => _NLeft }},
		 id => EId,
		 type => subst,
		 label => xcomp
	       },
	_NLeft < VLeft,
	conll_down_till_non_empty(_N,N),
	conll_first_verb_climbing(N,Head)
	.

%% embedded verb in CS
conll_simple_relation( edge{ label => 'S',
			     type => subst,
			     source => A::node{ cat => cat[~ prep] }
			   },Type,'R_CS') :- 
         \+ node!empty(A),
	 (fail, opt(sequoia) -> Type = 'obj.cpl' ; Type = Obj )
	.

%% juxtaposed sentences
conll!relation{ type => Type, dep => T, head => A, name => 'R_juxt_S', eid => EId} :-
	E::edge{ source => N::node{},
		 target => T::node{},
		 id => EId,
		 label => Label,
		 type => subst
	       },
	node!empty(N),
	\+ Label = coord,
	conll_up_till_sep_S(N,_A::node{ cat => Cat} ),
	( node!empty(_A) ->
	  chain( _A >> (subst @ start) >> node{}),
	  ( chain( _A >> subst >> A1::node{} >> subst >> A2::node{})
	  xor chain( _A >> subst @ comp >> A2)
	  ),
	  ( \+ node!empty(A1) -> A = A1 ;
            \+ node!empty(A2) -> A = A2 ;
	    chain(A2 >> subst >> A3::node{}),
            \+ node!empty(A3),
	    A = A3
	  )
	;   
	  A = _A
	),
	A = node{ cat => ACat },
	( ACat = cat[nc,np] ->
	  Type = dep
	;
	  Type = mod
	)
	.

%% prep mod
conll_simple_relation( edge{ type => adj, target => node{ cat => cat[~ coo]}, label => label[prep] },mod,'R_prep_mod').

conll_simple_relation( edge{ type => adj, target => node{ cat => cat[~ coo]}, label => label[csu] },mod,'R_csu_mod').

conll_simple_relation( edge{ type => adj, target => node{ cat => cat[~ [coo,adv,advneg,que_restr,adj,adjPref,advPref]]}, label => label[adv] },mod,'R_adv_altmod').

%% csu on N2
conll_simple_relation( edge{ type => adj,
			     target => node{ cat => csu},
			     source => node{ cat => nominal[] }
			   },mod,'R_csu_on_N2').

%% en intro particiale
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_en_participiale', eid => EId } :-
	chain( Head::node{ cat => prep, lemma => Lemma} << lexical @ en <<
	       node{} >> (EId : subst @ 'SubS') >> Dep::node{} ),
	domain(Lemma,[en,'tout en']),
	(opt(sequoia) -> Type = 'obj.p' ; Type = obj )
	.

%% extracted modifiers
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_extracted_modifier', eid => EId } :-
	chain( Rel::node{ lemma => Lemma } << (EIdRel : lexical @ label[prel,pri]) << S::node{} << adj << V::node{} ),
	( chain(S >> (EIdPrep : lexical @ prep) >> Prep::node{}) ->
	  ( Type = mod, Dep = Prep, Head = V, EId = EIdPrep
	  ; (opt(sequoia) -> Type = 'obj.p' ; Type = obj), 
	    Dep = Rel, Head = Prep, EId = EIdRel
	  )
	 ; Lemma = dont ->
	       ( chain(V >> subst @ preparg >> node{ cat => prep, lemma => de}) ->
		     Type = mod, Dep = Rel, EId = EIdRel
		;
		Type = de_obj, Dep = Rel, EId = EIdRel
	       ),
	  conll_first_verb_climbing(V,Head)
	;
	  conll_modal_climbing(V,Head),
	  Type = mod, Dep = Rel, EId = EIdRel
	)
	.

conll!relation{ type => mod, dep => Prep, head => V, name => 'R_wh_modifier', eid => EId } :-
	chain( Prep::node{ cat => prep } << (EId : subst @ wh) << node{} << adj << V::node{} )
	.

%% quels que S, S
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_quel_que', eid => EId } :-
	chain( VSub::node{} << (EIdVSub : subst @ 'SRel') << S::node{} >> (EIdQuel : subst @ 'N2') >> Quel::node{} ),
	node!empty(S),
	chain( S << adj << V::node{} ),
	( Type = mod, Dep = Quel, Head = V, EId = EIdQuel
	; Type = mod_rel, Dep = VSub, Head = Quel, EId = EIdVSub
	)
	.

%% CS
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_CS_subs', eid => EId } :-
%	chain( SSub::node{} << subst @ 'SubS' << CSU::node{ cat => csu } << adj << S::node{} ),
	chain( SSub::node{} << (EId : subst @ 'SubS') << CSU::node{ cat => csu } ),
	(opt(sequoia) -> Type = 'obj.cpl' ; Type = obj), 
	Dep = SSub, Head = CSU
	.

%% N2 countable mod: 20 euros la tonne
conll_simple_relation( edge{ type => adj,
			     label =>  'N2',
			     source => node{ cat => cat[~ coo] },
			     target => node{ cat => cat[nc] }
			   },
		       mod,
		       'R_N2_countable_mod'
		     ).

%% Clefted constructions
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_cleft1', reroot => Reroot, eid => EId } :-
	chain( Etre::node{ lemma => �tre, cat => aux, id => EtreNId } << (EIdV : adj @ _) << V::node{ id => VNId } 
	     >> lexical @ 'CleftQue' >> Que::node{ id => QueNId, cluster => cluster{ left => Left }}
	     ),
	chain( V
	     >> (EIdN : edge_kind[subst,lexical] @ Label)
	     >> N::node{ id => NId, lemma => Lemma, cluster => cluster{ right => Right } } ),
	chain( Etre >> (EIdSubj : lexical @ subject) >> Subj::node{} ),
	Right =< Left,
	conll_rep(QueNId,QuePos),
	conll_rep(NId,NPos),
	conll_rep(VNId,VPos),
	conll_rep(EtreNId,EtrePos),
	record( Shift::conll_edge_shift(NPos,VPos,QuePos,EtrePos) ),
	%% format( '## register edge shift ~w\n',[Shift]),
	(   Type = ats, Dep = N, Head = Etre, EId = EIdN
	;   (opt(sequoia) -> Type = 'mod.cleft' ; Type = mod), 
	    Dep = V, Head = Etre, EId = EIdV
	;   Type = suj, Dep = Subj, Head = Etre, EId = EIdSubj
	)
	.

conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_cleft2', reroot => Reroot, eid => EId } :-
	chain( Etre::node{ lemma => �tre, cat => aux, id => EtreNId }
	     << (EIdV : adj @ _ ) << V::node{ id => VNId } 
	     >> (EIdN1 : adj @ _ ) >> N1::node{}
	     >> (EIdQue : lexical @ 'CleftQue') >> Que::node{ id => QueNId, cluster => cluster{ left => Left }}
	     ),
	( node!empty(N1) ->
	    chain( N1 >> (EIdN : subst @ _ )>> N::node{ id => NId, lemma => Lemma, cluster => cluster{ right => Right } })
	;
	  N1 = N,
	  EIdN = EIdN1
	),
	chain( Etre >> (EIdSubj : lexical @ subject) >> Subj::node{} ),
	%% Right =< Left,
	conll_rep(QueNId,QuePos),
	conll_rep(NId,NPos),
	conll_rep(VNId,VPos),
	conll_rep(EtreNId,EtrePos),
	record( Shift::conll_edge_shift(NPos,VPos,QuePos,EtrePos) ),
	%% format( '## register edge shift ~w\n',[Shift]),
	(   Type = ats, Dep = N, Head = Etre, EId = EIdN
	;   (opt(sequoia) -> Type = 'mod.cleft' ; Type = mod), 
	    Dep = V, Head = Etre, EId = EIdV
	;   Type = suj, Dep = Subj, Head = Etre, EId = EIdSubj
	;   Type = mod, Dep = Que, Head = V, EId = EIdQue
	)
	.

%% special cleft "le chef c'est lui."
conll!relation{ type => Type, dep => Dep, head => Head, name => 'R_special_cleft', eid => EId } :-
	chain( S::node{ cat => 'S' }
	     >> ( (EIdComp : subst @ comp) >> Comp::node{ cluster => cluster{ left => Comp_Left} }
		& (EIdSubj : subst @ subject) >> Subj::node{}
		& adj @ label['S'] >> V::node{ cat => aux, cluster => cluster{ left => V_Left }}
		>> (EIdCe : lexical @ subject) >> Ce::node{}
		)
	     ),
	node!empty(S),
	( chain( V >> (EIdQue : lexical @ csu) >> Que::node{ cat => que }) ->
	      ( Type = suj, Dep = Ce, Head = V, EId = EIdCe
	       ; Type = obj, Dep = Subj, Head = Que, EId = EIdSubj
	       ; Type = ats, Dep = Comp, Head = V, EId = EIdComp
	       ; Type = dis,	% new label introduced in FQB 
		 Dep = Que, Head = V, EId = EIdQue
	      )
	 ; Comp_Left < V_Left ->
	  ( Type = suj, Dep = Ce, Head = V, EId = EIdCe
	  ; Type = ats, Dep = Subj, Head = V, EId = EIdSubj
	  ; Type = suj, Dep = Comp, Head = V, EId = EIdComp
	  )
	  ;
	  ( Type = suj, Dep = Ce, Head = V, EId = EIdCe
	  ; Type = suj, Dep = Subj, Head = V, EId = EIdSubj
	  ; Type = ats, Dep = Comp, Head = V, EId = EIdComp
	  )
	)
	.
	
%% a prep modifying a prep becomes the head
conll!relation{ type => obj, dep => Dep, head => Head, name => 'R_prep_on_prep', eid => EId } :-
	edge{ type => adj,
	      label => prep,
	      id => EId,
	      source => Dep::node{ cat => prep },
	      target => Head::node{ cat => prep }
	    }
	.


conll!relation{ type => mod,
		dep => N1::node{ cat => Cat::cat[adv,advneg], cluster => cluster{ left => L1, right => R1 }},
		head => N2::node{ cat => Cat2::cat[v,adj,adv], cluster => cluster{ left => L2, right => R2 } },
		name => 'R_robust1',
		eid => EId
	      } :-
	recorded( mode(robust) ),
	N1,
	\+ edge{ target => N1, type => edge_kind[adj] },
	(  R1 = L2, N2, \+ ( Cat=advneg, Cat2=cat[adv,adj] )
	xor L1 = R2, Cat2=v, N2
	xor L1 is R2+1, Cat2=v, 
	   node{ cat => cln, cluster => cluster{ left => R2, right => L1}},
	   N2
	)
	.

conll!relation{ type => mod,
		dep => N1::node{ cat => cat[adj], cluster => cluster{ left => L1, right => R1 }},
		head => N2::node{ cat => cat[nc], cluster => cluster{ left => L2, right => R2 } },
		name => 'R_robust2',
		eid => EId
	      } :-
	recorded( mode(robust) ),
	N1,
	\+ edge{ target => N1, type => edge_kind[adj,lexical] },
	(  R1 = L2, N2
	xor L1 = R2, N2
	)
	.

%% short sentences as incises
conll!relation{ type => mod, dep => Dep, head => Head, name => 'R_shortS_as_incise', eid => EId } :-
	chain( S::node{} << (EIdS : subst @ 'S_incise') << node{} << adj << Head::node{} ),
	chain( S::node{} >> subst @ start >> node{ cat => start }),
	
	( node!empty(S) ->
	  chain( S::node{} >> (EIdMod : subst @ Label) >> Mod::node{} ),
	  Label \== start,
	  ( node!empty(Mod) ->
	    chain( Mod >> (EId : subst @ _) >> Dep::node{} )
	  ;
	    Dep = Mod, EId = EIdMod
	  )
	;
	 Dep = S, EId = EIdS
	)
	.

%% Pas as starter is not the root of a short sentence
conll!relation{ type => mod, dep => Pas, head => Head, name => 'R_advneg_starter', eid => EId } :-
	chain( Pas::node{ cat => advneg}
	     << (EId : lexical @ advneg) << S::node{}
	     >> subst @ start >> node{}
	     ),
	( node!empty(S) ->
	  chain( S::node{} >> subst @ Label >> Mod::node{} ),
	  Label \== start,
	  ( node!empty(Mod) ->
	    chain( Mod >> subst >> Head::node{} )
	  ;
	    Head = Mod
	  )
	;
	 Head = S
	)
	.

%% pas as mod on coord
conll!relation{ type => mod, dep => Pas, head => Head, name => 'R_advneg_start_coo', eid => EId} :-
	chain( Pas::node{ cat => advneg  }
	     << (EId : lexical @ advneg) << COO::node{ cat => coo }
	     << adj << Head::node{} )
	.

%% refmarks
conll!relation{ type => mod, dep => Mark, head => Head, name => 'R_refmark', eid => EId } :-
	chain( Head::node{} >> adj @ 'N2' >> N2 :: node{ tree => Tree } >> lexical >> Mark :: node{} ),
	domain('refmark_on_N2',Tree)
	.

:-std_prolog conll_up_till_sep_S/2.

%% climb up subst edges trough empty nodes till a S sep node (between two sentences)

conll_up_till_sep_S(N,A) :-
	node!empty(N),
	(   chain( N::node{} << subst << _N::node{} ) ->
	    conll_up_till_sep_S(_N,A)
	;
	    chain( N >> (lexical @ void) >> node{ lemma => L }),
	    domain(L,[',',';',':','.','...','_SENT_BOUND','(...)']),
	    chain( N << adj << A::node{} )
	)
	.

:-rec_prolog conll_cat/5.

conll_cat(cat[cln,cla,cld,clr,clg,ilimp,cll],'CL',_,N,_).
conll_cat(cat[clneg],'ADV',_,N,_).
conll_cat(cat[v,aux],T,_,N::node{},_) :-
	( single_past_participiale(N) -> T = 'A' ; T = 'V' ).

conll_cat(cat[det,number],L,_,N,Lex) :-
	( %fail,
	  domain(Lex,['millions','milliards','million','milliard',
		      'centaine','centaine','millier','milliers']) ->
	  L = 'N'
	;
	  L = 'D'
	).

conll_cat(Cat::cat[nc,ncpred,np,title],L,Lemma,N::node{ form => Form,
							cluster => cluster{left => Left,
									   right => Right,
									   token => Token,
									   lex => TLex
									  }
						      },Lex) :-
    (
	Lemma = '_NUMBER',
	chain( N >> adj >> node{ cat => coo }
	       >> (subst @ coord3) >> K::node{ cat => nc, lemma => '_NUMBER',
					       cluster => cluster{ lex => [_|_] ,
								   token => _Token }} ),
	rx!tokenize(_Token,' ',_TokenL),
	domain(_T::million[],_TokenL)
     ->
%	    format('K=~w\n',[K]),
	 L = 'D'
     ; Lemma = '_NUMBER',
       \+ chain( N << adj @ 'N' << node{} ),
       is_year(Token)
       ->
	   L = 'N'
     ;
        domain(Lemma,['_NUMBER','_NUM',un,autre,aucun,'le sien']),
	  \+ ( domain(Lex,['millions','milliards','million','milliard',
			    'centaine','centaine','millier','milliers',
			    'dizaine','dizaines',
			    'douzaine','douzaines',
			   'quinzaine','quinzaines',
			   'vingtaine','vingtaines',
			   'trentaine','trentaines',
			   'quarantaine','quarantaines',
			   'cinquantaine','cinquantaines'
			   ]),
	       chain( N >> adj >> node{ cat => prep, lemma => de } )
	     ),
	  \+ chain( N << adj @ 'N' << node{} )
	->
	  L = 'PRO'
	; Lemma = '_ETR' ->
	  L = 'ET'
	; domain(Lex,['-']) ->
	  L = 'PONCT'
	; Cat = np,
	  domain(Lex,[f�d�ral,f�d�rale,
		      saoudite,
		      international,internationales,internationaux,
		      nouvel,
		      europ�en,europ�enne,
		      g�n�ral,g�n�rale,
		      central
		     ]) ->
	      L = 'A'
	; domain(Lemma,['_DATE_artf','_DATE_arto']),
	  Cat = nc,
	  \+ TLex = [_|_],
	  ( chain( N << subst << node{ cat => prep }
		 << adj << node{ cat => prep}
		 >> subst >> node{ cat => nc, lemma => Lemma})
	   ; chain( N >> adj >> node{ cat => coo }
		 >> (subst @ coord3) >> node{ cat => nc, lemma => Lemma})
	  )
	  ->
	      L = 'A'
	;
	  L = 'N'
	).

conll_cat(_,'N','_META_TEXTUAL_GN',_,_).

conll_cat(adj,L,Lemma,N,Lex) :-
	( Lemma = '_ETR' ->
	  L = 'ET'
	; domain(Lex,['millions','milliards','million','milliard',
		      'centaine','centaine','millier','milliers',
		      'dizaine','dizaines',
		      'douzaine','douzaines',
		      'quinzaine','quinzaines',
		      'vingtaine','vingtaines',
		      'trentaine','trentaines',
		      'quarantaine','quarantaines',
		      'cinquantaine','cinquantaines'
		     ]) ->
	  L = 'N'
	;
	  L = 'A'
	).
	
conll_cat(cat[adv,advneg,que_restr,predet],'ADV',_,N,_).
conll_cat(cat[ponctw,poncts],'PONCT',_,N,_).
conll_cat(cat[csu,coo,que],'C',_,N,_).
conll_cat(cat[prep],Cat,Lemma,N::node{ form => Form, cluster => cluster{ right => R}},Lex) :-
	( Lex = 'du',
	  \+ node{ cat => det, cluster => cluster{ left => R } }
	->
	  %% to handle a hack in the lexer related to a bug in the current version of sxpipe
	  Cat = 'P+D'
	;
	  Cat = 'P'
	)
	.
conll_cat(cat[pro,prel,xpro,ce,caimp],'PRO',_,N,_).
conll_cat(cat[pri],T,Lemma,N,_) :-
	( pri(Lemma,'GR') -> T = 'ADV'
	; fail, domain(Lemma,['o�']) -> T = 'ADV' % to check
	; domain(Lemma,['quel?']) -> T = 'ADJ'
	;   T = 'PRO'
	)
	.
conll_cat(cat[pres],'I',_,N,_).
conll_cat('_','PONCT',Lemma,N,_) :-
	domain(Lemma,['.',',',';','!','?','(',')','[',']','!?','??','?!','...','"','''','-',':','_','(...)'])
	.
conll_cat('_','ADV',Lemma,N,_) :-	domain(Lemma,['etc.']).  %% should replace by a coanchor in FRMG
conll_cat('_','PRO',Lemma,N,_) :-	domain(Lemma,[ce]).  %% should replace by a coanchor in FRMG
conll_cat('_','P',Lemma,N,_) :-	domain(Lemma,[en]).  %% should replace by a coanchor in FRMG
conll_cat('_','C',Lemma,N,_) :-	domain(Lemma,[ou]).  %% should replace by a coanchor in FRMG

conll_cat('_','ADV',Lemma,N,_) :-
	recorded( mode(robust) ),
	domain(Lemma,[ne]).

conll_cat(_,'PONCT',Lemma,N,_) :- domain(Lemma,['_EPSILON','_META_TEXTUAL_PONCT']).

conll_cat(cat[adjPref,advPref],'PREF',_,N,_).

%%conll_cat(_,'ET','_ETR',_).

:-rec_prolog conll_fullcat/6.

conll_fullcat(cat[v,aux],CCat,OId,N,FullCat,_) :-
	OId \== '_',
	CCat \== 'A',
%	format('chek op feature ~w\n',[OId]),
	check_op_top_feature(OId,mode,OpMode),
%	format('check fullcat mode=~w lemma=~w left=~w right=~w\n',[Mode,Lemma,Left,Right]),
	('C'(Left,lemma{ cat => Cat, top => v{ mode => OpMode}, lemma => Lemma},Right) ->
	     Mode = OpMode
	 ; 'C'(Left,lemma{ cat => Cat, top => v{ mode => Mode}, lemma => Lemma},Right),
	   Mode = mode[infinitive,participle,gerundive,imperative,subjonctive] ->
	       true
	 ; Mode=OpMode
	),
%	format('mode ~w ~w\n',[OId,Mode]),
	( chain(N::node{cat => Cat, lemma => Lemma, cluster => cluster{left => Left, right => Right}} >> (adj @ label['Infl',aux]) >> node{}) ->
	  FullCat = 'VPP'
	;   chain(N::node{} >> (adj @ label['V',modal]) >> node{ cat => v, lemma => _Lemma, id => ModalNId}) ->
	  ( _Lemma = aller,
	    node2live_ht(ModalNId,ModalHTId),
	    check_arg_feature(ModalHTId,arg1,function,obl)
	    ->
	    FullCat= 'VPR'
	  ;
	    FullCat = 'VINF'
	  )
	; Mode = infinitive ->
	  FullCat = 'VINF'
	;   Mode = participle ->
	    FullCat = 'VPP'
	;   Mode = gerundive ->
	  FullCat = 'VPR'
	;   Mode = imperative ->
	  FullCat = 'VIMP'
	;   Mode = indicative ->
	  FullCat = 'V'
	;   Mode = subjonctive ->
	  FullCat = 'VS'
	;   
	  FullCat = 'V'
	).

conll_fullcat(cat[csu,que],_,_,_,'CS',_).
conll_fullcat(coo,_,_,_,'CC',_).
conll_fullcat('_','C',_,_,'CC',_). 	% case of lexical ou (should replace by a coanchor in FRMG)
conll_fullcat(Cat::cat[nc,ncpred,title],'N',
	      _,N::node{ lemma => Lemma,
			 cluster => cluster{ left => Left,
					     lex => Lex } },
	      L,_) :-
	( fail,
	  %% some NC are labelled NPP
	  %% but difficult to find them !
	  Left > 0,
          \+ edge{ target => N, label => 'Monsieur'},
	  \+ chain( N >> (subst @ det) >> node{} ),
	  Cat = cat[nc],
	  capitalized_cluster(Lex) ->
	  L = 'NPP'
	;
	  L = 'NC'
	)
	.

conll_fullcat(_,'D',_,node{ lemma => Lemma, cluster => cluster{ token => Token } },L,_) :-
	( domain(Lemma,[quel]) -> L = 'DETWH'
	;   L = 'DET'
	).
%conll_fullcat(cat[ponctw,poncts],_,_,_,'PONCT').
%conll_fullcat(cat[prep],_,_,_,'P').
conll_fullcat(clr,_,_,_,'CLR',_).
conll_fullcat(cat[cln,ilimp],_,_,_,'CLS',_).
conll_fullcat(cat[cla,cld,clg,cll],_,_,_,'CLO',_).
%conll_fullcat(clneg,_,_,_,'ADV').
conll_fullcat(np,conll_cat[~ ['A']],_,
	      N::node{ lemma => Lemma,
		       cluster => cluster{ left => Left,
					   token => Token,
					   lex => Lex } },L,_) :-
%%	format('token=~w lex=~w\n',[Token,Lex]),
	( domain(Token,['pib','rmi','smic','pnb','sa','tva','soci�t�','groupe','banque','csa','btp','hlm','ces']) -> L = 'NC'
	; L = 'NPP'
	)
	.
conll_fullcat(cat[np,adj,v,nc],'A',_,_,'ADJ',_).
conll_fullcat(adj,'N',_,_,'NC',_).
%conll_fullcat(_,'N',node{ form => '_META_TEXTUAL_GN' },_,'NC',_).
%conll_fullcat(cat[adv,advneg,que_restr,predet],_,_,_,'ADV').
%conll_fullcat(cat[pro,xpro,ce,caimp],_,_,_,'PRO').
conll_fullcat(cat[prel],'PRO',_,_,'PROREL',_).
conll_fullcat(cat[pri],'ADV',_,_,'ADVWH',_).
conll_fullcat(cat[pri],'PRO',_,_,'PROWH',_).
conll_fullcat(cat[pri],'ADJ',_,_,'ADJWH',_).
conll_fullcat(cat[pres],_,_,_,'I',_).

:-std_prolog conll_last_cat/2.

conll_last_cat([Cat|L],LastCat) :-
	( L=[] -> LastCat = Cat ; conll_last_cat(L,LastCat) )
	.

%% d'autres DET
%% nombre_de DET
%% d'autres PRO
%% court_terme ADJ
%% permis_de_conduire NC
%% Parce_que CS
%% manque_�_gagner NC
%% le_plus_souvent ADV
%% il_y_a V
%% il_est_vrai_que CS

:-std_prolog conll_collect_cats/5, conll_collect_cats_aux/3.

conll_collect_cats(N::node{ lemma => Lemma, form => Form, cluster => cluster{ lex => TIds}},CCat1,FullCat1,CCat,FullCat) :-
	recorded( conll_forward(N,Next) ),
	conll_collect_cats_aux(Next,_LCat,_LFullCat),
	LCat = [ CCat1 | _LCat ],
	LFullCat = [ FullCat1 | _LFullCat ],
	conll_last_cat(LCat,LastCat),
	conll_last_cat(LFullCat,LastFullCat),
%%	format('collect ~w ~w cat1=~w cat=~w N=~w\n',[LCat,LFullCat,CCat1,LastCat,N]),
%%	conll_verbose('collect ~w ~w cat1=~w cat=~w\n',[LCat,LFullCat,CCat1,LastCat]),
	( LFullCat = ['P','PROREL'] ->
	  CCat = 'P+PRO',
	  FullCat = 'P+PRO'
	; LFullCat = ['P','PRO'] ->
	  CCat = 'P+PRO',
	  FullCat = 'P+PRO'
	; LCat = ['P','D'],
%	  format('node ~w\n',[N]),
%	  domain(Lemma,[�,de]),
	  true
	->  
	    ( domain(Lemma,[�,de]) ->
		  CCat = 'P+D',
		  FullCat = 'P+D'
	     ;
	     CCat = 'P',
	     FullCat = 'P'
	    )
	; LCat = ['N','D'] ->
	  CCat = 'D',
	  FullCat = 'DET'
	; LCat = ['N','PONCT'] ->
	  CCat = 'N',
	  FullCat = FullCat1
	; LCat = ['ADV','ADV'] ->
	  CCat = 'ADV',
	  FullCat = 'ADV'
	; LCat = ['ADV','D'] ->
	  CCat = 'P',
	  FullCat = 'P'
	; LFullCat = ['CLO','VPR'] -> % en chantant 
	    CCat = 'ADV',
	    FullCat = 'ADV'
	; LCat = ['PRO','ADV'] ->
	    CCat = 'ADV',
	    FullCat = 'ADV'
	;   LCat = ['ADV','PRO'] ->
	    CCat = 'C',
	    FullCat = 'CS'
	;   LCat = ['ADV','D'] ->
	    CCat = 'ADV',
	    FullCat = 'ADV'
	; LFullCat = ['ADV','CS'] ->
	  CCat = 'C',
	  FullCat = 'CS'
	; LFullCat = ['NPP','ADJ'] ->
	    CCat = 'N',
	    FullCat = 'NPP'
	;   LFullCat = ['ADV','PROREL'] ->
	    CCat = 'C',
	    FullCat = 'CS'
	;   LFullCat = ['ADV','ADJ'] ->
	    CCat = 'A',
	    FullCat = 'ADJ'
	; LFullCat = ['ADJ','ADJ'] ->
	  CCat = 'A',
	  FullCat = 'ADJ'
	; LFullCat = ['ADV','CC','ADV'] ->
	  CCat = 'ADV',
	  FullCat = 'ADV'
	; LFullCat = ['NC','PP','VINF'] ->
	  CCat = 'N',
	  FullCat = 'NC'
	; LFullCat = ['CLS','V','ADJ'] ->
	  CCat = 'ADV',
	  FullCat = 'ADV'
	; LFullCat = ['PRO','PONCT','PRO'] ->
	  CCat = 'N',
	  FullCat = 'NC'
	; CCat1 = 'PREF' ->
	  CCat = LastCat,
	  FullCat = LastFullCat
	; CCat1 = 'V' ->
	  ( LastCat = 'CL' ->
	    CCat = 'CL',
	    FullCat = 'CLS'
	  ;
	    CCat = 'V',
	    FullCat = FullCat1
	  )
	; LCat = ['CL','CL','V'] -> % il y a
	  CCat = 'P',
	  FullCat = 'P'
	; LastCat = 'P' ->
	  CCat = 'P',
	  FullCat = 'P'
	; CCat1 = 'ADV', LastCat = 'D' ->
	  CCat = 'P',
	  FullCat = 'P'
	; CCat1 = 'P', LastCat = 'D' ->
	  CCat = 'P',
	  FullCat = 'P'
	; CCat1 = 'P', LastCat = 'C' ->
	  CCat = 'C',
	  FullCat = 'CS'
	; CCat1 = 'P' ->
	  CCat = 'ADV',
	  FullCat = 'ADV'
	; LastCat = 'N' ->
	  CCat = 'N',
	  ( FullCat1 = 'NNP' ->
	    FullCat = 'NNP'
	  ;
	    FullCat = 'NC'
	  )
	; LastCat = 'D',
	  domain('P',LCat) ->
	  CCat = 'P',
	  FullCat = 'P'
	; 
	 name_builder('~L',[['~w','+'],LCat],CCat),
	 name_builder('~L',[['~w','+'],LFullCat],FullCat)
	),
%	format('=> ~w ~w\n',[CCat,FullCat]),
	true
	.

conll_collect_cats_aux(N::node{ id => NId,
				cat => Cat,
				form => Form,
				cluster => cluster{ token => Token },
				lemma => Lemma },
			LCat,LFullCat) :-
	 ( node2op(NId,OId),
	    op{ id => OId }
	  -> true
	  ;
	    OId = '_'
	  ),
	(conll_cat(Cat,CCat,Lemma,N,Token) xor CCat = Cat),
	(conll_fullcat(Cat,CCat,OId,N,FullCat,Token) xor FullCat = CCat),
	( recorded( conll_forward(N,Next) ) ->
	  conll_collect_cats_aux(Next,_LCat,_LFullCat),
	  LCat = [CCat|_LCat],
	  LFullCat = [FullCat|_LFullCat]
	;
	  LCat = [CCat],
	  LFullCat = [FullCat]
	)
	.
