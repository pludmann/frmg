2010-03-11  Eric de la Clergerie  <Eric.De_La_Clergerie@inria.fr>

	* configure.ac, Makefile.am: updated release number to 2.0.1

2010-01-24  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Refined handling of COORDs.
	  Increased penalty on audience and similar incises.

	* easyforest.pl: Fixed bug on the conversion of chained COORDs,
	  inducing cycles.

2010-01-23  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* preheader.tag: Tried to switch to variance checking instead of
	  subsumption, but there seems to be a problem somewhere.

	* easyforest.pl: Added Named Entities in Passage format.

2010-01-12  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* m4: Added m4 directory

2010-01-02  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* tagml2html.xsl: updated to correctly display hypertags

2010-01-01  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easy2html.xsl: updated to display both Easy and Passage formats
	  (not perfectly for the Passage format).

	* Makefile.am, configure.ac, easyforest.pl, frmg_lexer.in,
	  main.tag, register_parserd.conf.in, restrictions.txt,
	  tag_generic.pl: Various optimizations or use of optimizations
	  provided by DyALog (-no_occur_check -compact)

	* frgram.smg: Added constraints to avoid spurious mixes of
	  enumerations and coordinations.

2009-11-20  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: integration passage compounds

	* Makefile.am, configure.ac, frgram.smg, missing.lex: updates

2009-11-13  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Penalized the new kinds of incises and fixed a
	  typo

	* easyforest.pl: Handling disambiguisation and conversion of the
	  new kinds of incises.

	* easyforest.pl, frgram.smg: Added
	  "reference" incise modifier, as in "... (Le Monde)"

	* frgram.smg: Try "audience" incise
	  modifier, as in "chers collegues, ...."

2009-11-12  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.map, main.tag: adpatation for lefff_new

2009-11-10  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl, register_parserd.conf.in: Small changes

2009-11-06  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Less memory and faster.

	* easyforest.pl: Use of backtrackable mutables whenever possible.

2009-11-05  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: Removed artefact notion
	  of participials with a subject. Removed usage of 'il' as ilimp.
	  We now assume ilimp has a good recall.

	* easyforest.pl: Some cleaning to be more efficient and limit
	  memory needs.

	* easyforest.pl: more efficient has_shared_derivs between 2 edges

2009-11-04  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl, frgram.map, main.tag: Handling of functions for
	  verb 'faire'

	* easyforest.pl, frgram.map, frmg_lexer.in: Propagation of Lefff's
	  functions in hypertags and use for the Passage conversion.

	* restrictions.txt: Remove some words

	* frgram.smg: adjPref may now modify an adjective

	* easyforest.pl: Rationalization of the search for subjects with
	  the notion of deep_subject

2009-11-03  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Added a mechanism to split some tokens returned
	  by SxPipe (namely dates) in order to express internal relations.

	* easyforest.pl: tuning on PP-attachement and MOD-V vs MOD-P

2009-11-02  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: light tabulation for relation{}

	* easyforest.pl, frmg_lexer.in, missing.lex: Tuning robust mode

	* addons.tag, easyforest.pl, main.tag: Tuning in robust mode

2009-11-01  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Fixed a bug in MOD-V for robust mode

	* easyforest.pl: Fixed confusion between subjects and objects in
	  wh-sentences.

2009-10-31  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Added a new prep_locution

	* easyforest.pl, frgram.map, frmg_lexer.in: Propagation control
	  information and use of prep_locutions for better SUJ-V

	* main.tag: Increase range from 100 to 1000 in autoload.

	* frgram.smg: a coma may be a final punctuation to handle some
	  strange sentence segmentations.

	* addons.tag: special tree 'follow_coord_aux' for missing aux in
	  coordinations.

	* main.tag, postheader.tag, tag_generic.pl: special left-corner
	  constraints for some trees.

2009-10-29  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* main.tag, tag_generic.pl: Try more sophisticated autoload
	  filtering conditions

2009-10-27  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: use of feature 'etre' to restrict acomp on
	  participles.

	* main.tag: default value for new feature 'etre' on v.

	* complete.lex, frmg_lexer.in: Fixed bug on complete and added new
	  feature on v

2009-10-25  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: Fixed the use of acomp-verb with participle
	  adjectives (and not just adjectives) as in "ce choix semble
	  justifie".

2009-10-24  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl, rx.pl, rx_c.c: Try a hack to cut very long
	  derivation list. We are no longer sure to get the real best
	  parse.

2009-10-23  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl, rx.pl, rx_c.c: More efficient XML processing in
	  easyforest.

	* frgram.smg: Should cover more adjectival constructions

	* easyforest.pl: Fixed bug in SUJ-V and tried to improve recall.

	* easyforest.pl: Fixed bug in MOD-N

2009-10-22  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Try to correct some mistakes in previous commit.

	* frgram.smg: May now compile with the new adjectival
	  constructions, thanks to the modifs in DyALog

2009-10-20  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: * updated wrt the new impersonal adjectival
	  constructions * some rationalization with get_head

	* frgram.smg: First test of verbal-like constructions anchored by
	  adjectives

	* addons.tag: try relaxed nominal trees to use in robust mode

	* easyforest.pl: added new restriction selections and passage
	  compound forms

2009-10-17  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Special cases for SUJ-V and OBJ-V

2009-10-14  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Added conversion rule MOD-V for time_mod

	* easyforest.pl: Find better heads for some relations.
	  Introduction of predicate get_head/2 to do that.

	* frgram.smg: Fix on DATE_arto

	* easyforest.pl: Test better control on coordinations

	* easyforest.pl: Again a fix on 'tout'

2009-10-13  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Fix on 'tout'

	* easyforest.pl: * some adaptions for the revised guide v2 * fixed
	  bug related to ellided forms * some tests with selection
	  restrictions

2009-10-11  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Another set of weights for the selection
	  restrictions

2009-10-10  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Test with may selection restriction extracted
	  from Europarl.

2009-10-06  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl, missing.lex: * track some missing relations

2009-10-05  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: * more uses of the notion of node!first_main_verb

	* addons.tag, frgram.map, tag_generic.pl: * refine constrainte on
	  strace

2009-10-03  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl, frgram.smg: relaxed agreement constraints for
	  'vous' and past participle

	* complete.lex, easyforest.pl: * 'voici' and cousins as imperative
	  verbs * new disambiguation/conversion rules for APPOS and JUXT

	* easyforest.pl: * slightly favor post-adjectives * fix pb around
	  coords

2009-10-02  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl, frgram.smg: * post-position for autant as
	  comparer * fixed for ante adjective preferences

	* easyforest.pl, frgram.smg, missing.lex: * Added notion of
	  comparer in post-position for 'plutot'. Should also apply to
	  'autant'. * Added notion of ante_adj_pref for some adjective

	* frgram.map, frgram.smg, main.tag: More restrictions on the use
	  of pronouns as modifier.

2009-10-01  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* Makefile.am, frgram.smg: Added restrictions for participales as
	  acomp

	* easyforest.pl, frgram.smg, frmg_lexer.in, main.tag, missing.lex,
	  restrictions.txt: Various modifs, including the possibiity for
	  acomp to be a participial.

	* frgram.smg: added S+participle as acomp

2009-09-30  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg, frmg_lexer.in, missing.lex: More robustness for
	  short sentences

2009-09-22  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Some rules about causatives.

	* frmg_lexer.in: Misc. on subcat translation

	* frgram.smg: Changed kind into supermod_kind for supermod

2009-08-28  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: * allowed 'pour-sinf' constructions for verbs

	* easyforest.pl: * Refined restriction selection * Favour modifier
	  factorization through coordinations

	* frgram.smg: Many modifs, including
	  * preliminary subcat handling on nc and adj
	  * special treatment for 'comme'
	  * ....

	* restrictions.txt: Removed 'politique' as a v

	* missing.lex: Many new entries for titles and nc/adj with scompl
	  arguments

	* main.tag: better normalize for adv.

	* frgram.map: Added cat 'title', similar to nc

	* easyforest.pl: Many modifs, including
	  * more subcats on nc and adj
	  * better handling of comparative
	  * selection restrictions on nouns and adjs

2009-08-20  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Various modifs aroun the notion of 'composed
	  forms', trying to be compatible with both EASy and new Passage
	  rules. Still, some aspects are not satisfactory (parses should
	  not depend of some convention -- Lefff is providig info about
	  composed forms and we try to forget it to please Passage !)

	* missing.lex: added suffisamment comme predet

2009-08-17  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: * Added an extra construction for predet on pronoun
	  (nombre d'entre nous) * Relaxed condition to attach vmods on
	  post-verbal subjects (pv subj behave as other verbal arguments)

	* addons.tag, main.tag: Better handling of robust parses.

	* frmg_lexer.in: Try to correct some pbs coming frm sxpipe

	* missing.lex: Added combien as a predet.

	* easyforest.pl: Yet another way to handle disambiguation, now
	  relying more on OPs.

2009-07-15  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: - Exclude some useless features
	  - Added missing constraint on post verbal clitic subject

	* frgram.map: Exclude some useless features

	* frmg_lexer.in: Exclude some useless features

	* easyforest.pl: * Fixed bugs in edge cost computations * edge
	  cost information emitted with Passage format, for learning
	  experiments

2009-07-06  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* tsnlp.txt: prefixed with '*' some incorrect sentences.

	* frgram.smg: - Fixed some conditions about CS, in particular
	  allowing conditional for que-CS - Changed clitic ordering for
	  post-verbal clitics (for donne-le-moi)

	* easyforest.pl: Added PERSON_m and PERSON_f as named entities.

	* missing.lex: Added dieu as np until fixed by sxpipe

	* frmg_lexer.in: Normalize PERSON2 (and similar forms) into PERSON.

2009-07-03  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: Various extensions, including:
	  * revision of CS handling (with que seen as csu, and use of
	  attribute 'que' on csu)
	  * more flexibility on realizations for impsubj
	  * new notion of antepro realization for obj and acomp, used for
	  words such as rien or tout in 'ne rien manger' or "n'avoir rien
	  mange"

	* frmg_lexer.in: * Leff attribute 'wh' on csh is renamed into
	  'que', taking the following three values {-,+,que}.
	  * added new realization 'antepro' for obj and acomp, used for
	  words such as rien or tout in 'ne rien manger'

	* frgram.map: * Attribute 'wh' on csh has been renamed into 'que',
	  taking the following three values {-,+,que}.

	* main.tag: * Attribute 'wh' on csh has been renamed into 'que',
	  taking the following three values {-,+,que}.
	  * Removed cleaning rule used to glue quotes with words.

	* missing.lex, restrictions.txt: Added 'que' as csu, in addition
	  of que being of cat 'que'

	* easyforest.pl: * Fixed pb in number entities
	  * Added que in closed categories
	  * Penalties on N2 adjunct after a relative
	  * Try to resize groups and forms to remove some punctuations

2009-06-22  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Penalize new quoted_sentence_* constructions. They
	  are not yet stable.

2009-06-21  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: taking into acount that an xcomp edge may point to
	  an empty 'S' node (and not always a 'v' node)

2009-06-20  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Disamb rule for quoted_sentences as modifier

2009-06-15  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* addons.tag, easyforest.pl: Changed disambiguisation parameters,
	  related to _EPSILON, lexicals, ... But no yet OK.

2009-06-12  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Constraints on entities

	* frmg_lexer.in, main.tag, missing.lex: * the lexer deletes the new
	  semantic comments attached to entities
	  * remove the mechanim to build larger NPs from the one returned
	  by sxpipe
	  * fix AUT_CL

2009-06-11  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Even more penalties for COMASEP.

	* main.tag: More flexibility when merging proper nouns together

2009-06-10  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* missing.lex: * Added missing quote markups

2009-06-08  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* addons.tag, frmg_lexer.in, missing.lex, tag_generic.pl: * better
	  handling of epsilons
	  * preliminary handling of _MARKUP as epsilon

	* addons.tag, easyforest.pl, frgram.map, frgram.smg, frmg_lexer.in,
	  restrictions.txt, tag_generic.pl: * Better handling of _EPSILON
	  for addons trees.
	  * Preliminary handling of impersonal clitics ilimp and caimp

2009-05-26  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: * Modified the desambiguation algorithm. A local
	  best solution is now found for a node, given an incoming edge, a
	  span for the node, and a span for the parent node.

2009-05-14  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Refined the desambiguation algorithm (an entering
	  edge may not be compatible with all op attached to the target
	  node, even taking into accout a target span).

2009-05-04  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: Fixed lexical value for postverbal clitic -il

	* easyforest.pl: Added a sentence id attribute on dependency.

	* frmg_lexer.in: By default, now keep ident for tokens.

2009-04-11  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: Completed coord to handle pre-coord such as ni, in ni
	  X ni Y. But there is a bug somewhere in mgcomp.

	* frmg_lexer.in: No longer need special entries for voici, voila,
	  .... They are handled by frmg_lexer.

	* missing.lex, restrictions.txt: No longer need special entries for
	  voici, voila, .... They are handled by frmg_lexer.

2009-04-10  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: More syntactic constructions
	  * clefted sentences on verb modifiers (c'est demain qu'il vient)
	  * some narrative constructions seen as a topicalization on a
	  scomp arg.

	* postheader.tag: Use some prediction on non-terminal S (but not
	  sure it is worth it)

	* Makefile.am: Use of new mgcomp option -o <file>

2009-04-09  Benoit Sagot

	* frgram.map: liste de valeurs plus grande pour le fset 'prep'
	  (compatibilite avec lglex)

2009-04-09  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.map: Added jusqu'a dans prep

	* LefffDecoder.yp: Added char ' in realargs

	* tag_generic.pl: * debugging info
	  * testing filtering on the number of continuations per position.

	* preheader.tag: Changed arity of check_ok_cat to get more
	  debbuging information.

	* frgram.map: Tried to position the feature 'extraction' in the
	  first ones of 'S'. Does not seem to improve.

	* restrictions.txt: Removed restrictions on 'ce'

	* frmg_lexer.in: * exclude 'clive' feature on adv
	  * avoid emitting duplicate entries

	* missing.lex: * removed some entries

	* easyforest.pl: * Completed checks for the coherence of the
	  selected derivations (there seems to remains an error somewhere:
	  one error out the 4000 EASy sentences)
	  * Fixed typo on the emmitting of +
	  * Added rules for 'ce'
	  * Added rule for number ranges
	  * Refined handling of clefted sentences

2009-04-01  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Fixed various bugs to be able to produce more
	  correct disambiguated DEP XML files. In particular, fixed a bug
	  allowing to select incompatible derivations.

	* frmg_lexer.in, register_parserd.conf.in: Added handling of option
	  -flush to frmg_lexer to force stream flushing

2009-03-27  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frmg_lexer.in: Suppressed intial warning from lexed

2009-03-26  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: Many changes relatives to comparative and impersonal
	  subjects

	* frgram.map: Removed feature diathesis from aux

	* frmg_lexer.in: Removed use of recode (because changed in sxpipe).
	  Removed feature diathesis for aux.

	* complete.lex: Added new info on 'autant' et 'aussi' (equalizing
	  adverbs)

2009-03-20  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* tag_generic.pl: Refined tag_filter (for the Left Corner
	  relation).

	* LefffDecoder.yp, frmg_lexer.in, missing.lex: Updated to reflect
	  the change of delimiters in Lefff (from ' to ")

2009-03-18  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frmg_lexer.in: Added info on realizations

2009-03-17  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.map: Added diathesis on aux cat

	* frgram.smg, frmg_lexer.in, main.tag, missing.lex, postheader.tag:
	  Various changes:
	  * added subcat handling on preposition (allowing optional
	  argument)
	  * added subact handling on adverbs (when modifying sentences)
	  * added preliminary and incomplete support for causative, using a
	  new kind of verb argument 'vcompcaus' and new feature 'control'
	  on 'S'.

	* easyforest.pl: Some changes in the disambiguisation process

2009-03-06  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.map, frmg_lexer.in: Added more robustness on decoding
	  errors (by using a wrapping eval {}) Completed pcas list for
	  lglex

2009-03-01  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* tag_generic.pl: Use new anchor feature in ht{} to avoid spurious
	  ambiguities in verbose!struct

	* postheader.tag: Changed prediction step for 'S"

	* frmg_lexer.in: Tmp hack to avoid pb on 'A' not recognized as
	  prep by sxpipe.

	* addons.tag, frgram.map, frgram.smg, main.tag: New constructions
	  for predet + more restrictions on the use of enum, coordination
	  and apposition on N2. Fixed some pb in clean_sentences Added
	  arg. anchor in ht{}

	* missing.lex: Added some missing entries in Lefff. Tried a new
	  notion of predet (for 'beaucoup de', 'plein de', ...)

	* easyforest.pl: Adaptation to the modified notion of local
	  derivation for dependencies (see forest_utils).

2009-02-04  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Lexical nodes in depxml format are again emitted.
	  Still a pb for their op.

2009-02-03  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* addons.tag: A test

	* frmg_lexer.in: Removing of trail part in lemma.

	* frgram.smg: Several modifs, including the experimental
	  possibility to adjoin sentences with more ellipsis in S_coord
	  (such as in "la pomme que Paul veut et mange")

	* Makefile.am: added handling of postheader.tag
	  better set of make dependencies (no longer try to compile
	  addons.tag before building header.tag)

	* easyforest.pl: New rules and weights.

	* postheader.tag: Added a postheader to be able to add modulations
	  on non-terminals (with :-tag_mode(...))

2009-01-27  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Deep rewriting of the desambiguation process (for
	  both full and partial parses) and of the Passage conversion. The
	  result is much more efficient and should be more accurate. The
	  conversion process still produce a few errors when tried on
	  easydev, with relation anchors being not related to existing
	  word forms.

	* rx.pl, rx_c.c: rx!tokenize now accepts a 4th arg (a tail list)

	* main.tag: Better definition of partial parse covering in robust
	  mode

	* frgram.smg: Added more constraints on relnom_as_noun.

2009-01-06  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl, register_parserd.conf.in: Completed handling of
	  Passage output format.

2009-01-05  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Preliminary handling of Passage output format
	  (with option -passage)

2008-12-11  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frmg_lexer.in: Fixed handling of information for some clitic
	  realizations (en,y)

2008-11-30  Benoit Sagot

	* frmg_lexer.in: Aussi bizarre que ca puisse paraitre, les Loc
	  sont parfois des *-sinf ou des *-scompl

2008-10-21  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg, main.tag, style.css, t/tree_nb.t, tag_generic.pl:
	  Various modifs about superlatives, about the CSS style for
	  trees, and about the handling of non-verbal full parses.

2008-09-17  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* Makefile.am, frgram.map, frgram.smg, frmg_lexer.in, main.tag,
	  missing.lex, t/tree_nb.t: * Several extensions and corrections of
	  the meta-grammar.
	  * Fixed sxpipe unquoting in frmg_lexer
	  * Use of the new dump/restore mechanism provided by mgcomp

2008-09-09  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg: * Fixed some pb with locative arguments (as in
	  'resider a Paris')
	  * Reduced some sources of spurious ambiguities (for instance, on
	  relatives)
	  * Added relatives without antecedent.

	* main.tag, preheader.tag, tag_generic.pl: Updates to better
	  exploit Left-Corner handling

	* frmg_lexer.in: Fixed handling of locative prep and of some
	  special chars (*,+,?,")

2008-09-03  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* tag_generic.pl: Simplified handling of disjunctive lexical
	  values.

2008-09-01  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* frgram.smg, tag_generic.pl: Use of disjunctive lexical nodes.

2008-08-27  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Updated to take into account the modifs into
	  forest_utils (mandatory edges)

	* frgram.smg: Slight extension of coordinations: all of them now
	  allows an optional coma before the coo.

2008-08-26  Eric de la Clergerie <Eric_De_La_Clergerie@inria.fr>

	* easyforest.pl: Fixed pb related to recent modifs in forest_utils
	  (we no longer ensure
	  that nodes precede edges in DEP XML).

	* INSTALL, Makefile.am, frgram.smg: Udpated Makefile.am to include
	  frgram.tag.xml in the distrib Added constraints to reduce
	  ambiguities for enumerations

2008-08-25  Isabelle Cabrera

	* Makefile.am: frgram.tag.xml rather in BUILT_SOURCES

	* Makefile.am: include frgram.tag.xml in release

2008-08-21 1.2.0
	- trying to rationalize the names of various files.
          * lexer: frmg_lexer (ex easy.pl)
          * parser: frmg_parser (ex tig_parser)
          * parserd registration: register_parserd.conf (ex parserd.conf)
	  tag_parser is removed (not used for a very long time)
	- udpated dependency to DyALog 1.12.0
	- Fixed a pb on the handling of prefixes and suffixes
	- New expanded version of the metagrammar.
	- Modifs to be able to use the new -loop mechanisms with parsed on the
	  parsers from frmg
	- Removed some spurious ambiguities about saturated/non saturated
	  sentences.
	- No longer need for small_header.tag (and presmall_header.tag)
	- More explicit error messages in LefffDecoder and more complete
	  condition in easy.pl to be sre everything from lexed is being
	  fetched
	- Small modif in main to reduce spurious ambiguities
	- Handling of ':' as final ponct (to follow sxpipe conventions)
	- Fixed spurious ambiguities when no clr
	- Removed construction of deprecated 'lexer'
	- Removed cat_normalize on v. Now handled at the level of
	  lefff-frmg.
	- Better handling of tag_filter.
	- New handling of unknown words, avoiding the use of a finite set on
	  cats that raises some problems in relation with cat_normalize.
	- Fixed a pb on nc coord. Completed handling of adj after noun (now
	  attached on N2 rather than N, as done for biomg).
	- New Lefff Decoding grammar to be compiled with eyapp. Replace
	  lexed.gram which is no longer needed
	- Various optimization to speed-up the lexer (replacing Parse::RecDescent
	  by Parse::Eyapp) and the parser (using additional directives in
	  preheader.tag that trigger some recent optimizations of DyALog)
	- Modif on handling of xmldep element <deriv>

2007-04-21 1.1.0
	- Use of DyALog preliminary support for the computation of
	  tag_features constraints using DyALog option -analyze
	  tag_features. A new header file called 'features_header.tag' is
	  created with the analyze and used to compile the TIG parser.
	- Removed useless dependencies
	- Added scompl as possible realization for Attributes
	- Added topicalization of verb arguments with subject inversion as
	  in "rares sont les amis fideles."
	- Removed blocking on adj when in concurrence past participle
	- Better detection of COORD relations.
	- Updated to allow sentence id of the form E21.2, needed when long
	  EASy sentences are broken into smaller ones.
	- fix for some cases of wrapping adjunctions
	- added attribute form on nodes (related to modif in forest_utils)
	- added/refined several edge_elementary_cost rules
	- add regional_cost rule favoring coords with arguments of same
	   category
	- Completed/corrected treatement of wh-pronouns. They are now
	  handled through the construction of N2[wh=+,case=Case], allowing
	  potential adjunctions on them (as on N2). Also allows the
	  simplication of the extracted XGroup (less cases). Also added an
	  explicit case 'comp' for attributes (used for instance for
	  'comment' as in 'comment est-il ?').
	- Slight modifications to respect the new EASy DTD (no longer
	  'constituants').
	- Extended handling of attributes (acomp) : clitics 'le' and 'de Sinf'
	- Improved handling of partial parses, and handling of sentence
	  boundaries.
	- Modified various parameters for desambiguating and converting.
	- Extended support for comparatives.
	- Improved notion of best parse for partial parses.
	- Modified handling of coordination in relation with the EASy
	  conversion.
	- Added sbound as a sentence separator.
	- Improved recognition of lemma Identidiers (not alvays of the
	  form \w+____\d+)
	- Added option -test_decoder to ease debug of lexed information
	- New feature 'chunk_type' for epsilon.
	- Added extra field 'info' when decoding Lefff 3 information.
	- Added preliminary handling of node attribute 'w' used to provide a
	  vector of weights for input forests. Option -weights may be used to
	  exploit or not these weights.
	- Changed XML attr a'-propager into a-propager to be OK with the
	  EASy DTD
	- Use of new DyALog option -noforest to reduce memory costs.
	- Some modifs in forest disambiguator and converter.
	- Fixed wrt new lefff cats 'parento' and 'parentf' (rerooted to old
	  ponctw).	
	- Treatment of impersonals.
	- PP are now attached to 'N' rather than 'N2'.
	- use  dag2udag instead of unfold_dag (sxpipe)
	- The XML Dep disambiguated output now emits back Feature
	  Structure content (for op elements).
	  Also emits mode information (robust or not).
	- Fixed depxml output for partial forests (robust mode)
	  Added new disambiguation rules
	- Partial rewriting of the desambiguation algorithm.
	  Revision of EASY conversion rules
	  Addition of a DEP XML disambiguated output that may be offered
	  as a new service for frmg parsers.
	- Fixed some missing realizations for some grammatical functions and
	  added restrictions for reflexive pronouns 'se' (no longer of cat clar
	  or cldr).
	- Added a few more trees, changed handling of reflexives, extended
	  notions of sentence starters, ...
	- Modif for preps and Time GN used as adverbs, related to Lefff > #914
	- Small fix on use of recode
	- Strengthened agreement constraints on ancestors of v
	- Improved HTML view of trees
	- testparser was moved into parserd with new name callparser
	- Added test: count number of trees
	- register_parsers after install
	- Removed gtig_parser + few renames$
	- Exclusion of a new feature from Lefff on adv
	- Tmp fix for bad segmentation on 'est-ce que' (waiting for
	  text2dag to be updated)
	- Added a mechanism to first test verbal sentences and then, on
	  failure, non verbal sentences.  
	- Refined some constraints
	- Prelimary modifs for a more efficient handling of 'VMod' (less
	  features)
	- moved mg*.xsl files to package mgtools
	- Started to fix some pb in verb kernel.
	- Improved HTML view of trees with tree.pl and Javascript
	  libraries prototype.js and tree.js
	- Fixed bug with underspecied verbal entries that could trigger
	  lightverb constructions and block standard ones.
	- Trailers for lexids may end with __error in lefff
	- Minor correction related to forest display
	- Changed DLoc into Dloc to be coherent with Lefff
	- Added frmg.pc.in for pkg-config
	
2006-12-11 1.0.2
	- Various modifications to follow new conventions in Lefff #svn > 894
	- Added restrictions for 'que' and "c'", but would be better to
	  modify in the grammar.
	- Tried several things related to encoding. The conclusion is that
	  easy.pl should be called with Latin1 encoding to work properly.
	- Removed use of strdupa which is too GNU dependent.
	- Tried new definition for lemma identifier
	- Translate new 'ce' POS into 'det'
	- Small fixes due to port to Mac OS where some GNU C functions are
	  missing
	- Fixed pb in lexer and added handling of word prefixes in grammar
	  (adjPref and advPref)
	- Several modifs to adjust to modifs in Lefff
	- make install will no longer overwrite an existing config file
	  frmg.conf but install it as frmg.conf.distrib
	- Small fix to ensure that automake-dyalog is used in place of
	  automake when rebuilding Makefile.in
	- Refined quote procedure
	- Removed hard-coded path to sxpipe command.
	- Accept MAF entries without morpho-syntactic info
	- testparser no longer dies when unable to connect to a
	  dispatcher. It just sleeps and retries.
	- Now accept MAF wordForms without explicit Lefff entries
	- Updated to handle MAF format

2006-03-24 1.0.0
	- Added more flexibility to find config file
	- Updated path to default dicodir (now=lefff-frmg
	- Preliminary move to decode new Lefff format
	- Added correct dependance to dyalog-sqlite in Makefile.am
	- Update in easy.pl and testparser to handle MAF as input
	- Updated version of desambiguator/converter for EASy. Heuristics
	  rules have been named to help debugging.
	- Extended easy.pl to be able to take XML MAF files as input. Have
	  to check versus the latest version of MAF draft and versus the
	  various variants of MAF (compact, standoff, reduced FSM, ...)
	- Mixed partial and full parsing in same process.
	- Improved HTML view of grammars
	- Modification of the way option '-robust' is handled: full
	  parsing is first tried and if failure, robust parsing is then
	  tried whithout restarting the parser.
	- Added skipping actions over _EPSILON tokens now provided by sxpipe
	- A few corrections in easyforest.pl
	- Added finite set 'lightverb' to handle support verbs
	- Fixed bugs on token numbering when not inherited from EASy
	  comments
	- Fixed easyforest to emit proper XML (in particular for entities),
	  without empty groups.
	- Completed conversion rules (for NUMBER and ncpred)
	- Added option to select sxpipe command (for instance sxpipe-easy) and
	  fixed some minor bugs.
	- Fixed bugs related to the fact that dependency edges are now
	  associated to derivations.
	- Fixed handling of ncpred's
	- Updated EASy conversion (due to changes in dependency forest
	  format)
	- Displaced the position of ncpred. It is now almost like other
	  verbal argument, below V but before all other verb arguments. It
	  can be negated bu advneg.
	  ex: il ne fait que peu attention a sa sante
	  It seems mostly OK, except that it blocks cleft extraction on
	  arg1 and arg2. Don't know why !
	- Added preliminiary support for handling support verb, ie (verb +
	  ncpred) 
	      * ncpred is seen as a coanchor and is part of the verbal
	        kernel
	      * the verb steals the subcat frame of ncpred
	        the way it is done is kind of hack that has to be improved !
	      => raises pb with autoloading (that have been partially
	         solved)
	- Tried to find an ad-hoc solution for strange behaviour of
	  Data::Compare when loaded from a directory where the user can't
	  access (typically nobody in parser server)
	- Updated to accept maf dags. MAF dags should be sent as follow:
	  ## MAF BEGIN
	  <XML maf dag>
	  ## MAF END
	- Udpated and fixed to handle MAF format as input.
	- Updated to run as a client or dispatch.pl
	- Udpated to take into account format modifications in lefff.
	- Fixed pb in verbose!coanchor
	- Added watch file mechanism. Testparser stops whenever some file
	  is no longer present and save its current status.
	- Fixed bugs related to the use of sxpipe and dags
	- Fixed emit of lemma info for coanchors.
	- Emit lemma information in shared forests.
	- Updated script wrt to sxpipe
	- Use of sxpipe with (default) option -sxpipe
	- Fixed to have DAG positions starting at 0 for DAGs generated
	  from sxpipe
	- Add support for sxpipe DAG format
	- Added dotdep format.
	- Started updating LateX converting.
	- Revised version for easyforest, much more effecient
	- Added construction M. Paul and coordination M. et Mme Paul
	- Added PP attachement on pri
	- Activation of more cleft constructions
	- A few extensions to the grammar and improvment of easyforest
	- Corrected test of existing result files
	- Small corrections in the grammar
	- Skip analysis if all outputs files already present
	- Added new linguistic construction (interjection, sequences of
	  sentences ...)
	- Adapations to Pierre & Benoit's dagger
	- Various extensions of the segmentation process including new
	  local grmmars (title,tel,mail)
		
2004-12-08 0.0.4
	- Various cleanings and adaptions to EASY data
	- Partial description of cleft sentences
	- Allow substitution

2004-12-07 0.0.3
	- Update version number 0.0.3
	- Add header.tag in data files
	- Transform easy.pl at install time
	- Use substitutions at install to determine private perl modules
	  location

2004-12-06 0.0.2
	- Bumped version number
	- Cleanup flags
	- New data files
	- data files in , dico files in
	- Run substitutions through make instead of autoconf
	- Requires dyalog 1.10.6
	- Add MyRegExp.pm to distributed files
	- Tmp version (cleft not yet correctly working. Big questions
	  about the use of top ang bot attributes that may lead to a deep
	  rewrite)
	- Don't distribute secondary sources
	- Fix call to tag_converter
	- Set installation path
	- General clean-up
	
2004-11-25 0.0.1
	- Preliminary support for using EASY lexer
	
