/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015, 2016, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  passage.pl -- PASSAGE schema
 *
 * ----------------------------------------------------------------
 * Description
 * Conversion and Emission for PASSAGE schema (XML format, -passage), and the EASy variant (-easy)
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.
:-require 'best.pl'.

:-features(f,[cid,id,lex,rank,tid]).
:-features(groupe, [id,type,content,left,right] ).
:-features(relation, [id,type,arg1,arg2,arg3] ).

:-extensional
      f{},
  groupe{},
  used/1
.

:-light_tabular relation{}.
:-mode(relation{},[+|-]).

:-finite_set(const,['GN','GA','GP','NV','GR','PV']).

:-finite_set(rel,[ 'SUJ-V',	% sujet-verbe
		   'AUX-V',	% auxiliaire-verbe
		   'COD-V',	% cod-verbe
		   'CPL-V',	% complement-verbe
		   'MOD-V',	% modifier verbe
		   'COMP',	% complementeur: csu->NV, prep->(GN,NV)
		   'ATB-SO',	% attribut sujet/objet  
		   'MOD-N',	% modifier nom
		   'MOD-A',	% modifier adjectif
		   'MOD-R',	% modifier adverbe
		   'MOD-P',	% modifier preposition
		   'COORD',	% coordination
		   'APPOS',	% apposition
		   'JUXT'	% juxtaposition
		   ]).

% defined in depxml.pl
:-light_tabular source2wid/3.
:-mode(source2wid/3,+(+,+,-)).

%% defined in depxml.pl
:-light_tabular target2wid/3.
:-mode(target2wid/3,+(+,+,-)).

:-xcompiler
dependency_convert_to_passage :-      
    every(( word(0) )),
    verbose('Done word\n',[]),
    (opt(passage) ->
	 split_some_clusters	% some differences bewteen Passage and Easy about compounds
     ;
     true
    ),
    every(( % fail,
		 domain(Type,['NV','GN','GA','GR','PV','GP']),
		 verbose('Try build group ~w\n',[Type]),
		 build_group(Type),
		 verbose('Done build group ~w\n',[Type]),
		 true
	     )),
    verbose('Done group\n',[]),
    every(( extract_relation( relation{ type => rel[] } )))
.
    
:-std_prolog easy_emit/0.

easy_emit :-
	emit_multi('EASY'),
	sentence(SId),
	recorded(mode(Mode)),
	( recorded(has_best_parse) ->
	  Best = yes
	;
	  Best = no
	),
	event_ctx(Ctx,0),
	Handler=default([]),
	xevent_process(Handler,start_document,Ctx,Handler),
	xevent_process(Handler,xmldecl,Ctx,Handler),
	xml!wrapper(Handler,
		    Ctx,
		    'DOCUMENT',
		    [id='frmg',
		     xmlns!xlink= 'http://www.w3.org/1999/xlink'],
		    xml!wrapper(Handler,
				Ctx,
				'E', [ id:SId, mode:Mode, best: Best ],
				%% Constituants and relations
				xevent_process(Handler,[constituants,relations],Ctx,Handler)
			       )
		   ),
	xevent_process(Handler,end_document,Ctx,Handler),
	format('\n',[]),
	true
	.

:-std_prolog passage_emit/0.

passage_emit :-
	emit_multi('PASSAGE'),
	sentence(SId),
	recorded(mode(Mode)),
	( recorded(has_best_parse) ->
	  Best = yes
	;
	  Best = no
	),
	( Mode == full ->
	  Trust = 100
	; Mode == corrected ->
	  Trust = 90
	; Mode = robust ->
	  Trust = 50
	),
	event_ctx(Ctx,0),
	Handler=passage([]),
	xevent_process(Handler,start_document,Ctx,Handler),
	xevent_process(Handler,xmldecl,Ctx,Handler),
	xml!wrapper(Handler,
		    Ctx,
		    'Document',
		    [id='frmg',
		     dtdVersion='2.0',
		     xmlns!xlink= 'http://www.w3.org/1999/xlink'],
		    xml!wrapper(Handler,
				Ctx,
				'Sentence', [ id:SId, mode:Mode, best:Best, trust: Trust ],
				%% Constituants and relations
				( xevent_process(Handler,
						 ['TWG',relations,entities],
						 Ctx,Handler),
				  every( ( '$answers'(edge_cost(EId,W,Ws,_Cst)),
					   ( recorded(keep_edge(EId)) ->
					     Kept = yes,
					     E::edge{ id => EId,
						      label => Label,
						      source => node{ cat => SCat,
								      id => SNId,
								      cluster => cluster{ id => SCId,
											  left => SLeft
											}
								    },
						      target => node{ cat => TCat,
								      id => TNId,
								      cluster => cluster{ id => TCId,
											  right => TLeft
											}
								    }
						    }
					   ;
					     Kept = no,
					     recorded( erased(E) )
					   ),
					   ( SLeft < TLeft ->
					     Dir = right
					   ;
					     Dir = left
					   ),

					   source2wid(SNId,SCId,SWId),
					   target2wid(TNId,TCId,TWId),

					   every(( recorded(opt(cost)),
						   event_process(Handler,
								 element{ name => 'cost',
									  attributes => [eid:EId,
											 kept:Kept,
											 w:W,
											 ws:Ws,
											 info: [Dir,SCat,Label,TCat,SWId,TWId],
											 source: SCId,
											 target: TCId
											]
									},
								 Ctx)
						   )),
					   
					   true
					 ))
				  )
			       )
		   ),
	xevent_process(Handler,end_document,Ctx,Handler),
	format('\n',[]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% loading easy disamb hints

:-std_prolog load_easy_disamb_hints/0.

load_easy_disamb_hints :-
	W = 5000,
	every(( sentence(SId),
		( recorded(easy!group(SId,Left,Right,GType,First_Left:First_Right,Last_Left:Last_Right)),
		  ( GType = 'GP' ->
		    record_without_doublon( extra_node_cost( node{ cat => prep,
								   cluster => cluster{ left => First_Left,
										       right => First_Right
										     }
								 }
							   )
					  )
		  ; GType = 'PV' ->
		    record_without_doublon( extra_node_cost( node{ cat => prep,
								   cluster => cluster{ left => First_Left,
										       right => First_Right
										     }
								 }
							   )
					  ),
		    record_without_doublon( extra_node_cost( node{ cat => v,
								   cluster => cluster{ left => Last_Left,
										       right => Last_Right
										     }
								 }
							   )
					  ),
		      true
		  ; GType = 'GN' ->
		    record_without_doublon( extra_node_cost( node{ cat => nominal[],
								   cluster => cluster{ left => Last_Left,
										       right => Last_Right
										     }
								 }
							   )
					  )
%		  ; GType = 'NV' -> fail
		  ;
		      true
		  ),
%		  ( GType = 'NV' -> fail ; true),
%		    fail,
		    record( extra_chunk_cost(Left,Right,GType,W) ),
		    true
		  ;
%%		  fail,
		  recorded(easy!rel(SId,Source:_,Target:TargetRight,RType)),
		  ( RType = rel['SUJ-V','MOD-V','CPL-V','COD-V','ATB-SO','AUX-V'] ->
		    SCat = cat[v,aux]
		  ; RType = rel['MOD-N'] ->
		    SCat = nominal[]
		  ; RType = rel['MOD-A'] ->
		    SCat = cat[adj]
		  ; RType = rel['MOD-R'] ->
		    SCat = cat[adv,advneg]
		  ; RType = rel['MOD-P'] ->
		    SCat = cat[prep]
		  ; RType = rel['COMP'] ->
		      fail
		  ;
		    true
		  ),
		    ( RType = rel['AUX-V'] ->
			TCat = cat[aux],
			Type = adj,
			Label = label['Infl',aux]
		    ;	RType = 'SUJ-V' ->
			Label = label[subject,impsubj],
			Type = edge_kind[subst,lexical],
			every((
			       edge{ source => node{ cat => v, cluster => cluster{ left => _Source } },
				     target => node{ cat => cat[v,aux], cluster => cluster{ left => Source } },
				     type => adj,
				     label => label['V','Infl',aux,modal,'mod.xcomp']
				   },
			       record_without_doublon(
						      extra_elem_cost(
								      edge{ source => node{ cat => cat[v,aux],
											    cluster => cluster{ left => _Source }},
									    target => node{ cat => cat[v,aux],
											    cluster => cluster{ left => Target }},
									    type => Type,
									    label => Label
									  },
								      W
								     )
						     )
			      ))
		    ;	% RType = rel['CPL-V','MOD-N','MOD-A','MOD-R','MOD-P'] ->
		      RType = rel['CPL-V','MOD-N','MOD-A','MOD-R','MOD-P'] ->
			every((
%			       fail,
			       edge{ source => node{ cat => prep, cluster => cluster{ left => PrepLeft} },
				     target => node{ cluster => cluster{ left => Target, right => TargetRight } },
				     type => edge_kind[subst,lexical]
				   },
			       recorded( easy!group(SId,PrepLeft,_TargetRight,_Const::const['GP','PV'],_,_) ),
			       ( _TargetRight = TargetRight xor _Const = 'PV'),
			       record_without_doublon( extra_elem_cost(
								       edge{ source => node{ cat => SCat,
											     cluster => cluster{ left => Source }},
									     target => node{ %% cat => prep,
											     cluster => cluster{ left => PrepLeft }},
									     type => Type,
									     label => Label
									   },
								       W
								      )
						     )
			      ))
		    ;	RType = rel['COD-V'] ->
			Label = label[object,xcomp,ncpred],
			record( extra_elem_cost(
						edge{ target => node{ cat => cat[v,aux],
								      cluster => cluster{ left => Source }},
						      source => node{ cat => cat[v,aux],
								      cluster => cluster{ left => Target }},
						      type => adj,
						      label => label['V',modal]
						    },
						W
					       )
			  ),
			every(( Label = xcomp,
				edge{ source => node{ cat => v, cluster => cluster{ left => _Target } },
				      target => node{ cat => cat[v,aux], cluster => cluster{ left => Target } },
				      type => adj,
				      label => label['V','Infl',aux,modal,'mod.xcomp']
				   },
				record_without_doublon(
						       extra_elem_cost(
								       edge{ source => node{ cat => cat[v,aux],
											     cluster => cluster{ left => Source }},
									     target => node{ cat => cat[v,aux],
											     cluster => cluster{ left => _Target }},
									     label => Label
									   },
								       W
								      )
						      )
			      
			      ))
		    ;	true
		    ),
		    record( extra_elem_cost(
					    edge{ source => node{ cat => SCat,
								  cluster => cluster{ left => Source }},
						  target => node{ cat => TCat,
								  cluster => cluster{ left => Target }},
						  type => Type,
						  label => Label
						},
					    W
					   )
			  )
		)
	      )
	     )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Building words

:-std_prolog word/1.

word(_Left) :-
	verbose( 'Word at left=~w\n',[_Left]),
	%% find shortest (non empty non redirected) cluster closest from _Left
	(   C::cluster{ id=> Id, left => Left, right => Right, lex => Lex, token => Token },
	    _Left =< Left,
	    verbose('Found potential cluster ~E after ~w\n',[C,_Left]),
	    Lex \== '',
	    \+ ( cluster{ id => _Id, left => __Left, right => __Right, lex => __Lex },
		   \+ recorded( redirect(_Id,_) ),
		   __Lex \== '',
		   (   (__Left == Left, __Right < Right)
		   xor ( _Left =< __Left, __Left < Left)
		   )
	       ),
	    verbose('Found cluster ~E after ~w\n',[C,_Left]),
	    \+ recorded( redirect(Id,_) )
	->
	    every((
		   (Lex = [_|_] -> domain(TokenId,Lex) ; TokenId=Lex),
		   token_register(TokenId,Id)
		  )),
	    word(Right),
	    every((
		   C2::cluster{ id => Id2, left => Right, right => Right2, lex => Lex, token => Token2 },
		   agglutinate(Token,Token2,AggLex),
		   ( recorded( redirect(Id,_Id)) xor Id=_Id),
		   verbose('Register redirected ~w -> ~w [~w]\n',[Id2,_Id,AggLex]),
		   record_without_doublon( redirect(Id2,_Id))
		  )),
	    true
	;   
	    true
	)
	.

:-std_prolog token_register/2.

token_register(TId,CId) :-
	verbose( 'Word ~w in cluster ~w\n',[Lex,CId]),
	sentence(SId),
	'T'(TId,Lex1),
	( recorded(opt(passage)) ->
	  name_builder('~wT~w',[SId,TId],Label)
	;
	  name_builder('~wF~w',[SId,TId],Label)
	),
	( clean_lex(Lex1,Lex2) xor Lex1=Lex2 ),
	( recorded(f{id=> Label, cid => CId0}) ->
	  verbose('Redirect f label=~w lex=~w cluster=~E cid0=~w\n',[Label,Lex2,CId,CId0]),
	  record_without_doublon( redirect(CId,CId0) )
	;
	  update_counter(fids,Rank),
	  verbose('Register f rank=~w label=~w lex=~w cluster=~E\n',[Rank,Label,Lex2,CId]),
	  record(f{id=>Label, lex=>Lex2, cid => CId, rank => Rank, tid => TId})
	)
	.


:-extensional clean_lex/2.

clean_lex('\\?','?').
clean_lex('\\!','!').

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% BUG SOMEWHERE IN DYALOG on @*

:-light_tabular node!group/4.
:-mode(node!group/4,+(+,-,-,-)).

node!group(Nodes,Words,Left,Right) :-
	%%	format('Start ~w\n',[Nodes]),
	@*{ goal => ( _Nodes1 = [ N::node{ cluster => cluster{ id => Id,
							       left => CLeft,
							       right => CRight }}
				| _Nodes],
			( domain(Id,Words) ->
			    _Words1 = _Words
			;   
			    _Words1 = [Id|_Words]
			),
%%			format('HERE ~w ~w ~w\n',[_Nodes,_Left,_Right]),
			( CLeft < _Left -> NLeft = CLeft ; NLeft = _Left ),
			( CRight > _Right -> NRight = CRight ; NRight = _Right ),
			true
		    ),
	    %%	    from => 0,
	    %%	    vars => [Words],
	    collect_first => [Nodes,Words,1000,0],
	    collect_last => [[],[],Left,Right],
	    collect_loop => [_Nodes1,_Words1,_Left,_Right],
	    collect_next => [_Nodes,_Words,NLeft,NRight]
	  },
	%% format('End ~w ~w ~w\n',[Words,Left,Right]),
	true
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Building constitutants

:-light_tabular constituant/3.

constituant(Left,Right,Const) :-
	'$answers'( Const::group(Type,Left,Right,N,Content) ),
	verbose('Potential const left=~w const ~w\n',[Left,Const]),
	\+ used(Const),
	verbose('alive\n',[]),
	true
	.

constituant(Left,Right,Const) :-
	Const::cluster{ id => Label, left => Left, right => Right, lex => Lex },
	Lex \== '',
	\+ used(Label)
	.

:-std_prolog x_command/2.

x_command(N1::node{},N2::node{}) :-
	(   edge{ source => N1, target => N2 }
	;   edge{ source => N3, target => N1, id => EId1 },
	    edge{ source => N3, target => N2, id => EId2 },
	    have_shared_derivs(EId1,EId2)
	)
	.

:-light_tabular build_group/1.

build_group(Type) :-
	verbose('Try build group ~w\n',[Type]),
	group(Type,N::node{ cluster => Cluster::cluster{ id => WId }},Nodes),
	verbose('Potential group ~w ~w ~w\n',[Type,N,Nodes]),
	\+ used(WId),
%%	verbose('TEST0 group ~w ~w ~w\n',[Type,N,Nodes]),
	%% In passage, a redirected cluster may be part of several groups
	\+ ( opt(easy),
	     recorded( redirect( WId,WId1) ), % Needed to avoid void group
	     ( used(WId1)
	     xor recorded( redirect( _WId,WId1) ), used(_WId)
	     )
	   ),
	verbose('TEST group ~w ~w ~w\n',[Type,N,Nodes]),
	( Nodes = group(_,_Left1,Right,_,Content1) ->
	  Cluster = cluster{ id => Id, left => Left, right => Left1 },
	  _Cluster = cluster{ id => _Id, right => _Left1 },
	  ( recorded( redirect( _Id, Id) ) xor Id=_Id ),
	  ( domain(WId,Content1) ->
	    Content = Content1
	  ;
%%	    verbose('Try Left cluster filler ~E ~w ~w => ~w\n',[Cluster,_Left1,Content1,Content]),
	    cluster!add_left_fillers(Content1,Content,_Left1,Cluster),
	    verbose('Left cluster filler ~E ~w ~w => ~w\n',[Cluster,_Left1,Content1,Content])
%%	    Content = [WId|Content1]
	  ),
	  mark_as_used(Content)
	;   
	  node!add(N,Nodes,Nodes2),
	  node!add_fillers(Nodes2,Nodes3),
	  node!group(Nodes3,Content,Left,Right)
	),
	verbose('TEST2 group ~w ~w ~w\n',[Type,N,Nodes]),
	( Left \== Right xor recorded(splitted(WId,_)) ),
	verbose('TEST3 group ~w ~w ~w\n',[Type,N,Nodes]),
	group(Type,Left,Right,N,Content)
	.

:-std_prolog cluster!add_left_fillers/4.

cluster!add_left_fillers(Content,XContent,
			Right,
			LeftCluster::cluster{ id => WId, right => R} ):-
	(R=Right ->
	 XContent = [WId|Content]
	; cluster{ id => _WId, left => _Left, right => Right },
	 _Left < Right,
	 R =< _Left,
%%	 verbose('Try left filler Left=~w newright=~w Right=~w\n',[R,_Left,Right]),
	 cluster!add_left_fillers([_WId|Content],XContent,_Left,LeftCluster)
	)
	.
	

:-light_tabular group/5.
:-mode(group/5,+(+,+,+,+,+)).

group(Type,Left,Right,N,Content) :-
	verbose('GROUP ~w ~w ~w ~E ~w\n',[Type,Left,Right,N,Content]),
	mark_as_used(Content)
	.

:-rec_prolog group/3.

group('NV',N,Nodes) :-
	N::node{ cat => Cat::cat[v,aux], cluster => cluster{ left => LeftN, right => RightN }},
	\+ single_past_participiale(N),
	node!first_v_ancestor(N,P1),
	node!collect( (N1::node{ cat => Cat1,
				 lemma => Lemma1,
				 form => Form1,
				 cluster => cluster{ left=> Left1, right => Right1}}) ^
		    (
			node!terminal(N,cat[v,aux],['Infl','V',aux,modal]),
			node!terminal(P1,cat[v,adj],['Infl','V',aux,modal]),
			%% N is first aux or v
			%% get subject of older ancestor P or from N (post subject clitic)
			node!older_ancestor(N,cat[v,aux,adj],P,['Infl','V',aux,modal]),
			record_without_doublon( terminal_v(P,N) ), %% for the SUJ-V relation
			verbose('Terminal_v p=~E n=~E p1=~E\n',[P,N,P1]),
			%%			format('OLD ANCESTOR ~w => ~w\n',[N,P]),
			Cat1 = cat[cln,ilimp,caimp,pro],
			(   node!dependency(out,P,N1,_)
			;   N \== P, node!dependency(out,N,N1,_)
			; %% robust mode
			    recorded(mode(robust)),
			    node!all_neighbours(left,N,
						_Model::node{ cat => cat[cln,ilimp,caimp,clg,cla,cld,cll,clr,clneg,advneg,pro,advPref]},
						N1),
			    \+ (node!neighbour(right,N1,node{ cat => cln })),
			    true
			),
			( Cat1 = cat[cln,ilimp,caimp]
			;
			  Cat1 = pro,
			  domain( Lemma1, ['ça','ceci','cela','ce'] ),
			  Right1 =< LeftN,
			  \+ ( 
			       edge{ source => N1::node{},
				     target => N1_Dep::node{ cluster => cluster{ left => N1_Dep_Left}}
				   }
			     ,
			       N1_Dep_Left >= Right1
			     ),
			  true
			),
		     true
		    ;	
			node!terminal(N,cat[aux],['Infl','V',aux,modal]),
			%% N is first aux or v in a local chain
			%% (may be preceded by a modal verb)
			%% get all clitics from first v ancestor
			(   Cat1 = cat[clg,cla,cld,cll,clr,clneg,advneg,adv],
			    ( node!dependency(out,P1,N2::node{ cat => Cat1 },[E])
			    ;	recorded(mode(robust)),
				node!all_neighbours(left,N,_Model,N1),
				\+ (node!neighbour(right,N1,node{ cat => cln }))
			    )
			; 
			    N \== P1,
			    Cat1 = cat[adv,advneg],
			    ( node!dependency(out,N,N2,[E])
			    ; recorded(mode(robust)),
			      node!all_neighbours(left,N,_Model,N1)
			    )
			),
			%% only keep adv or advneg if left of N and find all dep. from advneg
			%% and only if attached to v (not to S) or some advneg
			%% and preceded by a clneg !
			( Cat1 = cat[adv,advneg],
%			    format('ADV* ~w ~w\n',[N2,E]),
			    E=edge{ label => cat[v,advneg], type => adj },
			    edge{ source => N, target => node{ cat => clneg } },
			    node!neighbour(xleft,N,N2),
			    (	N1 = N2 ;
				node!safe_dependency(xout,N2,N1),
%%				format('NV XOUT cat=~w N=~w P1=~w N2=~w -> N1=~w\n',[Cat1,N,P1,N2,N1]),
				true
			    )
			;  Cat1 = cat[~ [adv,advneg]],
			    N1 = N2
			)
		    ),
		      Nodes ),
	true
	.

group('NV',N,[]) :-
	N::node{ lemma => L},
	domain(L,['il y a']).	

group('GN',N,Nodes) :-
	N::node{ cat => Cat::cat[adj,nc,pro,np,pri,prel,ncpred,xpro,ce],
		 lemma => L,
		 form => F,
		 cluster => _C_N::cluster{ left => Left }
	       },
	( Cat = adj ->
	    edge{ source => N, type => subst, label => det }
	;   Cat = cat[pri] ->
	  ( \+ edge{ source => node{ cat => 'S'}, target => N },
	    \+ edge{ source => node{ cat => v}, target => N, type => adj }
	  xor pri(L,'GN')),
%%	  \+ ( F \== 'qui',
%%	       edge{ target => N, label => 'CleftQue' }
%%	     ),
	  true
	;   Cat = cat[prel] ->
	  \+ edge{ source => node{ cat => 'S'}, target => N },
	  \+ domain(L,[dont]),
%%	  \+ ( F \== 'qui',
%%	       edge{ target => N, label => 'CleftQue' }
%%	     ),
	  ( edge{ source => V::node{ cat => v },
		  target => N,
		  label => 'CleftQue'
		} ->
	    edge{ source => V,
		  target => Clefted::node{ cluster => cluster{ right => Right }},
		  label => label[comp,subject,comp,impsubj],
		  type => subst
		},
	    Right =< Left
	  ; edge{ target => N,
		  label => 'CleftQue',
		  type => lexical
		} ->
	    fail
	  ;
	    true
	  ),
	  true
	; %% Cat = cat[nc,pro,np,ncpred,xpro,ce],
	  true
	),
%%	\+ node!neighbour(right,N,node{cat=>cat[nc,np]}),
	node!collect( (N1::node{ lemma => Lemma1,
				 cat => Cat1
			       })^(
		          ( edge{ source => N,
				target => _N1::node{ form => _F_N1,
						     lemma => _Lemma1,
						     cat => _Cat1
						   },
				label => _Label
			      }
			  ; %% for ncpred with det (for lglex)
			    %% not a very elegant solution (in terms of dependencies)
			    Cat = ncpred,
			    chain( N
				 << (lexical @ ncpred ) << node{ cat => v }
				 >> (subst @ det ) >> _N1
				 )
			  ),
			  ( N1=_N1
			  ; node!safe_dependency(xout,_N1,N1) ),
			  %%			   format('GN XOUT ~w -> ~w\n\tL=~L\n',[N,N1,['~E',' '],[]]),
%%				   format('TEST0 n1=~E\n',[N1]),
			  ( node!neighbour(xleft,N,N1),
			    _Label = label[~ ['N2']],
			  %%  format('TEST _lemma1=~w cat=~w n=~E\n',[_Lemma1,_Cat1,N1]),
			    ( Lemma1 = tout
			    xor \+ edge{ target => N1,
					 label => det,
					 type => adj
				       }
			    )
			  ;
			    N1=_N1, %only for immediate descendants of N
			    domain(_F_N1,['_-là','_-ci'])
			  ;
%%			    format('TEST2 _lemma1=~w cat=~w n=~E\n',[_Lemma1,_Cat1,N1]),
			    N1=_N1, %only for naked immediate descendants of N
			    \+ edge{ source => N1,
				     target => node{ cat => incise },
				     type => adj
				   },
			    ( _Lemma1 = number[],
			      \+ edge{ source => N1 },
			      node!neighbour(left,N1,N)
			    ;
			      Cat1 = np,
			      Cat = np
			    )
			  )
			 )
		    , _Nodes ),
	left_reduce(_Nodes,Nodes),
%	format('Group GN ~w ~w\n',[N,Nodes]),
	true
	.

group('GN',N,[]) :-
	recorded(mode(robust)),
	N::node{ form => Form,
		 cat => cat[cla,cld],
		 cluster => cluster{ right => R }
	       },
	\+ edge{ source => node{ cat => v },
		 target => N },
	( domain(Form,[le,la,les,'l''']) ->
	  \+ node{ cat => cat[adj,nc,np],
		   cluster => cluster{ left => R }
		 }
	;
	  true
	)
	.

%% for META_TEXTUAL_GN
group('GN',N,[]) :-
	N::node{ cat => epsilon, xcat => 'N2' }
	.

%% Remove skip node on the left
:-rec_prolog left_reduce/2.
left_reduce([],[]).
left_reduce([N|L],XL) :-
	( edge{ target => N, label => skip, type => epsilon } ->
	  left_reduce(L,XL)
	; edge{ target => N, type => lexical },
	  N = node{ lemma => ',' } ->
	  left_reduce(L,XL)
	;
	  XL=[N|L]
	)
	.

group('GN',N,[]) :-
	N::node{ lemma => '_META_TEXTUAL_GN' }
	.


%/*
%% this rule should be refined
group('GN',N,[]) :-
	N::node{ lemma => number[], cat => adj, cluster => cluster{ right => Right_N }},
	\+ ( chain( N << adj << node{ cat => cat[nc,np], cluster => cluster{ left => Left }} ),
	       Right_N =< Left
	   )
	.
%*/

group('GP',N,Nodes) :-
	edge{ source => S::node{ cat => cat['S','N2']},
	      target => N::node{ cat => Cat::cat[prel],
				 cluster => cluster{ left => L} }},
	node!collect( N1^( node!safe_dependency(xout,N,N1),
			   node!neighbour(xleft,N,N1) )
		    , __Nodes ),
	( edge{ source => S,
		target => Prep::node{ cat => prep, cluster => cluster{ right => R}},
		label => prep
	      },
	  R =< L ->
	  _Nodes = [Prep|__Nodes]
	;
	  _Nodes = __Nodes
	),
	left_reduce(_Nodes,Nodes)
	.

%% Clefted constructions
group('GP',N,[]) :-
	edge{ source => V::node{ cat => cat[v]},
	      target => N::node{ cat => Cat::cat[prel],
				 cluster => cluster{ left => L} },
	      label => 'CleftQue'
	    },
	edge{ source => V,
	      target => Clefted::node{ cluster => cluster{ right => R }},
	      label => preparg,
	      type => subst
	    },
	R =< L
	.

group('GP',N,Nodes) :-
	edge{ source => S::node{ cat => 'S'},
	      target => N::node{ cat => Cat::cat[pri], lemma => L,
				 cluster => cluster{ left => Left }}},
	verbose('TRY GP ~E ~w\n',[N,L]),
	( edge{ source => S,
		target => Prep::node{ cat => prep, cluster => cluster{ right => Right }},
		label => prep
	      },
	  Right =< Left ->
	  Nodes = [Prep]
	;
	  pri(L,'GP'),
	  Nodes = []
	)
	.

group('GP',N,Group1) :-
	'$answers'(Group1::group(const['GN','GR','GP','GA'],Left1,_,Head1,_)),
	node!neighbour(left,Left1,_N::node{ cat => prep }),
	verbose('TRY GP ~E ~w ~E\n',[_N,Left1,Head1]),
	( x_command(_N,Head1)
	; %% relative witout antecedent
	  %% should factorize this path with a predicate
	  edge{ source => _N,
		target => _N2::node{ cat => 'N2' },
		type => subst,
		label => 'N2'
	      },
	  edge{ source => _N2,
		target => _V::node{ cat => v },
		type => subst,
		label => 'SRel'
	      },
	  edge{ source => _V,
		target => Head1,
		label => label[subject,impsubj]
	      }
	),
	easy_compound_prep(_N,N::node{ lemma => Lemma }),
	\+ not_a_prep(Lemma),
	true
	.


group('PV',N,Group2) :-
	'$answers'(Group1::group('NV',Left1,_,Head1,_)),
	%% node!neighbour(left,Left1,_N),
	recorded( terminal_v(Head2,Head1) ),
	leftward_till_prep(Left1,_N,N,Group1,Group2),
	verbose( 'PV: left=~E head1=~E head2=~E\n',[_N,Head1,Head2]),
	/*
	(   _N = node{ cat => prep },
	    easy_compound_prep(_N,N)
	;
	    N = _N,
	    _N = node{ %% cat => '_',
		       lemma => en }
	),
	*/
	x_command(_N,Head2),
	verbose( 'Found PV: left=~E head1=~E head2=~E\n',[N,Head1,Head2]),
	true
	.


:-std_prolog leftward_till_prep/5.

leftward_till_prep(Left,_Prep,Prep,OldGroup,NewGroup) :-
%	format('LEFTWARD from ~w\n',[Left]),
	node!neighbour(left,Left,N::node{ cat => Cat , lemma => L, tree => Tree, cluster=> cluster{ left => _Left }} ),
%	format('=> left=~w ~w from=~w\n',[_Left,N,Left]),
	( (   Cat = prep,
	      _Prep = N,
	      easy_compound_prep(_Prep,Prep)
	  xor
	      L=en,
	      Prep = N,
	      _Prep = Prep
	  ) ->
	  NewGroup = OldGroup
	; Cat = cat[adv,advneg] ->
	  leftward_till_prep(N,_Prep,Prep,[N|OldGroup],NewGroup)
	; Tree = [lexical] ->
	  leftward_till_prep(N,_Prep,Prep,[N|OldGroup],NewGroup)
	;
	  fail
	).

group('PV',N,Group1) :-
	'$answers'(Group1::group('NV',Left1,_,Head1,_)),
	node!neighbour(left,Left1,N),
	recorded( terminal_v(Head2,Head1) ),
	verbose( 'Try PV step1: head1=~E head2=~E\n',[Head1,Head2]),
	edge{ source => Head2,
	      target => M::node{ cat => v },
	      type => adj,
	      label => label['S','S2']
	    },
	verbose( 'Try PV step2: M=~E\n',[Edge]),
	edge{ source => M, target => N::node{ cat => prep }, type => lexical }
	.

group(Type,N,Group) :-
	N::node{ cat => cat[adv,advneg], cluster => cluster{ id => IdN }, lemma => L},
	\+ ( edge{ target => N, source => node{ cat=>cat[adj,nc,prep],
						cluster=> cluster{ id => IdM } }},
	     recorded(redirect(IdM,IdN))
	   ),
	( adv_as_pp(L,_Type) ->
	    Type = _Type,
	    Group = []
	;
	    Type = 'GR',
	    ( chain( N >> (subst @ det ) >> Det::node{ cat => det } ) ->
		% in superlative add det to GR
		Group = [Det]
	    ;
		Group = []
	    )
	)
	.

:-extensional adv_as_pp/2.

adv_as_pp('en effet','GP').
adv_as_pp('de même','GP').
adv_as_pp('par exemple','GP').
adv_as_pp('de moins','GP').
adv_as_pp('en moins','GP').
adv_as_pp('de plus','GP').
adv_as_pp('d''abord','GP').
adv_as_pp('en moyenne','GP').
adv_as_pp('en revanche','GP').
adv_as_pp('d''autre part','GP').
adv_as_pp('en particulier','GP').
adv_as_pp('au mieux','GP').

group('GR',N,[]) :-		% que restr ?
	edge{ target => N::node{ cat => cat[csu] },
	      label => advneg,
	      source => node{ cat => cat[v,aux]}
	    }
	.

group('GR',N::node{ cat => pri, lemma => L },[]) :-
	( edge{ source => node{ cat => 'S'}, target => N }
	;
	  chain( N << adj << node{ cat => v })
	),
	%% by default, pri which are not arguments are GR unless
	%% otherwise explicetely mentionned
	pri(L,const[~ ['GP','GN']])
	.

group('GR',N,[]) :-
	edge{ target => N::node{ cat => que_restr },
	      label => advneg,
	      type => lexical
	    }
	.

group('GA',N,[]) :-
	N::node{ cat => adj, lemma => Lemma },
	Lemma \== number[]
	.


group('GA',N,[]) :-
	%% Participiales on nouns, non gerundive (ie. past participle)
	single_past_participiale(N)	
	.

group('GA',N,[]) :-
	chain( node{ cat => supermod } >> lexical >> N::node{ lemma => possible } )
	.

:-light_tabular easy_compound_prep/2.
:-mode(easy_compound_prep/2,+(+,-)).

%% Climb along a compound preposition like "à partir de"
%% when they form a single EASy cluster
easy_compound_prep( N1::node{ cat => prep, cluster => cluster{ id => Id1 }},
		    N2::node{ cat => prep, cluster => cluster{ id => Id2 }}
		  ) :-
	( edge{ source => M::node{ cluster => cluster{ id => IdM}},
		target => N1,
		type => adj
	      },
	  verbose('TRY EASY COMPOUND ~w ===> ~w\n',[N1,M]),
	 edge{ source => N3::node{ cat => prep, cluster => cluster{ id => Id3}},
	       target => M,
	       type => subst
	     },
	  verbose('TRY2 EASY COMPOUND ~w ===> ~w ~w\n',[N1,M,N3]),
	  easy_compound_prep(N3,N2),
	  verbose('TRY3 EASY COMPOUND ~w ===> ~w ~w\n',[N1,M,N2]),
	  recorded( redirect(Id1, Id2) ),
	  verbose('TRY4 EASY COMPOUND ~w ===> ~w ~w\n',[N1,M,N2]),
	  true
	xor N1=N2
	),
	verbose('EASY COMPOUND ~w ===> ~w\n',[N1,N2]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Extracting dependencies

:-std_prolog extract_relation/1.

extract_relation( R ) :-
	R::relation{ type => Type, arg1 => Arg1, arg2 => Arg2, arg3 => Arg3 },
	coord_reroot(R,XR::relation{ type => Type, arg1 => XArg1, arg2 => XArg2, arg3 => XArg3 }),
	arg2id(XArg1,Id1),
	arg2id(XArg2,Id2),
	arg2id(XArg3,Id3),
	( Id1 \== Id2 ),	% To avoid relations between same tokens
	relation( relation{ type => Type, arg1 => Id1, arg2 => Id2, arg3 => Id3 } )
	.

:-std_prolog arg2id/2.

arg2id(Arg,Id) :-
	( Arg = [] -> Id = []
	; opt(passage),
	  Arg = node{ id => Id } ->
	  true
	; opt(passage),
	  Arg = f(Id) ->
	  true
	;  Arg = node{ cluster => C::cluster{ id => CId , left => Left , right => Right}} ->
	    ( recorded( redirect(CId,_CId) ) ->
		_Left is Left - 1
	    ;	
		_Left = Left,
		_CId = CId
	    ), 
	    (	f{ cid => _CId, id => Id } 
	    xor	cluster_overlap(CId,CId2),
		f{ cid => CId2, id => Id }
	    )
	;   Id = Arg
	)
	.


:-std_prolog coord_reroot/2.

coord_reroot( R::relation{ type => Type,
			    arg1 => Arg1,
			    arg2 => Arg2,
			    arg3 => Arg3
			  },
	      XR::relation{ type => Type,
			    arg1 => XArg1,
			    arg2 => XArg2,
			    arg3 => XArg3
			  }
	    ) :-
	( Type = 'COORD' ->
	  R = XR
	%% the following case should be commented
	%% but currently, distrib of AUX-V over COORD is generally not done
	%% in the reference
	; Type = 'AUX-V' ->
	  R = XR
	; coord_relarg_reroot(Type,1,Arg1,XArg1,R),
	  coord_relarg_reroot(Type,2,Arg2,XArg2,R),
	  coord_relarg_reroot(Type,3,Arg3,XArg3,R)
	).

:-rec_prolog coord_relarg_reroot/5.

coord_relarg_reroot(Type,Pos,Arg,XArg,
		    R::relation{ arg1 => Arg1,
				 arg2 => Arg2
			       }
		   ) :-
	( \+ var(Arg),
	  Arg = node{ cluster => cluster{ left => L1, right => R1 } },
%%	  format('Try Coord reroot relarg ~w arg~w from ~E\n',[Type,Pos,Arg]),
	  ( _Arg = Arg
	  ; fail,
	    %% not yet ready for a right distrib
	    %% ex: il achète et mange une pomme.
	    %% but how to avoid: il sort et achète une pomme.
	    edge{ target => Arg,
		  label => coord3,
		  type => edge_kind[lexical,subst],
		  source => COO
		},
	    E
	  ; edge{ source => _Arg,
		  target => Arg,
		  type => edge_kind[subst,lexical],
		  label => label['N2']
		}
	  ),
	  E::edge{ source => _Arg::node{ cat => _Cat },
		   target => COO::node{ cat => coo,
					cluster => cluster{ left => CL,
							    right => CR }
				      },
		   type => adj
		 },
%%	  format('Try1 Coord reroot relarg ~w arg~w from ~E\n',[Type,Pos,Arg]),
	  \+ duplicate_in_coord(E,Type,Pos),
%%	  format('Try2 Coord reroot relarg ~w arg~w from ~E\n',[Type,Pos,Arg]),
	  \+ ( ( Pos=1 -> OtherArg=Arg2
	       ; Pos=2 -> OtherArg=Arg1
	       ; fail
	       ),
	       OtherArg = node{ cat => OtherArgCat,
				cluster => cluster{ left => OL,
						    right => OR }},
%%	       format('Other arg is ~E\n',[OtherArg]),
		 (   R1 =< OL,
		     OR =< CL
		 ;
		     OtherArgCat = cat[cla,cld,clg,cll,clr,clneg]
		 ;
		     _Cat = v,
		     \+ edge{ source => COO,
			      label => coord3,
			      type => subst,
			      target => node{ cat => v }
			    }
		 )
	     ),
	    allow_reroot(Type,Pos,Arg,XArg,R),
	    true
	->
	  %% XArg = COO,
	  %% old rule
	  %%	  find_first_coo(COO,XArg),
	  %% new rule seems to be to use the last coo
	  XArg = COO,
%%	  verbose('Coord reroot relarg ~w arg~w from ~E to ~E\n',[Type,Pos,Arg,XArg]),
%%	  format('Coord reroot relarg ~w arg~w from ~E to ~E\n',[Type,Pos,Arg,XArg]),
	  true
	;
	  XArg = Arg
	)
	.

:-light_tabular find_first_coo/2.
:-mode(find_first_coo/2,+(+,-)).

find_first_coo(Coo,First) :-
	( edge{ source => Coo::node{
				    cluster => cluster{ left => COO_Left }
				   },
		target => Coma::node{ 
				      cluster => cluster{ left => Coma_Left,
							  right => Coma_Right
							}
				    },
		type => lexical,
		label => void
	      },
	  Coma_Right < COO_Left,
%%	  format('COO coma_right=~w coo_left=~w\n',[Coma_Right,COO_Left]),
	  \+ ( edge{ source => Coo,
		     target => node{ 
				     cluster => cluster{ left => Coma2_Left }
				   },
		     type => lexical,
		     label => void
		   },
	       Coma2_Left < Coma_Left
	     ),
	  Coma = First
	xor Coo = First
	).
	

:-rec_prolog allow_reroot/5.

allow_reroot(rel[],_,_,_,_).

:-rec_prolog duplicate_in_coord/3.

%% This predicate is to avoid coord rerooting for cases like
%%   il parle et elle part
%% where the same role is duplicated in each coordinated
duplicate_in_coord( edge{ target => COO::node{} },
		    Type,
		    Pos ):-
	edge{ source => COO,
	      target => N,
	      label => coord3,
	      type => edge_kind[lexical,subst]
	    },
	( Pos=1 -> Arg1 = N
	; Pos=2 -> Arg2 = N
	; fail
	),
	relation{ type => Type,
		  arg1 => Arg1,
		  arg2 => Arg2
		}
	.

:-light_tabular nominal_head/2.
:-mode(nominal_head/2,+(+,-)).

nominal_head(_N1::node{}, N1 ) :-
	verbose('Search nominal head ~E\n',[_N1]),
	( node!empty(_N1) ->
	   edge{ source => _N1,
		 target => _N2::node{ cat => v },
		 label => 'SRel'
	       },
	  N1=node{ cat => prel },
	  ( edge{ source => _N2,
		  target => N1,
		  label => label[subject,impsubj]
		}
	  xor %% chain( _N2 >> adj >> node{ cat => 'S' } >> (lexical @ prel) >> N1)
	  edge{ source => _N2,
		target => _S::node{ cat => 'S' },
		type => adj
	      },
	    edge{ source => _S,
		  target => N1,
		  label => prel,
		  type => lexical
		}
	  )
	  xor edge{ source => _N1,
		    target => _N3,
		    label => quoted_N2,
		    type => subst
		  },
	    nominal_head(_N3,N1)
	  xor edge{ source => _N1,
		    target => _N3,
		    label => quoted_S,
		    type => subst
		  },
	    get_head(_N3,N1)
	;   
	    _N1 = N1
	),
	verbose('Found nominal head ~E => ~E\n',[_N1,N1]),
	true
	.

:-light_tabular relation/1.

relation(relation{ id => Label }) :- rel_gensym(Label).

:-std_prolog deep_subject/3.

deep_subject(N,Subj,L) :-
%%	format('start deep subject ~w\n',[N]),
	N::node{ cat => v },
	t_deep_subject(N,Subj,L),
%%	format('deep subject ~E sub=~E L=~w\n',[N,Subj,L]),
	true
	.

:-light_tabular t_deep_subject/3.
:-mode(t_deep_subject/3,+(+,-,-)).

t_deep_subject(N::node{ cat => v },Subj,L) :-
	edge{ source => N,
	      target => _Subj::node{ cluster => cluster{ right => Right }},
	      label => L::label[subject,impsubj]
	    },
	\+ (
	       chain( N >> (lexical @ 'CleftQue') >> Que ::node{ cat => prel, cluster => cluster{ left => Left } }),
	       Right =< Left
	   ),
	get_head(_Subj,Subj)
	.

t_deep_subject(N::node{ cat => v },Subj,L) :-
	edge{ source => N,
	      target => V::node{ cat => v},
	      label => label['V','S'],
	      type => adj
	    },
	%%	format('try climbing ~E => ~E\n',[N,V]),
	t_deep_subject(V,Subj,L)
	.

t_deep_subject(N::node{ cat => v}, Subj,L) :-
	edge{ source => N,
	      target => Aux::node{ cat => aux },
	      type => adj,
	      label => label['Infl',aux]
	    },
	clitic_subj(Aux,Subj,L)
	.

relation{ type => 'SUJ-V',
	  arg1 => N1::node{ cat => Cat1, cluster => cluster{ left => Left1 }},
	  arg2 => N2::node{ cluster => cluster{ right => Right2 }}
	} :-
	edge{ source => N3::node{ cat => cat[v,aux]},
	      target => _N1::node{ cat => _N1_Cat},
	      label => L::label[subject,impsubj] },
	( L = subject, _N1_Cat=cat[v,prep,csu] ->
	  %% to avoid a subject(manger,faut) in "il faut manger"
	  %% or in "il est recommandé de partir."
	  \+ edge{ label => impsubj, source => N3 }
	;
	  true
	),
	( _N1_Cat = cat[prep,csu] ->
	    chain( _N1 >> subst >> _N1_Head::node{} )
	;   
	    _N1_Head = _N1
	),
	nominal_head(_N1_Head,__N1),
	try_reroot_cleft(__N1,N3,N1),
%%	_N1=N1,
	(   %% recorded( terminal_v(N3,N2) )
	    verb_climbing(N3,N2)
	;
	    recorded(mode(robust)),
	    \+ verb_climbing(N3,_),
	    Cat1 = cat[cln,ilimp,caimp],
	    N2 = N3,
	    Left1 = Right2
	),
%%	format('SUBJ0 v=~E subj=~E\n',[N2,N1]),
	true
	.


relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	( edge{ source => _N2::node{ cat => adj },
		target => _N1,
		label => impsubj
	      }
	xor edge{ source => _N2,
		  target => _N1,
		  label => subject
		}
	),
	nominal_head(_N1,__N1),
	try_reroot_cleft(__N1,_N2,N1),
	verb_climbing(_N2,N2::node{ cat => cat[v,aux] })
	.

relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	%%	edge{ source => N3, target => _N1, label => label[subject,impsubj] },
	edge{ source => N3, target => N4, label => label['S','S2',vmod,mod], type => adj },
	deep_subject(N3,_N1,_),
	node!empty(N4),
	edge{ source => N4, target => N5::node{ cat => v }, type => subst },
	\+ deep_subject(N5,_,_),
	verb_climbing(N5,N2),
	nominal_head(_N1,N1),
%%	format('SUBJA v=~E subj=~E\n',[N2,N1]),
	true
	.

relation{ type => 'SUJ-V',
	  arg1 => N1::node{ cat => pro, tree => Tree },
	  arg2 => N2::node{}
	} :-
	edge{ source => N3,
	      target => N1,
	      label => label['S','S2','vmod',mod],
	      type => adj
	    },
	domain('pronoun_as_mod',Tree),
	verb_climbing(N3,N2)
	.

relation{ type => 'SUJ-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	( deep_subject(_N2,N1,impsubj)
	xor deep_subject(_N2,N1,subject)
	),
	aux_climbing(_N2,N2)
	.

:-light_tabular clitic_subj/3.
:-mode(clitic_subj/2,+(+,-,-)).

clitic_subj(N1,N2::node{ cat => cln },L) :-
	edge{ source => N1,
	      target => N2,
	      label => _L::label[subject,impsubj],
	      type => lexical
	    },
	_L=L
	xor
	edge{ source => N1,
	      target => N3,
	      label => label['V','Infl',aux,modal],
	      type => adj
	    },
	clitic_subj(N3,N2,L)
	.

:-light_tabular verb_climbing/2.
:-mode(verb_climbing/2,+(+,-)).

%% return all potential anchoring verb for a subject
%% example: il a voulu manger -> [a] and [manger]
verb_climbing(V,Anchor) :-
	(  
	   ( edge{ source => V,
		   target => Aux::node{ cat => cat[aux,v] },
		   label => label['Infl',aux],
		   type => adj
		 } ->
	     verb_climbing(Aux,Anchor)
	   ;
	     V=Anchor
	   )
	;
	   edge{ source => V,
		 target => Modal::node{ cat => v },
		 label => label['V',modal],
		 type => adj
	       },
	   verb_climbing(Modal,Anchor)
	),
%%	format('climbing ~E => ~E\n',[V,Anchor]),
	true
	.

:-light_tabular aux_climbing/2.
:-mode(aux_climbing/2,+(+,-)).

aux_climbing(V,Anchor) :-
	( edge{ source => V,
		target => Aux::node{ cat => cat[aux,v] },
		label => label['Infl',aux],
		type => adj
	      } ->
	  aux_climbing(Aux,Anchor)
	;
	  V=Anchor
	)
	.

/*
%% A quoi sert la clause suivante ? pour les modaux
relation{ type => 'SUJ-V',
	  arg1 => N1::node{ cat => Cat1, cluster => cluster{ left => Left1 }},
	  arg2 => N2::node{ cluster => cluster{ right => Right2 }}
	} :-
	edge{ source => N2, target => _N1, label => label[subject,impsubj] },
	nominal_head(_N1,N1),
	\+ edge{ source => N2, target => node{ cat => aux }, label => 'Infl', type => adj },
%%	format('SUBJB v=~E subj=~E\n',[N2,N1]),
	true
	.
*/

relation{ type => 'SUJ-V',
	  arg1 => N1::node{ lemma => N1_Lemma, form => N1_Form, cat => N1_Cat },
	  arg2 => N2::node{}
	} :-
	edge{ source => N3::node{ lemma => Lemma, id => N3Id },
	      target => _N2::node{},
	      label => xcomp },
	\+ edge{ source => _N2, label => label[subject,impsubj] },
	verbose('Trying ctrl verb: ~E ~E\n',[N3,_N2]),
	(   edge{ source => N3, target => N1, label => object }
	xor edge{ source => N3, target => _N1, label => preparg },
	    ( _N1=node{ cat => prep },
	      edge{ source => _N1, target => N1, type => subst }
	    xor N1 = _N1)
	xor deep_subject(N3,N1,subject),
	    %% edge{ source => N3, target => N1, label => label[subject] },
	    node2live_ht(N3Id,HTId),
	    %%	    format('SUJ-V CTR ~E ~E derivs=~w did=~w hid=~w\n',[N3,N2,Derivs,DId,HId]),
	    check_ht_feature(HTId,ctrsubj,suj),
	    Lemma \== 'être'
	),
	\+ N1_Cat = cat[v,aux],
	\+ domain(N1_Lemma,[ce,cela]),
	aux_climbing(_N2,N2),
%%	format('SUBJC v=~E subj=~E\n',[N2,N1]),
	true
	.

relation{ type => 'SUJ-V',
	  arg1 => N1,
	  arg2 => XN2
	} :-
	E::edge{ source => N2::node{ cat => v },
		 target => N3::node{ cat => 'S', tree => Tree },
		 type => adj },
	domain('person_on_s',Tree),
	edge{ source => N3, target => N1, type => subst, label => 'N2' },
	verb_climbing(N2,XN2),
%%	format('SUBJD v=~E subj=~E\n',[N2,N1]),
	true
	.

relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	edge{ source => N1,
	      target => N3::node{},
	      type => adj,
	      label => 'N2'
	    },
	node!empty(N3),
	edge{ id => EId,
	      source => N3,
	      target => V::node{cat => v },
	      label => 'SubS',
	      type => subst
	    },
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,gerundive),
%%	(   recorded( terminal_v(V,N2) ) xor N2 = V )
	verb_climbing(V,N2),
%%	format('SUBJE v=~E subj=~E\n',[N2,N1]),
	true
	.

%% for "il sera à même de conduire"
%% the acomp arg introduces an infinitive
relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	edge{ label => comp,
	      source => V::node{ cat => v},
	      target => Comp,
	      type => subst
	    },
	edge{ label => xcomp,
	      source => Comp,
	      target => _N2
	    },
	edge{ label => prep,
	      source => Comp,
	      target => node{ cat => prep }},
	(  edge{ label => object,
		 target => N1,
		 source => V
	       } 
	xor
	   edge{ label => subject,
		 target => N1,
		 source => V
	       }
	),
	( node!first_main_verb(_N2,N2)
	xor N2=_N2
	)
	.

%% This rule should be tested !
%% it is not clear that the suject of a main clause
%% is also the subject of subordonate PVs
relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => XN2::node{}
	} :-
	edge{ label => label['S','S2','vmod',mod],
	      source => V::node{ cat => v},
	      target => Mod,
	      type => adj
	    },
	edge{ label => 'PP',
	      source => Mod,
	      target => Prep::node{ cat => prep }},
	\+ got_prep_locution(Prep),
	get_head(Prep,N2::node{ cat => v }),
	aux_climbing(N2,XN2),
	deep_subject(V,N1,subject),
	\+ deep_subject(N2,_,_)
	.

:-extensional prep_locution/3.

:-finite_set(prep_verb,[partir,compter,oublier,sortir,dire,passer,citer,mentionner,dater]).

prep_locution(�,prep_verb[partir,compter,sortir,dater],[]).
prep_locution(sans,prep_verb[compter,oublier,mentionner],[]).
prep_locution(pour,prep_verb[dire,passer,citer,mentionner],[]).

:-std_prolog got_prep_locution/1.

got_prep_locution( Prep::node{ cat => prep, lemma => Lemma} ) :-
	prep_locution(Lemma,VLemma,AuxPrep),
	edge{ source => Prep,
	      target => node{ cat => v, lemma => VLemma },
	      type => subst
	    }
	.

relation{ type => 'AUX-V',
	  arg1 => N1::node{ cat => aux},
	  arg2 => N2
	} :-
	edge{ source => N2, target => N1, label => label['Infl',aux] }
	.

relation{ type => Type,
	  arg1 => N1,
	  arg2 => N2,
	  arg3 => Arg3
	} :-
	E::edge{ source => N2::node{ id => N2Id},
		 target => _N1,
		 label => Label::label[object,ncpred,clg,clr],
		 type => edge_kind[subst,lexical]
	       },

	nominal_head(_N1,__N1),
	try_reroot_cleft(__N1,N2,N1),
	verbose('COD ~E ~E edge=~E\n',[N1,N2,E]),
	( _N1 = node{ cat => cla },
	  N2 = node{ lemma => Lemma },
	  domain(Lemma,[�tre,sembler,para�tre,demeurer,rester]) ->
	  %% this case would be better handled within the meta-grammar
	  Type = 'ATB-SO',
	  Arg3 = sujet
	; _N1 = node{ cat => clg },
	  node2live_ht(N2Id,HTId),
	  check_arg_feature(HTId,Arg::args[arg1,arg2],function,function[objde,att]) ->
	  Type = 'CPL-V'
	; _N1 = node{ cat => clr } ->
	  fail,			% not yet ready to be activated
	  node2live_ht(N2Id,HTId),
	  check_arg_feature(HTId,Arg,real,clr),
	  ( check_arg_feature(HTId,Arg,function,function[obj]) ->
	    Type = 'COD-V'
	  ; check_arg_feature(HTId,Arg,function,function[obj�]) ->
	    Type = 'CPL-V'
	  ;
	    fail
	  )
	; _N1 = node{ cat => prep } ->
	  %% accusative infinitive relatives
	  %% example: il a un livre à finir
	  fail
	;
	  Type = 'COD-V'
	),
	true
	.

relation{ type => Type,
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => v},
	  arg3 => Arg3
	} :-
	edge{ source => N2::node{ lemma => Lemma, id => N2Id },
	      target => _N1,
	      label => L::label[scomp,xcomp]
	    },
	try_reroot_cleft(_N1,N2,__N1),
	get_head(__N1,N1),
	( Lemma == être ->
	  Type = 'ATB-SO',
	  Arg3 = sujet
	; deep_subject(_N1,_,_) ->
	  Type = 'COD-V'
	;
	  ( \+ edge{ source => N2, label => object },
	    %%	  \+ edge{ source => N2, label => clr },
	    node2live_ht(N2Id,HTId),
	    ( check_xarg_feature( HTId,args[arg1,arg2],
				  obj,
				  fkind[scomp,vcomp,prepvcomp,prepscomp,whcomp,prepwhcomp,vcompcaus],
				  _
				)
	    )) ->
	  Type = 'COD-V'
	;
	  Type = 'CPL-V'
	)
	.

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => v},
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N2, target => N3, label => 'preparg' },
	edge{ source => N3, target => N1, label => 'S' },
	\+ edge{ source => N2, label => object }
	.

/*
relation{ type => 'COD-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N2, target => N3::node{ cat => prep, lemma => de}, label => preparg },
	edge{ source => N3, target => N1 },
	\+ edge{ source => N2, label => object }
	.
*/

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => cat[v,aux] },
	  arg2 => N2::node{ cat => v, id => N2Id }
	} :-
	edge{ source => _N1, target => N2, label => 'V' },
	\+ edge{ source => N2, label => object },
	node2live_ht(N2Id,HTId),
	check_arg_feature(HTId,args[arg1,arg2],function,function[obj]),
	( edge{ source => _N1, target => N1, label => label['Infl',aux], type => adj },
	  N1 = node{ cat => cat[aux,v] }
	xor _N1=N1
	)
	.

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => v },
	  arg2 => N2::node{ cat => v, tree => Tree }
	} :-
	edge{ source => _N1::node{ cat => v },
	      target => N2,
	      type => adj,
	      label => 'S'
	    },
	domain('arg1:real_arg_xcomp_by_adj',Tree),
	node!first_main_verb(_N1,N1)
	.

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => v },
	  arg2 => N2::node{ cat => v }
	} :-
	edge{ source => N2,
	      target => _N1::node{ cat => v },
	      label => subject
	    },
	edge{ source => N2,
	      label => impsubj
	    },
	node!first_main_verb(_N1,N1)
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3, lemma => Lemma3 },
	      label => label[preparg]
	    },
	( Cat3 = prep ->
	  /*
	    (	Lemma3 \== de 	% not true for all de-obj verb
	    xor edge{ source => N2, label => object }
	    ),
	  */
	  edge{ source => N3,
		target => _N1::node{ cluster => cluster{ right => R} },
		type => edge_kind[subst,lexical] },
	  try_reroot_cleft(_N1,N2,__N1)
	; Cat3 = cld ->
	  %% chain(N2 >> (subst @ comp) >> node{ cat => adj }),
	  has_cld(N2,_),
	  N3 = __N1
	;
	  N3 = __N1
	),
	get_head(__N1,N1)
	.

:-light_tabular try_reroot_cleft/3.

try_reroot_cleft( N::node{ cluster => cluster{ right => NR, left => NL  } },
		  V::node{},
		  M
		) :-
	( edge{ source => V,
		target => Que::node{ cat => prel,
				     cluster => cluster{ left => L }
				   },
		label => 'CleftQue'
	      },
	  chain( V >> (adj @ label['S','S2']) >> node{ cat => aux,
					   lemma => être,
					   cluster => cluster{ right => AuxR }
					 }),
	  NR =< L,
	  AuxR =< NL
	  ->
	  M = Que
	;
	  M = N
	)
	.



relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat },
	      label => label['S','S2',vmod,mod],
	      type => adj
	    },
	( node!empty(N3) ->
	    edge{ source => N3, target => N4::node{ cat => prep } },
	    true
	;   Cat = prep,
	    N3 = N4
	),
	(   \+ chain( N3 >> (lexical @ 'CleftQue') >> node{ cat => prel } ),
	    edge{ source => N4, target => N1, type => edge_kind[subst,lexical] }
	;
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] },
	    N1 = node{ cat => prel }
	),
	\+ node!empty(N1)
	.

%% For clefted sentence, such as c'est à Paul qu'il parle
%% the guide states that 'à Paul' should be an ATB-SO
%% by many annotation samples use an CPL-V of être
relation{ type => 'CPL-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => aux }
	} :-
	(
	    chain( N2
		 << (adj @ 'S') << node{ cat => v }
		 >> (adj @ 'S') >> S::node{ cat => 'S' }
		 >> ( (lexical @ 'CleftQue') >> Que::node{ cat => prel,  cluster => cluster{ left => L} }
		    & (   subst @ 'PP') >> _N1::node{ cat => prep, cluster => cluster{ right => R} }
		    )
		 )
	;
	    chain( N2
		 << (adj @ 'S2') << V::node{ cat => v }
		 >> ( (lexical @ 'CleftQue') >> Que
		    &  (subst @ preparg) >> _N1
		    )
		 )
	),
	R =< L,
	get_head(_N1,N1)
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{ cat => v },
	  arg2 => N1::node{ cat => xnominal[] }
	} :-
	chain( _N2::node{ cat => v }
	     >> ( (adj @ 'S') >> node{ cat => 'S' }
		>> ( (lexical @ 'CleftQue') >> Que::node{ cat => prel}
		   & (subst @ 'PP') >> _N1::node{}
		   )
		;   ( (lexical @ 'CleftQue') >> Que
		    & (   subst @ label[preparg,subject,object,comp]) >> _N1
		    )
		)
	     ),
	precedes(_N1,Que),
	get_head(_N1,N1),
	node!first_main_verb(_N2,N2)
	.

relation{ type => 'ATB-SO',
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => aux },
	  arg3 => sujet
	} :-
	chain( N2
	     << (adj @ 'S2') << node{ cat => v }
	     >> ( (lexical @ 'CleftQue') >> Que::node{ cat => prel,  cluster => cluster{ left => L} }
		& (subst @ label[object,subject]) >> _N1::node{ cluster => cluster{ right => R} }
		)
	     ),
        R =< L,
	get_head(_N1,N1)
	.

:-finite_set(progp,['où','dont']).

relation{ type => 'CPL-V',
	  arg1 => N1::node{ cat => cat[pri,prel], lemma => progp[] },
	  arg2 => N2::node{ cat => v }
	} :-
	E1::edge{ source => N2,
		  target => N3::node{ cat => 'S' },
		  label => label['S','S2','vmod',mod],
		  type => adj },
	node!empty(N3),
	E2::edge{ source => N3,
		  target => N1,
%%		  label => pri,
		  type => edge_kind[subst,lexical]
		},
	\+ edge{ source => N3,
		 target => node{ cat => prep }
	       }
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => cat[pri], lemma => Lemma },
	  arg2 => N2::node{ cat => v }
	} :-
	chain( N2 >> adj >> N1 ),
	pri(Lemma,GR)
	.

relation{ type => Type,	% participiales
	  arg1 => N1::node{ cat => cat[v] },
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{},
	      label => label['S','S2',vmod,mod],
	      type => adj
	    },
	node!empty(N3),
	edge{ source => N3, target => _N1, type => subst, label => 'SubS' },
	node!first_main_verb(_N1,N1),
	( chain( N3 >> lexical >> node{ lemma => en } ) ->
	  Type = 'CPL-V'
	;
	  Type = 'MOD-V'
	)
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N2, target => N1::node{ cat => cll }, label => cll }
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	E1::edge{ source => N2,
		  target => N3::node{ cat => 'S' },
		  label => label['S','S2','vmod',mod],
		  type => adj },
	node!empty(N3),
	E2::edge{ source => N3,
		  target => N4::node{ cat => prep},
		  label => label['PP',wh], type => subst
		},
	\+ chain( N3 >> (lexical @ 'CleftQue' ) >> node{ cat => prel} ),
	E3::edge{ source => N4,
		  target => N1,
		  type => edge_kind[subst,lexical]
		},
	verbose('CPL-V ~E ~E ~E\n',[E1,E2,E3]),
	true
	.

relation{ type => 'CPL-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	chain( N2 >> (
		       (lexical @ impsubj) >> node{}
		     & (subst @ subject) >> node{ cat => prep } >> '$head' >> N1
		     )
	     )
	.

relation{ type => Type,
	  arg1 => N1::node{ cat => cat[adv,advneg,nc] },
	  arg2 => N2::node{ cat => Cat }
	} :-
	edge{ source => _N2,
	      target => N1,
	      label => label['V','V1','S','v','S2','vmod',advneg,mod], type => edge_kind[adj,lexical] },
	get_head_no_climb(_N2,N2),
	( Cat = cat[v,aux] ->
	    Type = 'MOD-V'
	; Cat = xnominal[] ->
	    Type = 'MOD-N'
	; Cat = cat[adv,advneg] ->
	    Type = 'MOD-R'
	; Cat = adj ->
	    Type = 'MOD-A'
	)
	.

relation{ type => Type,
	  arg1 => N1::node{ cat => Cat::cat[adv,advneg], cluster => cluster{ left => L1, right => R1 }},
	  arg2 => N2::node{ cat => Cat2::cat[v,adj,adv], cluster => cluster{ left => L2, right => R2 } }
	} :-
	recorded( mode(robust) ),
	N1,
	\+ edge{ target => N1, type => edge_kind[adj] },
	(  R1 = L2, N2, \+ ( Cat=advneg, Cat2=cat[adv,adj] )
	xor L1 = R2, Cat2=v, N2
	xor L1 is R2+1, Cat2=v, 
	   node{ cat => cln, cluster => cluster{ left => R2, right => L1}},
	   N2
	),
	( Cat2 = cat[v,aux] -> Type = 'MOD-V'
	; Cat2 = adj -> Type = 'MOD-A'
	; Type = 'MOD-R'
	)
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => csu },
	  arg2 => N2::node{ cat => v }
	} :-
	edge{ source => N2,
	      target => N1,
	      label => advneg,
	      type => lexical
	    }
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => cat[pri,prel], lemma => Lemma },
	  arg2 => N2::node{ cat => v }
	} :-
	E1::edge{ source => N2,
		  target => N3::node{ cat => 'S' },
		  label => label['S','S2','vmod',mod],
		  type => adj },
	node!empty(N3),
	E2::edge{ source => N3,
		  target => N1,
%%		  label => pri,
		  type => edge_kind[subst,lexical]
		},
	\+ edge{ source => N3,
		 target => node{ cat => prep }
	       },
	\+ Lemma = progp[]
	.

relation{ type => 'MOD-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N2::node{ cat => cat[v] },
	      target => N3::node{ cat => cat[csu] },
	      label => label['S','S2','vmod',mod],
	      type => adj },
	edge{ source => N3, target => _N1, label => label['SubS'], type => subst },
	get_head(_N1,N1)
	.

relation{ type => 'MOD-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => V::node{ cat => v },
	      target => S::node{ cat => cat['VMod','S'] },
	      type => adj
	    },
	edge{ source => S,
	      target => N1,
	      type => subst,
	      label => time_mod
	    },
	get_head(V,N2)
	.


relation{ type => 'COMP',
	  arg1 => N1,
	  arg2 => N2::node{ cat => v }
	} :-
	edge{ source => N3::node{ cat => cat[nc,v,adj]},
	      target => _N2,
	      label => label[scomp,xcomp],
	      type => subst
	    },
	edge{ source => N3, target => N1, label => label[csu], type => lexical },
	node!first_main_verb(_N2,N2)
	.

%% COMP pour csu sans que: quand il vient, il mange

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => csu },
	  arg2 => N2::node{ cat => cat[v,aux] }
	} :-
	edge{ source => N1,
	      target => _N2,
	      label => label['SubS','S','S2'],
	      type => subst
	    },
%%	recorded( terminal_v(_N2,N2) )
	node!first_main_verb(_N2,N2)
	.

%% COMP pour si and comme in whcomp

relation{ type => 'COMP',
	  arg1 => N1::node{ lemma => N1_Lemma },
	  arg2 => N2::node{ cat => cat[v,aux] }
	} :-
	domain(N1_Lemma,[si,comme]),
	chain( N1 << lexical << node{ cat => v } >> (subst @ xcomp) >> _N2::node{ cat => v } ),
	node!first_main_verb(_N2,N2)
	.

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => que },
	  arg2 => N2::node{}
	} :-
	edge{ target => Super::node{ cat => supermod },
	      label => supermod,
	      type => adj
	    },
	edge{ source => Super,
	      target => N1,
	      label => que,
	      type => lexical
	    },
	edge{ source => Super,
	      target => _N2::node{ cat => Cat },
	      label => 'Modifier',
	      type => subst
	    },
	get_head(_N2,N2)
	.

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => que },
	  arg2 => N2::node{ cat => v }
	} :-
	chain( N1 << lexical <<
	     node{ cat => aux, lemma => être } << (adj @ 'S') << N2 )
	.

%% The Passage manual is not very clear on cases such as
%% qu'est ce qu'il mange ?
%% we choose [qu']_1 as COD of mange and [qu']_2 as COMP
%% but another choice could be
%% [qu']_1 ATB-SO est, [qu']_2 COD of mange, and [qu']_2 MOD-N [qu']_1
%% corresponding as an wh-extraction of attribute from the cleft-extracted  COD.
relation{ type => 'COMP',
	  arg1 => Que::node{ cat => prel, lemma => que },
	  arg2 => V::node{ cat => v }
	} :-
	chain( Que << (lexical @ 'CleftQue') << V
	     >> _ >> X::node{ cluster => cluster{ right => XR }}
	     ),
	chain(V >> (adj @ 'S') >> node{ cat => aux,
					lemma => être,
					cluster => cluster{ left => AuxL }
				      }),
	XR =< AuxL
	.

%% COMP entre Prep et (GN GA ou NV) quand discontinus

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => prep, cluster => cluster{ right => Right1} },
	  arg2 => N2::node{ cat => cat[nc,cln,pro,np,pri,prel,v,adj,ilimp,caimp],
			    cluster => cluster{ id => CId2, left => Left2 }}
	} :-
	x_command(N1,_N2::node{ cat => cat[nc,cln,pro,np,pri,prel,v,adj,ilimp,caimp] }),
	/*
	\+ ( edge{ source => _N3,
		   target => N1,
		   label => preparg,
		   type => subst
		 },
	     edge{ source => _N3,
		   target => _N2,
		   label => label[xcomp,object,comp],
		   type => subst
		 }
	    ),
	*/
	( chain( N1 >> _ >> _N2)
	; chain( N1 << (lexical @ prep ) << node{} >> (subst @ xcomp) >> _N2)
	),
	verbose('Found pre potential COMP ~E ~E\n',[N1,_N2]),
	( _N2 = N2,
	  \+ '$answers'( group(const['GP','PV'],_,_,N1,_) )
	; edge{ source => _N2, target => COO::node{ cat => coo, lemma => COO_Lemma }, type => adj },
	  domain(COO_Lemma,[et,ou]),
	  edge{ source => COO,
		target => N2::node{ lemma => N2_Lemma },
		label => label[coord,coord2, coord3] },
	  %% avoid locution 'ou non' (should not occur)
	  \+ (COO_Lemma = ou, N2_Lemma=non)
	),
	Right1 =< Left2,
	verbose('Found potential COMP ~E ~E\n',[N1,N2]),
	'$answers'( group(const['GN','NV','GA'],_,_,N2,_) ),
	\+ ( '$answers'( group(const['GP','PV'],_,_,N1,Content) ),
	     verbose('Content ~E ~L\n',[N1,['~E ', ' '],Content]),
	     domain(CId2,Content)
	   )
	.

relation{ type => 'ATB-SO',
	  arg1 => YN1,
	  arg2 => N2::node{ cat => v},
	  arg3 => SO
	} :-
	edge{ source => N2, target => _N1::node{ cat => _Cat1}, label => comp },
	( _Cat1 = cat[comp,prep] ->
	  edge{ source => _N1, target => __N1::node{ cat => X}, type => subst }
	;
	  _N1 = __N1
	),
	( X = 'N2' ->
	  nominal_head(__N1,N1)
	;
	  __N1=N1
	),
	( edge{ source => N2, label => object } -> SO = objet ; SO = sujet ),
	try_reroot_cleft(N1,N2,XN1),
	get_head(XN1,YN1)
	.

relation{ type => 'ATB-SO',
	  arg1 => N2,
	  arg2 => N1,
	  arg3 => sujet
	} :-
	edge{ source => N2::node{ cat => adj },
	      target => N1::node{ cat => v },
	      label => label['Infl',aux],
	      type => adj
	    }.

relation{ type => Type,
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => N2Cat::cat[adj,nc] },
	  arg3 => faux
	} :-
	edge{ source => N2,
	      target => N1,
	      type => subst,
	      label => label[preparg,xcomp]
	    },
	( N2Cat = nc -> Type = 'MOD-N'
	; N2Cat = adj -> Type = 'MOD-A'
	; fail
	)
	.

relation{ type => Type,
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => N2Cat::xnominal[] , xcat => N2XCat },
	  arg3 => faux
	} :-
	( edge{ source => N2,
		target => N3::node{ cat => Cat3::cat[~ coo] },
		%%	      type => edge_kind[adj,lexical]
		type => edge_kind[adj],
		label => Label1
	      },
	  (domain(N2XCat,['N2','N'])
	  xor
	  %% case of on quantity-N2 modifying a prep
	  N2XCat=prep, N2Cat=nc
	  )
	;
	  N2Cat = ce,
	  chain( N2 << (lexical @ ce) << node{ cat => coo }
	       >> (adj @ label[vmod,mod]) >> node{} >> subst >> N3
	       )
	),
	( N2Cat = adj ->
	  \+ domain(Label1,['V','Infl',aux,modal]),
	  Type = 'MOD-A' ;
	  Type = 'MOD-N' ),
	( node!empty(N3) ->
	  %% Relatives, csu, participiales, ... 
	  edge{ source => N3, target => _N1, type => subst, label => Label },
	  ( _N1 = node{ cat => cat[v] },
	    node!first_main_verb(_N1,N1)
	  ; _N1 = node{ cat => cat[adj] } ->
	    edge{ source => _N1,
		  target => _V::node{ cat => v},
		  type => adj,
		  label => label['Infl',aux]
		},
	    node!first_main_verb(_V,N1)
	  ; %% for cases like '3 enfants dont deux filles'
	    Label = 'N2Rel',
	    _N1=N1
	  )
	;   Cat3 = cat[prep,csu] ->
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	  \+ (Cat3 = np),
	  N3 = N1
	)
	.

/*
%% titles
relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => title },
	  arg2 => N2::node{ cat => cat[nc,np,pro] },
	  arg3 => faux
	} :-
	chain( N2 >> ( lexical @ _ ) >> N1 )
	.
*/

%% Predet (to be checked)
relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => predet },
	  arg2 => N2::node{ cat => xnominal[nc,pro,np,adj] },
	  arg3 => faux
	} :-
	(
	 chain( N2 >> (subst @ det) >> N1 )
	;
	 chain( N2 >> (subst @ det) >> node{ cat => det } >> (adj @ det) >> N1 )
	)
	.

%% Special cases for ce
relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => N2Cat::xnominal[ce], xcat => 'N2' },
	  arg3 => faux
	} :-
	edge{ source => N2,
	      target => _N1::node{ cat => Cat3::cat[~ coo] },
	      label => 'SRel',
	      type => edge_kind[subst]
	    },
	node!first_main_verb(_N1,N1)
	.

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => N2Cat::xnominal[ce] },
	  arg3 => faux
	} :-
	chain( N2 << (lexical @ coord3) << node{ cat => coo } >> adj >> _N1::node{} ),
	get_head(_N1,N1)
	.

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cat[adj,adv] },
	  arg2 => N2::node{ cat => ncpred },
	  arg3 => faux
	} :-
	edge{ source => N3::node{ cat => v},
	      target => N1,
	      type => adj,
	      label => ncpred
	    },
	edge{ source => N3,
	      target => N2,
	      type => lexical,
	      label => ncpred
	    }
	.

%% Case for genitives through N2Rel
%% il a deux enfants dont trois filles
relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => prel },
	  arg2 => N2::node{ cat => N2Cat::xnominal[], xcat => 'N2' },
	  arg3 => faux
	} :-
	edge{ source => N3, target => N1, label => prel },
	edge{ source => N3, target => N2, label => 'N2Rel' }
	.

%% Case for adj_on_s
relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cat[adj] },
	  arg2 => N2::node{},
	  arg3 => faux
	} :-
	edge{ target => N1,
	      type => adj,
	      source => V::node{ cat => cat[v] }
	    },
	deep_subject(V,N2,subject)
	.

:-extensional det_as_modn/1.

det_as_modn(number[]).
det_as_modn('quelque').
det_as_modn('différent').
det_as_modn('divers').
%% det_as_modn('chaque').
%% det_as_modn('tout').
%det_as_modn('tel').
%det_as_modn('telle').
%det_as_modn('telles').
%det_as_modn('tels').

relation{ type => Type,
	  arg1 => N1::node{ cat => cat[det,number,adj], lemma => Lemma },
%	  arg2 => N2::node{ cat => N2Cat::xnominal[] , xcat => 'N2' },
	  arg2 => N2::node{ cat => N2Cat::xnominal[] },
	  arg3 => faux
	} :-
	edge{ source => N2,
	      target => _N1::node{ cat => cat[det,number], lemma => _Lemma },
	      type => subst,
	      label => det
	    },
	( N1 = _N1,
	  det_as_modn(Lemma)
	;
	  edge{ source => _N1,
		target => N1,
		type => edge_kind[lexical,adj]
	      },
	  %%	  \+ domain(Lemma,[tout]), %tout seems to be a special case
	  true
	),
	( N2Cat = adj -> Type = 'MOD-A' ; Type = 'MOD-N' ),
	true
	.

relation{ type => 'MOD-N',
	  arg1 => N1,
	  arg2 => N1,
	  arg3 => vrai
	} :-
	N1::node{ cat => np, cluster => cluster{ left => Left, right => Right} },
	Left + 1 < Right
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{ cat => terminal[] },
	  arg2 => N1::node{ cat => nominal[] },
	  arg3 => faux
	} :-
	edge{ source => N1, target => N2::node{ cat => np }, label => 'np',
	      type => edge_kind[~ [lexical]]
	    },
	\+ edge{ source => N1, label => 'MLex', type => lexical }
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{},
	  arg2 => N1::node{},
	  arg3 => faux
	} :-
	fail,
	edge{ source => V::node{cat => v},
	      target => S::node{cat => cat['S','VMod']},
	      type => adj,
	      label => label['S','vmod','S2',mod] },
	edge{ source => S,
	      target => N2::node{ cat => v },
	      label => 'SubS',
	      type => 'subst',
	      id => EId
	    },
	edge2top(EId,OId),
	check_op_top_feature(OId,mode,Mode),
	domain(Mode,[participle]),
	deep_subject(V,N1,subject)
	.

relation{ type => Type,
	  arg1 => N1::node{ cat => que_restr,
			    cluster => cluster{ right => R }},
	  arg2 => N3
	} :-
	'$answers'( group(GType,R,_,N2,Content) ),
	N1,
	( GType = 'GN' -> Type = 'MOD-N', N3=N2
	; GType = 'GA' -> Type = 'MOD-A', N3=N2
	; GType = 'GR' -> Type = 'MOD-R', N3=N2
	; GType = 'NV' -> Type = 'MOD-V', N3=N2
	; GType = 'GP' -> Type = 'MOD-N',
	  ( edge{ source => N2,
		  target => N3,
		  type => subst
		}
	  xor edge{ source => V,
		    target => N1
		  },
	    edge{ source => V,
		  target => N3::node{ cluster => cluster{ id=> N3_CId } }
		},
	    domain(N3_CId,Content)
	  )
	; GType = 'PV' -> Type = 'MOD-V',
	  ( edge{ source => N2,
		  target => N3,
		  type => subst
		}
	  xor edge{ source => V,
		    target => N1
		  },
	    edge{ source => V,
		  target => N3::node{ cluster => cluster{ id=> N3_CId } }
		},
	    domain(N3_CId,Content)
	  )
	; fail
	).

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cat[adj], cluster => cluster{ left => L1, right => R1 }},
	  arg2 => N2::node{ cat => cat[nc], cluster => cluster{ left => L2, right => R2 } }
	} :-
	recorded( mode(robust) ),
	N1,
	\+ edge{ target => N1, type => edge_kind[adj,lexical] },
	(  R1 = L2, N2
	xor L1 = R2, N2
	)
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{ cat => nominal[] },
	  arg2 => N1::node{ cat => terminal[] }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => nominal[], lemma => number[] },
	      label => 'Nc2',
	      type => lexical
	    }
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{ cat => nominal[] },
	  arg2 => N1::node{ cat => terminal[], lemma => Lemma1 }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => nominal[] },
	      label => 'Np2',
	      type => lexical
	    },
	\+ appos_function(Lemma1)
	.

relation{ type => 'MOD-N',
	  arg1 => Adj::node{ cat => cat[adj] },
	  arg2 => N::node{ cat =>nominal[] }
	} :-
	chain( Adj << (lexical @ label[predet_ante,predet_post]) << N )
	.

relation{ type => 'MOD-A',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[adj] , xcat => cat[~ ['N2']] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj,
	      label => Label
	    },
	\+ domain(Label,['V','Infl',aux,modal]),
	( node!empty(N3) ->
	    %% not sure it may arise for adjectives !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	    N3 = N1
	)
	.

%% For impersonal adjectival construction such as
%% il est judicieux qu'il vienne
%% 'qu'il vienne' is a deep subjet but a shallow MOD-A
relation{ type => 'MOD-A',
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => adj }
	} :-
	edge{ source => N2,
	      label => impsubj
	    },
	edge{ source => N2,
	      target => _N1,
	      label => subject
	    },
	get_head(_N1,N1)
	.

relation{ type => 'MOD-A',
	  arg1 => N1::node{ cat => cld, form => Form },
	  arg2 => N2::node{ cat => adj }
	} :-
	\+ Form = y,
	chain( N2 >> (lexical @ preparg) >> N1)
	.

relation{ type => 'MOD-A',
	  arg1 => N1::node{ cat => cld, form => Form },
	  arg2 => N2::node{ cat => adj }
	} :-
	\+ Form = y,
	chain( N2 << (subst @ comp) << V::node{ cat => v } >> (lexical @ preparg) >> N1 ),
	\+ has_cld(V,_)
	.

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cld, form => Form },
	  arg2 => N2::node{}
	} :-
	\+ Form = y,
	chain( N2 << (_ @ object) << V::node{ cat => v } >> (lexical @ preparg) >> N1 ),
	\+ has_cld(V,_)
	.

:-light_tabular has_cld/2.
:-mode(has_cld/2,+(+,-)).

has_cld(V::node{ id => NId },Arg) :-
	node2live_ht(NId,HTId),
	check_xarg_feature(HTId,args[arg1,arg2],obj�,_,cld[])
	.

relation{ type => 'MOD-R',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[adv,advneg] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj
	    },
%%	format('HERE ~w ~w\n',[N2,N3]),
	( node!empty(N3) ->
	    %% not sure it may arise for adverbs !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    %% not sure it may arise for adverbs !
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	    N3 = N1
	)
	.

relation{
	 type => 'MOD-R',
	 arg1 => N1::node{ cat => terminal[] },
	 arg2 => N2::node{ cat => cat[adv,advneg] }
	} :-
	( edge{ source => N2,
		target => N1,
		type => lexical,
		label => void
	      },
	  N1 = node{ cat => pro, form => ce }
	xor
	edge{ source => N2,
	      target => N3,
	      type => subst,
	      label => Label::label[preparg,xcomp]
	    },
	  ( Label = xcomp ->
	    N3 = N1
	  ;
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	  )
	).
	
relation{ type => 'MOD-R',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[adv] }
	} :-
	edge{ source => N2,
	      target => Super::node{ cat => supermod },
	      label => supermod,
	      type => adj
	    },
	(
	 edge{ source => Super,
	       target => CSU::node{ cat => csu }
	     },
	 edge{ source => CSU,
	       target => N1,
	       type => edge_kind[subst,lexical]
	     }
	;
	 edge{ source => Super,
	       target => N1,
	       label => 'Modifier',
	       type => subst
	     }
	)
	.

relation{ type => 'MOD-P',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[prep] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj },
%%	format('HERE ~w ~w\n',[N2,N3]),
	( node!empty(N3) ->
	    %% not sure it may arise for prepositions !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    %% not sure it may arise for prepositions !
	  %% edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	  N3=N1
	;   Cat3 = adv ->
	  \+ ( edge{ source => node{ cat => cat[v,'S','VMod'] },
		     target => N2
		   }
	     ),
	  N3 = N1
	;
	  N3 = N1
	)
	.


relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => adv },
	  arg2 => N2::node{ cat => v}
	} :-
	( edge{ source => _N2::node{ cat => cat[v] },
		target => P::node{ cat => prep }
	      }
	;
	  edge{ source => _AUX::node{ cat => cat['S','VMod'] },
		target => P::node{ cat => prep }
	      },
	  edge{ source => _N2,
		target => _AUX,
		type => adj
	      }
	),
	edge{ source => P,
	      target => N1,
	      type => adj,
	      label => 'PP'
	    },
	node!first_main_verb(_N2,N2)
	.

%% MOD-V are also present when a floating adv is attached to a coo
relation{ type => Type,
	  arg1 => Mod::node{},
	  arg2 => N::node{ cat => Cat }
	} :-
	edge{ source => COO::node{ cat => coo },
	      target => _Mod::node{ cluster => cluster{ right => _Mod_Right }},
	      type => edge_kind[adj,lexical],
	      label => label[coo,pas]
	    },
	edge{ source => COO,
	      target => _N::node{ cluster => cluster{ left => _N_Left }},
	      label => label[coord2,coord3,coord],
	      type => edge_kind[subst,lexical]
	     },
	_Mod_Right =< _N_Left,
%%	format('potential mod on coo ~E and ~E\n',[_Mod,_N]),
%%	climb_till_condition(N,V),
	get_head(_Mod,Mod),
	get_head(_N,N),
	( Cat = v ->
	    Type = 'MOD-V'
	; Cat = adv ->
	    Type = 'MOD-R'
	; Cat = xnominal[] ->
	    Type = 'MOD-N'
	)
	.

%% MOD-R in superlative with 'possible'
relation{ type => 'MOD-R',
	  arg1 => Mod::node{ lemma => possible },
	  arg2 => Adv::node{ cat => adv }
	} :-
	chain(Adv >> (adj @ supermod) >> node{ cat => supermod } >> lexical >>  Mod)
	.

:-std_prolog climb_till_condition/2.

climb_till_condition(N::node{}, V::node{}) :-
	( N=V
	xor
	  chain( N << _ << _N::node{}),
	  climb_till_condition(_N,V)
	)
	.

relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => vide,
	  arg3 => N3::node{}
	} :-
	edge{ source => Start::node{ cat => 'S' },
	      target => Coord::node{ cat => cat[coo] },
	      type => lexical,
	      label => starter
	    },
	edge{ source => N3,
	      target => Start,
	      type => adj
	    }
	.

relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => XN2::node{},
	  arg3 => XN3::node{}
	} :-
	edge{ source => Start::node{},
	      target => LastCoord::node{ cat => cat[coo] },
	      type => adj,
	      label => StartLabel
	    },
	(   _N2 = Start
	;   edge{ source => LastCoord, target => _N2, label => coord2 }
	),
	coord_next(LastCoord,_N2,Coord),
	coord_next(LastCoord,Coord,_N3),
	\+ node!empty(Coord),
	( node!empty(_N2) ->
	  edge{ source => _N2,
		target => N2,
		type => edge_kind[subst,lexical]
	      },
	  N2=node{ cat => cat[v,prep,nc,np] }
	; _N2 = Start, StartLabel = xcomp ->
	  %% coordination on sentential argument of a verb
	  %% need to follow an indirection to retrieve the argument
	  edge{ source => Start,
		target => N2,
		label => StartLabel,
		type => edge_kind[~ [adj]]
	      }
	;
	  N2 = _N2
	),
	( node!first_main_verb(N2,XN2)
	xor get_head(N2,XN2)
	),
	( node!empty(_N3) ->
	  edge{ source => _N3,
		target => N3,
		type => edge_kind[subst,lexical]
	      },
	  N3=node{ cat => cat[v,prep,nc,np] }
	;   
	  N3 = _N3
	),
	%%( node!first_main_verb(N3,XN3) xor N3=XN3 ),
	get_head(N3,XN3),
	true
	.

/*
relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => XN2::node{},
	  arg3 => XN3::node{}
	} :-
  */
relation{ type => 'JUXT',
	  arg1 => XN2::node{},
	  arg2 => XN3::node{}
	} :-
	edge{ source => Start::node{},
	      target => Enum::node{ tree => Tree },
	      type => adj
	    },
	domain('N2_enum',Tree),
	LastCoord = Enum,
%	format('test enum lastcord=~w\n',[LastCoord]),
	(   _N2 = Start
	;   edge{ source => LastCoord, target => _N2, label => coord }
	),
	coord_next(LastCoord,_N2,Coord),
%	format('phase1 next2=~w coord=~w\n',[_N2,Coord]),
	coord_next(LastCoord,Coord,_N3),
%	format('phase2 coord=~w next3=~w\n',[Coord,_N3]),
	( node!empty(_N2) ->
	  edge{ source => _N2,
		target => N2,
		type => edge_kind[subst,lexical]
	      },
	  N2=node{ cat => cat[v,prep,nc,np] }
	;   
	  N2 = _N2
	),
	( node!first_main_verb(N2,XN2) xor N2=XN2 ),
	( node!empty(_N3) ->
	  edge{ source => _N3,
		target => N3,
		type => edge_kind[subst,lexical]
	      },
	  N3=node{ cat => cat[v,prep,nc,np] }
	;   
	  N3 = _N3
	),
	( node!first_main_verb(N3,XN3) xor N3=XN3 ),
	XN2 \== XN3,
	true
	.

relation{ type => 'APPOS',
	  arg1 => N2::node{ cat => terminal[] },
	  arg2 => N1::node{ cat => nominal[] }
	} :-
	edge{ source => _N2, target => N3::node{ cat => 'N2' }, type => adj },
	node!empty(N3),
	edge{ source => N3, target => N1, type => subst, label => 'N2app' },
	nominal_head(_N2,N2),
	true
	.

/*
relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => nominal[] },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => np },
	      label => 'np',
	      type => lexical
	    },
%%	edge{ source => N1, label => 'MLex', type => lexical },
	true
	.
*/

relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => Cat1::nominal[] },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => np },
	      label => 'N2',
	      type => adj
	    },
	Cat1 = nc,
%%	edge{ source => N1, label => 'MLex', type => lexical },
	true
	.

relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => nominal[], lemma => N2L },
	      label => 'Nc2',
	      type => lexical
	    },
	\+ N2L=number[]
	.

relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => terminal[], lemma => Lemma1 },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N1,
	      target => N2::node{ cat => nominal[] },
	      label => 'Np2',
	      type => lexical
	    },
	appos_function(Lemma1)
	.

%% N_1->S_2 S_2 S_2->+N_2
%% or S_1 ->+ N_1 S_1 -> S_2 S_2->+N_2
relation{ type => 'JUXT',
	  arg1 => N1::node{ },
	  arg2 => N2::node{ }
	} :-
	edge{ source => S1::node{ cat => CatS1 },
	      target => S2::node{ cat => cat['S'] },
	      label => label['S','S2'],
	      type => adj
	    },
	node!empty(S2),
	edge{ source => S2, target => _N2::node{ cat => _Cat2 }, type => subst, label => label['S'] },
	edge{ source => S2,
	      target => N4::node{ cluster => cluster{ token => Token }},
	      type => lexical },
	domain(Token,[',',';','.',':']),
	( _Cat2 = cat[v] -> recorded(terminal_v(_N2,N2 ))
	; _Cat2 = cat['S'],
	  edge{ source => _N2, target => Comp2, type => subst, label => comp },
	  edge{ source => Comp2, target => N2, type => subst, label => label[comp,'N2'] }
	),
	( CatS1 = cat[v] -> recorded(terminal_v(S1,N1 ))
	; CatS1 = cat['S'],
	  edge{ source => S1, target => Comp1, type => subst, label => comp },
	  edge{ source => Comp1, target => N1, type => subst, label => label[comp,'N2'] }
	)
	.

relation{ type => 'JUXT',
	  arg1 => First,
	  arg2 => Second
	} :-
	edge{ source => N2::node{ cat => cat[v] ,
				  cluster => cluster{ left => L2}},
	      target => N3::node{ cat => cat['S','VMod'] },
	      type => adj,
	      label => label['S','S2',vmod,mod]
	    },
	edge{ source => N3,
	      target => N1::node{ %% cat => xnominal[],
				  cluster => cluster{ left => L1 }
				},
	      type => subst,
	      label => label[person_mod,audience,reference,'S_incise',position,ce_rel]
	    },
	node!first_main_verb(N2,_N2),
	( L1 < L2 ->
	  First =N1, Second = _N2
	;
	  First = _N2, Second = N1
	)
	.

relation{ type => 'JUXT',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	fail,			% handled as a coord (see above)
	edge{ source => N1,
	      target => N3,
	      type => adj,
	      label => 'N2'
	    },
	edge{ source => N3,
	      target => N2,
	      type => subst,
	      label => coord
	    }
	.

%%% Intertoken relations
%% for instance, for 'dimanche prochain'

relation{ type => 'MOD-N',
	  arg1 => f(FId1),
	  arg2 => f(FId2)
	} :-
	fail,
	opt(passage),
	N::node{ cat => nc,
		 lemma => date[],
		 cluster => cluster{ id => CId }
	       },
	f{ cid => CId, id => FId1, lex => Lex, rank => R },
	domain(Lex,[ suivant,suivante,suivants,
		     prochain,prochaine,prochains,
		     dernier,derni�re,derniers
		   ]),
	S is R-1,
	f{ cid => CId, id => FId2, rank => S },
	format('Intertoken ~E fid1=~w lex=~w rank=~w fid2=~w\n',[N,FId1,Lex,R,FId2]),
	true
	.


%% JUXT: to be done

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Decoding relations

:-extensional xrelation/2.

xrelation( relation{ type => Type::'SUJ-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(sujet,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'AUX-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(auxiliaire,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COD-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(cod,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'CPL-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(complement,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COMP', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arghead(complementeur,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'ATB-SO', arg1 => Id1, arg2 => Id2, arg3 => SO, id => Id },
	   xrelation(Type,Id,[ arg(attribut,Id1), arg(verbe,Id2), so(SO) ] )
	 ).

xrelation( relation{ type => Type::'MOD-N', arg1 => Id1, arg2 => Id2, arg3 => Prop, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(nom,Id2), 'a-propager'(Prop) ] )
	 ).

xrelation( relation{ type => Type::'MOD-A', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(adjectif,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-R', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(adverbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-P', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(preposition,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COORD', arg1 => Id1, arg2 => Id2, arg3 => Id3, id => Id },
	   xrelation(Type,Id,[ arg(coordonnant,Id1),arg('coord-g',Id2), arg('coord-d',Id3) ] )
	 ).

xrelation( relation{ type => Type::'APPOS', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(premier,Id1), arg(appose,Id2) ] )
	 ).

xrelation( relation{ type => Type::'JUXT', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(premier,Id1), arg(suivant,Id2) ] )
	 ).

:-rec_prolog f2groups/3.

f2groups([],[],_).

f2groups([Arg|Args],[XArg|XArgs],All) :-
	f2groups(Args,XArgs,All),
	(   Arg = arg(Type,FId),
	    f2group(FId,GId),
	    \+ ( f2group(FId2,GId),
		 FId2 \== FId,
		 domain( arg(_,FId2), All) ) ->
	    XArg = arg(Type,GId)
	; Arg = arghead(Type,FId) ->
	    XArg = arg(Type,FId)
	;   
	    XArg = Arg
	)
	.

/*
f2groups([Arg|Args],[XArg|XArgs]) :-
	f2groups(Args,XArgs),
	( Arg = arghead(Type,FId) ->
	  XArg = arg(Type,FId)
	;   
	  XArg = Arg
	)
	.
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Process for printing XML objects
%%   extended for EASy Objects

xevent_process( H::default(Stream), xmldecl, Ctx, Ctx, XH ) :-
	format(Stream,'<?~w~L?>',
	       [xml,
		[' ~A',' '],
		[ version:'1.0', %'
		  encoding:'ISO-8859-1']]
	      )
	.
	
xevent_process( H::default(_), C::cluster{ id => Id, left => Left },Ctx1,Ctx2,XH) :-
%%	format('Cluster ~w: ~w\n',[XH,C]),
	event_process(XH,iterate_in_cluster(Id,Left),Ctx1,Ctx2).

xevent_process( H::default(_), iterate_in_cluster(_CId,Left),Ctx1,Ctx2,XH ) :-
	mutable(M,[],true),
	every(( ( CId=_CId
		; recorded( redirect(_CId,CId) )),
	        F::f{ cid => CId },
		mutable_read(M,_L),
		f_sorted_add(F,_L,_LL),
		mutable(M,_LL)
	      )),
	mutable_read(M,L),
%%	format('Emit Cluster: ~w => ~w\n',[CId,L]),
	event_process( XH, fs(L), Ctx1,Ctx2 )
	.

:-rec_prolog f_sorted_add/3.

f_sorted_add(F,[],[F]).
f_sorted_add(F1::f{ rank => R1 }, L::[F2::f{ rank => R2 }|L2],XL) :-
	(  R1 =< R2 ->
	    XL = [F1|L]
	;
	    XL = [F2|XL2],
	    f_sorted_add(F1,L2,XL2)
	)
	.
	    
xevent_process( H::default(_),fs([]),Ctx,Ctx,_).
xevent_process( H::default(_),fs([F|L]),Ctx1,Ctx3,XH) :-
	event_process(XH,F,Ctx1,Ctx2),
	event_process(XH,fs(L),Ctx2,Ctx3)
	.

xevent_process( H::default(_),
		f{ id => Id, lex => Lex, cid => CId, rank => Rank },
		Ctx1,
		Ctx4,
		XH
	     ) :-
	verbose('Handling f id=~w lex=~w ctx=~w xh=~w\n',[Id,Lex,Ctx1,XH]),
	(   recorded( emitted(Id) ),
	    Ctx4 = Ctx1
	xor
	    \+ domain(Ctx1,[open(_,_,_),middle(_,_,_),close(_,_,_)]),
	    \+ used( CId ),
	    recorded( redirect(_CId,CId) ),
	    used(_CId),
	    Ctx4 = Ctx1,
	    verbose('To be emitted later in a group for cid=~w: f id=~w lex=~w ctx=~w\n',[_CId,Id,Lex,Ctx1]),
	    true
	xor  
	( domain(Lex,['«','"','''','»','&quot;']),
	  Ctx1 = open(_Ctx5,GId,Type) ->
	  Ctx4 = open(_Ctx4,GId,Type)
	; domain(Lex,['»','"','''','.','...','&quot;']),
	  Ctx1 = middle(_Ctx1,GId,Type),
	  \+ ( %% check Lex is indeed the last F of group GId
	       ( f{ id => _FId, cid => CId, rank => _Rank },
		 Rank < _Rank
	       ;
		 cluster{ id => CId, right => _Left },
		 cluster{ id => _CId, left => _Left, right => _Right },
		 _Left < _Right,
		 f{ id => _FId, cid => _CId }
	       ),
	       recorded( f2group(_FId,GId) )
	     )
	->
	  event_process( XH, end_element{ name => 'Groupe' }, _Ctx1, _Ctx5 ),
	  Ctx4=close(_Ctx4,GId,Type)
	; Ctx1 = open(_Ctx1,GId,Type) ->
	  event_process( XH, start_element{ name => 'Groupe',
					   attributes => [id:GId,type:Type] },
			 _Ctx1,
			 _Ctx5 ),
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = middle(_Ctx5,GId,Type) ->
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = close(_Ctx5,GId,Type) ->
	  Ctx4 = close(_Ctx4,GId,Type)
	; Ctx4 = _Ctx4,
	  Ctx1 = _Ctx5
	),
	    mutable(MCat,[],true),
	    every((( recorded( redirect(_CId,CId) ),
		     node{ cluster => cluster{ id => _CId }, cat => _Cat }
		   ; node{ cluster => cluster{ id => CId }, cat => _Cat }
		   ),
%		   mutable_read(MCat,_LCat),
%		   mutable(MCat,[_Cat|_LCat])
		   mutable_list_extend(MCat,_Cat)
		  )),
	    mutable_read(MCat,LCat),
	    ( LCat = [] -> Cat = ''
	    ; LCat = [Cat] -> true
	    ; name_builder('~L',[['~w',' '],LCat],Cat)
	    ),
	    event_process( XH, start_element{ name => 'F', attributes => [id:Id,cat:Cat] }, _Ctx5, _Ctx2 ),
	    event_process( XH, characters{ value => Lex }, _Ctx2, _Ctx3 ),
	    event_process( XH, end_element{ name => 'F' }, _Ctx3, _Ctx4 ),
	    record_without_doublon( emitted(Id) )
	)
	.

:-extensional f2group/2.

xevent_process( H::default(_),group(Type,_,_,N::node{ id =>NId },Content),Ctx1,Ctx4, XH) :-
	group_gensym(Id),
	record_without_doublon( group_head(NId,Id) ),
%%	verbose('Handling group ~w ~w ~w\n',[Id,Type,Content]),
	every(( domain(_CId,Content),
		( CId = _CId
		;  recorded( redirect( _CId, CId) )
		),
		f{ cid => CId, id => FId },
		verbose('Handling cid=~w fid=>~w\n',[CId,FId]),
		record_without_doublon( f2group(FId,Id) ))),
	Ctx1=Ctx2,
	event_process( XH,
		       flist(Content),
		       open(Ctx2,Id,Type),
		       close(Ctx3,Id,Type)
		     ),
	Ctx3=Ctx4,
	true
	.

xevent_process( H::default(_), flist([]),
		open(Ctx,GId,Type),
		close(Ctx,GId,Type),
		XH
	     ) :-
	verbose('*** Void group: ~w ~w\n',[GId,Type])
	.

xevent_process( H::default(_), flist([]),
	       close(Ctx,GId,Type),
	       close(Ctx,GId,Type),
		XH
	     ) :-
	verbose('*** Group already closed: ~w ~w\n',[GId,Type])
	.

xevent_process( H::default(_), flist([]),
		middle(Ctx1,GId,Type),
		close(Ctx2,GId,Type),
		XH
	     ) :-
	event_process( XH, end_element{ name => 'Groupe' }, Ctx1, Ctx2 )
	.
	
xevent_process( H::default(_), flist([Id|L]), Ctx1, Ctx3,XH ) :-
	C::cluster{id => Id },
	event_process(XH,C,Ctx1,Ctx2),
	event_process(XH,flist(L),Ctx2,Ctx3)
	.

xevent_process( H::default(_), constituants, Ctx1, Ctx2, XH) :-
%%	event_process( H, start_element{ name => 'constituants', attributes => [] }, Ctx1, Ctx2 ),
	event_process( XH, iterate_constituant(0), Ctx1, Ctx2 )
	.

xevent_process( H::default(_), iterate_constituant(N), Ctx1, Ctx3, XH ) :-
%%	format('ITERATE CONSTITUANTS ~w',[N]),
	(   constituant(Left,Right,Const),
	    Left >= N,
	    \+ ( constituant(_Left,_,_), _Left >= N, Left > _Left )
	->
%%	    format('CONST ~w\n',[Const]),
	    event_process(XH,Const,Ctx1,Ctx2),
	    event_process(XH,iterate_constituant(Right),Ctx2,Ctx3)
	;   
%%	    event_process(H,end_element{ name => 'constituants' }, Ctx1,Ctx3)
	    Ctx1=Ctx3
	)
	.

xevent_process( H::default(_), relations, Ctx1, Ctx4,XH ) :-
	event_process(XH,start_element{ name => relations, attributes => []}, Ctx1, Ctx2 ),
	mutable(Ctx,Ctx2,true),
	every(( domain(Type,rel[]),
		'$answers'( relation(R::relation{ type => Type }) ),
		xrelation(R,XR::xrelation(Type,Id,Content)),
		(   f2groups(Content,Content1,Content)		   
		xor Content = Content1 ),
		event_process(XH,xrelation(Type,Id,Content1),Ctx)
	      )),
	mutable_read(Ctx,Ctx3),
	event_process(XH,end_element{ name => relations }, Ctx3, Ctx4 )
	.

xevent_process( H::default(_), xrelation(Type,Label,Content), Ctx1, Ctx4,XH) :-
	event_process(XH,
		      start_element{ name => relation,
				     attributes => [ xlink!type: extended,
						     type: Type,
						     id: Label ] },
		      Ctx1,
		      Ctx2 ),
	mutable(Ctx,Ctx2,true),
	event_process(relarg(XH),Content,Ctx),
	mutable_read(Ctx,Ctx3),
	event_process(XH, end_element{ name => relation }, Ctx3, Ctx4 )
	.

xevent_process( relarg(XH::default(_)), arg(Type,Label), Ctx1, Ctx2,_) :-
	event_process(XH,
		      element{ name => Type,
			       attributes => [ xlink!type: locator,
					       xlink!href: Label
					     ]
			     },
		      Ctx1, Ctx2
		     )
	.

xevent_process( relarg(XH), so(SO), Ctx1, Ctx2,_ ) :-
	event_process(XH,
		      element{ name => 's-o',
			       attributes => [valeur: SO]
			     },
		      Ctx1, Ctx2
		     ).

xevent_process( relarg(XH::default(_)), 'a-propager'(Prop), Ctx1, Ctx2,_ ) :-
	Prop ?= faux,
	event_process(XH,
		      element{ name => 'a-propager',
			       attributes=> [booleen: Prop]
			     },
		      Ctx1, Ctx2
		     )
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Process for printing XML objects
%%   for Passage format

event_super_handler(passage(Stream),default(Stream)).

xevent_process( H::passage(_), 'TWG', Ctx1, Ctx2,XH) :-
	event_process( XH, iterate_twg(0), Ctx1, Ctx2 )
	.

xevent_process( H::passage(_), iterate_twg(N), Ctx1, Ctx3,XH ) :-
%%	format('ITERATE CONSTITUANTS ~w : ~w\n',[XH,N]),
	(   constituant(Left,Right,Const),
	    Left >= N,
	    \+ recorded(processed(Const)),
	    \+ ( constituant(_Left,_,_), _Left >= N, Left > _Left ),
	    %% to be sure that splitted constituants are not skipped
	    (
		% either this is a splitted constituant
		Left == Right
	    xor	% otherwise, all splitted const starting at Left have been processed
		\+ ( constituant(Left,Left,Const2),
		    \+ recorded(processed(Const2)) )
	    ),
	    true
	->
	    verbose('CONST h=~w xh=~w ~w\n',[H,XH,Const]),
	    record(processed(Const)),
	    event_process(XH,Const,Ctx1,Ctx2),
	    event_process(XH,twg(Const),Ctx1,Ctx2),
	    event_process(XH,iterate_twg(Right),Ctx2,Ctx3)
	;   
	    Ctx1=Ctx3
	)
	.

xevent_process( H::passage(_),
	       f{ id => Id, lex => Lex, cid => CId, rank => Rank, tid => TId },
	       Ctx1,
	       Ctx4,
		XH
	     ) :-
	verbose('Handling f id=~w lex=~w cid=~w ctx=~w\n',[Id,Lex,CId,Ctx1]),
	(   recorded( emitted(Id) ),
	    Ctx4 = Ctx1
	xor fail,
	    \+ domain(Ctx1,[open(_,_,_),middle(_,_,_),close(_,_,_)]),
	    \+ used( CId ),
	    recorded( redirect(_CId,CId) ),
	    used(_CId),
	    Ctx4 = Ctx1,
	    verbose('To be emitted later in a group for cid=~w: f id=~w lex=~w ctx=~w\n',[_CId,Id,Lex,Ctx1]),
	    true
	xor  
	( domain(Lex,['«','"','''','»','&quot;']),
	  Ctx1 = open(_Ctx5,GId,Type) ->
	  Ctx4 = open(_Ctx4,GId,Type)
	; domain(Lex,['»','"','''','.','...','&quot;']),
	  Ctx1 = middle(_Ctx1,GId,Type),
	  \+ ( %% check Lex is indeed the last F of group GId
	       ( f{ id => _FId, cid => CId, rank => _Rank },
		 Rank < _Rank
	       ;
		 cluster{ id => CId, right => _Left },
		 cluster{ id => _CId, left => _Left, right => _Right },
		 _Left < _Right,
		 f{ id => _FId, cid => _CId }
	       ),
	       recorded( f2group(_FId,GId) )
	     )
	->
%%	  event_process( H, end_element{ name => 'G' }, _Ctx1, _Ctx5 ),
	  _Ctx1=_Ctx5,
	  Ctx4=close(_Ctx4,GId,Type)
	; Ctx1 = open(_Ctx1,GId,Type) ->
%%	  event_process( XH, start_element{ name => 'G',
%%					    attributes => [id:GId,type:Type] },
%%			 _Ctx1,
%%			 _Ctx5 ),
	  _Ctx1=_Ctx5,
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = middle(_Ctx5,GId,Type) ->
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = close(_Ctx5,GId,Type) ->
	  Ctx4 = close(_Ctx4,GId,Type)
	; Ctx4 = _Ctx4,
	  Ctx1 = _Ctx5
	),
	    mutable(MCat,[],true),
	    every((( recorded( redirect(_CId,CId) ),
		     node{ cluster => cluster{ id => _CId }, cat => _Cat }
		   ; node{ cluster => cluster{ id => CId }, cat => _Cat }
		   ),
%%		   mutable_read(MCat,_LCat),
%%		   mutable(MCat,[_Cat|_LCat])
		   mutable_list_extend(MCat,_Cat)
		  )),
	    mutable_read(MCat,LCat),
	    ( LCat = [] -> Cat = ''
	    ; LCat = [Cat] -> true
	    ; name_builder('~L',[['~w',' '],LCat],Cat)
	    ),
	    %%	    event_process( XH, element{ name => 'W', attributes => [id:Id,pos:Cat,form:Lex] }, _Ctx5, _Ctx4 ),
	    ( recorded(offset(TId,Start,End)) ->
		  Attrs = [id:Id, start: Start, end: End]
	     ;
	     Attrs = [id: Id]
	    ),
	    event_process( XH, start_element{ name => 'T',
					      attributes => Attrs }, _Ctx5, _Ctx51 ),
	    event_process( XH, characters{ value => Lex },_Ctx51,_Ctx52 ),
	    event_process( XH, end_element{ name => 'T' },_Ctx52,_Ctx4),
	    record_without_doublon( emitted(Id) )
	)
	.

xevent_process( H::passage(_), flist([]),
		middle(Ctx1,GId,Type),
		close(Ctx2,GId,Type),
		XH
	     ) :-
%%	event_process( XH, end_element{ name => 'G' }, Ctx1, Ctx2 )
	Ctx1=Ctx2
	.

xevent_process( H::passage(_),
		twg(C::cluster{ id => Id, left => Left }),
		Ctx1,Ctx2,XH) :-
%	format('twg cluster ~w\n',[C]),
	N::node{ id => NId, cluster => C, cat => Cat },
%	format('twg node ~w\n',[N]),
	event_process( XH,
		       N,
		       Ctx1,
		       Ctx2 )
	.

xevent_process( H::passage(_),
		twg(group(Type,_,_,N::node{ id => NId},Content)),
		Ctx1,Ctx2,XH ) :-
	recorded( group_head(NId,Id) ),
	verbose('Dealing with group ~w type=~w headed by ~w\n',[Id,Type,N]),
	event_process( XH,
		       wlist(Content),
		       open(Ctx1,Id,Type),
		       close(Ctx2,Id,Type)
		     ),
	true
	.

xevent_process( H::passage(_), wlist([]),
		open(Ctx,GId,Type),
		close(Ctx,GId,Type),
		XH
	     ) :-
	verbose('*** Void group: ~w ~w\n',[GId,Type])
	.

xevent_process( H::passage(_), wlist([]),
	       close(Ctx,GId,Type),
	       close(Ctx,GId,Type),
		XH
	     ) :-
	verbose('*** Group already closed: ~w ~w\n',[GId,Type])
	.

xevent_process( H::passage(_), wlist([]),
		middle(Ctx1,GId,Type),
		close(Ctx2,GId,Type),
		XH
	     ) :-
	event_process( XH, end_element{ name => 'G' }, Ctx1, Ctx2 )
	.
	
xevent_process( H::passage(_), wlist([Id|L]), Ctx1, Ctx3,XH ) :-
	N::node{ id => NId, cluster => cluster{ id => Id} },
	event_process(XH,N,Ctx1,Ctx2),
	event_process(XH,wlist(L),Ctx2,Ctx3)
	.

xevent_process( H::passage(_),
		N::node{ id => NId,
			 cluster => cluster{ id => _CId },
			 cat => _Cat,
			 lemma => Lemma,
			 lemmaid => LemmaId,
			 form => Form,
			 deriv => Derivs
		       },
		Ctx1,
		Ctx4,
		XH ) :-
	%% Some tokens covered by a node may have to be removed
	%% to build their own form.
	%% this is the case for quotes, as in «lui»
%	format('Dealing with node ~w\n',[N]),
	(
	Ctx1 = open(_Ctx1,GId,Type) ->
	  event_process( XH,
			 start_element{ name => 'G',
					attributes => [id:GId,type:Type] },
			 _Ctx1,
			 _Ctx5 ),
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = middle(_Ctx5,GId,Type) ->
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = close(_Ctx5,GId,Type) ->
	  Ctx4 = close(_Ctx4,GId,Type)
	; Ctx4 = _Ctx4,
	  Ctx1 = _Ctx5
	),
	( cat_abstract(_Cat,Cat) xor Cat=_Cat ),
	( Lemma == '_SENT_BOUND' ->
	  _Ctx4=_Ctx5
	;
	  w_gensym(WId),
	  record_without_doublon( node2wid(NId,WId) ),
	  mutable(MToks,[],true),
	  every(( ( CId=_CId
		  ; recorded( redirect(_CId,CId))),
		  F::f{ cid => CId },
		  mutable_read(MToks,_L),
		  f_sorted_add(F,_L,_LL),
		  mutable(MToks,_LL)
		)),
	  mutable_read(MToks,Tokens),
	  get_fids(Tokens,TokenIds),
	  verbose('tokens ~w ~w => ~w\n',[N,Tokens,TokenIds]),
	  name_builder('~L',[['~w',' '],TokenIds],TIds),
	  ( edge{ target => _Target,
		  source => _Source::node{deriv => _Derivs},
		  id => EId
		},
	    alive_deriv(_Derivs,DID),
	    (_Target=N,
	     deriv(DID,EId,_,_,OId)
	    ;_Source=N,
	     deriv(DID,EId,_,OId,_)
	    ),
	     op{ id => OId, top => FS },
	    \+ var(FS)
	  ->
%	    format('FS mstag oid=~w ~w\n',[OId,FS]),
	    fs2flatfs(FS,_MSTAG),
	    MSTAG1=[mstag:_MSTAG],
%	    format('FS mstag oid=~w => ~w\n',[OId,MSTAG1]),
%	    format('=> MSTAG1 ~w\n',[MSTAG1]),
	    ( keep_ht(Cat),
	      deriv2ht(DID,hypertag{ ht => HT }),
	      \+ var(HT)
	    ->
%%	      format('HT ~w ~w\n',[OId,HT]),
	      fs2flatfs(HT,_HT),
	      MSTAG=[ht:_HT|MSTAG1]
	    ;
	      MSTAG=MSTAG1
	    )
	  ;
	    MSTAG=[]
	  ),
	  ( opt(passage_strict) ->
	    passage_pos(Cat,FS,XCat) xor XCat = Cat
	  ;
	    Cat = XCat
	  ),
	  event_process( XH,
			 element{ name => 'W',
				  attributes => [id:WId,pos:XCat,lemma:Lemma,form:Form,tokens:TIds|MSTAG] },
			 _Ctx5,
			 _Ctx4
		       ),
	  ( Lemma = entities[] ->
	    ( recorded('NE'(LemmaId,_,_)) ->
	      Entity_Id = LemmaId
	    ;
	      Entity_Id=[]
	    ),
	    record( passage_entity(WId,Cat,Lemma,Form,MSTAG,Entity_Id) )
	  ; Lemma = date[] ->
	    record( passage_entity(WId,Cat,Lemma,Form,MSTAG,[]) )
	  ;
	    true
	  )
	
	)
	.

% dangerous to use light_tabular on fs2flatfs
% because FS may contain internal variables
%:-light_tabular fs2flatfs/2.
%:-mode(fs2flatfs/2,+(+,-)).

:-std_prolog fs2flatfs/2.

fs2flatfs(FS,MSTAG) :-
	fs2path(FS,Path),
	name_builder('~L',[['~w',' '],Path],MSTAG)
	.

:-std_prolog fs2path/2.

fs2path(FS,Path) :-
%	format('FS ~w\n',[FS]),
	mutable(M,[],true),
	every(( inlined_feature_arg(FS,F,_,Vals),
		fs_keep(F),
		\+ var(Vals),
		( (atomic(Vals) xor is_finite_set(Vals)) ->
		  vals2flat(Vals,FlatVals),
		  name_builder('~w.~w',[F,FlatVals],FlatF),
		  %%		  mutable_read(M,_Flat),
		  %%		  mutable(M,[FlatF|_Flat])
		  mutable_list_extend(M,FlatF)
		;
		  fs2path(Vals,SubPath),
		  every((domain(_P,SubPath),
			 name_builder('~w.~w',[F,_P],P),
			 mutable_list_extend(M,P)
			))
		)
	      )),	
	mutable_read(M,Path),
%	format('FS ~w => Path ~w\n',[FS,Path]),
	true
	.

:-extensional fs_keep/1.

fs_keep(gender).
fs_keep(hum).
fs_keep(number).
fs_keep(tense).
fs_keep(mode).
fs_keep(time).
fs_keep(wh).
fs_keep(person).
fs_keep(case).
fs_keep(degree).
%fs_keep(pcas).
fs_keep(numberposs).
fs_keep(aux_req).
fs_keep(diathesis).
fs_keep(adv_kind).
fs_keep(def).
fs_keep(dem).
fs_keep(extracted).

fs_keep(arg0).
fs_keep(arg1).
fs_keep(arg2).
fs_keep(extraction).
fs_keep(kind).
fs_keep(pcas).
fs_keep(real).
fs_keep(subarg).
fs_keep(ctrsubj).
fs_keep(imp).
fs_keep(refl).
fs_keep(function).

:-extensional keep_ht/1.

keep_ht(cat[v,aux,adj,nc]).

%:-light_tabular vals2flat/2.
:-std_prolog vals2flat/2.

vals2flat(Vals,MSTAG) :-
	mutable(M,[],true),
%%	format('Val ~w\n',[Vals]),
	every(( domain(_V,Vals),
		(  _V == (+) ->
		   V=plus
		; _V == (-) ->
		   V=minus
		;
		   V=_V
		),
%		mutable_read(M,_Flat),
%		mutable(M,[V|_Flat])
		mutable_list_extend(M,V)
	      )),
	mutable_read(M,Flat),
	name_builder('~L',[['~w','.'],Flat],MSTAG)
	.

:-rec_prolog get_fids/2.

get_fids([],[]).
get_fids([f{ id => FId}|Tokens],[FId|FIds]) :-
	get_fids(Tokens,FIds).

xevent_process( H::passage(_), relations, Ctx1, Ctx2,XH ) :-
	mutable(Ctx,Ctx1,true),
	every(( domain(Type,rel[]),
		'$answers'( relation(R::relation{ type => Type }) ),
		xrelation(R,XR::xrelation(Type,Id,Content)),
		(   f2groups(Content,Content1,Content)		   
		xor Content = Content1 ),
		event_process(XH,xrelation(Type,Id,Content1),Ctx)
	      )),
	mutable_read(Ctx,Ctx2)
	.

xevent_process( H::passage(_), xrelation(Type,Label,Content), Ctx1, Ctx4,XH) :-
	event_process(XH,
		      start_element{ name => 'R',
				     attributes => [ type: Type,id: Label ] },
		      Ctx1,
		      Ctx2 ),
	mutable(Ctx,Ctx2,true),
	event_process(relarg(XH),Content,Ctx),
	mutable_read(Ctx,Ctx3),
	event_process(XH, end_element{ name => 'R' }, Ctx3, Ctx4 )
	.

xevent_process( relarg(XH::passage(_)), arg(Type,_Label), Ctx1, Ctx2,_) :-
	( Type == 'coord-g',
	  _Label = vide ->
	  Ctx1 = Ctx2
	;
	  ( recorded(node2wid(_Label,Label)) xor Label=_Label),
%	  format('arg in=~w out=~w\n',[_Label,Label]),
	  event_process(XH,
			element{ name => Type,
				 attributes => [  ref: Label  ]
			       },
			Ctx1, Ctx2
		       )
	)
	.

xevent_process( relarg(passage(_)), 'a-propager'(_), Ctx, Ctx,_ ).

xevent_process( H::passage(_), entities, Ctx1, Ctx2,XH) :-
	mutable(Ctx,Ctx1,true),
	every(( entity(Id,WId,Type,MSTAG,Entity_Id),
		_Attr = [id:Id,
			 lst:WId,
			 type:Type | MSTAG],
		( Entity_Id = [] ->
		  Attr=_Attr
		;
		  Attr=[eid:Entity_Id|_Attr]
		),
		event_process(H,
			      element{ name => 'NE',
				       attributes => Attr
				     },
			      Ctx)
	      )),
	mutable_read(Ctx,Ctx2)
	.

:-light_tabular entity/5.
:-mode(entity/4,+(-,-,-,-,-)).

entity(Id,WId,Type,MSTAG,Entity_Id) :-
	recorded(passage_entity(WId,Cat,Lemma,Form,MSTAG,Entity_Id)),
	( Lemma = entities['_PERSON',
			   '_PERSON_m',
			   '_PERSON_f'] ->
	  Type = individual,
	  SubType = person
	; Lemma = entities['_ORGANIZATION','_COMPANY'] ->
	  Type = organization
	; Lemma = entities['_PRODUCT'] ->
	  Type = mark
	; Lemma = entities['_LOCATION'] ->
	  Type = location
	; Lemma = entities['_NUMBER','_NUM','_ROMNUM'] ->
	  Type = measure
	; Lemma = date[] ->
	  Type = dateTime
	;
	  fail
	),
	ne_gensym(Id)
	.


:-std_prolog group_gensym/1.

group_gensym(L) :-
	sentence(Sent),
	update_counter(group,X),
	name_builder('~wG~w',[Sent,X],L)
	.

:-std_prolog w_gensym/1.

w_gensym(L) :-
	sentence(Sent),
	update_counter(w,X),
	name_builder('~wF~w',[Sent,X],L)
	.

:-std_prolog rel_gensym/1.

rel_gensym(L) :-
	sentence(Sent),
	update_counter(relation,X),
	name_builder('~wR~w',[Sent,X],L)
	.

:-std_prolog ne_gensym/1.

ne_gensym(L) :-
	sentence(Sent),
	update_counter(ne,X),
	name_builder('~wN~w',[Sent,X],L)
	.

:-std_prolog mark_as_used/1.

mark_as_used(L) :-
	every(( domain( Label, L),
		record_without_doublon(used(Label)),
		verbose('mark as used ~w\n',[Label]),
		true
	      )).

:-std_prolog split_some_clusters/0.

split_some_clusters :-
	every((
	       try_split_on_adj_mod( N::node{ id => NId,
					      cluster => cluster{ id => CId,
								  left => CLeft,
								  right => CRight,
								  token => CToken,
								  lex => CLex
								}
					    },
				     F::f{ cid => CId,
					   id => FId1,
					   lex => Lex
					 },
				     Lemma,
				     Pos
				   ),
	       node2live_deriv(NId,DId),
	       gensym(_PCId), PCId = gensym(_PCId),
	       record( PC::cluster{ id => PCId,
				    left => Pos,
				    right => Pos,
				    lex => CLex,
				    token => CToken
				  }
		     ),
	       erase(F),
	       record( f{ cid => PCId, id => FId1, lex => Lex, rank => R, tid => 0 } ),
%%	       record( redirect(PCId,CId) ),
	       record( splitted(PCId,FId1) ),
	       gensym(_PNId), PNId=gensym(_PNId),
	       record( PN::node{ id => PNId,
				 lemma => Lemma,
				 form => Lex,
				 cat => adj,
				 deriv =>[],
				 tree => [],
				 xcat => 'adjP',
				 cluster => PC,
				 lemmaid => Lemma,
				 w => []
			       } ),
	       gensym(_PEId), PEId = gensym(_PEId),
	       record( PE::edge{ id => PEId,
				 source => N,
				 target => PN,
				 type => adj,
				 label => 'N2',
				 deriv => [DId],
				 secondary => []
			       }
		     ),
	       verbose('added pseudo node N=~w\n\tPC=~w\n\tPN=~w\n\tPE=~w\n\n',[N,PC,PN,PE]),
%%	       format('added pseudo node N=~w\n\tPC=~w\n\tPN=~w\n\tPE=~w\n\n',[N,PC,PN,PE]),
	       true
	      )),
	verbose('done splitting\n',[]),
	true
	.

:-rec_prolog try_split_on_adj_mod/4.

try_split_on_adj_mod(N,F,Lemma,Pos) :-
	N::node{ cat => nc,
		 lemma => date[],
		 cluster => cluster{ id => CId, left => CLeft, right => CRight }
	       },
	F::f{ cid => CId, lex => Lex, rank => R },
	date_modifier(Lemma,Forms),
	domain(Lex,Forms),
	( S is R-1,
	  Pos = CRight
	;
	  S is R+1,
	  Pos = CLeft
	),
	f{ cid => CId, rank => S }
	.

:-extensional date_modifier/2.

date_modifier(prochain,[prochain,prochains,prochaine,prochaines]).
date_modifier(suivant,[suivant,suivants,suivante,suivantes]).
date_modifier(premier,[premier,premiers,premi�re,premi�res]).
date_modifier(dernier,[dernier,derniers,derni�re,derni�res]).


%% The modifier is detached only if being the last one
try_split_on_adj_mod(N,F,Lemma,CRight) :-
	N::node{ cat => np,
		 lemma => entities['_ORGANIZATION','_COMPANY'],
		 cluster => cluster{ id => CId,
				     left => CLeft,
				     right => CRight,
				     token => CToken,
				     lex => CLex
				   }
	       },
	F::f{ cid => CId, id => FId1, lex => Lex, rank => R },
	organization_modifier(Lemma,Forms),
	domain(Lex,Forms),
	S is R-1,
	f{ cid => _CId, id => FId2, rank => S },
	( _CId = CId xor recorded( redirect(CId,_CId) ) ),
	Next is R+1,
	(   \+ f{cid => CId, rank=>Next}),
	    true
	.

:-extensional organization_modifier/2.

organization_modifier(national,[national,nationale,nationaux,nationales]).
organization_modifier(europ�en,[europ�en,europ�enne,europ�ens,europ�ennes]).
organization_modifier(fran�ais,[fran�ais,fran�aise,fran�aises]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Official names for Passage POS

:-rec_prolog passage_pos/3.

passage_pos(cat[v,aux],_,verb).
passage_pos(nc,_,commonNoun).
passage_pos(np,_,properNoun).
passage_pos(prep,_,preposition).
passage_pos(cat[adv],_,adverb).
passage_pos(det,det{ def => Def, dem => Dem, numberposs => NPos },Type) :-
	( Def == (+) -> Type = definiteArticle
	; Dem == (+) -> Type = demonstrativeDeterminer
	; (NPos == sg ; NPos == pl) -> Type = possessiveDeterminer
	; Type = indefiniteDeterminer
	).
passage_pos(adj,_,qualifierAdjective). %% **
passage_pos(cat[cln,cla,cld,cll,clg,pro,ce],_,personalPronoun).
passage_pos(clr,_,reflexivePronoun).
passage_pos(coo,_,coordinatingConjunction).
passage_pos(csu,_,subordinatingConjunction).
passage_pos(prel,_,relativePronoun).
passage_pos(pri,_,interrogativePronoun). %% **
passage_pos(cat[clneg,advneg],_,negativeParticle).
passage_pos(que,_,subordinatingConjunction).
passage_pos(ponctw,_,secondaryPunctuation).
passage_pos(poncts,_,mainPunctuation).
passage_pos(ncpred,_,commonNoun).
passage_pos(number,_,numeral).
passage_pos(que_restr,_,negativeParticle). %% **
%% passage_pos(adjPref,???). %% **
%% passage_pos(advPref,???). %% **
%% passage_pos(xpro,???). %% **
passage_pos(predet,_,indefiniteDeterminer). %% **
passage_pos(pres,_,interjection). %% **
