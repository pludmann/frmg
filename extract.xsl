<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
     Author: Eric de la Clergerie "Eric.De_La_Clergerie@inria.fr"

     To convert from old to new MG formats

     - removed <nodes></nodes>
     - removed <constraints></constraints>
     - <fs></fs> are no longer nested inside <val></val>
     - relations (father, precedes, ...) are rewritten

  -->

<xsl:output
  encoding="ISO-8859-1"
  method="xml"
  indent="yes"/>

<xsl:param name="this"/>
<xsl:param name="search"/>

<xsl:strip-space elements="*"/>

<xsl:template match="tag">
  <xsl:choose>
    <xsl:when test="$this">
      <xsl:apply-templates select="." mode="id"/>
    </xsl:when>
    <xsl:when test="$search">
      <xsl:apply-templates select="." mode="search"/>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template match="tag" mode="id">
  <xsl:variable name="id">
    <xsl:value-of select="concat($this,' ')"/>
  </xsl:variable>
  <tag>
    <xsl:copy-of select="family/tree[starts-with(@name,$id)]"/>
  </tag>
</xsl:template>

<xsl:template match="tag" mode="search">
  <tag>
    <xsl:copy-of select="family/tree[contains(@name,$search)]"/>
  </tag>
</xsl:template>


</xsl:stylesheet>
