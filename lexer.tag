/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2000, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  xtag_main.tag -- French Toy XTAG (non lexicalized)
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.

:-require
%%	'lexicon.tag',
	'format.pl'
	.

?-recorded(L::'N'(_)),format('~q.\n',[L]),fail.

?- recorded('C'(L,Token : Cat : HT : Top,R)),
(   var(Cat),var(HT),var(Top),
    lexicon{ lex => Token } ->
    lexicon{lex=>Token, cat => Cat, top => Top, ht => HT}
;   lexicon{ lex => Token, cat => Cat, top => Top, ht => HT } ->
    lexicon{lex=>Token, cat => Cat, top => Top, ht => HT}
;   
       true % unknown word
),
   New_C = 'C'(L,
	       lemma{ lex => Token,
		      cat => Cat,
		      top => Top,
	%%	      lemma => Lemma,
		      anchor => tag_anchor{ name => HT }
		    },
	       R),
   optimized_format([New_C]^'~q.\n'),
   fail
   .
