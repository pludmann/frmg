/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015, 2016, 2017 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  extract.pl -- extraction of a shared dependency forest
 *
 * ----------------------------------------------------------------
 * Description
 * Extraction of a shared dependency forest from the shared derivation
 * forest produced by the parser
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.
:-require 'format.pl'.
:-require 'forest.pl'.
:-require 'utils.pl'.

:-std_prolog show_time/1.

:-finite_set(edge_kind,[adj,subst,lexical,virtual,epsilon,secondary]).

:-features(cluster, [id,left,right,lex,token] ).
:-features(node, [id,cat,cluster,tree,lemma,form,xcat,deriv,w,lemmaid] ).
:-features(edge, [ id,
		   type,
		   source,
		   target,
		   label,
		   deriv,
		   secondary	% used for pontential secondar source edge (may be used for cases of local-tree MCTAGs)
		 ]
	  ).
:-features(op,[id,cat,deriv,span,top,bot]).
:-features(hypertag,[id,deriv,ht]).

:-features(dstruct,[node,w,span,deriv,children,oid,constraint]).

:-extensional
	cluster{},
	node{},
	edge{},
	op{},
	hypertag{},

	edge2sop/2,
	edge2top/2,
	edge2ops/3,
	sop2edge/2,
	top2edge/2,
	
	node2inlabel/2
	.

:-extensional
      node2op/2,
  node_op2deriv/3,
  
  op2node/2
.


:-rec_prolog disamb_node/6.

disamb_node(T (Left,_),T,Token,Lex,Lemma,LemmaId).
disamb_node(T (Left,_) * _ (_,_),T,Token,Lex,Lemma,LemmaId).
disamb_node(verbose!anchor(_Token,Left,Right,_,T,(Addr : [Lemma,Lex,LemmaId]),_),T,Token,Lex,Lemma,LemmaId) :-
    recorded('C'(Left,lemma{ lex => Token },Right), Addr) xor Token = _Token
.
disamb_node(verbose!coanchor(Token,Left,Right,T,[Lemma,Lex,LemmaId]),T,Token,Lex,Lemma,LemmaId).
disamb_node(verbose!lexical([_Token],Left,Right,_Cat,[Lemma,Lex,LemmaId]),'_',Token,Lex,Lemma,LemmaId) :-
	( _Token = (Token : _Cat) xor _Token = Token )
	.
disamb_node(verbose!epsilon(Token,Left,Right,[Lemma,Lex,LemmaId]),'_',Token,Lex,Lemma,LemmaId).
disamb_node(pseudo(Left,_,T),T,'','','','').

:-light_tabular live_op/1.
:-mode(live_op/1,+(+)).

live_op(TOP) :-
	( recorded(root_op(TOP))
	;
	  recorded( top2sop(TOP,SOP) ),
	  live_op(SOP)
	),
%	format('live op ~w\n',[SOP]),
	true
	.

:-std_prolog dependency_forest_reader/0.

dependency_forest_reader :-
    '$interface'('Easyforest_Reset_DStruct', [return(none)]),
    %% if nbest option, reduce the forest
    ( recorded(nbest(NBest)) ->
	  '$interface'('Forest_Reduce_NBest'(NBest:int),[return(none)])
     ;
     NBest=0
    ),
    %% find all roots in the shared derivation forest
    %% and recursively extract all derivations
    %% clusters are created during the process
    %% all nodes, op, edges and hypertags are registered but not yet created (waiting to know all derivs)
    every((
		 recorded( O::'*ANSWER*'{}, Addr),
		 ( NBest = 0 ->
		       XAddr=Addr
		  ;
		  '$interface'('Forest_Partial'(Addr:ptr, NBest:int, NBest:int),[return(XAddr:ptr)])
		 ),
		 forest_root(XAddr),
		 true
	     )),
    abolish(deriv_addr/1),
    \+ opt(stop(pre_root)),
%    show_time(extract_preroot),
    every(( recorded(deriv_root(Add,T,Span)),
	    deriv_process(Add,T,Span),
	    '$answers'(register_op(Add,OId,_)),
	    %% format('Root ~w => ~w\n',[T,OId]),
	    record_without_doublon(root_op(OId))
	  )),
    abolish(deriv_shared_addr/2),
    abolish(answer_extract_deriv/4),
    abolish(info_derivs/1),
    abolish(edge_info/4),
    abolish(forest_indirect/1),
    \+ opt(stop(root)),
%    show_time(extract_root),

/*
    value_counter(extract_add,Ctr_Add),
    value_counter(extract_append,Ctr_Append),
    value_counter(extract_other,Ctr_Other),
    value_counter(extract_remove,Ctr_Remove),
    format('stat deriv extract add=~w append=~w other=~w remove=~w\n',[Ctr_Add,Ctr_Append,Ctr_Other,Ctr_Remove]),
*/
    %% create all nodes, with attached derivs
    every((  ( recorded(node2object(NId,Add,CId)),
%	       format('handling node nid=~w cid=~w\n',[NId,CId]),
	       recorded(_O,Add),
	       ( tab_item_term(_O,Term) xor Term=_O)
	      ;
	      recorded(pseudonode2object(NId,Term,CId))
	     ),
	     ( recorded(node2xcat(NId,XCat)) xor XCat = ''),
	     ( recorded(node2tree(NId,Tree)) xor Tree = lexical),
	     disamb_node(Term,T,Token,Lex,Lemma,LemmaId),
	     term2cat(T,Cat),
	     ( Tree == lexical ->
		   Derivs = [],
		   recorded( terminal_op(NId,OId) ),
		   live_op(OId)
	      ;
	      mutable(MDeriv,[],true),
	      every(( node_op2deriv(NId,OId,DId),
%		      format('potential deriv ~w oid=~w\n',[OId,DId]),
		      live_op(OId),
		      mutable_list_extend(MDeriv,DId)
		    )),
	      mutable_read(MDeriv,Derivs),
	      Derivs = [_|_]
	     ),
	     C::cluster{ id => CId },
	     record_without_doublon(live_cluster(CId)),
	     Token ?= '',
	     Lemma ?= '',
	     LemmaId ?= Lemma,
	     record( N::node{ id => NId,
			      cat => Cat,
			      cluster => C,
			      tree => Tree,
			      lemma => Lemma,
			      form => Token,
			      xcat => XCat,
			      lemmaid => LemmaId,
			      w => [],
			      deriv => Derivs
			    }
		   ),
%	     format('node ~w\n',[N]),
	     true
	  )),
    every(( C::cluster{ id => CId },
	    \+ recorded( live_cluster(CId) ),
	    erase(C)
	  )),
    %% create all edges, with attached derivs
%%    format('process edges\n',[]),
    every(( '$answers'( register_edge(SId,TId,Type,Label,EId,MDerivs) ),
%%	    format('process edge ~w\n',[EId]),
	    mutable_read(MDerivs,Derivs),
	    Source::node{ id => SId, cluster => SourceCluster },
	    Target::node{ id => TId, tree => TTree, cluster => TargetCluster },
	    once(( recorded(edge2sop(EId,SOP)), live_op(SOP) )),
	    %%	    format('trying edge ~w\n',[EId]),
	    mutable(M_Second,[],true),
	    every((
			 '$answers'(register_secondary_edge(EId,SId2,Label2,Dir,EId2,M_EId2_Derivs)),
			 SId2 \== SId,
			 mutable_read(M_EId2_Derivs,Derivs2),
			 _Secondary::node{ id => SId2 },
			 (Dir = direct ->
			      E2 ::= edge{ id => EId2,
					   type => secondary,
					   label => Label2,
					   source => _Secondary,
					   target => Target,
					   deriv => Derivs2,
					   secondary => []
					 }
			  ; Dir = reverse,
			      E2 ::= edge{ id => EId2,
					   type => secondary,
					   label => Label2,
					   target => _Secondary,
					   source => Target,
					   deriv => Derivs2,
					   secondary => []
					 }
			 ),
%%			 format('add secondary eid=~w eid2=~w derivs2=~w dir=~w\n',[EId,EId2,Derivs2,Dir]),
			 mutable_list_extend(M_Second,E2)
		     )),
	    mutable_read(M_Second,Secondary),
	    record( E::edge{ id => EId,
			     type => Type,
			     label => Label,
			     source => Source,
			     target => Target,
			     deriv => Derivs,
			     secondary => Secondary
			   }),
	    record( source2edge(SId,E) ),
	    %%	record( sourcecluster2edge(SourceCluster,Edge) ),
	    record( target2edge(TId,E) ),
	    record( targetcluster2edge(TargetCluster,E) ),
	    %		format('Edge ~w\n',[E]),
	    true
	  )),
    %% create all op, with attached derivs
    %	format('process ops\n',[]),
    every(( '$answers'( register_op(Add,OId,MDerivs) ),
	    live_op(OId),
	    %		format('process op ~w\n',[OId]),
	    mutable_read(MDerivs,Derivs),
	    recorded(_O,Add),
	    ( tab_item_term(_O,Term) xor Term=_O),
	    ( Term = Top (Left,Right) -> Bot = [], Span = [Left,Right]
	     ; Term = Top (Left,Right) * Bot (Left2,Right2) -> Span = [Left,Right,Left2,Right2]
	     ; Term = verbose!anchor(Token,Left,Right,_,Top,(_ : [Lemma,Lex,LemmaId]),_) -> Bot = [], Span = [Left,Right]
	     ; Term = verbose!coanchor(Token,Left,Right,Top,[Lemma,Lex,LemmaId]) -> Tree = lexical, Bot = [], Span = [Left,Right]
	     ; Term = verbose!lexical([_Token],Left,Right,_,[Lemma,Lex,LemmaId]) ->
		   (_Token = (Token : _) xor _Token = Token ),
		   Tree = lexical, Span = [Left,Right], Top = [], Bot = []
	     ; Term = verbose!epsilon(Token,Left,Right,[Lemma,Lex,LemmaId]) -> Top = [], Bot = [], Span = [Left,Right]
	    ),
	    term2cat(Top,Cat),
	    ( Span = [gen_pos(XL1,XL2),gen_pos(XR1,XR2)] ->
		  XSpan = [XL1,XR1]
	     ; Span = [gen_pos(XLeft,_),gen_pos(XRight,_),gen_pos(XLeft2,_),gen_pos(XRight2,_)] ->
		   XSpan = [XLeft,XRight,XLeft2,XRight2]
	     ; Span = XSpan
	    ),
	    record(Op::op{ id => OId,
			   cat => Cat,
			   deriv => Derivs,
			   span => XSpan,
			   top => Top,
			   bot => Bot
			 }
		  ),
	    %		format('Op ~w\n',[Op]),
	    true
	  )),
    %% create all hypertags, with attached derivs
    %	format('process hypertags\n',[]),
    every(( '$answers'( register_ht(Add,HId,MDerivs) ),
	    mutable_read(MDerivs,Derivs),
	    recorded( _O, Add ),
	    ( tab_item_term(_O,HTAG::verbose!struct(_,_HT)) xor HTAG=_O),
	    (_HT = ht{ ctrsubj => _HT_CtrSubj,
		       diathesis => _HT_Diathesis,
		       distrib => _HT_Distrib,
%		       extraction => _HT_Extraction,
		       id => _HT_Id,
		       imp => _HT_Imp,
		       refl => _HT_Refl
		     } ->
		 _HT_CtrSubj ?= (-),
		 _HT_Diathesis ?= (-),
		 _HT_Distrib ?= (-),
%		 _HT_Extraction ?= (-),
		 _HT_Id ?= (-),
		 _HT_Imp ?= (-),
		 _HT_Refl ?= (-)
	     ;
	     true
	    ),
	    record( HT::hypertag{ id => HId,
				  deriv => Derivs,
				  ht => _HT
				}
		  ),
	    %		format('Hypertag ~w\n',[HT]),
	    true
	  )),
    %% set mode
    ( recorded(robust) ->
	  record( mode(robust) )
     ; recorded( added_correction ) ->
	   record( mode(corrected) )
     ;
     record( mode(full) )
    )
.

:-std_prolog forest_follow_indirect/2.

forest_follow_indirect(Forest,XForest) :-
	( Forest = indirect(_Forest) -> forest_follow_indirect(_Forest,XForest)
	; Forest = _ : indirect(_Forest) -> forest_follow_indirect(_Forest,XForest)
	; Forest = XForest
	),
%	format('deref forest ~w => ~w\n',[Forest,XForest]),
	true
	.



:-light_tabular forest!true_forest/2.
:-mode(forest!true_forest/2,+(+,-)).

%:-xcompiler
forest!true_forest(Add,Forest) :-
%	format('process add=~w\n',[Add]),
	forest!forest(Add,_Forest),
%	format('=>got ~w\n',[_Forest]),
	forest_follow_indirect(_Forest,Forest),
%	format('=>got2 ~w\n',[Forest]),
	true
	.

:-std_prolog forest_root/1.

forest_root(Add) :-
	\+ Add == 0,
%	format('entering forest ~w\n',[Add]),
	forest!true_forest(Add,Forest),
%	format('forest ~w => ~w\n',[Add,Forest]),
	( domain(Forest,[void,call,init]) ->
	  fail
	; Forest = and(Add_X,Y) ->
	  (   Y = LY:Add_Y xor Y=Add_Y, LY=[] ),
	  (  % format('add_x=~w\n',[Add_X]),
	     forest_root(Add_X)
	  ;  % format('tmp1 ~w\n',[Add_Y]),
	     \+ Add_Y == 0,
	     recorded(O,Add_Y),
%	     format('tmp add_y=~w => ~w\n',[Add_Y,O]),
	     ( tab_item_term(O,T:: K(Left,Right) ) ->
		   %	       format('potential root ~w add=~w\n',[T,Add_Y]),
		   \+ recorded(deriv_root(Add_Y,_,_)),
		   record(deriv_root(Add_Y,T,[Left,Right])),
		   every((deriv_find_shared_addr(Add_Y) )),
		   true
	      ;
%	       format('add_y=~w O=~w\n',[Add_Y,O]),
	       forest_root(Add_Y)
	     )
	  )
	;
%	format('strange forest addr=~w ~w\n',[Add,Forest]),
	  fail
	)
	.

:-extensional extract_deriv_handler/3.

extract_deriv_handler(K (Left,Right), op, edge(subst,Label,[Left,Right],Info,Add_Y)).
extract_deriv_handler(K1 (Left1,Right1) * K2 (Left2,Right2), op, edge(adj,Label,[Left1,Right1,Left2,Right2],Info,Add_Y)).
extract_deriv_handler(Term::verbose!anchor(_,Left,Right,_,_,_,_), node, edge(anchor,Label,[Left,Right],Info,Add_Y)).
extract_deriv_handler(Term::verbose!coanchor(_,Left,Right,_,_), single_node, edge(lexical,Label,[Left,Right],Info,Add_Y)).
extract_deriv_handler(Term::verbose!lexical(_,Left,Right,_,_), single_node, edge(lexical,Label,[Left,Right],Info,Add_Y)).
extract_deriv_handler(Term::verbose!epsilon(_,Left,Right,_), single_node, edge(lexical,Label,[Left,Right],Info,Add_Y)).
extract_deriv_handler(Term::verbose!struct(_,_), hypertag, edge(hypertag,Label,[],Add_Y,Add_Y)).
extract_deriv_handler(Term::verbose!secondary(Label,Source2,Label2,local), secondary, edge(secondary,Label,[],(Source2:Label2),Add_Y)).
extract_deriv_handler(Term::verbose!secondary(Label,Source2,Label2,up), secondary, edge(secondary,Label,[],(up(Source2,direct):Label2),Add_Y)).
extract_deriv_handler(Term::verbose!secondary(Label,Source2,Label2,rup), secondary, edge(secondary,Label,[],(up(Source2,reverse):Label2),Add_Y)).
extract_deriv_handler(Term::verbose!secondary(Label,Source2,Label2,rename), secondary, edge(secondary,Label,[],rename(Label2),Add_Y)).

:-rec_prolog extract_deriv_register/4.

extract_deriv_register(op,Add_Y,Term,Info) :- register_op(Add_Y,Info,_).
extract_deriv_register(node,Add_Y,Term,Info) :- register_node(Add_Y,Term,Info).
extract_deriv_register(single_node,Add_Y,Term,Info) :- register_single_node(Add_Y,Term,NId,Info).
extract_deriv_register(default,Add_Y,Term,Add_Y).
extract_deriv_register(hypertag,HTAdd,Term,HTAdd) :- addr2tree(HTAdd,_,_,_,_).
extract_deriv_register(secondary,Add_Y,Term,Info).
		       
:-light_tabular addr2tree/5.
:-mode(addr2tree/5,+(+,-,-,-,-)).

addr2tree(HTAdd,Tree,HId,M_HId_Derivs,XTree) :-
	recorded(_O,HTAdd),
	(tab_item_term(_O,HTAG::verbose!struct(XTree,HT)) xor _O = HTAG),
	tree_tokenize(XTree,Tree),
	( HT = ht{} ->
	  register_ht(HTAdd,HId,M_HId_Derivs)
	;
	  HId = none,
	  M_HId_Derivs = none
	)
	.

:-std_prolog deriv_find_shared_addr/1.

deriv_find_shared_addr(Add) :-
%       format('entering shared addr ~w\n',[Add]),
	( Add == 0 ->
	      true
	 ; recorded(deriv_shared_addr(Add,MOcc)) ->
%	       format('shared addr inc ~w\n',[Add]),
	       mutable_add(MOcc,1),
	  true
	 ; recorded(deriv_addr(Add)) ->
%	       format('shared addr register ~w\n',[Add]),
	       mutable(MOcc,2),
	       record(deriv_shared_addr(Add,MOcc))
	;
	  record( deriv_addr(Add) ),
	  every(
		(
		    forest!true_forest(Add,Forest),
%		    format('process shared deriv ~w:~w\n',[Add,Forest]),
		    ( Forest = and(Add_X,Y) ->
			  (   Y=Label:Add_Y xor Y=Add_Y, Label = void ),
			  every(( deriv_find_shared_addr(Add_X) )),
			  Add_Y \== 0,
%		   format('\tfollow x-part ~w ~w\n',[Add,Add_X]),
		   
%		   format('\tfollow y-part ~w ~w\n',[Add,Y]),
		   (
		    ( recorded(edge_info(Add_Y,_,_Edge))
		    xor
		    recorded(O,Add_Y),
		      ( tab_item_term(O,Term) xor Term=O ),
		      extract_deriv_handler(Term,_Action,_Edge::edge(Type,Label,Span,Info,Add_Y)),
		      Label \== duplicate,
		      extract_deriv_register(_Action,Add_Y,Term,Info),
		      record(edge_info(Add_Y,Term,_Edge)),
		      ( Type=edge_kind[subst,adj] ->
			deriv_find_shared_addr(Add_Y)
		      ;
			true
		      )
		    )
		    ->
%			format('\t\tfound y-part ~w edge=~w\n',[Y,_Edge]),
		    true
		    ; forest!forest(Add_Y,_YForest),
%		      format('\t\ttry indirect on y=~w => ~w\n',[Y,_YForest]),
		      ( _YForest = indirect(YForest) ; _YForest = _ : indirect(YForest))
		      ->
			  deriv_find_shared_addr(Add_Y)
		   ; Type = other,
		     %		     format('\t\tforest y-other ~w:~w\n',[Add,Forest]),
		     true
		   )
		 ;
		   true
		 )
		)))
	.

:-extensional answer_extract_deriv/4.

:-std_prolog extract_deriv/4.

extract_deriv(XAdd,NAdj,Edges,LAdj) :-
    %	format('try forest add=~w\n',[Add]),
    (XAdd = keep(Add) ->
	 Keep = yes
     ; XAdd = Add,
       Keep = no
    ),
    ( Add == 0 ->
	  NAdj=0, Edges=[], LAdj = []
     ; recorded(info_derivs(Add))
       ->
	   %% Add is a shared deriv that has alreadby been processed
%	  format('extract deriv add=~w info_deriv\n',[Add]),
	   ( recorded(deriv_shared_addr(Add,MOcc)),
	     mutable_read(MOcc,Shared_Occ),
	     Shared_Occ > 0 ->
		 mutable_add(MOcc,-1)
	    ;
	    format('** warning: unexpected multiple call for addr=~w\n',[Add]),
	    fail
	   ),
	   (Shared_Occ == 1->
		%% we are processing the last occurrence of Add
		recorded(answer_extract_deriv(Add,NAdj,Edges,LAdj),_Add_Answer),
		delete_address(_Add_Answer),
		true
	    ;
	    answer_extract_deriv(Add,NAdj,Edges,LAdj)
	   )
     ; recorded(call_extract_deriv(Add)) ->
	   %% Add is a looping shared deriv !
	   %	  format('extract deriv add=~w call_extract_deriv\n',[Add]),
	   format('looping on ~w\n',[Add]),
	   fail
     ;
     ( recorded(deriv_shared_addr(Add,_)) ->
	   record( call_extract_deriv(Add) ), % protect against looping
	   Shared = yes
	  ;
	  Shared = no
     ),
     mutable(MAdjRestr,10,true),
     mutable(MDerivs,800,true),
     %     mutable(MDerivs,600,true),
     every(
	     (
		 %		 format('here2 add=~w\n',[Add]),
		 forest!true_forest(Add,Forest),
		 %		    format('process forest ~w:~w\n',[Add,Forest]),
		 ( Forest = and(Add_X,Y) ->
		       (   Y=Label:Add_Y xor Y=Add_Y, Label = void ),
		       (
			   Add_Y == 0 ->
			       %			    fail,
			       _Edges = [],
			       _NAdj = 0,
			       _LAdj = []
			;
			recorded(edge_info(Add_Y,Term,_Edge::edge(Type,Label,Span,Info,Add_Y))),
			%			format('found edge_info ~w ~w ~w\n',[Add_Y,Term,_Edge]),
			true
			->
			    %			 format('try extract deriv add_x ~w forest=~w\n',[Add_X,Forest]),
			    extract_deriv(Add_X,_NAdj2,_Edges2,_LAdj2),
			    mutable_check_min(MAdjRestr,_NAdj2),
			    ( Type=adj ->
				  '$interface'('LAdj_Update'(Label:term,_LAdj2:term,_LAdj:term,_NAdj:term,_NAdj2:int),[]),
				  % format('adj update label=~w adj2=~w => adj=~w nocc=~w\n',[Label,_LAdj2,_LAdj,NOcc]),
				  %_NAdj is _NAdj2+1,
				  mutable_check_min(MAdjRestr,_NAdj),
				  every((%fail,
					       recorded(robust),
					       deriv_process(Add_Y,Term,Span)
					   )),
				  true
			     ; 
			     _NAdj = _NAdj2,
			     _LAdj = _LAdj2,
			     every((%fail,
					  Type = subst,
					  recorded(robust),
					  deriv_process(Add_Y,Term,Span)
				   ))
			    ),
			    _Edges ::= [_Edge|_Edges2]
			; 
			forest!forest(Add_Y,_YForest),
			( _YForest = indirect(YForest)  xor _YForest = _ : indirect(YForest))
			->
			    %			format('indirect YForest ~w=~w forest=~w\n',[Add_Y,YForest,Forest]),
			    every((extract_deriv(keep(Add_X),__NAdjX,__EdgesX,__LAdjX))),
			    extract_deriv(Add_Y,_NAdjY,_EdgesY,_LAdjY),
			    mutable_check_min(MAdjRestr,_NAdjY),
			    answer_extract_deriv(Add_X,_NAdjX,_EdgesX,_LAdjX),
			    (_LAdjY = [] ->
				 _LAdj = _LAdjX,
				 _NAdj = _NAdjX
			     ;
%			     format('try adj append adjx=~w adjy=~w\n',[_LAdjX,_LAdjY]),
			     '$interface'('LAdj_Append'(_LAdjX:term,_LAdjY:term,_LAdj:term,_NAdj:term),[]),
%			     _NAdj is _NAdjX+_NAdjY,
%			     format('adj append adjx=~w adjy=~w => adj=~w nadj=~w\n',[_LAdjX,_LAdjY,_LAdj,_NAdj]),
			     true
			    ),
			    mutable_check_min(MAdjRestr,_NAdj),
			    %% fast_append(_EdgesX,_EdgesY,_Edges)
			    (_EdgesX=[] -> _Edges = _EdgesY
			     ; _EdgesY=[] -> _Edges = _EdgesX
			     ; _Edges ::= [_EdgesX|_EdgesY]
			    )
			; Type = other,
			  extract_deriv(Add_X,_NAdj,_Edges,_LAdj),
			  mutable_check_min(MAdjRestr,_NAdj),
			  true

		       )
		  ;
		  %% format('strange forest ~w ~w\n',[Add,Forest]),
		  _Edges = [],
		  _NAdj = 0,
		  _LAdj = []
		 ),
		 \+ recorded(KK::answer_extract_deriv(Add,_NAdj,_Edges,_LAdj)),
		 record(KK),

		 mutable_add(MDerivs,-1),
		 %% remove a derivation if needed (maybe the very last one, but not necessarly)
		 mutable_read(MDerivs,0),
		 mutable_check_min(MAdjRestr,1),
		 remove_some_derivs(Add,MAdjRestr,MDerivs),
		 
		 true
	     )),
     ( Shared == yes ->
	   erase( call_extract_deriv(Add) ),
	   record( info_derivs(Add) )
      ;
      (
	  true
       ;
       Keep = no,
       erase(answer_extract_deriv(Add,_,_,_)),
       fail
      )
     ),
     answer_extract_deriv(Add,NAdj,Edges,LAdj)
    )
.

:-xcompiler
remove_some_derivs(Add,MAdjRestr,MDerivs) :-
	%% repeat until true
	repeat((
		mutable_read(MAdjRestr,AdjRestr),
		( AdjRestr > 1 -> 
		  mutable_add(MAdjRestr,-1),
		  ( recorded(answer_extract_deriv(Add,AdjRestr,_,_)) ->
		    every(( recorded(answer_extract_deriv(Add,AdjRestr,_Edges,_),_Add),
%			    format('Remove deriv ~w nadj=~w edges=~w\n',[Add,AdjRestr,_Edges]),
%			    update_counter(extract_remove,_),
			    delete_address(_Add),
			    mutable_add(MDerivs,1)
			  ))
		  ;
		    fail
		  )
		;
		  true
		)
	       ))
	.

:-std_prolog deep_domain/2.

deep_domain(A,L) :-
    (domain(A,L) ; domain(_L::[_|_],L), deep_domain(A,_L))
.

:-std_prolog deep_length/2.

deep_length(L,N) :-
    length(L,N0),
    mutable(M,N0),
    every(( domain(_L::[_|_],L),
	    mutable_add(M,-1),
	    deep_length(_L,_N),
	    mutable_add(M,_N)
	  )),
    mutable_read(M,N)
.

:-light_tabular deriv_process/3.
:-mode(deriv_process/3,+(+,+,+)).

deriv_process(Add,Term,Span::[Left|_]) :-
    ( register_op(Add,SOP,M_SOP_Derivs),
%      format('deriv process ~w\n',[Add]),
      extract_deriv(Add,_,Edges,_),
      deriv_process_one(Add,Term,Span,SOP,M_SOP_Derivs,Edges),
      true
     ;
     erase(answer_extract_deriv(Add,_,_,_)),
     fail
    )
.

:-extensional tmp_edge/2.

:-xcompiler
deriv_process_one(Add,Term,Span::[Left|_],SOP,M_SOP_Derivs,Edges) :-

%    format('start process one add=~w span=~w\n\tterm=~w\n\tedges=~w\n',[Add,Span,Term,Edges]),

    %% Get XTree
	(deep_domain(edge(hypertag,_,_,HTAdd,HTAdd),Edges) xor HTAdd = 0),
	'$answers'(addr2tree(HTAdd,Tree,HId,M_HId_Derivs,XTree)),

	%% Get NId
	( deep_domain(edge(anchor,_,_,NId,_),Edges) ->
	  ( recorded(node2xcat(NId,XCat))
	  xor
	  term2cat(Term,XCat),
	    record(node2xcat(NId,XCat))
	  )
	;
	  %%	  format('need empty node ~w\n',[Term]),
	  ( Term = Top (Left,_) xor Term = (Top (Left,_) * _ (_,_)) ),
	  term2cat(Top,XCat),
	  register_pseudo_node( pseudo(Left,HTAdd,XCat),NId)
	),

	%% Test if we keep the deriv for this node
	value_counter(node(NId),NId_DId),
	( NId_DId < 200 xor
	  process_deriv_check_constraint(XCat,XTree,Span,Term,Edges),
	  NId_DId < 5000,
	  true
	),
	update_counter(node(NId),_),

		
	record_without_doublon(op2node(SOP,NId)),
	%% may use renaming info on labels provided by dependency_label

	( opt(rename),
	  dependency_label(XTree,XLabel) ->
	      record_without_doublon(node2inlabel(NId,XLabel))
	 ;
	 true
	),
	
	update_counter(deriv,DId),
	mutable_list_extend(M_SOP_Derivs,DId),
	register_deriv(DId,NId),
	deriv_set_op(DId,SOP,Span),

%	format('new did=~w edges=~w\n',[DId,Edges]),
	
	 %% handle hypertags
	 every((
		record_without_doublon(node2tree(NId,Tree)),
		HId \== none,
		deriv_set_ht(DId,HId),
		mutable_list_extend(M_HId_Derivs,DId),
		true
		  )),

	  every(( %fail,
		      deep_domain(_F::edge(edge_kind[subst,adj],_Label,_Span,_,_Add),Edges),
		      recorded(edge_info(_Add,_Term,_F)),
		      deriv_process(_Add,_Term,_Span),
		      true
		  )),

	 every(( %fail,
		      deep_domain(_E1::edge(_E1_Kind::edge_kind[subst,adj,lexical,secondary],_Label,_Span,_,_Add),Edges),
		      %% domain(_E1::edge(_E1_Kind::edge_kind[subst,adj,lexical],_Label,_,_,_),Edges),
		      record(tmp_edge(_Label,_E1)),
		      %	      format('\ttmp_edge ~w ~w\n',[_Label,_E1]),
		      _E1_Kind = secondary,
%		      format('secondary nid=~w e=~w\n',[NId,_E1]),
		      true
		  )),
	 
	 %%	format('deriv process one ~w ~w ~w\n',[Add,NId,DId]),
	every((
	       deep_domain(_E::edge(_Type::edge_kind[subst,adj,lexical,epsilon],_Label,_Span,_TOP,_Add),Edges),
	       record_without_doublon( top2sop(_TOP,SOP) ),
	       op2node(_TOP,_NId),
	       (node2inlabel(_NId,_XLabel) -> true
		; tmp_edge(_Label,edge(secondary,_Label,_,rename(_XLabel),_)) ->   true
		; _XLabel = _Label
	       ),
	       %%		format('ready to register edge top=~w nid=~w\n',[_Top,_NId]),
	       ( _Type == adj,
		 tmp_edge(_Label,edge(lexical,_Label,_,_TOP2,_Add2)) ->
		 %% rerooting, using _NId2 as root instead of true one NId, but keeping info about the original source NId
		 op2node(_TOP2,_NId2),
		 register_edge(_NId2,_NId,_Type,_XLabel,_EId,M_EId_Derivs),
		 record_without_doublon( reroot_source(_EId,_Span,_NId2,NId) )
		;
		register_edge(NId,_NId,_Type,_XLabel,_EId,M_EId_Derivs)
	       ),
%%	       format('register deriv edge EId=~w DId=~w TId=~w _TOP=~w label=~w\n',[_EId,DId,_NId,_TOP,_XLabel]),
	       mutable_list_extend(M_EId_Derivs,DId),
	       deriv_add_edge(DId,info(_EId,SOP,_TOP,_NId,_Span)),
%%	       format('done\n',[]),
	       %%		record_without_doublon( edge2sop_and_top(EId,SOP,_TOP) ),
	       (edge2sop(_EId,SOP)
			xor
			record( edge2sop(_EId,SOP) ),
		record( sop2edge(SOP,_EId) )
	       ),
	       (edge2top(_EId,_TOP)
			xor record( edge2top(_EId,_TOP) ),
		record( top2edge(_TOP,_EId) )
	       ),

%	       fail,
	       
	       every((
%			    _Type = edge_kind[subst,lexical],
			    deriv_search_secondary_edge(XTree,Edges,_XLabel,_NId,_Type,_TOP2,Label2,RootLabel),
			    op2node(_TOP2,_NId2),
			    register_secondary_edge(_EId,_NId2,Label2,direct,EId2,M_EId2_Derivs),
%			    format('secondary edge prim=~w second=~w label2=~w nid2=~w nid=~w span=~w op2=~w did=~w\n',[_EId,EId2,Label2,_NId2,NId,Span,_TOP2,DId]),
			    mutable_set_extend(M_EId2_Derivs,DId),
			    record_without_doublon(_K::secondary2edge(EId2,[],_EId,_TOP2,SOP,direct)),
			    %			    format('secondary2edge ~w\n',[_K]),
			    every((
					 %fail,
					 (%fail,
					     NId2=_NId2,
					     TOP2=_TOP2,
					     Adj=no
					  ; tmp_edge(RootLabel,edge(adj,RootLabel,_,TOP2,_)),
					    op2node(TOP2,NId2),
					    Adj=yes
					 ),
					 '$answers'(_P::register_secondary_propagate(NId2,propagated(NId3,_EId3,SOP3,RefLabel3),Label2,direct,SPId3,M_Prop)),
					 compatible_label(RefLabel3,Label2),
%					 format('secondary from propagated ~w\n',[_P]),
					 register_secondary_edge(_EId,NId3,Label2,direct,EId3,M_EId3_Derivs),
					 mutable_set_extend(M_EId3_Derivs,DId),
					 record_without_doublon(secondary2edge(EId3,[_EId3|SPId3],_EId,SOP3,SOP,direct))
				     )),
				   
			    true
			)),

	       every((
%			    fail,
			    _Type = edge_kind[subst,lexical],
			    tmp_edge(_Label,edge(secondary,_Label,[],(up(_RootLabel,Dir):_Label2),_SAdd)),
%			    format('secondary propagate up ~w\n',[_P]),
			    %			    fail,
			    %			    register_secondary_propagate(NId,up(_EId,SOP),(_RootLabel:_Label2),Dir,SPId,M_Prop),
			    register_secondary_propagate(NId,up(_EId,SOP,1),(_RootLabel:_Label2),Dir,SPId,M_Prop),
			    secondary_propagate_chain(SPId,_EId,[]),
			    mutable_set_extend(M_Prop,DId),
			    true
			)),
	       
	       every((
			    %			    _Type = edge_kind[subst,lexical],
			    '$answers'(register_secondary_propagate(_NId,up(_EId0,_SOP0,UpCtr),(_RootLabel:_Label2),Dir,_SPId0,M_Prop)),
%			    format('found secondary propgate nid=~w root=~w _l2=~w\n',[_NId,_RootLabel,_Label2]),
			    (   _RootLabel \== head,
				deep_domain(edge(secondary,_AltLabel,[],(Label:Label2),_),Edges),
				tmp_edge(_AltLabel,edge(edge_kind[subst,lexical],_AltLabel,_AltSpan,_AltTOP,_)),
				(_Label2 = Label2 xor compatible_label(_Label2,Label2)),
%				format('try compatible nid=~w root=~w _l2=~w l2=~w l=~w alt=~w\n',[_NId,_RootLabel,_Label2,Label,Label2,_AltLabel]),
				true
			     ->
				    op2node(_AltTOP,_AltNId),
				    register_secondary_edge(_EId0,_AltNId,_Label2,Dir,EId2,M_EId2_Derivs),
				    mutable_append(M_EId2_Derivs,M_Prop),
				    record_without_doublon(secondary2edge(EId2,[_EId|_SPId0],_EId0,_AltTOP,_SOP0,Dir))
			     ; tmp_edge(_RootLabel,edge(edge_kind[subst,lexical],_RootLabel,_,_TOP2,_)) ->
				   op2node(_TOP2,_NId2),
				  %			    format('consume up edge ~w nid=~w top=~w\n',[_Up,_NId,_TOP]),
				   register_secondary_edge(_EId0,_NId2,_Label2,Dir,EId2,M_EId2_Derivs),
				   %			    format('\t=> eid2=~w\n',[EId2]),
				   mutable_append(M_EId2_Derivs,M_Prop),
				   record_without_doublon(secondary2edge(EId2,[_EId|_SPId0],_EId0,_TOP2,_SOP0,Dir))
			     ;
			     fail,
			     _RootLabel \== head,
			     UpCtr > 0,
			     UpCtr2 is UpCtr - 1,
			     %			     format('propagate ~w\n',[_UP]),
			     register_secondary_propagate(NId,up(_EId0,_SOP0,UpCtr2),(_RootLabel:_Label2),Dir,SPId,M_Prop2),
			     secondary_propagate_chain(SPId,_EId,_SPId0),
			     %			     mutable_append(M_Prop2,M_Prop),
			     mutable_set_extend(M_Prop2,DId),
			     true
			    ),
			    true
			)),

	       every((
			    %			    _Type = edge_kind[subst,lexical],
			    '$answers'(register_secondary_propagate(_NId,head,(_RootLabel:_Label2),Dir,_SPId,M_Prop)),
			    (tmp_edge(_RootLabel,edge(edge_kind[subst,lexical],_RootLabel,_,_TOP2,_)) ->
				 op2node(_TOP2,_NId2),
				 %			    format('consume up edge nid=~w top=~w\n',[_NId,_TOP]),
				 register_secondary_edge(_EId,_NId2,_Label2,Dir,EId2,M_EId2_Derivs),
				 %			    format('\t=> eid2=~w\n',[EId2]),
				 mutable_set_extend(M_EId2_Derivs,DId),
				 record_without_doublon(secondary2edge(EId2,[_EId|_SPId],_EId,_TOP2,SOP,Dir))
			     ;
			     %			     format('propagate head as up _nid=~w nid=~w eid=~w\n',[_NId,NId,_EId]),
			     register_secondary_propagate(NId,up(_EId,SOP,1),(_RootLabel:_Label2),Dir,SPId,M_Prop2),
			     secondary_propagate_chain(SPId,_EId,_SPId),
			     mutable_set_extend(M_Prop2,DId)
			    ),
			    %			    mutable_list_extend(M_EId2_Derivs,DId0),
			    %			    record_without_doublon(secondary2edge(EId2,DId0,_EId,_TOP2,head(SOP0,Span0),_NId2,Dir)),
			    true
			)),

	       every(( '$answers'(register_secondary_propagate(_NId,up(EId0,SOP0,_),(head:_Label2),Dir,SPId0,M_Props)),
%		       format('potential secondary head-source eid0=~w nid=~w label2=~w did=~w did0=~w\n',[EId0,NId,_Label2,DId,DId0]),
		       register_secondary_edge(EId0,NId,_Label2,Dir,EId2,M_EId2_Derivs),
		       mutable_append(M_EId2_Derivs,M_Props),
		       record_without_doublon(secondary2edge(EId2,[_EId|SPId0],EId0,SOP,SOP0,Dir))
		     )),

	       every((  %fail,
			deep_domain(edge(secondary,RefLabel,_,(_Label:Label2),_),Edges),
  		         \+ tmp_edge(RefLabel,edge(edge_kind[subst,lexical],RefLabel,_,_,_)),
%			 format('phase0 try propagate nid=~w _nid=~w label=~w label2=~w refabel=~w edges=~w\n',[NId,_NId,_Label,Label2,RefLabel,Edges]),
%			 fail,
			 register_secondary_propagate(NId,propagated(_NId,_EId,_TOP,RefLabel),Label2,direct,SPId2,M_Prop),
			 secondary_propagate_chain(SPId2,_EId,[]),
			 mutable_set_extend(M_Prop,DId),
			 every(( '$answers'(register_secondary_propagate(_NId,propagated(_NId3,_EId3,_TOP3,_RefLabel3),Label2,direct,_SPId3,M_Prop3)),
				 compatible_label(_RefLabel3,Label2),
%				 fail,
				 %				 register_secondary_propagate(NId,propagated(_NId3,_EId3,_TOP3,_RefLabel3),Label2,direct,_SPId3bis,M_Prop3bis),
				 register_secondary_propagate(NId,propagated(_NId3,_EId3,_TOP3,RefLabel),Label2,direct,_SPId3bis,M_Prop3bis),
				 secondary_propagate_chain(_SPId3bis,_EId,_SPId3),
				 mutable_append(M_Prop3bis,M_Prop)
			       ))
			)),
	       
	       true
 		 )),

	every(( %fail,
		tmp_edge(head,edge(secondary,head,[],(up(_RootLabel,Dir):_Label2),_SAdd)),
%		format('secondary propagate head nid=~w rootlabel=~w label2=~w\n',[NId,_RootLabel,_Label2]),
		register_secondary_propagate(NId,head,(_RootLabel:_Label2),Dir,SPId,M_Prop),
		secondary_propagate_chain(SPId,head(NId,SOP),[]),
		mutable_set_extend(M_Prop,DId),
		true
	      )),

	abolish(tmp_edge/2),
	
	%%	  format('did=~w anchor cat=~w xcat=~w span=~w lex=~w truelex=~w lemma=~w\n',[DId,Cat,XCat,SpanAnchor,Lex,TrueLex,Lemma]),
	true
.

:-extensional compatible_label/2.

compatible_label(subject,ctrsubj).
compatible_label(genitive,ctrsubj).
compatible_label(subject,ctrobj).
compatible_label(ctrsubj,ctrobj).
compatible_label(ctrobj,ctrsubj).

:-xcompiler
mutable_set_extend(M,X) :-
    mutable_read(M,S),
    length(S,N),
    (N > 10 ->
	 true
     ;
     '$interface'('DyALog_Set_Extend'(S:term,X:term,XS:term),[]),
     mutable(M,XS)
    )
.

:-xcompiler
mutable_append(M_Dest,M_Src) :-
    mutable_read(M_Src,LSrc),
    mutable_read(M_Dest,LDest),
    (LDest = [] ->
	 mutable(M_Dest,LSrc)
     ; LDest == LSrc ->
%	   format('same dest and src\n',[]),
	   true
     ; length(LDest,NDest),
       NDest > 10 ->
	   true
     ;
     '$interface'('DyALog_Set_Append'(LSrc:term,LDest:term,LDest2:term),[]),
     length(LDest2,NDest2),
%     format('append N2=~w LDest2=~w\n',[NDest2,LDest2]),
     mutable(M_Dest,LDest2)
    )
.

:-std_prolog deriv_search_secondary_edge/8.

deriv_search_secondary_edge(XTree,Edges,Label,NId,Type,TOP2,Label2,RootLabel) :-
%    format('try secondary label=~w nid=~w\n',[Label,NId]),
    Type = edge_kind[subst,lexical],
    tmp_edge(Label,edge(secondary,Label,_,(RootLabel:Label2),_)),
    %    format('found potential secondary label=~w nid=~w rootlabel=~w label2=~w\n',[Label,NId,RootLabel,Label2]),
    %    domain(edge(edge_kind[subst,lexical],RootLabel,_,TOP2,_),Edges),
    (tmp_edge(RootLabel,edge(edge_kind[subst,lexical],RootLabel,_,TOP2,_))
	     xor tmp_edge(_RootLabel,edge(secondary,_RootLabel,_,rename(RootLabel),_)),
     tmp_edge(_RootLabel,edge(edge_kind[subst,lexical],_RootLabel,_,TOP2,_))
    ),
    %    format('found secondary label=~w nid=~w rootlabel=~w label2=~w\n',[Label,NId,RootLabel,Label2]),
    true
.

:-light_tabular register_secondary_propagate/6.
:-mode(register_secondary_propagate/6,+(+,+,+,+,-,-)).

register_secondary_propagate(NId,Info,Labels,Dir,Id,M_Derivs) :-
%    fail,
    update_counter(secondary_propagate,Id),
    mutable(M_Derivs,[])
.

:-xcompiler
secondary_propagate_chain(SPId,Info,PrevSPId) :-
    record_without_doublon(secondary_propagate_bptr(SPId,Info,PrevSPId))
.				   

:-std_prolog process_deriv_check_constraint/5.

process_deriv_check_constraint(XCat,Tree,Span,Term,Edges) :-
	( recorded(deriv_constraint(XCat,Tree,Constraint)),
	  \+ deriv_check_constraint(Constraint,Span,Term,Edges) ->
%	      format('unsat constraint ~w on xcat=~w span=~w tree=~w term=~w edges=~w\n',[Constraint,XCat,Span,Term,Edges]),
	  fail
	;
	  true
	)
	.

:-rec_prolog deriv_check_constraint/4.

deriv_check_constraint((Cst1,Cst2),Span,Term,Edges) :-
	deriv_check_constraint(Cst1,Span,Term,Edges),
	deriv_check_constraint(Cst2,Span,Term,Edges)
	.

deriv_check_constraint((Cst1;Cst2),Span,Term,Edges) :-
	( deriv_check_constraint(Cst1,Span,Term,Edges)
	;
	  deriv_check_constraint(Cst2,Span,Term,Edges)
	)
	.

deriv_check_constraint((length < N),Span::[Left,Right|_],Term,Edges) :-
	Right - Left < N
	.

deriv_check_constraint((edges < N),Span::[Left,Right|_],Term,Edges) :-
	deep_length(Edges,Length),
	Length < N
	.

:-xcompiler
term2cat(T1,Cat) :-
	( T1 = T (_,_)
	xor T1 = T (_,_) * _ (_,_)
	xor T1 == [], T = '_'
	xor T1 = T
	),
	T =.. [F|_],
	functor2cat(F,Cat)
	.

:-light_tabular functor2cat/2.
:-mode(functor2cat/2,+(+,-)).

functor2cat(F,Cat) :-
	atom_module(F,A,B),
	( A = [] -> Cat = B ; Cat = A )
	.

:-light_tabular register_pseudo_node/2.
:-mode(register_pseudo_node/2,+(+,-)).

register_pseudo_node(Term::pseudo(Left,_,Cat),NId) :-
	update_counter(node,NId),
%%	format('register node ~w ~w\n',[NId,Add]),
	(Left = gen_pos(XLeft,_) xor Left = XLeft),
	Right = XLeft,
	%% ( Left == Right -> format('registering empty node id=~w left=~w term=~w\n',[NId,Left,Term]) ; true ),
	Lex = '',
	Token = '',
	( recorded( C::cluster{ id => CId,
				left => XLeft,
				right => Right,
				lex => Lex,
				token => Token
			      } )
	xor update_counter(cluster,CId),
%	  format('Register empty Cluster ~w Term=~w\n',[C,Term]),
	  record(C)
	),
	record( pseudonode2object(NId,Term,CId) ),
	record( node2xcat(NId,Cat) ),
	true
	.

:-light_tabular register_node/3.
:-mode(register_node/3,+(+,+,-)).

register_node(Add,Term,NId) :-
	update_counter(node,NId),
%%	format('register node ~w ~w\n',[NId,Add]),
	( Term = T (Left,_) , Type = subst
	xor Term = T (Left,_) * _ (_,_), Type = adj
	xor Term = verbose!anchor(Token,Left,Right,_,Top,(_LexAddr : [Lemma,TokenId,LemmaId]),_), Type=anchor
	xor Term = verbose!coanchor(Token,Left,Right,Top,[Lemma,TokenId,LemmaId]), Type = lexical
	xor Term = verbose!lexical([_Token],Left,Right,_,[Lemma,TokenId,LemmaId]), Type=lexical,
	  (_Token = (Token : _) xor _Token = Token )
	xor Term = verbose!epsilon(Token,Left,Right,[Lemma,TokenId,LemmaId]), Type=lexical
	),
	Right ?= Left,
	%% ( Left == Right -> format('registering empty node id=~w left=~w term=~w\n',[NId,Left,Term]) ; true ),
	TokenId ?= '',
	Token ?= '',
%	format('register node ~w ~w tokenid=~w\n',[NId,Add,TokenId]),
	tokenid2token(TokenId,XToken),
%	format('register node ~w ~w tokenid=~w xtoken=~w\n',[NId,Add,TokenId,XToken]),
	( Left = gen_pos(XL1,XL2),
	  Right = gen_pos(XR1,XR2) ->
	  ( XL1 == XR1, Type == lexical -> 
	    XLeft = XL2,
	    XRight = XR2
	  ;
	    XLeft = XL1,
	    XRight = XR1
	  )
	;
	  Left = XLeft,
	  Right = XRight
	),
	( recorded( C::cluster{ id => CId,
				left => XLeft,
				right => XRight,
				lex => TokenId,
				token => XToken
			      } )
	xor update_counter(cluster,CId),
%	  format('Register Cluster ~w Term=~w\n',[C,Term]),
	  record(C)
	),
	record( node2object(NId,Add,CId) ),
	(var(Top) xor
%%	    format('register node2top ~w => ~w\n',[NId,Top]),
	    record(node2top(NId,Top))),
	(var(_LexAddr) xor record(node2lexaddr(NId,_LexAddr))),
	true
	.

:-light_tabular tokenid2token/2.
:-mode(tokenid2token/2,+(+,-)).

tokenid2token(TokenId,Token) :-
	( TokenId = '' -> Token = ''
	; TokenId = [_|_] ->
	  tokenid2token_aux(TokenId,_Token),
	  name_builder('~L',[['~w',' '],_Token],_Token2),
	  '$interface'( 'clean_apostrophe'(_Token2:string),[return(Token:string)])
	;
	  tokenid2normalized_token(TokenId,Token)
	)
	.

:-xcompiler
tokenid2normalized_token(TokenId,Token) :-
	'T'(TokenId,_Token),
	'$interface'(lowercase(_Token:string),[return(Token:string)])
	.

:-std_prolog tokenid2token_aux/2.

tokenid2token_aux(LTokenId,LTokens) :-
	( LTokenId = [TokenId|LTokenId2] ->
	  LTokens = [Token|LTokens2],
	  tokenid2normalized_token(TokenId,Token),
	  tokenid2token_aux(LTokenId2,LTokens2)
	;
	  LTokens = []
	)
	.

:-light_tabular register_single_node/4.
:-mode(register_single_node/4,+(+,+,-,-)).

register_single_node(Add,Term,NId,OId) :-
	register_node(Add,Term,NId),
%	format('register single node add=~w nid=~w term=~w\n',[Add,OId,Term]),
	register_op(Add,OId,_),
	record( terminal_op(NId,OId) ),
	record_without_doublon(op2node(OId,NId))
	.

:-light_tabular register_op/3.
:-mode(register_op/3,+(+,-,-)).

register_op(Add,OId,MDerivs) :-
	update_counter(op,OId),
%%	format('register op ~w ~w\n',[OId,Add]),
	record( op2object(OId,Add) ),
	mutable(MDerivs,[]),
%	format('registered op ~w\n',[OId]),
	true
	.

:-light_tabular register_ht/3.
:-mode(register_ht/3,+(+,-,-)).

register_ht(Add,HId,MDerivs) :-
	update_counter(ht,HId),
	record( ht2object(HId,Add) ),
	mutable(MDerivs,[]),
%	format('registered ht ~w\n',[HId]),
	true
	.

:-light_tabular register_edge/6.
:-mode(register_edge/6,+(+,+,+,+,-,-)).

register_edge(SId,TId,Type,Label,EId,MDerivs) :-
	update_counter(edge,EId),
	mutable(MDerivs,[]),
%%	format('registered edge ~w sid=~w tid=~w type=~w label=~w\n',[EId,SId,TId,Type,Label]),
	true
	.

:-light_tabular register_secondary_edge/6.
:-mode(register_secondary_edge/6,+(+,+,+,+,-,-)).

register_secondary_edge(EId,SId2,Label,Dir,EId2,MDerivs) :-
	update_counter(edge,EId2),
	mutable(MDerivs,[]),
%	format('registered secondary edge ~w sid2=~w label=~w => ~w\n',[EId,SId2,Label,EId2]),
	true
	.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Derivs

:-extensional deriv/2.

:-xcompiler
register_deriv(DId,NId) :-
	(   deriv(DId,_)
	xor
            '$interface'('Deriv_New'(NId:term),[return(Deriv:ptr)]),
%	    format('register deriv ~w nid=~w\n',[DId,NId]),
	    record(deriv(DId,Deriv)),
	    true
	).

:-xcompiler
zderiv(DId,NId,OId,HTId) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Info'(Deriv:ptr,NId:term,OId:term,HTId:term),[]),
%	format('deriv info ~w => nid=~w oid=~w span=~w htid=~w\n',[DId,NId,OId,Span,HTId]),
	true
	.

:-xcompiler
deriv2node(DId,NId) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Get_Node'(Deriv:ptr,NId:term),[]).

:-xcompiler
deriv2htid(DId,HTId) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Get_Hypertag'(Deriv:ptr,HTId:term),[]).


:-xcompiler
deriv2ht(DId,HT) :-
	deriv2htid(DId,HTId),
	HT::hypertag{ id => HTId }
	.

:-xcompiler
deriv2edge(DId,Info) :-
    deriv(DId,Deriv),
    '$interface'('Deriv_Edge'(Deriv:ptr,Info:term),[choice_size(1)]),
    true
	.

:-xcompiler
deriv(DId,EId,Span,SOP,TOP) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Edge'(Deriv:ptr,info(EId,SOP,TOP,_,Span):term),[choice_size(1)])
	.

:-xcompiler
deriv_set_op(DId,OId,Span) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Set_Op'(Deriv:ptr,OId:term,NId:term),[]),
	record_without_doublon( node2op(NId,OId) ),
	record(K::node_op2deriv(NId,OId,DId)),
	true
	.


:-xcompiler
node_op2deriv_alt(NId,OId,DId) :-
    recorded(node2op(NId,OId,NodeOp)),
    '$interface'('NodeOp_Deriv'(NodeOp:ptr,DId:term),[choice_size(1)]),
    true
	.

:-xcompiler
deriv_set_ht(DId,HTId) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Set_Hypertag'(Deriv:ptr,HTId:term),[return(none)])
	.

:-xcompiler
deriv_add_edge(DId,GId) :-
	deriv(DId,Deriv),
	'$interface'('Deriv_Add_Edge'(Deriv:ptr,GId:term),[return(none)]),
%	format('added edge did=~w info=~w\n',[DId,GId]),
	true
	.

:-xcompiler
deriv2best_parse(DId,DStruct::dstruct{ constraint => _Cst }) :-
	zderiv(DId,NId,OId,_),
	'$answers'( best_parse(NId,OId,_,_Cst,DStruct) )
	.

:-xcompiler
alive_deriv(Derivs,Deriv) :-
	domain(Deriv,Derivs),
	recorded( keep_deriv(Deriv) ),
	true
	.

:-xcompiler
alive_ht(Derivs,HTId) :-
	alive_deriv(Derivs,DId),
	deriv2htid(DId,HTId).

:-light_tabular node2live_deriv/2.
:-mode(node2live_deriv/2,+(+,-)).

node2live_deriv(NId,DId) :-
	N::node{ id => NId, deriv => Derivs },
	alive_deriv(Derivs,DId)
	.

:-light_tabular node2live_ht/2.
:-mode(node2live_ht,+(+,-)).

node2live_ht(NId,HTId) :-
	node2live_deriv(NId,DId),
	deriv2htid(DId,HTId)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Tools

:-light_tabular lex_tokenize/2.
:-mode(lex_tokenize/2,+(+,-)).

lex_tokenize(TokenId,LLex) :-
	( TokenId = '' -> LLex = ['']
	; TokenId = [_|_] ->
	  lex_tokenize_aux(TokenId,LLex)
	; 'T'(TokenId,Lex),
	  LLex = [Lex]
	).

:-std_prolog lex_tokenize_aux/2.

lex_tokenize_aux(LTokenId,LLex) :-
	( LTokenId = [TokenId|LTokenId2] ->
	  'T'(TokenId,Lex),
	  LLex = [Lex|LLex2],
	  lex_tokenize_aux(LTokenId2,LLex2)
	;
	  LLex = []
	).

:-light_tabular tree_tokenize/2.
:-mode(tree_tokenize/2,+(+,-)).

tree_tokenize(XTree,Tree) :-
    rx!tokenize(XTree,' ',Tree),
    record_without_doublon(tree2xtree(Tree,XTree))
.

format_hook(0'E,Stream,[edge{ id => Id, source => N1, target => N2, label => L}|R],R) :- %'0
        format(Stream,'~w:~E-~w->~E',[Id,N1,L,N2])
        .

format_hook(0'E,Stream,[node{ id => Id,cat => Cat,cluster => C, lemma => L, form=> F }|R],R) :- %'0
	( L == '' ->
	  format(Stream,'~w/~w/~E',[Id,Cat,C])
	;
	  format(Stream,'~w/~w:~w__~w/~E',[Id,F,L,Cat,C])
	)
        .

format_hook(0'E,Stream,[cluster{ id => Id, lex => Lex, token => Token}|R],R) :- %'0
	(Token == Lex ->
	 format(Stream,'~w{~w}',[Id,Lex])
	;
	 format(Stream,'~w{~w:~w}',[Id,Lex,Token])
	)
	.

format_hook(0'e,Stream,[Id|R],R) :- %'0
	E::edge{ id => Id },
	format(Stream,'~E',[E])
	.

format_hook(0'L,Stream,[[Format,Sep],AA|R],R) :- %'0
        mutable(M,0,true),
        every((   domain(A,AA),
                  mutable_inc(M,V),
                  (   V == 0 xor write(Stream,Sep) ),
                  format(Stream,Format,[A]) ))
        .

format_hook(0'U,Stream,[[Format,Sep],AA|R],R) :- %'0
        mutable(M,0,true),
        every((   domain(A:B,AA),
                  mutable_inc(M,V),
                  (   V == 0 xor write(Stream,Sep) ),
                  format(Stream,Format,[A,B]) ))
        .


