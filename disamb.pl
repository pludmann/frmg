/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2015, 2016, 2018 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  disamb.pl -- handling disambiguation and conversions
 *
 * ----------------------------------------------------------------
 * Description
 * A head file to call the post parsing actions
 * - processing options
 * - converting the shared derivation forest into a dependency forest (extract.pl)
 * - disambiguating the dependency forest (best.pl)
 * - possibly transforming the resulting dependency tree (transform.pl)
 * - converting and emitting for a given schema and format (depxml.pl, passage.pl, conll.pl, depconll.pl)
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.
:-require 'format.pl'.
:-require 'extract.pl'.
:-require 'best.pl'.
:-require 'transform.pl'.
:-require 'depxml.pl'.
:-require 'passage.pl'.
:-require 'conll.pl'.
:-require 'depconll.pl'.
:-require 'udep.pl'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Parse command line options (related to post-parsing phases)
%% these options should come after -disamb

:-std_prolog parse_options/1.

parse_options(Options) :-
        ( Options == [] -> true
	;
	  ( Options=['-e',Id|Rest] -> record( sentence(Id) )
	  ; Options = ['-verbose'|Rest] -> record_without_doublon( opt(verbose) )
	  ; Options = ['-verbose_cost'|Rest] -> record_without_doublon( opt(verbose_cost) )
	  ; Options=  ['-restrictions',File|Rest] ->
	    record_without_doublon( opt(restrictions(File)) )
	  ; Options=  ['-restrictions2',File|Rest] ->
	    record_without_doublon( opt(restrictions2(File)) )
	  ; Options=  ['-oracle',File|Rest] ->
	    record_without_doublon( opt(oracle(File)) )
	  ; Options = ['-strong_oracle'|Rest] ->
	    record_without_doublon( opt(strong_oracle) )
	  ; Options = ['-nodis'|Rest] ->
%%	    record_without_doublon(opt(depxml)),
	    record_without_doublon(opt(nodis))
	  ;   Options = ['-depxml'|Rest ] ->
	    record_without_doublon(opt(depxml)) 
	  ;   Options = ['-xmldep'|Rest] ->
	    record_without_doublon(opt(depxml))
	  ;   Options = ['-depconll'|Rest] ->
	    record_without_doublon(opt(depconll))
	  ;   Options = ['-nodis'|Rest] ->
	    record_without_doublon(opt(nodis))
	  ;   Options = ['-passage'|Rest] ->
	    record_without_doublon(opt(passage))
	  ;   Options = ['-passage_strict'|Rest] ->
	    record_without_doublon(opt(passage_strict))
	  ;   Options = ['-conll'|Rest] ->
	    record_without_doublon(opt(conll))
	  ;   Options = ['-sequoia'|Rest] -> % new version of the annotation scheme for SEQUOIA
	    record_without_doublon(opt(conll)),
	    record_without_doublon(opt(sequoia))
	  ;   Options = ['-fqb'|Rest] -> % new version of the annotation scheme for SEQUOIA
	    record_without_doublon(opt(conll)),
	    record_without_doublon(opt(sequoia)),
	    record_without_doublon(opt(fqb))
	  ;   Options = ['-spmrl'|Rest] -> % new version of the annotation scheme for SPMRL
		  record_without_doublon(opt(conll)),
		  record_without_doublon(opt(sequoia)),
		  record_without_doublon(opt(spmrl))
	  ;   Options = ['-easy'|Rest] ->
	    record_without_doublon(opt(easy))
	  ;   Options = ['-udep'|Rest] ->
	    record_without_doublon(opt(udep))
	  ;   Options = ['-dstats'|Rest] ->
	    record_without_doublon(opt(dstats))
	  ;   Options = ['-conll_verbose'|Rest] ->
	    record_without_doublon(opt(conll_verbose))
	  ;   Options = ['-weights'|Rest] ->
	    record_without_doublon(opt(weights))
	  ;   Options = ['-stop',Stop|Rest] ->
	    record_without_doublon(opt(stop(Stop)))
	  ;   Options = ['-cost'|Rest] ->
	    record_without_doublon(opt(cost))
	  ;   Options = ['-bcost'|Rest] ->
	    record_without_doublon(opt(bcost))
%	  ;   Options = ['-lpreader'|Rest] ->
%	    record_without_doublon(opt(lpreader))
	  ; Options = ['-multi'|Rest] ->
	    record_without_doublon(opt(multi))
	  ;   Options = ['-nocompound'|Rest] ->
	    record_without_doublon(opt(nocompound))
	  ; Options = ['-only_dummy_rule'|Rest] ->
	    record_without_doublon(opt(only_dummy_rule))
	  ; Options = ['-zero_cost'|Rest] ->
	    record_without_doublon(opt(zero_cost))
	  ;   Options = ['-eid',XEId,XW|Rest] ->
	    atom_number(XW,W),
	    atom_number(XEId,EId),
	    record_without_doublon(extra_elem_cost(edge{ id => EId},W))
	  ;   Options = ['-edge',XSLemma,XSCat,XLabel,XTLemma,XTCat,XW|Rest] ->
	    atom_number(XW,W),
	    ( XSLemma == '_' xor SLemma = XSLemma ),
	    ( XSCat == '_' xor SCat = XSCat ),
	    ( XTLemma == '_' xor TLemma = XTLemma ),
	    ( XTCat == '_' xor TCat = XTCat ),
	    ( XLabel == '_' xor Label = XLabel),
	    record( extra_elem_cost( edge{ source => node{ lemma => SLemma, cat => SCat },
					   target => node{ lemma => TLemma, cat => TCat },
					   label => Label
					 },
				     W
				   ))
	  ;   Options = ['-xedge',XSource,XTarget,XW|Rest] ->
	    atom_number(XW,W),
	    atom_number(XSource,Source),
	    atom_number(XTarget,Target),
	    record( extra_elem_cost( edge{ source => node{ cluster => cluster{ left => Source }},
					   target => node{ cluster => cluster{ left => Target }}
					 },
				     W
				   ))
	  ;   Options = ['-node',XLemma,XCat,XLeft,XRight,XW|Rest] ->
	    atom_number(XW,W),
	    (	  XLemma == '_' xor Lemma = XLemma ),
	    (	  XCat == '_' xor Cat = XCat ),
	    (	  XLeft == '_' xor atom_number(XLeft,Left) ),
	    (	  XRight == '_' xor atom_number(XRight,Right) ),
	    record( extra_node_cost( node{ lemma => Lemma, cat => Cat, cluster => cluster{ left => Left, right => Right} },
				     W
				   ))
	  ; Options = ['-nid',XNId,XW|Rest] ->
	    atom_number(XW,W),
	    atom_number(XNId,NId),
	    record( extra_node_cost( node{ id => NId }, W))
	  ;   Options = ['-chunk',XChunkType,XLeft,XRight,XW|Rest] ->
	    atom_number(XW,W),
	    (	  XChunkType == '_' xor ChunkType = XChunkType ),
	    (	  XLeft == '_' xor atom_number(XLeft,Left) ),
	    (	  XRight == '_' xor atom_number(XRight,Right) ),
	    record( extra_chunk_cost( Left,
				      Right,
				      ChunkType,
				      W
				    ))
	  ; Options = ['-transform'|Rest] -> record_without_doublon(opt(transform)) % edge rewriting
	  ; Options = ['-rename'|Rest] -> record_without_doublon(opt(rename)) % edge renaming
	  ; Options = ['-res',File|Rest] -> read_dbfile(File)
	  ; Options = ['-nbest',_NBest|Rest] ->
	    erase(nbest(_)),
	    atom_number(_NBest,NBest),
	    record(nbest(NBest))
	  ; Options = ['-no_ssfeatures'|Rest] ->
	    %% block the use of ssfeatures
	    record_without_doublon(opt(no_ssfeatures))
	  ; Options = ['-load_model',Model|Rest] ->
	    record_without_doublon(opt(load_model(Model))),
	    model!load(Model),
	    record_without_doublon(use_model),
	    show_time(model_load),
	    true
	  ; Options = ['-load_binary_model',Model|Rest] ->
	    record_without_doublon(opt(load_model(Model))),
	    model!load_binary(Model),
	    record_without_doublon(use_model),
	    show_time(model_load),
	    true
	  ; Options = ['-save_model',Model|Rest] ->
	    %% not yet activated
	    record_without_doublon(opt(save_model(Model))),
	    true
	  ; Options = ['-no_hand_cost'|Rest] ->
		record_without_doublon(opt(no_hand_cost)),
		true
	  ; Options = ['-altstats'|Rest] ->
		record_without_doublon(opt(altstats)),
		true
	  ; Options = ['-v',Level|Rest] ->
	    '$interface'('DyALog_Set_Verbose_Level'(Level:string),[])
	  ; Options = ['-parse_verbose'|Rest] ->
	    record_without_doublon(opt(parse_verbose))
	  ; Options = [ParserOption|Rest] ->
	    true
	  ;  fail
	  ),
	  parse_options(Rest)
        )
        .


:-std_prolog utime/1.

utime(T) :- '$interface'('DyALog_Utime'(),[return(T:int)]).


:-std_prolog show_time/1.

show_time(Step) :-
	( (Step==latency xor opt(multi)) ->
	  utime(Time),
	  ( recorded(utime(M)) ->
	    mutable_read(M,OldTime),
	    Delta is Time - OldTime,
	    fast_mutable(M,Time)
	  ;
	    mutable(M,Time),
	    record(utime(M)),
	    Delta = Time
	  ),
	  disable_verbose((format('<~w_time> ~wms\n',[Step,Delta])))
	;
	  true
	)
	.

process_options :-	
	op(700,fx,[template,stemplate,btemplate,rtemplate]),
	argv(Options),
	%%	parse_options(Options),
	( recorded(parsed_options)
	xor
	parse_options(Options),
	  record( parsed_options),
	  every(( generate_templates ))
	),
	persistent!add(parsed_options),
	%% the following avoid reparsing options at each loop iteration
	%% using persistent facts
	persistent!add_fact(opt(_)),
	( recorded(xoptions(XOptions)) ->
	      %% one can use extra options for a sentence. They are not kept for the following sentences
	      parse_options(XOptions)
	 ;
	 true
	),
	%	persistent!add_fact(use_cluster_feature),
	persistent!add_fact(cluster_feature_table(_)),
	persistent!add_fact(cluster_feature_prepare(_)),
	persistent!add_fact(deriv_constraint(_,_,_)),
	persistent!add_fact(tree_block(_)),
	persistent!add_fact(edge_reroot_constraint(_,_,_,_,_)),
	persistent!add_fact(templates(_,_,_,_)),
	persistent!add_fact(rtemplates(_,_,_)),
	persistent!add_fact(use_model),
	persistent!add_fact(xvector_rule_name(_,_,_)),
	persistent!add_fact(rule_multiply(_)),
	persistent!add_fact(tree_constraint(_,_,_)),
	(recorded(features_persistent_table(_)) ->
	     true
	 ;
	 local_table!new(Feature_Table),
	 record(features_persistent_table(Feature_Table))
	),
	persistent!add_fact(features_persistent_table(_)),
	(recorded(lctag_table(LCTAG_Table)) ->
	     true
	 ;
	 local_table!new(LCTAG_Table),
	 record(lctag_table(LCTAG_Table))
	),
	persistent!add_fact(lctag_table(_)),
	every(( recorded(opt(restrictions2(File))),
		verbose('open restriction2 database ~w\n',[File]),
		%% all dependencies
		sqlite!open_readonly(File,DB2),
		record_without_doublon( opt(restrictions2,DB2) )
	      )),
	every(( recorded(opt(restrictions(File))),
		verbose('open restriction database ~w\n',[File]),
		%% dependency info filtered by clustering
		sqlite!open_readonly(File,DB),
		record_without_doublon( opt(restrictions,DB) ),
		prepare_restriction(_)
	      )),
	every(( recorded(opt(oracle(File))),
		verbose('open oracle database ~w\n',[File]),
		sqlite!open_readonly(File,DB),
		record_without_doublon( opt(oracle,DB) ),
		prepare_oracle(_)
	      )),

	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Running the post-parsing actions

%% The predicate to be callled after parsing for disambiguation, conversion, and emitting a parse
parsing_post_process :-
	wait,
	%% deal with post-parsing specific options
	%%
	(\+ opt(parse_verbose) xor set_verbose_level(0)),
	show_time(parsing),
%	process_options,
	(   sentence(_) xor (recorded('S'(SId)) xor SId=1), record( sentence(SId) ) ),
	%%  convert the derivation forest into a dependency forest
	dependency_forest_reader,
	abolish(answer_extract_deriv/5),
	abolish(info_derivs/1),
	abolish(op2node/2),
	show_time(extract),
	\+ opt(stop(reader)),
	(\+ opt(verbose) xor
	    every(( C::cluster{}, format('Cluster ~E\n',[C]) )),
	    every(( N::node{}, format('Node ~E\n',[N]) )),
	    every(( E::edge{}, format('Edge ~E\n',[E]) ))
	),
	( opt(dstats) -> dstats_emit, show_time(dstats) ; true ),
	( opt(nodis) -> depxml_emit(nodis), show_time(nodis) ; true ),
	load_easy_disamb_hints,
	( opt(nodis) xor compute_all_edge_costs ),
	( opt(stop(cost)) -> show_time(disamb), fail ; true ),
	wait((best_dependency_tree)),	% get best dependency tree	
	dependency_convert,		% convert
	show_time(disamb),
	\+ opt(stop(dis)),
	record(disambiguated),
	(
	    (  opt(depxml), depxml_emit(disamb), show_time(depxml)
	     ; opt(depconll), depconll_emit, show_time(depconll)
	     ; opt(conll), conll_emit, show_time(conll)
	     ; opt(udep), udep_emit, show_time(udep)
	     ; opt(passage), passage_emit, show_time(passage), abolish(emitted/1)
	     ; opt(easy), easy_emit, show_time(easy), abolish(emitted/1)
	    ),
	    fail
	 ;
	 %% some cleaning
	 parsing_cleaning,
	 fail
	)
	.

parsing_cleaning :-
    every((recorded(xoptions(XArgs)),
	   domain('-v',XArgs),
	   '$interface'('DyALog_Set_Verbose_Level'(none:string),[])))
	.
    

:-xcompiler
dependency_convert :-
    every(( (opt(depxml) xor opt(nodis) xor opt(depconll)),
	    every(( opt(transform),
		    repeat((
				  every((edge_transform)),
				  ( recorded(reroot_loop) ->
					erase(reroot_loop), % if some reroot was done, we loop again
					fail
				   ;
				   true
				  )
			      ))
		  )),
	    true
	   ; (opt(passage) xor opt(easy)),
	     dependency_convert_to_passage,
	     true
	  )),
    true
	.

